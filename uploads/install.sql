-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 26, 2019 at 07:26 AM
-- Server version: 5.6.35
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cw-install`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent` int(11) DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL,
  `font_awesome_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `code`, `name`, `parent`, `slug`, `date_added`, `last_modified`, `font_awesome_class`, `thumbnail`) VALUES
(1, 'b863c40b88', 'Marketing', 0, 'marketing', 1560121200, NULL, 'fab fa-black-tie', 'category-thumbnail.png'),
(2, '8259184b26', 'Finance', 0, 'finance', 1560121200, NULL, 'fas fa-money-bill-alt', 'category-thumbnail.png'),
(3, 'f525d474f0', 'Designing', 0, 'designing', 1560121200, NULL, 'fas fa-paint-brush', 'category-thumbnail.png'),
(4, '99f042581e', 'Digital &amp; Social Media', 0, 'digital-amp-social-media', 1560121200, NULL, 'fas fa-globe', 'category-thumbnail.png'),
(5, 'aade8e9920', 'Human Resourses', 0, 'human-resourses', 1560121200, NULL, 'fas fa-users', 'category-thumbnail.png'),
(6, 'd3dc655ca3', 'Academic Learning', 0, 'academic-learning', 1560121200, NULL, 'fas fa-book', 'category-thumbnail.png'),
(7, '3b219f3b7e', 'Office Tools', 0, 'office-tools', 1560121200, NULL, 'fas fa-laptop', 'category-thumbnail.png'),
(8, '70f0d2eec2', 'Health &amp; Fitness', 0, 'health-amp-fitness', 1560121200, NULL, 'fas fa-universal-access', 'category-thumbnail.png'),
(9, '146c42717e', 'Management', 0, 'management', 1560121200, NULL, 'far fa-handshake', 'category-thumbnail.png'),
(10, '6d8e561eab', 'Personal Development', 0, 'personal-development', 1560121200, NULL, 'far fa-star', 'category-thumbnail.png'),
(11, '31e0c59f84', 'Fundamentals of Marketing', 1, 'fundamentals-of-marketing', 1560121200, NULL, NULL, NULL),
(12, 'f8385ee05b', 'Branding', 1, 'branding', 1560121200, NULL, NULL, NULL),
(13, '1d62baef3f', 'Sales Techniques', 1, 'sales-techniques', 1560121200, NULL, NULL, NULL),
(14, '7d17c4330c', 'Corporate Communication', 1, 'corporate-communication', 1560121200, NULL, NULL, NULL),
(15, '559089c93e', 'Advertising', 1, 'advertising', 1560121200, NULL, NULL, NULL),
(16, 'dbec5e8c5b', 'Media', 1, 'media', 1560121200, NULL, NULL, NULL),
(17, 'd2b2abc231', 'Marketing Analysis', 1, 'marketing-analysis', 1560121200, NULL, NULL, NULL),
(18, '32853c721e', 'Corporate Finance', 2, 'corporate-finance', 1560121200, NULL, NULL, NULL),
(19, 'ce4167fb96', 'Financial Analysis', 2, 'financial-analysis', 1560121200, 1560121200, NULL, NULL),
(20, 'c7c7457104', 'Financial Planning', 2, 'financial-planning', 1560121200, NULL, NULL, NULL),
(21, '232ce260d4', 'Financial Trading', 2, 'financial-trading', 1560121200, NULL, NULL, NULL),
(22, '0f3d4b0f52', 'Accounting', 2, 'accounting', 1560121200, NULL, NULL, NULL),
(23, 'f1da87e35b', 'Financial Tools', 2, 'financial-tools', 1560121200, NULL, NULL, NULL),
(24, 'f79921bbae', 'Graphic Designing', 3, 'graphic-designing', 1560121200, 1560121200, NULL, NULL),
(25, '016c3979cd', 'Animation', 3, 'animation', 1560121200, NULL, NULL, NULL),
(26, '043997a2c0', 'Illustration', 3, 'illustration', 1560121200, NULL, NULL, NULL),
(27, '5b748c7b15', 'Product Designing', 3, 'product-designing', 1560121200, NULL, NULL, NULL),
(28, 'e16e96000c', 'Web Designing', 3, 'web-designing', 1560121200, NULL, NULL, NULL),
(29, 'eaa878d6d8', 'Search Engine Optimization (SEO)', 4, 'search-engine-optimization-seo', 1560121200, NULL, NULL, NULL),
(30, '3a23410699', 'Search Engine Marketing (SEM)', 4, 'search-engine-marketing-sem', 1560121200, NULL, NULL, NULL),
(31, 'd89f4dfd08', 'Google Analytics', 4, 'google-analytics', 1560121200, NULL, NULL, NULL),
(32, 'aa8511698e', 'Social Media Marketing', 4, 'social-media-marketing', 1560121200, NULL, NULL, NULL),
(33, 'f644388a7e', 'HR Policies', 5, 'hr-policies', 1560121200, NULL, NULL, NULL),
(34, '6327e60127', 'HR Management', 5, 'hr-management', 1560121200, NULL, NULL, NULL),
(35, '55f3d6eef4', 'Training Tools', 5, 'training-tools', 1560121200, NULL, NULL, NULL),
(36, '28e6fb2656', 'Interview Techniques', 5, 'interview-techniques', 1560121200, NULL, NULL, NULL),
(37, '0ad17513c2', 'Sourcing &amp; Recruiting', 5, 'sourcing-amp-recruiting', 1560121200, NULL, NULL, NULL),
(38, '2d53cf28f5', 'Employee Management', 5, 'employee-management', 1560121200, NULL, NULL, NULL),
(39, 'f58e8784d1', 'Performance Appraisal', 5, 'performance-appraisal', 1560121200, NULL, NULL, NULL),
(40, '20bab539be', 'Languages', 6, 'languages', 1560121200, NULL, NULL, NULL),
(41, '9136716888', 'Science', 6, 'science', 1560121200, NULL, NULL, NULL),
(42, 'f10efcba1b', 'Mathematics', 6, 'mathematics', 1560121200, NULL, NULL, NULL),
(43, 'af8e2f27b3', 'Medicine', 6, 'medicine', 1560121200, NULL, NULL, NULL),
(44, 'ffec991bf0', 'Engineering', 6, 'engineering', 1560121200, NULL, NULL, NULL),
(45, 'e769149afd', 'ILS English Test', 6, 'ils-english-test', 1560121200, NULL, NULL, NULL),
(46, '1f8ded5a84', 'Exam Training', 6, 'exam-training', 1560121200, NULL, NULL, NULL),
(47, '22507a4bbe', 'Other Subjects', 6, 'other-subjects', 1560121200, NULL, NULL, NULL),
(48, '94aada62f9', 'Microsoft Excel', 7, 'microsoft-excel', 1560121200, NULL, NULL, NULL),
(49, 'd722d88f3f', 'Advanced Excel', 7, 'advanced-excel', 1560121200, NULL, NULL, NULL),
(50, 'daa860578c', 'Microsoft Power Point', 7, 'microsoft-power-point', 1560121200, NULL, NULL, NULL),
(51, 'da8c694cf6', 'Apple Applications', 7, 'apple-applications', 1560121200, NULL, NULL, NULL),
(52, '2b0c83e910', 'Google Products', 7, 'google-products', 1560121200, NULL, NULL, NULL),
(53, 'e593a2c30a', 'Modern Yoga', 8, 'modern-yoga', 1560121200, NULL, NULL, NULL),
(54, '4981b184d1', 'Sports', 8, 'sports', 1560121200, NULL, NULL, NULL),
(55, '9e52f5849f', 'Health &amp; Nutrition', 8, 'health-amp-nutrition', 1560121200, NULL, NULL, NULL),
(56, '7734c8370c', 'Dancing Classes', 8, 'dancing-classes', 1560121200, NULL, NULL, NULL),
(57, '9a3c056234', 'Cooking Classes', 8, 'cooking-classes', 1560121200, NULL, NULL, NULL),
(58, 'b78d58b925', 'Dietician', 8, 'dietician', 1560121200, NULL, NULL, NULL),
(59, '923d06dd8a', 'Sales Strategies', 9, 'sales-strategies', 1560121200, NULL, NULL, NULL),
(60, '840adcd2bf', 'Operations Management', 9, 'operations-management', 1560121200, NULL, NULL, NULL),
(61, '20034f0db6', 'Office Set-up', 9, 'office-set-up', 1560121200, NULL, NULL, NULL),
(62, 'a95bda9bbe', 'Budget Planning', 9, 'budget-planning', 1560121200, NULL, NULL, NULL),
(63, '68f250222c', 'Project Management', 9, 'project-management', 1560121200, NULL, NULL, NULL),
(64, 'efef1f0141', 'Corporate Presentations', 9, 'corporate-presentations', 1560121200, NULL, NULL, NULL),
(65, 'b77bfcc173', 'Personality Development', 10, 'personality-development', 1560121200, NULL, NULL, NULL),
(66, 'ea26d98950', 'Leadership Skills', 10, 'leadership-skills', 1560121200, NULL, NULL, NULL),
(67, 'fcb591a916', 'Public Speaking', 10, 'public-speaking', 1560121200, NULL, NULL, NULL),
(68, '146d3b4875', 'Networking Skills', 10, 'networking-skills', 1560121200, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ccavenue`
--

DROP TABLE IF EXISTS `ccavenue`;
CREATE TABLE `ccavenue` (
  `id` int(11) NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ccavenue`
--

INSERT INTO `ccavenue` (`id`, `key`, `value`) VALUES
(1, 'ccavenue', '[{\"active\":\"1\",\"currency\":\"USD\",\"merchant_id\":\"43560\",\"access_code\":\"AVNB03GH07BD82BNDB\",\"working_key\":\"65BF333B856A78FF1D8637194E969B74\"}]'),
(2, 'ccavenue_supported', 'AED,USD,EUR,GBP,SAR,BHD,OMR,KWR,QAR');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE `ci_sessions` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('23fc3fd7477bbc2a299a8bb474662e13f92e034f', '::1', 1569401612, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536393430313631323b636172745f6974656d737c613a303a7b7d757365725f69647c733a313a2231223b726f6c655f69647c733a313a2231223b726f6c657c733a353a2241646d696e223b6e616d657c733a373a2261646d696e2031223b61646d696e5f6c6f67696e7c733a313a2231223b),
('1588b10d70f3c79d222e12f48f1f1458de03381d', '::1', 1569401601, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536393430313630313b636172745f6974656d737c613a303a7b7d757365725f69647c733a313a2232223b726f6c655f69647c733a313a2232223b726f6c657c733a343a2255736572223b6e616d657c733a31323a2252616368656c204d616c6f6f223b757365725f6c6f67696e7c733a313a2231223b),
('f2ca14c4878efc0d374307be8deae122733fa644', '::1', 1569403591, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536393430333539303b636172745f6974656d737c613a303a7b7d757365725f69647c733a313a2232223b726f6c655f69647c733a313a2232223b726f6c657c733a343a2255736572223b6e616d657c733a31323a2252616368656c204d616c6f6f223b757365725f6c6f67696e7c733a313a2231223b),
('e4506814147d8254dc60d39ac46bf670a7fd9105', '::1', 1569402441, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536393430323434313b636172745f6974656d737c613a303a7b7d757365725f69647c733a313a2231223b726f6c655f69647c733a313a2231223b726f6c657c733a353a2241646d696e223b6e616d657c733a373a2261646d696e2031223b61646d696e5f6c6f67696e7c733a313a2231223b),
('70669426ab8c21401d34c8344b801a57324c62cd', '::1', 1569403182, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536393430333138323b636172745f6974656d737c613a303a7b7d757365725f69647c733a313a2231223b726f6c655f69647c733a313a2231223b726f6c657c733a353a2241646d696e223b6e616d657c733a373a2261646d696e2031223b61646d696e5f6c6f67696e7c733a313a2231223b),
('b2463bcb7da3fafd27f3a648164655248a87b6d4', '::1', 1569403783, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536393430333738333b636172745f6974656d737c613a303a7b7d757365725f69647c733a313a2231223b726f6c655f69647c733a313a2231223b726f6c657c733a353a2241646d696e223b6e616d657c733a373a2261646d696e2031223b61646d696e5f6c6f67696e7c733a313a2231223b),
('e88481097841a125b630cf7d1e9ad6275e72a1ff', '::1', 1569403631, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536393430333539303b636172745f6974656d737c613a303a7b7d757365725f69647c733a313a2232223b726f6c655f69647c733a313a2232223b726f6c657c733a343a2255736572223b6e616d657c733a31323a2252616368656c204d616c6f6f223b757365725f6c6f67696e7c733a313a2231223b),
('b2b50ab8e66e084c41f5cc6e51e1f853c5741599', '::1', 1569406152, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536393430363135323b636172745f6974656d737c613a303a7b7d757365725f69647c733a313a2231223b726f6c655f69647c733a313a2231223b726f6c657c733a353a2241646d696e223b6e616d657c733a373a2261646d696e2031223b61646d696e5f6c6f67696e7c733a313a2231223b),
('3d3f581e843fe61788dce473db1ea5694db56892', '::1', 1569406481, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536393430363438313b636172745f6974656d737c613a303a7b7d757365725f69647c733a313a2231223b726f6c655f69647c733a313a2231223b726f6c657c733a353a2241646d696e223b6e616d657c733a373a2261646d696e2031223b61646d696e5f6c6f67696e7c733a313a2231223b),
('2afb9295f45782a872bc73287a48a6d602c44a86', '::1', 1569406793, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536393430363739333b636172745f6974656d737c613a303a7b7d757365725f69647c733a313a2231223b726f6c655f69647c733a313a2231223b726f6c657c733a353a2241646d696e223b6e616d657c733a373a2261646d696e2031223b61646d696e5f6c6f67696e7c733a313a2231223b),
('a43d8f256adc52a79f96cd94b0f9bbea74f020d4', '::1', 1569407431, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536393430373433313b636172745f6974656d737c613a303a7b7d757365725f69647c733a313a2231223b726f6c655f69647c733a313a2231223b726f6c657c733a353a2241646d696e223b6e616d657c733a373a2261646d696e2031223b61646d696e5f6c6f67696e7c733a313a2231223b),
('165c0aac71ec34b4c8bb0bfa9cc302c495d089f5', '::1', 1569407432, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536393430373433313b636172745f6974656d737c613a303a7b7d757365725f69647c733a313a2231223b726f6c655f69647c733a313a2231223b726f6c657c733a353a2241646d696e223b6e616d657c733a373a2261646d696e2031223b61646d696e5f6c6f67696e7c733a313a2231223b),
('178a309559ee4ed5de7b36e94a374134eff2dcd4', '::1', 1569415361, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536393431353336303b636172745f6974656d737c613a303a7b7d),
('cb643b6b7d70c456284b86ba6d49717b098cdd68', '::1', 1569476928, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536393437363932383b757365725f69647c733a313a2231223b726f6c655f69647c733a313a2231223b726f6c657c733a353a2241646d696e223b6e616d657c733a373a2261646d696e2031223b61646d696e5f6c6f67696e7c733a313a2231223b636172745f6974656d737c613a303a7b7d),
('83bb71889861c1aff7b1c3cbc0492a14cb964f79', '::1', 1569476949, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536393437363932383b757365725f69647c733a313a2231223b726f6c655f69647c733a313a2231223b726f6c657c733a353a2241646d696e223b6e616d657c733a373a2261646d696e2031223b61646d696e5f6c6f67696e7c733a313a2231223b636172745f6974656d737c613a303a7b7d);

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` int(11) UNSIGNED NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `commentable_id` int(11) DEFAULT NULL,
  `commentable_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_description` longtext COLLATE utf8_unicode_ci,
  `description` longtext COLLATE utf8_unicode_ci,
  `outcomes` longtext COLLATE utf8_unicode_ci,
  `language` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `section` longtext COLLATE utf8_unicode_ci,
  `requirements` longtext COLLATE utf8_unicode_ci,
  `price` double DEFAULT NULL,
  `discount_flag` int(11) DEFAULT '0',
  `discounted_price` int(11) DEFAULT NULL,
  `level` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL,
  `visibility` int(11) DEFAULT NULL,
  `is_top_course` int(11) DEFAULT '0',
  `is_admin` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `course_overview_provider` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` longtext COLLATE utf8_unicode_ci,
  `meta_description` longtext COLLATE utf8_unicode_ci,
  `is_free_course` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `title`, `short_description`, `description`, `outcomes`, `language`, `category_id`, `sub_category_id`, `section`, `requirements`, `price`, `discount_flag`, `discounted_price`, `level`, `user_id`, `thumbnail`, `video_url`, `date_added`, `last_modified`, `visibility`, `is_top_course`, `is_admin`, `status`, `course_overview_provider`, `meta_keywords`, `meta_description`, `is_free_course`) VALUES
(1, 'Essential Cooking Skills', 'Cook like a pro, master the basic techniques used in the World\'s culinary industry! Key Techniques.', '<p>When one takes up cooking, the amount of information, recipes and \r\nsuggestions can be overwhelming. It\'s easy to get lost in a sea of \r\nrecipes, to spend time pondering WHAT to cook and miss one of the most \r\nimportant aspects when it comes to preparing a great meal - the HOW. </p><p>The\r\n real success of a dish comes from correct preparation techniques just \r\nas much as it comes from high quality ingredients, flavour combinations \r\nor creative plating. \r\nIn this course I gathered some of the most important 17 techniques that \r\nany cook, anywhere in the world, be they amateur or professional, \r\nabsolutely needs to use in his kitchen. They have been developed and \r\nperfected over generations by professional chefs and, for many years, \r\nhave been set as golden standards in international cuisine. \r\n</p><p>I have done my utmost to make this an effective and pleasant experience \r\nfor every student by combining clear practical demonstrations and useful\r\n information. \r\nThis is just the first of a series of courses to follow, which I will \r\ndevelop taking into account all the feedback from my students and the \r\ntopics they are interested in.                                </p>', '[\"Master basic cooking techniques which can then be used to create an endless variety of dishes.\",\"Understand the HOW of cooking, before thinking of the WHAT to cook.\"]', 'english', 8, 57, '[1,2,3,4,5,6]', '[\"Genuine passion for cooking\",\"Interest in building sound technical skills in the kitchen.\"]', 20, 1, 15, 'beginner', 3, NULL, 'https://www.youtube.com/watch?v=TdzXHenC91E', 1560121200, 1562799600, NULL, 1, 0, 'active', 'youtube', 'Cakes', 'Essential Cooking Skills - Cook like a pro', NULL),
(2, 'Learn Search Engine Optimization', 'Wondered what is SEO? website ranking is low? In this SEO training we\'ll rank higher, master SEO tools, keyword research ', '<ul><li><p>This course is built with a <b> beginners</b> point of view <b>as well as advanced</b> point of view.</p></li><li><p> It will help you in <b>growing your online business or website</b> in a systematic manner and helps you in<b> overall development of your skill. </b></p></li><li><p>After completion of this course <b>anyone can apply for the SEO job in any organisation</b> or anyone online can improve their own company.</p></li><li><p>After Completion of this course you are able to know that what are the <b>free tools as well as paid tools from the SEO perspective.</b></p></li></ul>\r\n<p><b>Who this course is for:</b></p>\r\n\r\n<ul><li>Anyone who have interest in SEO and want to add SEO as a skill to \r\ntheir career or anyone who wants to grow their business and wants to \r\nlearn every aspect of Search Engine Optimization can opt for this \r\ncourse.</li></ul>', '[\"Student will able to know all the basic to advance term related to SEO and can apply for the job of ON page or Off page SEO in any organisation or if they have their own business or website then they also grow it by learning this course.\"]', 'english', 4, 29, '[5,7,8,9]', '[\"A little knowledge of search engines like Google, Yahoo etc will work. All the major parts of this Search Engine and SEO would teach by me in this course . Some free tools which are also provided in course last lesson will work.\"]', 25, 1, 20, 'beginner', 5, NULL, 'https://www.youtube.com/watch?v=eXyexPny7fo', 1552953600, 1562886000, NULL, 1, 0, 'active', 'youtube', '', 'search engine optimization made easy', NULL),
(3, 'Microsoft Excel - Excel from Beginner to Advanced', 'Excel with this A-Z Microsoft Excel Course. Microsoft Excel 2010, Excel 2013, Excel 2016', '<p>Enroll now to go through a deep dive of the most popular spreadsheet \r\ntool on the market, Microsoft Excel. </p><p>As your instructor I will use my \r\n15+ years of Excel training to guide you step by step through the \r\nbeginner to advanced level and beyond.\r\n\r\n</p><p>As you participate in each of the 4 courses you will master Excel tools \r\nthat will clear away the pain of stumbling through your daily tasks. </p><p>You\r\n will start with the basics, building a solid foundation that will give \r\nyou further knowledge as you progress into intermediate and advanced \r\nlevel topics.\r\n\r\n</p><p>At completion of this course you will have mastered the most popular \r\nExcel tools and come out with confidence to complete any Excel tasks \r\nwith efficiency and grace. </p><p>Below are just a few of the topics that you \r\nwill master:\r\n\r\n</p><ul><li>Creating effective spreadsheets\r\n</li><li>Managing large sets of data\r\n</li><li>Mastering the use of some of Excel\'s most popular and highly sought \r\nafter functions (SUM, VLOOKUP, IF, AVERAGE, INDEX/MATCH and many \r\nmore...)\r\n</li><li>Create dynamic report with Excel PivotTables\r\n</li><li>Unlock the power and versatility of Microsoft Excel\'s AddIn, PowerPivot\r\n</li><li>Audit Excel Worksheet formulas to ensure clean formulas\r\n</li><li>Automate your day to day Excel tasks by mastering the power of Macros \r\nand VBA\r\n</li></ul><p>So, what are you waiting for, enroll now and take the next step in \r\nmastering Excel and go from Excel Newbie to Excel Guru!                   \r\n             </p>', '[\"Master Microsoft Excel from Beginner to Advanced\",\"Maintain large sets of Excel data in a list or table\",\"Harness the full power of Microsoft Excel by automating your day to day tasks through Macros and VBA\",\"Learn the most common Excel functions used in the Office\",\"Build a solid understanding on the Basics of Microsoft Excel\",\"Create dynamic reports by mastering one of the most popular tools, PivotTables\"]', 'english', 7, 48, '[13,14,15,16]', '[\"Access to a working copy of Microsoft Excel\"]', 25, NULL, 0, 'beginner', 6, NULL, 'https://www.youtube.com/watch?v=eXyexPny7fo', 1560121200, 1562799600, NULL, 1, 0, 'active', 'youtube', '', 'Learn Spreadsheets', NULL),
(4, 'Bake a Cake in 3 Steps', 'Like changing a flat tire and sewing on a button, knowing how to bake a cake from scratch is an essential life skill. (Dang, I keep meaning to learn how to change a tire.) It might not be as important as starting a fire without matches, but it’s not far behind. Everybody needs cake!', '<p>People always assume that because I bake, I’m a whiz at cooking too. <b>\r\nIt’s not true</b>. \r\n\r\nBaking and cooking are two entirely different things. I don’t throw \r\ningredients in a pan and create magic. <b>That’s cooking.</b> \r\n\r\n</p><p>But I do carefully measure ingredients according to a recipe and create \r\nchemical reactions that result in deliciousness. That’s baking. \r\n\r\nWhich brings me to rule number one in baking a cake: Follow the recipe \r\nclosely. Don’t add “a dash of this” and a “dash of that” unless you’re \r\ncomfortable experimenting and can accept the risk of a flat, too dense \r\nor undercooked cake. <span xss=removed><span xss=removed><b>Precision is important.</b></span></span> \r\n</p><p>\r\nSo let’s bake a simple, universally adored cake: plain yellow. And if \r\nyou aren’t ready for scratch yet, the rules below also apply to baking a\r\n cake from a mix.                                </p>', '[\"Learn the Perfect Cake Making Method\",\"Learn to Decorate the Cake\"]', 'english', 8, 57, '[17,18,19,20]', '[\"Basic Cooking skills\",\"Knowledge of Ingredients\",\"Knowledge of Baking Oven Usage\"]', 0, NULL, 0, 'beginner', 2, NULL, 'https://www.youtube.com/watch?v=xrjCJOke4Gc', 1560121200, 1566946800, NULL, 1, 0, 'active', 'youtube', 'Cake,Bake,cakes,decoration', 'Bake a Cake in 3 Steps', 1),
(5, 'Double your Gmail productivity in just over one hour', 'Learn over 20 different strategies and tools to cut your time spent on email in half and massively increase productivity', '<p>Do you ever feel like EMAIL CONTROLS YOUR LIFE?\r\n\r\n</p><p>You know...like there\'s those days where literally all you do is spend \r\ntime in your inbox reading and respond to emails. It\'s like you\'re a \r\nlittle email salmon swimming trying to fight the upstream inbox current.\r\n\r\nI\'ve got some amazing news for you.... IT DOESN\'T HAVE TO BE THIS WAY.\r\n\r\n</p><p>This Gmail course has taught people how to effectively manage their \r\nemail to increase productivity and GET HOURS OF YOUR LIFE BACK EACH \r\nWEEK.\r\n</p><p>\r\nBy the end of this course you\'ll learn the deepest, darkest secrets of \r\nGmail that only the greatest email ninjas know, like:\r\n\r\n</p><ul><li>Specific tactics to spend dramatically less time in your inbox (and why \r\nthat\'s actually more productive!)\r\n</li><li>The best way to empty that inbox to zero like Speedy Gonzalez fast - BTW\r\n its super important to do that\r\nStrategies to decrease the amount of mail you get. Yep, it\'s possible</li><li>\r\nTools to not only manage all your contacts and connections, but ones \r\nthat will also help you write emails that are a like...A ZILLION TIMES \r\nmore effective\r\n20 different strategies and tools to be more productive with email      \r\n                          </li></ul>', '[\"How to set up Gmail more maximum efficiency\",\"How to reduce the amount of junk email that clutters your inbox to almost zero\",\"The best practices to outsource your inbox to a virtual assistant\",\"The 10 step workflow that will double the speed at which you process email\",\"How to automate the most tedious and repetitive email tasks\",\"The exact methodology I\'ve used to hire the best possible virtual assistant\"]', 'english', 7, 52, '[21,22,23,24,25]', '[\"All you need is a Gmail account and the desire to be in control of your inbox\"]', 12, 1, 10, 'beginner', 5, NULL, 'https://www.youtube.com/watch?v=JrHb98LtEng', 1560121200, 1562799600, NULL, 1, 0, 'active', 'youtube', 'Gmai,Emails', 'Gpaim Productivity', NULL),
(6, 'Mega Digital Marketing Course A-Z: 12 Courses in 1', 'Digital Marketing Strategy, Social Media Marketing, WordPress, SEO, Digital Sale, Email, Instagram, Facebook, ads', '<p>Find your target audience, easily convince them to become your customer \r\nand buy your products. \r\n</p><p>Build a effective website for marketing and sale from scratch (no \r\ncoding!) </p><p>Increase your conversion rate by building advanced landing \r\npages, write powerful copies and sell more.\r\n</p><p>Earn extremely powerful knowledge of digital marketing strategies to use\r\n in any online platform to get results.\r\n</p><p>Grow your sales by doing successful email marketing, following step by \r\nstep instructions to get results.\r\n</p><p>Inject constant traffic into your website & business with SEO, rank \r\nin the first page of Google & other search engines.\r\n</p><p>Become a master of social media marketing, grow your business on \r\nFacebook & Instagram and bring traffic to your website.\r\n</p><p>Get more customers by doing successful advertisement campaigns on \r\ndifferent social media platforms.\r\n</p><p>Bring back the people who already visited your site by advanced \r\nretargeting.\r\n</p><p>Fix your buisiness problems before they even happen using Google \r\nanalytics, to avoid expensive problems in your business                 \r\n               </p>', '[\"Find your target audience, easily convince them to become your customer and buy your products.\",\"Become a master of social media marketing, grow your business on Facebook & Instagram and bring traffic to your website\",\"Grow your sales by doing successful email marketing, following step by step instructions to get results\",\"Fix your buisiness problems before they even happen using Google analytics, to avoid expensive problems in your business\"]', 'english', 4, 32, '[26,27,28,29,30]', '[\"No previous knowledge required\",\"Suitable for all types of businesses (digital product, physical product, service, B2B, B2C).\",\"Open with an open mind, hungry to learn amazing stuff!\"]', 20, NULL, 0, 'beginner', 5, NULL, 'https://www.youtube.com/watch?v=xA_yMYN19ug  ', 1560121200, 1562799600, NULL, 1, 0, 'active', 'youtube', '', 'Mega Digital Marketing Course', NULL),
(7, 'Human Resources (HR) Strategy Development', 'This course explores how to make the strategic connections between the organization’s strategy and the people strategy. ', '<p>In this course we discuss how to make the strategic connections between the organization’s strategy and the people strategy.</p><p>We\r\n begin with a review of how linking Human Resources (HR) processes \r\neffectively to support the organization can drive strategic value. Next \r\nwe discuss how to develop HR’s influence at the senior level in the \r\norganization and transform the HR function’s effectiveness.  Then we \r\nwork through the key aspects of HR strategy from identifying \r\norganization needs and priorities and defining situation specific, \r\nintegrated HR strategies that drive value for organizations in key \r\nareas.</p><p>This course serves Human Resource professionals who need to\r\n develop their strategic thinking and translating their learning into \r\npractical plans for implementing a HR strategy in the workplace.</p>', '[\"Identify the key Human Resources (HR) issues and external\\/internal strategic context for organizations\",\"Explore the key steps to developing an effective HR Strategy that is suitable for the organization\"]', 'english', 5, 34, '[31,32,33]', '[\"No advanced preparation or prerequisites are required for this course.\"]', 20, 1, 15, 'beginner', 7, NULL, 'https://www.youtube.com/watch?v=RODLmeefbMA', 1560121200, 1564614000, NULL, 1, 0, 'active', 'youtube', 'HR,Human Resourses', 'Human Resources Strategy Development', NULL),
(8, 'Learn Adobe Photoshop from Scratch', 'The Complete Beginners Guide for Learning Adobe Photoshop', '<p><strong>Photoshop </strong>is single most important tool for graphic \r\ndesigners and we bring together a course crafted for easy understanding \r\nand quick assimilation. This course starts with basic design \r\nintroduction and follow it up with tools like Selection tools, \r\nmanipulation tools , creation tools and workspace tools. You will also \r\nlearn professional design principles and tips and tricks to get you \r\nstarted on photoshop. This practical course focus on the most important \r\nphotoshop techniques and follows an unique task based pedagogy which is \r\ngreat for beginners and intermediate learners alike.</p>\r\n<p><strong>What will you get out of this course?</strong> </p>\r\n<p><strong>A complete hands on training on Adobe photoshop<br></strong><strong>Practical tips and tricks for professional designers<br></strong><strong>Task based lectures focusing on learning outcomes<br></strong><strong>Through discussion on photoshop tools<br></strong><strong>Detail process lectures on photoshop techniques and Effects..</strong></p>\r\n<p>This course is comprehensive introduction to professional photoshop \r\ndesign principles. You will surely learn a lot and will be to use \r\nphotoshop in various commercial and personal projects. </p>\r\n', '[\"Use different manipulation and creation tools\",\"Master Photo correction techniques\",\"Use Photoshop in professional projects\",\"Learn workflow tools\",\"Learn to add different effects using photoshop\"]', 'english', 3, 24, '[36]', '[\"No specific requirement except for a zeal to learn and a copy of the software on your pc\"]', 10, NULL, 0, 'beginner', 6, NULL, 'https://www.youtube.com/watch?v=sF_jSrBhdlg', 1560812400, 1562799600, NULL, 0, 0, 'active', 'youtube', 'photoshop,designing', 'Learn Adobe Photoshop from Scratch', 1),
(9, 'Personality and Soft Skills Development', 'Attitude & Motivation, Self-esteem Improvement, Other Aspects of Personality Development & Interview Etiquette', '<p>In this course (Personality and Soft Skills Development) we will see \r\nhow to analyse our personality and ways to improve it. We will see about\r\n the concept dimensions and significance of personality development. </p><ul><li><p>How our attitude reflects in our personalty </p></li><li><p>How Self - Motivation contributes for our personality improvement.</p></li><li><p>How Self - Esteem plays a major role in our personality development.</p></li><li><p>Other\r\n aspects of personality development like Body language, Problem solving \r\nskills, Decision making skill, Time management, Team work, Work Ethics, \r\nConflict Management & Stress Management and Leadership Qualities.</p></li><li><p>Employability quotient in which we will see Resume building, Group discussion and facing an interview.</p></li></ul>', '[\"How to speak infront of other people\",\"Change in attitude in good way\",\"Understanding of Employability Quotient\",\"How to behave in front of people\"]', 'english', 10, 65, '[]', '[\"Good Computer with popular we browser is enough.\"]', 25, 1, 20, 'beginner', 6, NULL, 'https://www.youtube.com/watch?v=wS0FPIpRPYo', 1560812400, 1562799600, NULL, 0, 0, 'active', 'youtube', 'Personality', 'Personality Development', NULL),
(19, 'Complete Python Bootcamp: Go from zero to hero in Python 3', 'Learn Python like a Professional! Start from the basics and go all the way to creating your own applications and games!', '<p xss=removed><span xss=removed>Become a Python Programmer and learn one of employer\'s most requested skills of 2018!</span><br></p><p xss=removed>This is the <span xss=removed>most comprehensive, yet straight-forward, course for the Python programming language on Udemy!</span> Whether you have never programmed before, already know basic syntax, or want to learn about the advanced features of Python, this course is for you! In this course we will <span xss=removed>teach you Python 3. </span>(Note, we also provide older Python 2 notes in case you need them)</p><p xss=removed>With <span xss=removed>over 100 lectures</span> and more than 20 hours of video this comprehensive course leaves no stone unturned! This course includes quizzes, tests, and homework assignments as well as 3 major projects to create a Python project portfolio!</p><p xss=removed>This course will teach you Python in a practical manner, with every lecture comes a full coding screencast and a corresponding code notebook! Learn in whatever manner is best for you!</p><p xss=removed>We will start by helping you get Python installed on your computer, regardless of your operating system, whether its Linux, MacOS, or Windows, we\'ve got you covered!</p><p xss=removed>We cover a wide variety of topics, including:</p><ul xss=removed><li>Command Line Basics</li><li>Installing Python</li><li>Running Python Code</li><li>Strings</li><li>Lists </li><li>Dictionaries</li><li>Tuples</li><li>Sets</li><li>Number Data Types</li><li>Print Formatting</li><li>Functions</li><li>Scope</li><li>args/kwargs</li><li>Built-in Functions</li><li>Debugging and Error Handling</li><li>Modules</li><li>External Modules</li><li>Object Oriented Programming</li><li>Inheritance</li><li>Polymorphism</li><li>File I/O</li><li>Advanced Methods</li><li>Unit Tests</li><li>and much more!</li></ul><p xss=removed><span xss=removed>You will get lifetime access to over 100 lectures plus corresponding Notebooks for the lectures! </span><br></p><p xss=removed>This course comes with a <span xss=removed>30 day money back guarantee!</span> If you are not satisfied in any way, you\'ll get your money back. Plus you will keep access to the Notebooks as a thank you for trying out the course!</p><p xss=removed><span xss=removed>So what are you waiting for? Learn Python in a way that will advance your career and increase your knowledge, all in a fun and practical way!</span></p><div class=\"audience\" data-purpose=\"course-audience\" xss=removed><div class=\"audience__title\" xss=removed>Who this course is for:</div><ul class=\"audience__list\" xss=removed><li>Beginners who have never programmed before.</li><li>Programmers switching languages to Python.</li><li>Intermediate Python programmers who want to level up their skills!</li></ul></div>', '[\" Learn to use Python professionally, learning both Python 2 and Python 3\",\"Learn advanced Python features, like the collections module and how to work with timestamps!\",\"Understand complex topics, like decorators.\",\"Get an understanding of how to create GUIs in the Jupyter Notebook system!\",\"Understand how to use both the Jupyter Notebook and create .py files\"]', 'english', 1, 11, '[34]', '[\"Anyone can take it \",\"you should have the interest \"]', 10, 1, 2, 'beginner', 14, NULL, '', 1563840000, NULL, NULL, NULL, 0, 'active', '', '', 'great course ', NULL),
(21, 'Learn everything about digital marketing in one course ', 'Learn FB advertising, Instagram Advertising, Linkedin management, YouTube marketing and SEM ', '<p>All you need to about Digital marketing and how to get your brand recognised</p>', '[\"You will learn everything about digital marking and much more \"]', 'english', 1, 11, '[41]', '[\"Anyone who is passionate about digital marketing \"]', 20, NULL, 10, 'advanced', 14, NULL, 'https://www.youtube.com/watch?v=XnkFYKTDCvU', 1563926400, NULL, NULL, NULL, 0, 'pending', 'youtube', '', 'digital marketing, facebook marketing, instagram marketing, ', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

DROP TABLE IF EXISTS `currency`;
CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `symbol` varchar(255) DEFAULT NULL,
  `paypal_supported` int(11) DEFAULT NULL,
  `stripe_supported` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `name`, `code`, `symbol`, `paypal_supported`, `stripe_supported`) VALUES
(1, 'Leke', 'ALL', 'Lek', 0, 1),
(2, 'Dollars', 'USD', '$', 1, 1),
(3, 'Afghanis', 'AFN', '؋', 0, 1),
(4, 'Pesos', 'ARS', '$', 0, 1),
(5, 'Guilders', 'AWG', 'ƒ', 0, 1),
(6, 'Dollars', 'AUD', '$', 1, 1),
(7, 'New Manats', 'AZN', 'ман', 0, 1),
(8, 'Dollars', 'BSD', '$', 0, 1),
(9, 'Dollars', 'BBD', '$', 0, 1),
(10, 'Rubles', 'BYR', 'p.', 0, 0),
(11, 'Euro', 'EUR', '€', 1, 1),
(12, 'Dollars', 'BZD', 'BZ$', 0, 1),
(13, 'Dollars', 'BMD', '$', 0, 1),
(14, 'Bolivianos', 'BOB', '$b', 0, 1),
(15, 'Convertible Marka', 'BAM', 'KM', 0, 1),
(16, 'Pula', 'BWP', 'P', 0, 1),
(17, 'Leva', 'BGN', 'лв', 0, 1),
(18, 'Reais', 'BRL', 'R$', 1, 1),
(19, 'Pounds', 'GBP', '£', 1, 1),
(20, 'Dollars', 'BND', '$', 0, 1),
(21, 'Riels', 'KHR', '៛', 0, 1),
(22, 'Dollars', 'CAD', '$', 1, 1),
(23, 'Dollars', 'KYD', '$', 0, 1),
(24, 'Pesos', 'CLP', '$', 0, 1),
(25, 'Yuan Renminbi', 'CNY', '¥', 0, 1),
(26, 'Pesos', 'COP', '$', 0, 1),
(27, 'Colón', 'CRC', '₡', 0, 1),
(28, 'Kuna', 'HRK', 'kn', 0, 1),
(29, 'Pesos', 'CUP', '₱', 0, 0),
(30, 'Koruny', 'CZK', 'Kč', 1, 1),
(31, 'Kroner', 'DKK', 'kr', 1, 1),
(32, 'Pesos', 'DOP ', 'RD$', 0, 1),
(33, 'Dollars', 'XCD', '$', 0, 1),
(34, 'Pounds', 'EGP', '£', 0, 1),
(35, 'Colones', 'SVC', '$', 0, 0),
(36, 'Pounds', 'FKP', '£', 0, 1),
(37, 'Dollars', 'FJD', '$', 0, 1),
(38, 'Cedis', 'GHC', '¢', 0, 0),
(39, 'Pounds', 'GIP', '£', 0, 1),
(40, 'Quetzales', 'GTQ', 'Q', 0, 1),
(41, 'Pounds', 'GGP', '£', 0, 0),
(42, 'Dollars', 'GYD', '$', 0, 1),
(43, 'Lempiras', 'HNL', 'L', 0, 1),
(44, 'Dollars', 'HKD', '$', 1, 1),
(45, 'Forint', 'HUF', 'Ft', 1, 1),
(46, 'Kronur', 'ISK', 'kr', 0, 1),
(47, 'Rupees', 'INR', 'Rp', 1, 1),
(48, 'Rupiahs', 'IDR', 'Rp', 0, 1),
(49, 'Rials', 'IRR', '﷼', 0, 0),
(50, 'Pounds', 'IMP', '£', 0, 0),
(51, 'New Shekels', 'ILS', '₪', 1, 1),
(52, 'Dollars', 'JMD', 'J$', 0, 1),
(53, 'Yen', 'JPY', '¥', 1, 1),
(54, 'Pounds', 'JEP', '£', 0, 0),
(55, 'Tenge', 'KZT', 'лв', 0, 1),
(56, 'Won', 'KPW', '₩', 0, 0),
(57, 'Won', 'KRW', '₩', 0, 1),
(58, 'Soms', 'KGS', 'лв', 0, 1),
(59, 'Kips', 'LAK', '₭', 0, 1),
(60, 'Lati', 'LVL', 'Ls', 0, 0),
(61, 'Pounds', 'LBP', '£', 0, 1),
(62, 'Dollars', 'LRD', '$', 0, 1),
(63, 'Switzerland Francs', 'CHF', 'CHF', 1, 1),
(64, 'Litai', 'LTL', 'Lt', 0, 0),
(65, 'Denars', 'MKD', 'ден', 0, 1),
(66, 'Ringgits', 'MYR', 'RM', 1, 1),
(67, 'Rupees', 'MUR', '₨', 0, 1),
(68, 'Pesos', 'MXN', '$', 1, 1),
(69, 'Tugriks', 'MNT', '₮', 0, 1),
(70, 'Meticais', 'MZN', 'MT', 0, 1),
(71, 'Dollars', 'NAD', '$', 0, 1),
(72, 'Rupees', 'NPR', '₨', 0, 1),
(73, 'Guilders', 'ANG', 'ƒ', 0, 1),
(74, 'Dollars', 'NZD', '$', 1, 1),
(75, 'Cordobas', 'NIO', 'C$', 0, 1),
(76, 'Nairas', 'NGN', '₦', 0, 1),
(77, 'Krone', 'NOK', 'kr', 1, 1),
(78, 'Rials', 'OMR', '﷼', 0, 0),
(79, 'Rupees', 'PKR', '₨', 0, 1),
(80, 'Balboa', 'PAB', 'B/.', 0, 1),
(81, 'Guarani', 'PYG', 'Gs', 0, 1),
(82, 'Nuevos Soles', 'PEN', 'S/.', 0, 1),
(83, 'Pesos', 'PHP', 'Php', 1, 1),
(84, 'Zlotych', 'PLN', 'zł', 1, 1),
(85, 'Rials', 'QAR', '﷼', 0, 1),
(86, 'New Lei', 'RON', 'lei', 0, 1),
(87, 'Rubles', 'RUB', 'руб', 1, 1),
(88, 'Pounds', 'SHP', '£', 0, 1),
(89, 'Riyals', 'SAR', '﷼', 0, 1),
(90, 'Dinars', 'RSD', 'Дин.', 0, 1),
(91, 'Rupees', 'SCR', '₨', 0, 1),
(92, 'Dollars', 'SGD', '$', 1, 1),
(93, 'Dollars', 'SBD', '$', 0, 1),
(94, 'Shillings', 'SOS', 'S', 0, 1),
(95, 'Rand', 'ZAR', 'R', 0, 1),
(96, 'Rupees', 'LKR', '₨', 0, 1),
(97, 'Kronor', 'SEK', 'kr', 1, 1),
(98, 'Dollars', 'SRD', '$', 0, 1),
(99, 'Pounds', 'SYP', '£', 0, 0),
(100, 'New Dollars', 'TWD', 'NT$', 1, 1),
(101, 'Baht', 'THB', '฿', 1, 1),
(102, 'Dollars', 'TTD', 'TT$', 0, 1),
(103, 'Lira', 'TRY', 'TL', 0, 1),
(104, 'Liras', 'TRL', '£', 0, 0),
(105, 'Dollars', 'TVD', '$', 0, 0),
(106, 'Hryvnia', 'UAH', '₴', 0, 1),
(107, 'Pesos', 'UYU', '$U', 0, 1),
(108, 'Sums', 'UZS', 'лв', 0, 1),
(109, 'Bolivares Fuertes', 'VEF', 'Bs', 0, 0),
(110, 'Dong', 'VND', '₫', 0, 1),
(111, 'Rials', 'YER', '﷼', 0, 1),
(112, 'Zimbabwe Dollars', 'ZWD', 'Z$', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `enrol`
--

DROP TABLE IF EXISTS `enrol`;
CREATE TABLE `enrol` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `enrol`
--

INSERT INTO `enrol` (`id`, `user_id`, `course_id`, `date_added`, `last_modified`) VALUES
(1, 2, 8, 1566342000, NULL),
(2, 2, 1, 1566946800, NULL),
(3, 2, 4, 1568156400, NULL),
(4, 6, 3, 1568329200, NULL),
(5, 7, 4, 1568761200, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `frontend_settings`
--

DROP TABLE IF EXISTS `frontend_settings`;
CREATE TABLE `frontend_settings` (
  `id` int(11) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `frontend_settings`
--

INSERT INTO `frontend_settings` (`id`, `key`, `value`) VALUES
(1, 'banner_title', 'Course Wings'),
(2, 'banner_sub_title', 'Upskill Yourself. Learn New Skills From Industry Experts.'),
(4, 'about_us', '<p>Course Wings website is owned by \"Course Wings Portal\", a company incorporated in Dubai, United Arab Emirates.<br><br>We strongly endorse the fact that Online Education is the way forward and Course Wings was established with the purpose of sharing knowledge with everyone who is interested in learning and anyone who is interested in sharing their knowledge with the world.  We are a platform where experts or instructors can upload pre-recorded educational video courses and students can view them and broaden their knowledge. <br><br>We always believe that “easy access to learning is of paramount importance in one\'s life”. At Course Wings, we strive to share the knowledge that will not only benefit people in their life\'s but will also help them in their professional careers and broaden their horizon. We believe that, if you have a talent or if you specialize in a certain field, then come showcase your knowledge in the form of a video on our site and let the world benefit from it. We never know, through these course videos, we might transform someone\'s life for the better.<br><br>In general, Course Wings is a portal where you can easily access a wide spectrum of courses covering a large gamut of topics ranging from Intellectual, Educational, Business, Digital, Management to topics related to your Hobbies, Lifestyle, Fun and many more ... we have it all covered in one site. <br><br>All topics on Course Wings are uploaded by experienced instructors who have vast knowledge in their respective fields.  We have a stringent weeding process where we only select the best instructors. We believe if you want to move up the ladder or if you want to pursue something in order to get noticed, the first step is to enhance your knowledge and Course Wings is the right platform to start.<br><br><b>Welcome to Course Wings... Your platform to online learning!</b><br></p>'),
(10, 'terms_and_condition', '<p><b>1. Introduction: </b><br>Welcome to Course Wings Portal terms and conditions page. These terms and conditions apply to the online uploading and buying of videos provided by Course Wings website, a company owned by Course Wings Portal (henceforth to be addressed as Course Wings), based in the Dubai, United Arab Emirates. Course Wings (www.coursewings.com) was launched in order to help experts / instructors upload professional videos and give an opportunity to students / users to view these videos at a cost or in some instances free of cost. <br><br>The primary purpose of the terms and conditions are to outline the rules and regulations for the use of Course Wings Website. These terms and conditions are in addition to the website disclaimer and apply to the uploading or sale of any online videos.  Please read them carefully before uploading or buying any video.<br><br>By accessing our site www.coursewings.com, accepting or buying our videos through Course Wings website, it is understood that you accept our terms and conditions. We kindly request you not to continue using Course Wing website if you do not agree to accept all of the terms and conditions.<br><br>Please note, the following terminology/clauses apply to these Terms and Conditions page: \"Client/Student\", \"You\" and \"Your\" refers to you, the person log on this website and compliant to the Company’s terms and conditions. \"The Company\", \"Ourselves\", \"We\", \"Our\" and \"Us\", refers to our Company  Course Wings.  Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to same.<br><br>As instructors, if you upload any video on Course Wings, it is understood that you agree to the “Instructors Terms and Conditions” and agree to furnish your personal details. Simultaneously, as Students, if you agree to buy a course/video, you agree to the “Students Terms and Conditions” and agree to furnish your personal details. <br><br>Please be advised that Course Wings always reserves the rights to change the terms and conditions (T&C), any time or as many times it wants. In most cases, whatever changes we make will be effective after 20 days of incorporation and if you are using our services it is understood that you accept our T&C. Therefore, we request you to constantly review our T&C page. Any kind of disagreement between Course Wings and Users (Students or Instructors) will be addressed based on the present T&C. <br><br>This Agreement shall begin on the date hereof a user agrees to engage in our services.<br><br><br><b>2. Cookies:</b><br>We employ the use of cookies. By accessing Course Wings, you agreed to use cookies in agreement with the Course Wings Privacy Policy.<br><br>Most interactive websites use cookies to let us retrieve the user’s details for each visit. Cookies are used by our website to enable the functionality of certain areas to make it easier for people visiting our website. Some of our affiliate/advertising partners may also use cookies.<br><br><b><br>3. License, Trademark & Communication with the Users:</b><br>Unless otherwise specified, Course Wings Portal and/or its licensors own the intellectual property rights for all materials published on Course Wings Portal. All intellectual property rights are reserved. <br><br><i>You may not: </i><br>•    Republish material/videos from Course Wings website <br>•    Sell, rent or sub-license material from Course Wings website <br>•    Reproduce, duplicate or copy material from Course Wings website<br>•    Redistribute content from Course Wings website<br>•    Tamper with Course Wing Site and its contents<br>•    You can’t use Course Wings Name, Logo and Domain Name to represent your business, as this is a registered name in the UAE.<br><br>All communication will be done electronically and if you sign-up with us it is understood that you accept this mode of communication. <br><br><i>Point of Agreement: </i><br>•    As a student, when you open an account with Course Wings either to view a course or for any other reason/s, you are granted the permission to access/view our courses. <br>•    As soon as you open an account (either as a student or lecturer) you agree to all the terms and conditions.<br>•    As a student, in most cases, you will have lifetime access to most of our course. However, we have the right to cancel your account at any given point of time without notice. <br><br>The same is the case if you breach the terms and conditions, we reserve the right to shut down your account. As instructors, each time you upload a course, you give us the permission/license to market your contents/courses anywhere in the world, to any student in the world, either for free or for the said agreed fee. We also reserve the right to cancel your account anytime without notice. Please also note: as instructors, you can\'t give any license of your courses to students in order for them to resell your courses.<br><br>•    As a student, you get an approval from Course Wings to view videos/courses only on Course Wings platform. However, you may not resell / or reproduce / share / illegally download/share to download material especially through pirated / illegal sites etc.<br>•    It’s up to the instructor’s discretion that he will no longer offer any assistance to students related to the course they have uploaded.<br><br> <br><b>4. Course-related comments and offensive comments: </b><br>Parts of this website offer an opportunity for experts to post videos and exchange opinions and information. Course Wings does not filter, edit, publish or review comments prior to their presence on the website. Comments (from anyone) do not reflect the views and opinions of Course Wings, its agents and or its affiliates. Comments reflect the views and opinions of the person who post the content. To the extent permitted by applicable laws, Course Wings shall not be liable for the Comments or for any liability, damages or expenses caused and/or suffered as a result of any use of and/or posting of and/or appearance of the Comments on this website. <br><br><i>You warrant and represent that:</i><br>•    You are entitled to post the Comments on our website and have all necessary licenses and consents to do so;<br>•    The Comments do not invade any intellectual property right, including without limitation copyright, patent or trademark of any third party;<br>•    The Comments do not contain any defamatory, libellous, offensive, indecent or otherwise unlawful material which is an invasion of privacy<br>•    Comments will not be used to solicit or promote business or custom or present commercial activities or unlawful activity/ies.<br><br>You hereby grant Course Wings owned by Course Wings Portal, a non-exclusive license to use, reproduce, edit and authorize others to use, reproduce and edit any of your Comments in any and all forms, formats or media.<br><br>Course Wings Portal reserves the right to monitor all Comments and to remove any Comments without prior notice which appear to be inappropriate, offensive, violent, or causes a breach of these Terms and Conditions. It is understood that you as a user (Student or Instructor) of the Course Wings Portal will not post anything concerning pornography; racist/sexist comments; hateful speech or intent toward an individual, group, company, politic party, insulting to anyone, offensive topic, society or community. <br><br><br><b>5. Reservation of rights and Termination of your account: </b><br>We reserve the right to request that you remove all videos uploaded by you, links or any particular link to our Website. You approve to immediately remove all videos/links to our Website upon request. We also reserve the right to amend these terms and conditions and it’s policies at any time we think necessary. By continuously connecting/visiting our website, you agree to be bound to and follow these linking terms and conditions.<br><br>Course Wings Portal also reserves the right to terminate/closing your account within 24 hours if we observe that you have violated the said terms and conditions without any warning. In doing so, we also reserve the rights not to offer any products and services to you again in the future and you cannot hold us responsible for the action. Offering services to you will be solely at our discretion. Settling of outstanding bills will be discussed and settled if the resolution committee feels it is justifiable. <br><br><br><b>6. Site Traffic, Maintenance and Peripheral Links: </b><br>For promotion purposes or any other reason, Course Wings may contain external links in the site. The contents in these links have nothing to do with Course Wings and we are not responsible for the contents in it. If for some reason, the user clicks on these sites, they will be doing so at their own discretion/risk and we are not responsible in any way for the contents on these external links. <br><br>Our intention will always be to run the site for 24 hours a day, however, there might be times when the site is under maintenance and it might cause some delays. In addition, due to Internet connection/speed, there might be a scenario where a video/content might take time to commence or download. In this regard, the user can not hold Course Wings Portal responsible in any way.  <br><br><br><b>7. Payment Terms & Conditions, Taxes & Refund procedure: </b><br>If you sign-up with Course Wings and agree to pay the said amount set by the Instructor or our Marketing Team, then you authorize us to debit your credit/debit card or through any other mode. All payments need to be made in the name of Course Wings Portal and all information furnished towards the payment will be shared with our payment provider with the utmost confidentiality. Primary payment currency is US Dollars however; there will be an option of paying in the local country currency. We accept Visa or MasterCard or Paypal and the payment will be accepted only through an online payment. If the payment is declined for some reason, then you will have to pay the said fees within 30 days with a late charge fee. <br><br>For the Instructors, you will have to furnish us with all the details of your account and any personal information requested. Once your course or courses are purchased, the money will be remitted into your account after deduction of service charges within 30 days. <br><br>Taxes or Value Added Tax will be collected as per the country law and remitted to the concerned authorities every time a transaction is made. In case it\'s a currency other than the local country, the same will be converted based on the local currency and the Tax will be remitted to the concerned authority. <br><br>At Course Wings Portal only the best videos in the industry are uploaded and it is understood that if you purchase the video you are getting the best product that money can buy. Keeping our quality in mind we don\'t have a policy of returning the money to our users. However, due to certain unforeseen circumstances, if the video is corrupted for some reason and the content cannot be viewed, you will have to get in touch with us through the help centre and we will review the case and revert back with a feedback. If the concern is genuine, then we will refund your money within 30 days through online payment medium. Please note, we will only accept this request if the concern is registered with 3 days of the purchase. <br><br>Note: All course prices on Course Wings are based on what the instructor has decided and should be in the line with our payment policy. We will also run some promotions on a regular basis on a price agreed upon by the Instructor or Course Wings or Both. <br><br><br><b>7. Limitations of Liability:</b><br>We shall not be held responsible for any content that appears on our Website posted by anyone. You agree to protect and defend us against all claims. No link(s) should appear on any Website that may be interpreted as libellous, obscene or criminal, or which infringes, otherwise violates, or advocates the infringement or other violation of, any third party rights. You agree that you cannot hold Course Wings Portal, Partners, Agents, Suppliers or anyone associated with us, responsible for any damages (direct/indirect), injuries, health issues, loss, mental stress or anything that harms your health, loss of data, profits, cause of death, fraud, punitive, consequential damage, lead to financial loses (direct/indirect), accuse us of wrong advise, etc., while you are viewing our courses or videos (either promotional videos or purchased). This is applicable even if you have alerted us in advance of possible damage, health issue, loss or Death that might arise.  These courses/videos are posted in good faith and if you choose to view them or buy them you will be doing so at your own risk and you can\'t sue us or take us to court or claim for damages. <br><br><br><b>8. iFrames:</b><br>Without prior approval and written permission, you may not produce/create frames/layouts around our Webpages that alter in any way the visual presentation or appearance of our Website.<br><br><br><b>9. Force Majeure event:</b><br>In the case of an unforeseen force majeure event or circumstance, that is out of our control, such as war, strike, fire, hacking, internet related issues, riot, enforceable law, crime, or an event described by the legal term act of God (hurricane, flood, earthquake, volcanic eruption, etc.) or any other unforeseen circumstances, Course Wings Portal is not responsible for the services we provide or the quality related to it. <br><br> <br><b>10. Your Privacy:</b><br>Please read Privacy Policy for more details and if you have any reservations, you could write directly to us or choose not to view or buy our courses. <br><br><br><b>11. Students/Instructor login:  </b><br>To browse promo videos and contents on Course Wings, you don\'t need to feed in your credentials to open an account. However, if you want to take benefits of viewing courses and uploading courses, you will have to create an account with us. All personal information furnished will be kept confidential, except the content that is meant for public viewing. <br><br><br><b>12. Age Limit: </b><br>We don\'t allow minors to upload videos on our platform. Nor do we allow them to open accounts on Course Wings. In other words, both students and Expects/Instructors need to be 18+. You need to be above the permissible age limit for accessing online content and creating accounts based on your countries regulations. Course Wings Portal reserves the right to cancel your account if we find out you\'re below the age limit stipulated by us (+18 years). <br><br><br><b>13. Login Credentials, Security, Content, & Code of Ethics: </b><br>If you choose to use our site then you also agree to compile with all relevant laws, country-specific country regulations, copyrights regulations and or any other copyrights issues or personal identity. If you want full access to our site, you will have to first open an account using your credentials. You also agree to furniture your personal detail and we will keep it confidential to the best of our ability. It is your responsibility that all information provided is accurate and it\'s not falsely quoted. All information should not be doctored, offend anyone and anything to do with religion. You agree not to transfer virus or junk content into the site and or thing concerning representing a 3rd Party etc.<br><br>Please be careful of your username, email address and password. It is your property and you need to guard it and please don\'t share it with anyone. You are solely responsible for the activities on your page/profile Course Wings. We are not responsible to amicably settle any disagreements between students and instructors unless until either of the party has acted against the Terms & Conditions stated by Course Wings. Please be advised, if we suspect any unusual activity on your site, we hold the right to close it. In any event, where you have lost your password or username or both, we will help you retrieve it provided you furnish us with some answers to security question(s). <br><br>If unfortunately, the user passes away due to unforeseen circumstances, Course Wing is authorized to close down the account, provided enough evidence is presented. If you also suspect that some third party individual/individuals are using your account, please do get in touch with us immediately. Please be advised that you can buy/view our videos for only lawful reasons and not for unlawful reasons.  You may not misuse our platform for any cybercrime, transfer any kind of virus, promote your personal material, use it for network marketing, junk material that can infect the site, or use Course Wings for any kind of commercial gain. <br><br>Finally, it is your responsibility to obey the cyber law that is practised in the country you reside in.<br><br>You agree that we may record all or any part of any Course (including voice chat communications) for quality control and delivering, marketing, promoting, demonstrating or operating the Site and the Products. You hereby grant Course Wings permission to use your name, likeness, image or voice in connection with offering, delivering, marketing, promoting, demonstrating, and selling Products, Courses, Company Content and Submitted Content and waive any and all rights of privacy, publicity, or any other rights of a similar nature in connection therewith, to the extent permissible under applicable law.<br><br>All rights not expressly granted in these Terms are retained by the Content owners and these Terms do not grant any implied licenses.<br><br><br><b>14. Use our Platform at your own risk:</b><br>We are offering a platform that can be used by anyone and everyone. People are free to upload course but you can\'t hold Course Wings accountable is it is offensive to you in any way. If you agree to use our platform then you are doing so at your own risk. This implies to both Students and Instructors. In this view, Course Wings is a platform where there will be a lot of communication between an Instructor and a Student or Both with the Company Admin. In case someone gets offended, we cannot be held responsible for it.  In other words, you cannot hold us responsible for arguments, disagreements, dues, damages, losses, grievances, etc. during Instructor and Student interaction. <br><br><br><b>15. Validity of Terms & Conditions:</b><br>The terms and conditions will be updated regularly and we encourage users to constantly keep observing the terms. All parameter of the Terms and Condition are considered valid until otherwise stated.  <br><br><br><b>16. Promotion-related activities:</b><br>As a user of our portal, you grant us the rights to promote your content or reproduce your content and post it on third-party platforms like Websites, YouTube, Facebook, Instagram, LinkedIn, Twitter, SnapChat or any other Social Media Platform. The core initiative will be to only promote Course Wings Portal and your contents / video / course.<br><br><br><b>17. Account Opening:</b><br>If you need full access to our site and videos you need to open an account. In doing so, you can assign yourself a user name and password. When you agree to open an account then you agree to get into a joint venture between you and Course Wings and that you agree to all the terms and Condition. While you create your account please ensure you furnish accurate information as you will be the sole owner of the content displayed in your account and we are not responsible about the content in your page. Please try not to share your credentials with other people, as we will not be responsible if you end up in a dispute. <br><br><br><b>18. Payment Commitment: </b><br>You opt to access or use our products/services that involves a payment, then it is understood that you have agreed to pay us for the services offered. This will also mean that you have agreed to accept all the terms and conditions as well. In doing so you have now agreed to pay an agreed sum, and will be since responsible for payment including all taxes associated with the product/services. In the event of using a credit card, you confirm that the information pertaining to your credit card is authentic and not fabricated and that you have given Course Wings Portal all the rights to debit your account with the agreed amount set by Course Wings. Due to some reason if your credit card is maxed out or not functional then we reserve the right of collecting the money using other methods or collection through authorized methods like involving a collection company that specializes in this respect, involve a Law firm or any other method deemed necessary. Course Wings Portal also reserve the right of freezing any other unresolved matter/s or amount relating to you if we discover foul play. <br><br><br><b>19. Non-Disclosure of information about Students or Mentors:</b><br>As a Student, it is understood that you will not disclose any personal information to the Instructor that could include your contact details or any other matter that is not relevant to Course Wings. In addition, you will also not reveal any other information personal or professional to any third person. <br><br>As Mentors, you agree that you will not disclose any information of Students to any other person other than the Course Wings administration team. You will also not indulge in a conversation with a Student through any other platform like personal email, or telephone calls etc. The only mode of communication is through Course Wings Website. <br><br><br><b>20. Duplicating or Reproducing content for Private Advantage / Profit / Benefit / Gain: </b><br>If you sign up with Course Wings Portal you agree that you will not duplicate / reproducing content for private advantage / distribution / profit / benefit or gain. The content posted should be original (your thoughts, experience and ideas) and not plagiarized from other sources. <br><br><br><b>21. Law and Legal Affairs: </b><br>Course Wings website is a portal that is accessible to the public and disputes and claims, if any, will be impeccably settled between you and Course Wings. You agree not to take us to a court or file a case against us in the Court of Law, in any country. This is a public domain and we don\'t take responsibility for anything. If this is not acceptable, then please don\'t register with us or view our site. <br><br><br><b>22. Disclaimer:</b><br>To the maximum extent permitted by applicable law, we exclude all representations, warranties and conditions relating to our website and the use of this website. Nothing in this disclaimer will:<br><br>•    Limit or exclude our or your liability for death or personal injury;<br>•    Limit or exclude our or your liability for fraud or fraudulent misrepresentation;<br>•    Limit any of our or your liabilities in any way that is not permitted under applicable law; or<br>•    Exclude any of our or your liabilities that may not be excluded under applicable law.<br><br>The limitations and prohibitions of liability set in this section and elsewhere in this disclaimer: (a) are subject to the preceding paragraph; and (b) govern all liabilities arising under the disclaimer, including liabilities arising in contract, in tort and for breach of statutory duty.<br><br>The videos, contents, products and services offered on Course Wings Portal, has no warranty or guarantee under the country’s applicable law where you’re residing. Course Wings Portal hereby disclaims all such things including content accuracy, typos, fitness, errors if any, commercially acceptable quality, merchantability etc and we will not be liable for any loss or damage of any nature.  As a user of our site,  you cannot claim for any damages/losses of any nature including personal or professional. <br><br><br><b>23. Indemnification:</b><br>We always reserve the right of taking legal action against you if you get us in legal trouble or don\'t abide by the term  & conditions or privacy policy (available in the privacy page). You need to agree that you will not harm Course Wings in any way including its employees, associated suppliers; their admin and owners or whoever is directly or indirectly involved with Course Wings Portal. <br><br><br><b>24. Dispute Department:</b><br>If there is any dispute that arises between parties involved, please send us a written complain through a letter or thru email and we will respond to the same at the earliest depending on the nature of the concern. <br><br><br><b>25. Obligatory/Binding Agreement</b><br>As soon as you register to use our products, contents and services, you are agreeing to an agreement that is legally binding with Course Wings and are obliged to abide by all the points mention in this Terms and Conditions. Please go thru these terms and conditions thoroughly and only after agreeing to it register otherwise you are requested not to register.  If there is any concern the matter has to be brought to the Company’s attention in writing immediately. <br><br><br><b>26. Terms and Conditions Updates: </b><br>On a regular basis, we will be updating these terms and conditions section and Course Wings reserves the right to make any changes. Hence, it is advisable for users to view the T&C regularly. Note: The Terms and Conditions will be in effect from the time it is posted.<br></p>'),
(11, 'privacy_policy', '<p>Welcome to Course Wings Privacy Policy. Your privacy is extremely critical and important to us. Course Wings is a company registered under the trade license name Course Wings Portal and the company is located in the Emirate of Dubai, United Arab Emirates.</p> <p>It is Course Wings policy to respect your privacy regarding any information we may collect while operating our website. This Privacy Policy applies to coursewings.com (hereinafter, \"us\", \"we\", or \"coursewings.com\" or Course Wings Portal). We respect your privacy and are committed to protecting personally identifiable information you may provide us through the Website www.coursewings.com. We have adopted this privacy policy (\"Privacy Policy\") to explain what information may be collected on our Website, how we use this information, and under what circumstances we may reveal the information to third parties. This Privacy Policy applies only to information we collect through the Website and does not apply to our collection of information from other sources.</p> <p>This Privacy Policy, together with the Terms and Conditions posted on our Website, set forth the general rules and policies governing your use of our Website. Depending on your activities / specialization when visiting our Website, you may be required to agree to additional terms and conditions, if necessary.</p> <p>Irrespective of which country your based or residing in, if you agree to the Privacy Police it is understood that you are getting into a legally binding contract with Course Wings Portal. In other words, this policy is applicable worldwide. If not acceptable, with the terms of use and with the privacy terms, then you are strongly recommended not to use our services. </p> <p><b>1. Website Visitors:</b></p> <p>Like most website operators, Course Wings collects non-personally-identifying information of the sort that web browsers and servers typically make available, such as the browser type, language preference, referring site, and the date and time of each visitor request. Course Wings purpose in collecting non-personally identifying information is to better understand how Course Wings visitors use the website. From time to time, Course Wings may release non-personally-identifying information in the aggregate, e.g., by publishing a report on trends in the usage of its website.</p> <p>Course Wings also collects potentially personally identifying information like Internet Protocol (IP) addresses for logged in users and for users leaving comments on www.coursewings.com blog posts or instructor page. Course Wings only discloses logged in user and commenter IP addresses under the same circumstances that it uses and discloses personally identifying information as described below.<br></p> <p><b>2. Security of Personal Information:</b></p> <p>The security of your Personal Information is important to us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Information, we cannot guarantee its absolute security.</p> <p>The following information will be collected, but not limited to: </p> <blockquote> <p>1. Information during registration with us (either directly or indirectly through third party links) as a Student or as an Instructor that may include – your name, profession, gender, date of birth, country residing in, preferred mode of receiving payment, bank address, email address, telephone no., address, password, username, or any other information as required. </p> <p>2. Your profile will be public for anyone to view, hence you are recommended not to disclose false information and Course Wings doesn\'t take any responsibility regarding any information you furnish us with. </p> <p>3. Course Wings is a public platform and we allow users to post comments and share details. For our internal use, we keep these data for customer tracking or for contacting users. </p> <p>4. Since this a public platform, it is understood that you have given us permission (non-exclusive rights) to use the comments / contents and suggestions shared by users in order to promote Course Wings. </p> <p>5. Please be advised, Course Wings cannot guarantee any flow of content from either Students or Instructors and is since not liable for any suggestions / content or conclusion. </p> <p></p> </blockquote> <p><b>5. Gathering of Personally-Identifying Information:</b></p> <p>Certain visitors to Course Wings websites choose to interact or open an account with Course Wings in ways that require Course Wings to gather personally identifying information. The amount and type of information that Course Wings gathers depend on the nature of the interaction. For example, we ask Students to sign up for a course or ask Instructors to upload course at www.coursewings.com and in doing so Students / Instructors will have to provide their information to Course Wings.</p> <p> </p> <p><b>6. Gathering of Credit Card details: </b> </p> <p>As Course Wings, we operate with a recognized payment gate and we are in line with the all the online payment norms and guidelines and Payment Card Industry Data Security Standard. All information collected from users for payment purposes will be kept strictly confidential by the payment gateway company and we, as Course Wings, don\'t store any information.<span xss=removed> </p> <p> </p> <p><b>7. Advertisements:</b></p> <p>Ads on our website may be delivered to users by advertising partners, who may set cookies. These cookies allow the ad server to recognize your computer each time they send you an online advertisement to compile information about you or others who use your computer. This information allows ad networks to, among other things, deliver targeted advertisements that they believe will be of most interest to you. This Privacy Policy covers the use of cookies by Course Wings and does not cover the use of cookies by any advertisers.</p> <p> </p> <p><b>8. Age limit concerning usage of Course Wings</b></p> <p>Age limit to use our site is 18+ years. In other words, we consider people under the age of 18 as minors and do not accept their involvement with our site. People below the age of 18 will be violating the terms of our policy and the information collected or the page created by them will be terminated immediately. We are also not responsible for the loss of contents in this case.</p> <p> </p> <p><b>9. Links To External Sites</b></p> <p>Our Service may contain links to external sites that are not operated by us. If you click on a third party link, you will be directed to that third party\'s site. We strongly advise you to review the Privacy Policy and the Terms and Conditions of every site you visit.</p> <p>We have no control over and assume no responsibility for the content, Privacy Policies or practices of any third party sites, products or services. </p> <p>In case your providing us information through a third party site, then it is understood that you are allowing us to collate information from the third party site and use it for our use. </p> <p> </p> <p><b>10. Aggregated Statistics / Collection and Sharing of Customer Data:</b></p> <p>When you access our site, Course Wings may collect statistics / data about the behavior of visitors to its website through automated methods. Information might relate to your IP address, behaviour traits, demographics, location, your computer, wireless device, domain etc or in other words only aggregated information of the user. Course Wings may also deploy web beacons or similar technology to track e-mail movements, page visits, to run promotions etc. </p> <p> </p> <p>Course Wings may display this information publicly or provide it to others. However, Course Wings does not disclose your personally identifying information. This information also might be shared by third-party sources in case we need them to get involved with the marketing of the site or courses. In other words, we reserve the rights of sharing your data with Students, Instructors, external advertising / social media agencies, email campaigns, freelancers, partners, survey agencies, for push notification, for security & law reasons etc.<span xss=removed> We also reserve the right for communicating with you concerning your account / preference, sending you messages concerning your course or action or resolving disputes, newsletters, request feedback, data for survey, any attempt to improve the customer experience etc.</p> <p> </p> <p><b>11. Survey and opt-out option</b></p> <p>Course Wings also may conduct surveys to understand customer needs and preferences. The survey may demand that we collect relevant data to conduct the survey and this might include, but not limited to: Your Name, Phone No., E-mail address, Physical Address, Age, etc. We might display the content on our public platform or might just use it for internal purpose depending on the nature of the survey. The sole purpose of collecting the data is to promote our services better and to understand how we can better our services. </p> <p> </p> <p>It is your right to opt-out of the survey or any personal information we request. It is also your right to cancel your account anytime if you feel you don\'t want to disclose any information. You also have the option of not accepting our cookies policy and opting out of any promotion. </p> <p> </p> <p>All updates and access to your personal data is your right and it must be requested through a written request to info@coursewings.com. </p> <p> </p> <p><b>12. Cookies</b></p> <p>To enrich and perfect your online experience, Course Wings uses \"Cookies\", similar technologies and services provided by others to display personalized content, appropriate advertising and store your preferences on your computer.</p> <p> </p> <p>A cookie is a string of information that a website stores on a visitor\'s computer, and that the visitor\'s browser provides to the website each time the visitor returns. Course Wings uses cookies to help Course Wings identify and track visitors, their usage of http://coursewings.com, protect from fraud logins / activities enabling the process of your course purchasing process and their website access preferences. </p> <p> </p> <p>Course Wings visitors who do not wish to have cookies placed on their computers should set their browsers to refuse cookies before using Course Wings websites, with the drawback that certain features of Course Wings websites may not function properly without the aid of cookies.</p> <p>By continuing to navigate our website without changing your cookie settings, you hereby acknowledge and agree to Course Wings use of cookies.</p> <p> </p> <p><b>13. Privacy Policy Changes</b></p> <p>Although most changes are likely to be minor, Course Wings may change its Privacy Policy from time to time; it is at the sole discretion of Course Wings to do so. Course Wings encourages visitors to frequently check this page for any changes to its Privacy Policy. Your continued use of this site after any change in this Privacy Policy will constitute your acceptance of such change.</p> <p> </p> <p><b>14. Contact Information</b></p> <p>If you have any questions about this Privacy Policy, please contact us via email: info@coursewings.com.</p> <p> </p> <p><i>ThisPrivacy Policy was last updated on 6th July 2019</i></p>'),
(13, 'theme', 'default');

-- --------------------------------------------------------

--
-- Table structure for table `hits`
--

DROP TABLE IF EXISTS `hits`;
CREATE TABLE `hits` (
  `id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `course_category` int(11) DEFAULT NULL,
  `course_subcategory` int(11) DEFAULT NULL,
  `course_author` int(11) DEFAULT NULL,
  `hit_conter` double NOT NULL DEFAULT '1',
  `last_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hits`
--

INSERT INTO `hits` (`id`, `course_id`, `course_category`, `course_subcategory`, `course_author`, `hit_conter`, `last_updated_at`) VALUES
(1, 19, 1, 11, 14, 3, '2019-09-21 14:51:29'),
(2, 2, 4, 29, 5, 3, '2019-09-23 17:22:56'),
(3, 3, 7, 48, 6, 2, '2019-09-19 10:43:55'),
(4, 4, 8, 57, 2, 2, '2019-09-21 14:51:54'),
(5, 7, 5, 34, 7, 22, '2019-09-21 15:51:10'),
(6, 1, 8, 57, 3, 3, '2019-09-21 19:52:48'),
(7, 8, 3, 24, 6, 1, '2019-09-19 10:47:23');

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

DROP TABLE IF EXISTS `language`;
CREATE TABLE `language` (
  `phrase_id` int(11) NOT NULL,
  `phrase` longtext COLLATE utf8_unicode_ci,
  `english` longtext COLLATE utf8_unicode_ci,
  `Bengali` longtext COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`phrase_id`, `phrase`, `english`, `Bengali`) VALUES
(1, 'manage_profile', NULL, NULL),
(140, 'category_code', NULL, NULL),
(3, 'dashboard', NULL, NULL),
(4, 'categories', NULL, NULL),
(5, 'courses', NULL, NULL),
(6, 'students', NULL, NULL),
(7, 'enroll_history', NULL, NULL),
(8, 'message', NULL, NULL),
(9, 'settings', NULL, NULL),
(10, 'system_settings', NULL, NULL),
(11, 'frontend_settings', NULL, NULL),
(12, 'payment_settings', NULL, NULL),
(13, 'manage_language', NULL, NULL),
(14, 'edit_profile', NULL, NULL),
(15, 'log_out', NULL, NULL),
(16, 'first_name', NULL, NULL),
(17, 'last_name', NULL, NULL),
(18, 'email', NULL, NULL),
(19, 'facebook_link', NULL, NULL),
(20, 'twitter_link', NULL, NULL),
(21, 'linkedin_link', NULL, NULL),
(22, 'a_short_title_about_yourself', NULL, NULL),
(23, 'biography', NULL, NULL),
(24, 'photo', NULL, NULL),
(25, 'select_image', NULL, NULL),
(26, 'change', NULL, NULL),
(27, 'remove', NULL, NULL),
(28, 'update_profile', NULL, NULL),
(29, 'change_password', NULL, NULL),
(30, 'current_password', NULL, NULL),
(31, 'new_password', NULL, NULL),
(32, 'confirm_new_password', NULL, NULL),
(33, 'delete', NULL, NULL),
(34, 'cancel', NULL, NULL),
(35, 'are_you_sure_to_update_this_information', NULL, NULL),
(36, 'yes', NULL, NULL),
(37, 'no', NULL, NULL),
(38, 'system settings', NULL, NULL),
(39, 'system_name', NULL, NULL),
(40, 'system_title', NULL, NULL),
(41, 'slogan', NULL, NULL),
(42, 'system_email', NULL, NULL),
(43, 'address', NULL, NULL),
(44, 'phone', NULL, NULL),
(45, 'youtube_api_key', NULL, NULL),
(46, 'get_youtube_api_key', NULL, NULL),
(47, 'vimeo_api_key', NULL, NULL),
(48, 'purchase_code', NULL, NULL),
(49, 'language', NULL, NULL),
(50, 'text-align', NULL, NULL),
(51, 'update_system_settings', NULL, NULL),
(52, 'update_product', NULL, NULL),
(53, 'file', NULL, NULL),
(54, 'install_update', NULL, NULL),
(55, 'system_logo', NULL, NULL),
(56, 'update_logo', NULL, NULL),
(57, 'get_vimeo_api_key', NULL, NULL),
(58, 'add_category', NULL, NULL),
(59, 'category_title', NULL, NULL),
(60, 'sub_categories', NULL, NULL),
(61, 'actions', NULL, NULL),
(62, 'action', NULL, NULL),
(63, 'manage_sub_categories', NULL, NULL),
(64, 'edit', NULL, NULL),
(65, 'add_course', NULL, NULL),
(66, 'select_category', NULL, NULL),
(67, 'title', NULL, NULL),
(68, 'category', NULL, NULL),
(69, '#_section', NULL, NULL),
(70, '#_lesson', NULL, NULL),
(71, '#_enrolled_user', NULL, NULL),
(72, 'view_course_details', NULL, NULL),
(73, 'manage_section', NULL, NULL),
(74, 'manage_lesson', NULL, NULL),
(75, 'student', NULL, NULL),
(76, 'add_student', NULL, NULL),
(77, 'name', NULL, NULL),
(78, 'date_added', NULL, NULL),
(79, 'enrolled_courses', NULL, NULL),
(80, 'view_profile', NULL, NULL),
(81, 'admin_dashboard', NULL, NULL),
(82, 'total_courses', NULL, NULL),
(83, 'number_of_courses', NULL, NULL),
(84, 'total_lessons', NULL, NULL),
(85, 'number_of_lessons', NULL, NULL),
(86, 'total_enrollment', NULL, NULL),
(87, 'number_of_enrollment', NULL, NULL),
(88, 'total_student', NULL, NULL),
(89, 'number_of_student', NULL, NULL),
(90, 'manage_sections', NULL, NULL),
(91, 'manage sections', NULL, NULL),
(92, 'course', NULL, NULL),
(93, 'add_section', NULL, NULL),
(94, 'lessons', NULL, NULL),
(95, 'serialize_sections', NULL, NULL),
(96, 'add_lesson', NULL, NULL),
(97, 'edit_section', NULL, NULL),
(98, 'delete_section', NULL, NULL),
(99, 'course_details', NULL, NULL),
(100, 'course details', NULL, NULL),
(101, 'details', NULL, NULL),
(102, 'instructor', NULL, NULL),
(103, 'sub_category', NULL, NULL),
(104, 'enrolled_user', NULL, NULL),
(105, 'last_modified', NULL, NULL),
(106, 'manage language', NULL, NULL),
(107, 'language_list', NULL, NULL),
(108, 'add_phrase', NULL, NULL),
(109, 'add_language', NULL, NULL),
(110, 'option', NULL, NULL),
(111, 'edit_phrase', NULL, NULL),
(112, 'delete_language', NULL, NULL),
(113, 'phrase', NULL, NULL),
(114, 'value_required', NULL, NULL),
(115, 'frontend settings', NULL, NULL),
(116, 'banner_title', NULL, NULL),
(117, 'banner_sub_title', NULL, NULL),
(118, 'about_us', NULL, NULL),
(119, 'blog', NULL, NULL),
(120, 'update_frontend_settings', NULL, NULL),
(121, 'update_banner', NULL, NULL),
(122, 'banner_image', NULL, NULL),
(123, 'update_banner_image', NULL, NULL),
(124, 'payment settings', NULL, NULL),
(125, 'paypal_settings', NULL, NULL),
(126, 'client_id', NULL, NULL),
(127, 'sandbox', NULL, NULL),
(128, 'production', NULL, NULL),
(129, 'active', NULL, NULL),
(130, 'mode', NULL, NULL),
(131, 'stripe_settings', NULL, NULL),
(132, 'testmode', NULL, NULL),
(133, 'on', NULL, NULL),
(134, 'off', NULL, NULL),
(135, 'test_secret_key', NULL, NULL),
(136, 'test_public_key', NULL, NULL),
(137, 'live_secret_key', NULL, NULL),
(138, 'live_public_key', NULL, NULL),
(139, 'save_changes', NULL, NULL),
(141, 'update_phrase', NULL, NULL),
(142, 'check', NULL, NULL),
(143, 'settings_updated', NULL, NULL),
(144, 'checking', NULL, NULL),
(145, 'phrase_added', NULL, NULL),
(146, 'language_added', NULL, NULL),
(147, 'language_deleted', NULL, NULL),
(148, 'add course', NULL, NULL),
(149, 'add_courses', NULL, NULL),
(150, 'add_course_form', NULL, NULL),
(151, 'basic_info', NULL, NULL),
(152, 'short_description', NULL, NULL),
(153, 'description', NULL, NULL),
(154, 'level', NULL, NULL),
(155, 'beginner', NULL, NULL),
(156, 'advanced', NULL, NULL),
(157, 'intermediate', NULL, NULL),
(158, 'language_made_in', NULL, NULL),
(159, 'is_top_course', NULL, NULL),
(160, 'outcomes', NULL, NULL),
(161, 'category_and_sub_category', NULL, NULL),
(162, 'select_a_category', NULL, NULL),
(163, 'select_a_category_first', NULL, NULL),
(164, 'requirements', NULL, NULL),
(165, 'price_and_discount', NULL, NULL),
(166, 'price', NULL, NULL),
(167, 'has_discount', NULL, NULL),
(168, 'discounted_price', NULL, NULL),
(169, 'course_thumbnail', NULL, NULL),
(170, 'note', NULL, NULL),
(171, 'thumbnail_size_should_be_600_x_600', NULL, NULL),
(172, 'course_overview_url', NULL, NULL),
(173, '0%', NULL, NULL),
(174, 'manage profile', NULL, NULL),
(175, 'edit_course', NULL, NULL),
(176, 'edit course', NULL, NULL),
(177, 'edit_courses', NULL, NULL),
(178, 'edit_course_form', NULL, NULL),
(179, 'update_course', NULL, NULL),
(180, 'course_updated', NULL, NULL),
(181, 'number_of_sections', NULL, NULL),
(182, 'number_of_enrolled_users', NULL, NULL),
(183, 'add section', NULL, NULL),
(184, 'section', NULL, NULL),
(185, 'add_section_form', NULL, NULL),
(186, 'update', NULL, NULL),
(187, 'serialize_section', NULL, NULL),
(188, 'serialize section', NULL, NULL),
(189, 'submit', NULL, NULL),
(190, 'sections_have_been_serialized', NULL, NULL),
(191, 'select_course', NULL, NULL),
(192, 'search', NULL, NULL),
(193, 'thumbnail', NULL, NULL),
(194, 'duration', NULL, NULL),
(195, 'provider', NULL, NULL),
(196, 'add lesson', NULL, NULL),
(197, 'add_lesson_form', NULL, NULL),
(198, 'video_type', NULL, NULL),
(199, 'select_a_course', NULL, NULL),
(200, 'select_a_course_first', NULL, NULL),
(201, 'video_url', NULL, NULL),
(202, 'invalid_url', NULL, NULL),
(203, 'your_video_source_has_to_be_either_youtube_or_vimeo', NULL, NULL),
(204, 'for', NULL, NULL),
(205, 'of', NULL, NULL),
(206, 'edit_lesson', NULL, NULL),
(207, 'edit lesson', NULL, NULL),
(208, 'edit_lesson_form', NULL, NULL),
(209, 'login', NULL, NULL),
(210, 'password', NULL, NULL),
(211, 'forgot_password', NULL, NULL),
(212, 'back_to_website', NULL, NULL),
(213, 'send_mail', NULL, NULL),
(214, 'back_to_login', NULL, NULL),
(215, 'student_add', NULL, NULL),
(216, 'student add', NULL, NULL),
(217, 'add_students', NULL, NULL),
(218, 'student_add_form', NULL, NULL),
(219, 'login_credentials', NULL, NULL),
(220, 'social_information', NULL, NULL),
(221, 'facebook', NULL, NULL),
(222, 'twitter', NULL, NULL),
(223, 'linkedin', NULL, NULL),
(224, 'user_image', NULL, NULL),
(225, 'add_user', NULL, NULL),
(226, 'user_update_successfully', NULL, NULL),
(227, 'user_added_successfully', NULL, NULL),
(228, 'student_edit', NULL, NULL),
(229, 'student edit', NULL, NULL),
(230, 'edit_students', NULL, NULL),
(231, 'student_edit_form', NULL, NULL),
(232, 'update_user', NULL, NULL),
(233, 'enroll history', NULL, NULL),
(234, 'filter', NULL, NULL),
(235, 'user_name', NULL, NULL),
(236, 'enrolled_course', NULL, NULL),
(237, 'enrollment_date', NULL, NULL),
(238, 'biography2', NULL, NULL),
(239, 'home', NULL, NULL),
(240, 'search_for_courses', NULL, NULL),
(241, 'total', NULL, NULL),
(242, 'go_to_cart', NULL, NULL),
(243, 'your_cart_is_empty', NULL, NULL),
(244, 'log_in', NULL, NULL),
(245, 'sign_up', NULL, NULL),
(246, 'what_do_you_want_to_learn', NULL, NULL),
(247, 'online_courses', NULL, NULL),
(248, 'explore_a_variety_of_fresh_topics', NULL, NULL),
(249, 'expert_instruction', NULL, NULL),
(250, 'find_the_right_course_for_you', NULL, NULL),
(251, 'lifetime_access', NULL, NULL),
(252, 'learn_on_your_schedule', NULL, NULL),
(253, 'top_courses', NULL, NULL),
(254, 'last_updater', NULL, NULL),
(255, 'hours', NULL, NULL),
(256, 'add_to_cart', NULL, NULL),
(257, 'top', NULL, NULL),
(258, 'latest_courses', NULL, NULL),
(259, 'added_to_cart', NULL, NULL),
(260, 'admin', NULL, NULL),
(261, 'log_in_to_your_udemy_account', NULL, NULL),
(262, 'by_signing_up_you_agree_to_our', NULL, NULL),
(263, 'terms_of_use', NULL, NULL),
(264, 'and', NULL, NULL),
(265, 'privacy_policy', NULL, NULL),
(266, 'do_not_have_an_account', NULL, NULL),
(267, 'sign_up_and_start_learning', NULL, NULL),
(268, 'check_here_for_exciting_deals_and_personalized_course_recommendations', NULL, NULL),
(269, 'already_have_an_account', NULL, NULL),
(270, 'checkout', NULL, NULL),
(271, 'paypal', NULL, NULL),
(272, 'stripe', NULL, NULL),
(273, 'step', NULL, NULL),
(274, 'how_would_you_rate_this_course_overall', NULL, NULL),
(275, 'write_a_public_review', NULL, NULL),
(276, 'describe_your_experience_what_you_got_out_of_the_course_and_other_helpful_highlights', NULL, NULL),
(277, 'what_did_the_instructor_do_well_and_what_could_use_some_improvement', NULL, NULL),
(278, 'next', NULL, NULL),
(279, 'previous', NULL, NULL),
(280, 'publish', NULL, NULL),
(281, 'search_results', NULL, NULL),
(282, 'ratings', NULL, NULL),
(283, 'search_results_for', NULL, NULL),
(284, 'category_page', NULL, NULL),
(285, 'all', NULL, NULL),
(286, 'category_list', NULL, NULL),
(287, 'by', NULL, NULL),
(288, 'go_to_wishlist', NULL, NULL),
(289, 'hi', NULL, NULL),
(290, 'my_courses', NULL, NULL),
(291, 'my_wishlist', NULL, NULL),
(292, 'my_messages', NULL, NULL),
(293, 'purchase_history', NULL, NULL),
(294, 'user_profile', NULL, NULL),
(295, 'already_purchased', NULL, NULL),
(296, 'all_courses', NULL, NULL),
(297, 'wishlists', NULL, NULL),
(298, 'search_my_courses', NULL, NULL),
(299, 'students_enrolled', NULL, NULL),
(300, 'created_by', NULL, NULL),
(301, 'last_updated', NULL, NULL),
(302, 'what_will_i_learn', NULL, NULL),
(303, 'view_more', NULL, NULL),
(304, 'other_related_courses', NULL, NULL),
(305, 'updated', NULL, NULL),
(306, 'curriculum_for_this_course', NULL, NULL),
(307, 'about_the_instructor', NULL, NULL),
(308, 'reviews', NULL, NULL),
(309, 'student_feedback', NULL, NULL),
(310, 'average_rating', NULL, NULL),
(311, 'preview_this_course', NULL, NULL),
(312, 'includes', NULL, NULL),
(313, 'on_demand_videos', NULL, NULL),
(314, 'full_lifetime_access', NULL, NULL),
(315, 'access_on_mobile_and_tv', NULL, NULL),
(316, 'course_preview', NULL, NULL),
(317, 'instructor_page', NULL, NULL),
(318, 'buy_now', NULL, NULL),
(319, 'shopping_cart', NULL, NULL),
(320, 'courses_in_cart', NULL, NULL),
(321, 'student_name', NULL, NULL),
(322, 'amount_to_pay', NULL, NULL),
(323, 'payment_successfully_done', NULL, NULL),
(324, 'filter_by', NULL, NULL),
(325, 'instructors', NULL, NULL),
(326, 'reset', NULL, NULL),
(327, 'your', NULL, NULL),
(328, 'rating', NULL, NULL),
(329, 'course_detail', NULL, NULL),
(330, 'start_lesson', NULL, NULL),
(331, 'show_full_biography', NULL, NULL),
(332, 'terms_and_condition', NULL, NULL),
(333, 'about', NULL, NULL),
(334, 'terms_&_condition', NULL, NULL),
(335, 'sub categories', NULL, NULL),
(336, 'add_sub_category', NULL, NULL),
(337, 'sub_category_title', NULL, NULL),
(338, 'add sub category', NULL, NULL),
(339, 'add_sub_category_form', NULL, NULL),
(340, 'sub_category_code', NULL, NULL),
(341, 'data_deleted', NULL, NULL),
(342, 'edit_category', NULL, NULL),
(343, 'edit category', NULL, NULL),
(344, 'edit_category_form', NULL, NULL),
(345, 'font', NULL, NULL),
(346, 'awesome class', NULL, NULL),
(347, 'update_category', NULL, NULL),
(348, 'data_updated_successfully', NULL, NULL),
(349, 'edit_sub_category', NULL, NULL),
(350, 'edit sub category', NULL, NULL),
(351, 'sub_category_edit', NULL, NULL),
(352, 'update_sub_category', NULL, NULL),
(353, 'course_added', NULL, NULL),
(354, 'user_deleted_successfully', NULL, NULL),
(355, 'private_messaging', NULL, NULL),
(356, 'private messaging', NULL, NULL),
(357, 'messages', NULL, NULL),
(358, 'select_message_to_read', NULL, NULL),
(359, 'new_message', NULL, NULL),
(360, 'email_duplication', NULL, NULL),
(361, 'your_registration_has_been_successfully_done', NULL, NULL),
(362, 'profile', NULL, NULL),
(363, 'account', NULL, NULL),
(364, 'add_information_about_yourself_to_share_on_your_profile', NULL, NULL),
(365, 'basics', NULL, NULL),
(366, 'add_your_twitter_link', NULL, NULL),
(367, 'add_your_facebook_link', NULL, NULL),
(368, 'add_your_linkedin_link', NULL, NULL),
(369, 'credentials', NULL, NULL),
(370, 'edit_your_account_settings', NULL, NULL),
(371, 'enter_current_password', NULL, NULL),
(372, 'enter_new_password', NULL, NULL),
(373, 're-type_your_password', NULL, NULL),
(374, 'save', NULL, NULL),
(375, 'update_user_photo', NULL, NULL),
(376, 'update_your_photo', NULL, NULL),
(377, 'upload_image', NULL, NULL),
(378, 'updated_successfully', NULL, NULL),
(379, 'invalid_login_credentials', NULL, NULL),
(380, 'blank_page', NULL, NULL),
(381, 'no_section_found', NULL, NULL),
(382, 'select_a_message_thread_to_read_it_here', NULL, NULL),
(383, 'send', NULL, NULL),
(384, 'type_your_message', NULL, NULL),
(385, 'date', NULL, NULL),
(386, 'total_price', NULL, NULL),
(387, 'payment_type', NULL, NULL),
(388, 'edit section', NULL, NULL),
(389, 'edit_section_form', NULL, NULL),
(390, 'reply_message', NULL, NULL),
(391, 'reply', NULL, NULL),
(392, 'log_in_to_your_account', NULL, NULL),
(393, 'no_result_found', NULL, NULL),
(394, 'enrollment', NULL, NULL),
(395, 'enroll_a_student', NULL, NULL),
(396, 'report', NULL, NULL),
(397, 'admin_revenue', NULL, NULL),
(398, 'instructor_revenue', NULL, NULL),
(399, 'instructor_settings', NULL, NULL),
(400, 'view_frontend', NULL, NULL),
(401, 'number_of_active_courses', NULL, NULL),
(402, 'number_of_pending_courses', NULL, NULL),
(403, 'all_instructor', NULL, NULL),
(404, 'active_courses', NULL, NULL),
(405, 'pending_courses', NULL, NULL),
(406, 'no_data_found', NULL, NULL),
(407, 'view_course_on_frontend', NULL, NULL),
(408, 'mark_as_pending', NULL, NULL),
(409, 'add category', NULL, NULL),
(410, 'add_categories', NULL, NULL),
(411, 'category_add_form', NULL, NULL),
(412, 'icon_picker', NULL, NULL),
(413, 'enroll a student', NULL, NULL),
(414, 'enrollment_form', NULL, NULL),
(415, 'admin revenue', NULL, NULL),
(416, 'total_amount', NULL, NULL),
(417, 'instructor revenue', NULL, NULL),
(418, 'status', NULL, NULL),
(419, 'instructor settings', NULL, NULL),
(420, 'allow_public_instructor', NULL, NULL),
(421, 'instructor_revenue_percentage', NULL, NULL),
(422, 'admin_revenue_percentage', NULL, NULL),
(423, 'update_instructor_settings', NULL, NULL),
(424, 'payment_info', NULL, NULL),
(425, 'required_for_instructors', NULL, NULL),
(426, 'paypal_client_id', NULL, NULL),
(427, 'stripe_public_key', NULL, NULL),
(428, 'stripe_secret_key', NULL, NULL),
(429, 'mark_as_active', NULL, NULL),
(430, 'mail_subject', NULL, NULL),
(431, 'mail_body', NULL, NULL),
(432, 'paid', NULL, NULL),
(433, 'pending', NULL, NULL),
(434, 'this_instructor_has_not_provided_valid_paypal_client_id', NULL, NULL),
(435, 'pay_with_paypal', NULL, NULL),
(436, 'this_instructor_has_not_provided_valid_public_key_or_secret_key', NULL, NULL),
(437, 'pay_with_stripe', NULL, NULL),
(438, 'create_course', NULL, NULL),
(439, 'payment_report', NULL, NULL),
(440, 'instructor_dashboard', NULL, NULL),
(441, 'draft', NULL, NULL),
(442, 'view_lessons', NULL, NULL),
(443, 'course_title', NULL, NULL),
(444, 'update_your_payment_settings', NULL, NULL),
(445, 'edit_course_detail', NULL, NULL),
(446, 'edit_basic_informations', NULL, NULL),
(447, 'publish_this_course', NULL, NULL),
(448, 'save_to_draft', NULL, NULL),
(449, 'update_section', NULL, NULL),
(450, 'analyzing_given_url', NULL, NULL),
(451, 'select_a_section', NULL, NULL),
(452, 'update_lesson', NULL, NULL),
(453, 'website_name', NULL, NULL),
(454, 'website_title', NULL, NULL),
(455, 'website_keywords', NULL, NULL),
(456, 'website_description', NULL, NULL),
(457, 'author', NULL, NULL),
(458, 'footer_text', NULL, NULL),
(459, 'footer_link', NULL, NULL),
(460, 'update_backend_logo', NULL, NULL),
(461, 'update_favicon', NULL, NULL),
(462, 'favicon', NULL, NULL),
(463, 'active courses', NULL, NULL),
(464, 'product_updated_successfully', NULL, NULL),
(465, 'course_overview_provider', NULL, NULL),
(466, 'youtube', NULL, NULL),
(467, 'vimeo', NULL, NULL),
(468, 'html5', NULL, NULL),
(469, 'meta_keywords', NULL, NULL),
(470, 'meta_description', NULL, NULL),
(471, 'lesson_type', NULL, NULL),
(472, 'video', NULL, NULL),
(473, 'select_type_of_lesson', NULL, NULL),
(474, 'text_file', NULL, NULL),
(475, 'pdf_file', NULL, NULL),
(476, 'document_file', NULL, NULL),
(477, 'image_file', NULL, NULL),
(478, 'lesson_provider', NULL, NULL),
(479, 'select_lesson_provider', NULL, NULL),
(480, 'analyzing_the_url', NULL, NULL),
(481, 'attachment', NULL, NULL),
(482, 'summary', NULL, NULL),
(483, 'download', NULL, NULL),
(484, 'system_settings_updated', NULL, NULL),
(485, 'course_updated_successfully', NULL, NULL),
(486, 'please_wait_untill_admin_approves_it', NULL, NULL),
(487, 'pending courses', NULL, NULL),
(488, 'course_status_updated', NULL, NULL),
(489, 'smtp_settings', NULL, NULL),
(490, 'free_course', NULL, NULL),
(491, 'free', NULL, NULL),
(492, 'get_enrolled', NULL, NULL),
(493, 'course_added_successfully', NULL, NULL),
(494, 'update_frontend_logo', NULL, NULL),
(495, 'system_currency_settings', NULL, NULL),
(496, 'select_system_currency', NULL, NULL),
(497, 'currency_position', NULL, NULL),
(498, 'left', NULL, NULL),
(499, 'right', NULL, NULL),
(500, 'left_with_a_space', NULL, NULL),
(501, 'right_with_a_space', NULL, NULL),
(502, 'paypal_currency', NULL, NULL),
(503, 'select_paypal_currency', NULL, NULL),
(504, 'stripe_currency', NULL, NULL),
(505, 'select_stripe_currency', NULL, NULL),
(506, 'heads_up', NULL, NULL),
(507, 'please_make_sure_that', NULL, NULL),
(508, 'system_currency', NULL, NULL),
(509, 'are_same', NULL, NULL),
(510, 'smtp settings', NULL, NULL),
(511, 'protocol', NULL, NULL),
(512, 'smtp_host', NULL, NULL),
(513, 'smtp_port', NULL, NULL),
(514, 'smtp_user', NULL, NULL),
(515, 'smtp_pass', NULL, NULL),
(516, 'update_smtp_settings', NULL, NULL),
(517, 'phrase_updated', NULL, NULL),
(518, 'registered_user', NULL, NULL),
(519, 'provide_your_valid_login_credentials', NULL, NULL),
(520, 'registration_form', NULL, NULL),
(521, 'provide_your_email_address_to_get_password', NULL, NULL),
(522, 'reset_password', NULL, NULL),
(523, 'want_to_go_back', NULL, NULL),
(524, 'message_sent!', NULL, NULL),
(525, 'selected_icon', NULL, NULL),
(526, 'pick_another_icon_picker', NULL, NULL),
(527, 'show_more', NULL, NULL),
(528, 'show_less', NULL, NULL),
(529, 'all_category', NULL, NULL),
(530, 'price_range', NULL, NULL),
(531, 'price_range_withing', NULL, NULL),
(532, 'all_categories', NULL, NULL),
(533, 'all_sub_category', NULL, NULL),
(534, 'number_of_results', NULL, NULL),
(535, 'showing_on_this_page', NULL, NULL),
(536, 'welcome', NULL, NULL),
(537, 'my_account', NULL, NULL),
(538, 'logout', NULL, NULL),
(539, 'visit_website', NULL, NULL),
(540, 'navigation', NULL, NULL),
(541, 'add_new_category', NULL, NULL),
(542, 'enrolment', NULL, NULL),
(543, 'enrol_history', NULL, NULL),
(544, 'enrol_a_student', NULL, NULL),
(545, 'language_settings', NULL, NULL),
(546, 'congratulations', NULL, NULL),
(547, 'oh_snap', NULL, NULL),
(548, 'close', NULL, NULL),
(549, 'parent', NULL, NULL),
(550, 'none', NULL, NULL),
(551, 'category_thumbnail', NULL, NULL),
(552, 'the_image_size_should_be', NULL, NULL),
(553, 'choose_thumbnail', NULL, NULL),
(554, 'data_added_successfully', NULL, NULL),
(555, '', NULL, NULL),
(556, 'update_category_form', NULL, NULL),
(557, 'student_list', NULL, NULL),
(558, 'choose_user_image', NULL, NULL),
(559, 'finish', NULL, NULL),
(560, 'thank_you', NULL, NULL),
(561, 'you_are_almost_there', NULL, NULL),
(562, 'you_are_just_one_click_away', NULL, NULL),
(563, 'country', NULL, NULL),
(564, 'website_settings', NULL, NULL),
(565, 'write_down_facebook_url', NULL, NULL),
(566, 'write_down_twitter_url', NULL, NULL),
(567, 'write_down_linkedin_url', NULL, NULL),
(568, 'google_link', NULL, NULL),
(569, 'write_down_google_url', NULL, NULL),
(570, 'instagram_link', NULL, NULL),
(571, 'write_down_instagram_url', NULL, NULL),
(572, 'pinterest_link', NULL, NULL),
(573, 'write_down_pinterest_url', NULL, NULL),
(574, 'update_settings', NULL, NULL),
(575, 'upload_banner_image', NULL, NULL),
(576, 'update_light_logo', NULL, NULL),
(577, 'upload_light_logo', NULL, NULL),
(578, 'update_dark_logo', NULL, NULL),
(579, 'upload_dark_logo', NULL, NULL),
(580, 'update_small_logo', NULL, NULL),
(581, 'upload_small_logo', NULL, NULL),
(582, 'upload_favicon', NULL, NULL),
(583, 'logo_updated', NULL, NULL),
(584, 'favicon_updated', NULL, NULL),
(585, 'banner_image_update', NULL, NULL),
(586, 'frontend_settings_updated', NULL, NULL),
(587, 'setup_payment_informations', NULL, NULL),
(588, 'update_system_currency', NULL, NULL),
(589, 'setup_paypal_settings', NULL, NULL),
(590, 'update_paypal_keys', NULL, NULL),
(591, 'setup_stripe_settings', NULL, NULL),
(592, 'test_mode', NULL, NULL),
(593, 'update_stripe_keys', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lesson`
--

DROP TABLE IF EXISTS `lesson`;
CREATE TABLE `lesson` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `duration` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `video_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL,
  `lesson_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachment_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `summary` longtext COLLATE utf8_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `lesson`
--

INSERT INTO `lesson` (`id`, `title`, `duration`, `course_id`, `section_id`, `video_type`, `video_url`, `date_added`, `last_modified`, `lesson_type`, `attachment`, `attachment_type`, `summary`, `order`) VALUES
(1, '7 Skills You Must Have If You Want To Learn To Cook', '00:12:31', 1, 1, 'YouTube', 'https://www.youtube.com/watch?v=Nti79Cj527g', 1560121200, NULL, 'video', NULL, 'url', 'This video highlights the skills everyone should possess if they want to learn to cook anything at any time and be confident it will always come out great.', 0),
(2, 'Use Knife like a Pro', '00:04:06', 1, 2, 'YouTube', 'https://www.youtube.com/watch?v=Ydc_SaQ_eRQ', 1560121200, NULL, 'video', NULL, 'url', '', 0),
(3, 'Basic &amp; French Vegetable Cuts', '00:12:38', 1, 3, 'YouTube', 'https://www.youtube.com/watch?v=HyUBsva-e9g', 1560121200, NULL, 'video', NULL, 'url', '', 0),
(4, 'How To Cook Rice Perfectly', '00:06:36', 1, 4, 'YouTube', 'https://www.youtube.com/watch?v=GZlqWhzIVBc', 1560121200, NULL, 'video', NULL, 'url', '', 0),
(5, 'Vegetable Stock Broth', '00:07:25', 1, 5, 'YouTube', 'https://www.youtube.com/watch?v=wv8CO6AcAbQ', 1560121200, NULL, 'video', NULL, 'url', '', 0),
(6, 'Garnishing Techniques', '00:04:45', 1, 6, 'YouTube', 'https://www.youtube.com/watch?v=ihxsCYijU20', 1560121200, NULL, 'video', NULL, 'url', '', 0),
(7, 'What You Should Know Before You Start', '00:08:08', 2, 7, 'YouTube', 'https://www.youtube.com/watch?v=spf_AhwMT_k', 1560121200, NULL, 'video', NULL, 'url', 'This video will explain you SEO (Search Engine Optimization), How Search Engine works', 0),
(8, 'Advanced Keyword Research Tutorial', '00:10:50', 2, 8, 'YouTube', 'https://www.youtube.com/watch?v=TaOA_Zy2XUw', 1560121200, NULL, 'video', NULL, 'url', 'This Advanced Keyword Research Tutorial will help you find the right keywords for your business and the right audience to target and invest.', 0),
(9, 'How To Write Website Content With Good SEO', '00:08:02', 2, 9, 'YouTube', 'https://www.youtube.com/watch?v=9JGLtl-01nU', 1560121200, NULL, 'video', NULL, 'url', 'A good product page can serve as both SEO content and a PPC landing page', 0),
(10, 'On Page Optimization', '00:24:32', 2, 10, 'YouTube', 'https://www.youtube.com/watch?v=nmX-C9emvEs', 1560121200, NULL, 'video', NULL, 'url', '', 0),
(11, 'How to Add Structured Data to Your Website', '00:26:12', 2, 11, 'YouTube', 'https://www.youtube.com/watch?v=PfylPuHIBNo', 1560121200, NULL, 'video', NULL, 'url', '', 0),
(12, 'Enrich Search Results with JSON-LD Markup through Google Tag Manager', '00:12:41', 2, 11, 'YouTube', 'https://www.youtube.com/watch?v=i8P-B5aa5E0', 1560121200, NULL, 'video', NULL, 'url', '', 0),
(13, 'How to Submit Website to Search Engines', '00:10:04', 2, 12, 'YouTube', 'https://www.youtube.com/watch?v=3NiKhIqDRSw', 1560121200, NULL, 'video', NULL, 'url', '', 0),
(14, 'Basic Excel Fundamentals  ', '00:14:46', 3, 13, 'YouTube', 'https://www.youtube.com/watch?v=3kNEv3s8TuA', 1560121200, NULL, 'video', NULL, 'url', '', 0),
(15, 'Excel Basic Formulas and Functions', '00:16:40', 3, 14, 'YouTube', 'https://www.youtube.com/watch?v=2t3FDi98GBk', 1560121200, NULL, 'video', NULL, 'url', '', 0),
(16, 'Insert Text Boxes and Shapes', '00:03:40', 3, 15, 'YouTube', 'https://www.youtube.com/watch?v=RlCC3QyRMcM', 1560121200, NULL, 'video', NULL, 'url', '', 0),
(17, 'Excel Charts &amp; Graphs: Learn the Basics for a Quick Start', '00:14:10', 3, 16, 'YouTube', 'https://www.youtube.com/watch?v=DAU0qqh_I-A', 1560121200, NULL, 'video', NULL, 'url', '', 0),
(18, 'Cake making ingredients', '00:03:19', 4, 17, 'YouTube', 'https://www.youtube.com/watch?v=qiD8rVXfm_c', 1560121200, NULL, 'video', NULL, 'url', '', 0),
(19, 'Tools For making a Cake', '00:05:40', 4, 18, 'YouTube', 'https://www.youtube.com/watch?v=JV4xpv7ODco', 1560121200, NULL, 'video', NULL, 'url', '', 0),
(20, 'The Baking Process', '00:09:59', 4, 19, 'YouTube', 'https://www.youtube.com/watch?v=PSHZ3E7yIAo', 1560121200, NULL, 'video', NULL, 'url', '', 0),
(21, 'Lets Decorate the Cake', '00:09:42', 4, 20, 'YouTube', 'https://www.youtube.com/watch?v=6RSKUxhDHqQ', 1560121200, NULL, 'video', NULL, 'url', '', 0),
(22, 'How to Get Your Gmail Inbox Under Control', '00:09:02', 5, 21, 'YouTube', 'https://www.youtube.com/watch?v=1bektiDaIhk', 1560121200, NULL, 'video', NULL, 'url', '', 0),
(23, 'How to Combine 2 Email Accounts', '00:13:55', 5, 22, 'YouTube', 'https://www.youtube.com/watch?v=CjFc_F8pq1U', 1560121200, NULL, 'video', NULL, 'url', '', 0),
(24, 'Block all SPAM emails in Gmail', '00:03:31', 5, 23, 'YouTube', 'https://www.youtube.com/watch?v=e7luNclI_1g', 1560121200, NULL, 'video', NULL, 'url', '', 0),
(25, 'How to Manage Contacts in Gmail Account', '00:05:28', 5, 24, 'YouTube', 'https://www.youtube.com/watch?v=QA8O4j7SHnk', 1560121200, NULL, 'video', NULL, 'url', '', 0),
(26, 'Easy way to auto-refresh POP3 accounts in Gmail every 5 minutes', '00:01:51', 5, 25, 'YouTube', 'https://www.youtube.com/watch?v=sokploJ-zyg', 1560121200, NULL, 'video', NULL, 'url', '', 0),
(27, 'What is Digital Marketing? - Digital Marketing Introduction in Detail', '00:22:09', 6, 26, 'YouTube', 'https://www.youtube.com/watch?v=4EbvyOTZcJs', 1560121200, NULL, 'video', NULL, 'url', '', 0),
(28, 'Digital Marketing Fundamentals', '00:15:58', 6, 27, 'YouTube', 'https://www.youtube.com/watch?v=z8GRHM7NVwQ', 1560121200, NULL, 'video', NULL, 'url', '', 0),
(29, 'Know Your Target Market Before Selling', '00:07:45', 6, 28, 'YouTube', 'https://www.youtube.com/watch?v=XSvC4GzNh5Q', 1560121200, NULL, 'video', NULL, 'url', '', 0),
(30, 'Marketing Research Process - For Digital Marketing', '00:36:35', 6, 29, 'YouTube', 'https://www.youtube.com/watch?v=nJIfunlpIGM', 1560121200, NULL, 'video', NULL, 'url', '', 0),
(31, 'Essential Links for Further Study', NULL, 6, 30, NULL, NULL, 1560121200, NULL, 'other', 'ac86fa723c8a2a4c852c5a3c601e0f3c.txt', 'txt', 'Please go these links for furthering your knowledge', 0),
(32, '5 Big HR Terms', '00:01:00', 7, 32, 'YouTube', 'https://www.youtube.com/watch?v=9OfHu7FkVuk', 1560121200, 1560985200, 'video', '', 'url', '', 0),
(33, 'Introduction to Human Resource Management', '00:10:26', 7, 31, 'YouTube', 'https://www.youtube.com/watch?v=d8FpuHxd7MQ', 1560121200, NULL, 'video', NULL, 'url', '', 0),
(34, 'Creating value through HR Strategy', NULL, 7, 33, NULL, NULL, 1560121200, NULL, 'other', '6ced1492b627e8bf43cb63af26cd20c2.pdf', 'pdf', 'This paper explores the strategic role of the human resources of an organization. The HR function focuses on building the human capital that drives the organizational activities to success.', 0),
(35, 'Basics ', '06:14:07', 19, 34, 'YouTube', 'https://www.youtube.com/watch?v=_uQrJ0TkZlc', 1563840000, NULL, 'video', NULL, 'url', 'Welcome to Course Wings', 0),
(37, 'The Basics for Beginners', '00:36:57', 8, 36, 'YouTube', 'https://www.youtube.com/watch?v=pFyOznL9UvA', 1566342000, NULL, 'video', NULL, 'url', 'Adobe Photoshop Tutorial For Beginners, teaching the Basics Of Adobe Photoshop. Photoshop Tutorial for Beginners, going over many important aspects when first starting graphic design!', 0),
(43, 'Fundamental 2', '00:00:05', 3, 13, 'upload', 'http://localhost/cwfinal2/uploads/lesson_videos/afe19743b509c81a1437156d20894587.mp4', 1568070000, NULL, 'video', NULL, 'url', '', 0),
(44, 'Digital Marketing Intro', '00:00:05', 21, 41, 'upload', 'http://localhost/cwfinal2/uploads/lesson_videos/afe19743b509c81a1437156d20894587.mp4', 1568070000, NULL, 'video', NULL, 'url', 'Sample Uploaded video', 0),
(45, 'Test Lesson 2 Upload', '00:00:05', 4, 17, 'upload', 'http://localhost/cwfinal2/uploads/lesson_videos/afe19743b509c81a1437156d20894587.mp4', 1568156400, NULL, 'video', NULL, 'url', 'This is summary for Test Lesson 2 Upload', 0);

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `message_id` int(11) NOT NULL,
  `message_thread_code` longtext,
  `message` longtext,
  `sender` longtext,
  `timestamp` longtext,
  `read_status` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `message_thread`
--

DROP TABLE IF EXISTS `message_thread`;
CREATE TABLE `message_thread` (
  `message_thread_id` int(11) NOT NULL,
  `message_thread_code` longtext COLLATE utf8_unicode_ci,
  `sender` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `receiver` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `last_message_timestamp` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `payment_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL,
  `admin_revenue` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instructor_revenue` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instructor_payment_status` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `user_id`, `payment_type`, `course_id`, `amount`, `date_added`, `last_modified`, `admin_revenue`, `instructor_revenue`, `instructor_payment_status`) VALUES
(1, 2, 'ccAvenue', 1, 15, 1566946800, NULL, '6', '9', 0),
(2, 6, 'ccAvenue', 3, 25, 1568329200, NULL, '10', '15', 0);

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
CREATE TABLE `question` (
  `id` int(11) UNSIGNED NOT NULL,
  `quiz_id` int(11) DEFAULT NULL,
  `title` longtext COLLATE utf8_unicode_ci,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `number_of_options` int(11) DEFAULT NULL,
  `options` longtext COLLATE utf8_unicode_ci,
  `correct_answers` longtext COLLATE utf8_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

DROP TABLE IF EXISTS `rating`;
CREATE TABLE `rating` (
  `id` int(11) UNSIGNED NOT NULL,
  `rating` double DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ratable_id` int(11) DEFAULT NULL,
  `ratable_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL,
  `review` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `rating`
--

INSERT INTO `rating` (`id`, `rating`, `user_id`, `ratable_id`, `ratable_type`, `date_added`, `last_modified`, `review`) VALUES
(1, 3, 2, 8, 'course', 1566342000, NULL, 'just one course?'),
(2, 4, 2, 1, 'course', 1568070000, NULL, 'Very nice course covers all basic cooking skills. \nHighly recommended');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`, `date_added`, `last_modified`) VALUES
(1, 'Admin', 1234567890, 1234567890),
(2, 'User', 1234567890, 1234567890);

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

DROP TABLE IF EXISTS `section`;
CREATE TABLE `section` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `section`
--

INSERT INTO `section` (`id`, `title`, `course_id`, `order`) VALUES
(1, 'Basic Skills ', 1, 0),
(2, 'Knife Skills', 1, 0),
(3, 'Vegetable Cuts', 1, 0),
(4, 'Boiling Rice', 1, 0),
(5, 'Making Vegetable Stock', 1, 0),
(6, 'Garnishing', 1, 0),
(7, ' Getting Started ', 2, 0),
(8, 'Picking Powerful Keywords', 2, 0),
(9, 'Understanding the Role of Content', 2, 0),
(10, 'Basic Page Optimization – A Few More Good Things', 2, 0),
(11, 'Using Structured Data Markup', 2, 0),
(12, 'Submitting to Search Engines', 2, 0),
(13, 'Microsoft Excel Fundamentals', 3, 0),
(14, 'Entering and Editing Text and Formulas', 3, 0),
(15, 'Inserting Images and Shapes into an Excel Worksheet', 3, 0),
(16, 'Creating Basic Charts in Excel', 3, 0),
(17, 'Gather your ingredients', 4, 1),
(18, ' Tools For making a Cake', 4, 2),
(19, 'Baking Process', 4, 3),
(20, 'Cake Decoration', 4, 4),
(21, 'An Introduction to Increasing Your Productivity in Gmail', 5, 0),
(22, 'The Most Effective Way to Process Your Email', 5, 0),
(23, 'Decreasing the Amount of Mail You Receive', 5, 0),
(24, 'Contact Management', 5, 0),
(25, 'Increase the Speed At Which You Process Your Email', 5, 0),
(26, ' Introduction to Digital Marketing ', 6, 0),
(27, 'Digital Marketing Fundamentals', 6, 0),
(28, 'Target Audience', 6, 0),
(29, 'Market Research', 6, 0),
(30, 'Links you need', 6, 0),
(31, 'Introduction to HR Strategy Development', 7, 2),
(32, 'Key Terms and Definitions2', 7, 1),
(33, 'Driving Strategic Value', 7, 3),
(34, 'section 1 ', 19, 0),
(36, 'Photoshop Basics', 8, 0),
(40, 'section1', 13, 0),
(41, 'Digital Marketing Intro', 21, 0);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(11) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `value`) VALUES
(1, 'language', 'english'),
(2, 'system_name', 'Course Wings'),
(3, 'system_title', 'Course Wings'),
(4, 'system_email', 'coursewings@gmail.com'),
(5, 'address', 'Course Wings Portal\r\nJVC, Autumn 2 &quot;B&quot;, Dubai, United Arab Emirates\r\nP.O Box: 328772'),
(6, 'phone', '+123-45-6789'),
(7, 'purchase_code', 'your-purchase-code'),
(8, 'paypal', '[{\"active\":\"1\",\"mode\":\"sandbox\",\"sandbox_client_id\":\"AZDxjDScFpQtjWTOUtWKbyN_bDt4OgqaF4eYXlewfBP4-8aqX3PiV8e1GWU6liB2CUXlkA59kJXE7M6R\",\"production_client_id\":\"1234\"}]'),
(9, 'stripe_keys', '[{\"active\":\"1\",\"testmode\":\"on\",\"public_key\":\"pk_test_c6VvBEbwHFdulFZ62q1IQrar\",\"secret_key\":\"sk_test_9IMkiM6Ykxr1LCe2dJ3PgaxS\",\"public_live_key\":\"pk_live_xxxxxxxxxxxxxxxxxxxxxxxx\",\"secret_live_key\":\"sk_live_xxxxxxxxxxxxxxxxxxxxxxxx\"}]'),
(10, 'youtube_api_key', 'AIzaSyAlxF2mMzknfjPexOGstZbElXKueM2X0Ec'),
(11, 'vimeo_api_key', 'e1c925e1ea7b2aea0a78f0d931936e43942a8e14'),
(12, 'slogan', 'Learn From Industry Experts'),
(13, 'text_align', NULL),
(14, 'allow_instructor', '1'),
(15, 'instructor_revenue', '70'),
(16, 'system_currency', 'USD'),
(17, 'paypal_currency', 'USD'),
(18, 'stripe_currency', 'USD'),
(19, 'author', 'ETC'),
(20, 'currency_position', 'left'),
(21, 'website_description', 'Course Wings is an online learning and teaching marketplace connecting Teachers &amp; Students.'),
(22, 'website_keywords', 'e-learning,teach online,sell courses,learn online,online videos,online courses,online training, online education, online classes,online learning'),
(23, 'footer_text', NULL),
(24, 'footer_link', 'http://www.etcnet.net/'),
(25, 'protocol', 'smtp'),
(26, 'smtp_host', 'ssl://smtp.googlemail.com'),
(27, 'smtp_port', '465'),
(28, 'smtp_user', ''),
(29, 'smtp_pass', ''),
(30, 'version', '2.4'),
(31, 'student_email_verification', 'enable'),
(32, 'ccavenue', '[{\"active\":\"1\",\"currency\":\"USD\",\"merchant_id\":\"43560\",\"access_code\":\"AVNB03GH07BD82BNDB\",\"working_key\":\"65BF333B856A78FF1D8637194E969B74\"}]');

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
CREATE TABLE `tag` (
  `id` int(11) UNSIGNED NOT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tagable_id` int(11) DEFAULT NULL,
  `tagable_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

DROP TABLE IF EXISTS `testimonials`;
CREATE TABLE `testimonials` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `text` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `status` tinyint(4) DEFAULT NULL,
  `last_modified_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='store user testimonials';

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `user_id`, `text`, `status`, `last_modified_at`) VALUES
(1, 2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nibh cras pulvinar mattis nunc sed. Integer quis auctor elit sed vulputate mi sit amet mauris. Arcu dictum varius duis at. Egestas maecenas pharetra convallis posuere morbi leo urna molestie.', 1, '2019-09-10 16:13:54'),
(2, 7, 'Leo urna molestie at elementum eu facilisis sed odio. Nibh ipsum consequat nisl vel pretium lectus. Ultrices vitae auctor eu augue ut. Nisl suscipit adipiscing bibendum est ultricies integer quis. Ridiculus mus mauris vitae ultricies leo integer malesuada. Integer quis auctor elit sed vulputate mi sit amet. Sem viverra aliquet eget sit amet tellus cras adipiscing.', 1, '2019-08-29 17:28:47'),
(7, 7, 'In+aliquam+sem+fringilla+ut+morbi+tincidunt+augue.+Id+aliquet+risus+feugiat+in+ante+metus+dictum+at+tempor.+Neque+laoreet+suspendisse+interdum+consectetur+libero.+Aliquam+eleifend+mi+in+nulla+posuere+sollicitudin+aliquam+ultrices+sagittis.+In+hendrerit+gravida+rutrum+quisque+non+tellus+orci+ac.+%0D%0A%0D%0APellentesque+habitant+morbi+tristique+senectus+et+netus+et+malesuada+fames.+Quam+pellentesque+nec+nam+aliquam+sem+et.+Neque+vitae+tempus+quam+pellentesque+nec+nam.+Id+venenatis+a+condimentum+vitae.+Ipsum+consequat+nisl+vel+pretium+lectus+quam.+Et+netus+et+malesuada+fames+ac+turpis+egestas.+Quam+quisque+id+diam+vel+quam+elementum+pulvinar.+Egestas+diam+in+arcu+cursus+euismod.+Ridiculus+mus+mauris+vitae+ultricies+leo+integer.+', 1, '2019-09-04 18:22:14'),
(8, 6, 'Hi%2C+This+is+a+nice+website%2C+i+hope+to+use+is+more+often+now', 1, '2019-09-11 18:13:05'),
(9, 2, 'Nice+site%2C+i+can+now+make+money+selling+courses+from+the+comfort+of+my+home%21', 1, '2019-09-11 18:13:06');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `social_links` longtext COLLATE utf8_unicode_ci,
  `biography` longtext COLLATE utf8_unicode_ci,
  `role_id` int(11) DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL,
  `watch_history` longtext COLLATE utf8_unicode_ci,
  `wishlist` longtext COLLATE utf8_unicode_ci,
  `title` longtext COLLATE utf8_unicode_ci,
  `paypal_keys` longtext COLLATE utf8_unicode_ci,
  `stripe_keys` longtext COLLATE utf8_unicode_ci,
  `verification_code` longtext COLLATE utf8_unicode_ci,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `social_links`, `biography`, `role_id`, `date_added`, `last_modified`, `watch_history`, `wishlist`, `title`, `paypal_keys`, `stripe_keys`, `verification_code`, `status`) VALUES
(1, 'admin', '1', 'admin@admin.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(2, 'Rachel', 'Maloo', 'abc@abc.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '{\"facebook\":\"https:\\/www.facebook.com\\/rachel\",\"twitter\":\"\",\"linkedin\":\"\"}', '<p>Rachel is a IDDBA National Award Winning professional cake designer with 30 years cake decorating experience as lead designer for a major St. Louis bakery.</p>\r\n<p>Rachel has a passion for designing, decorating and teaching. She is a third generation baker who has the unique ability to convey her decorating skills in a manner that is easy to learn.</p>\r\n<p>Her motto is \"Simplify\"</p>', 2, 1560145693, 1563277730, '[{\"lesson_id\":\"37\",\"progress\":\"0\"},{\"lesson_id\":\"39\",\"progress\":\"0\"},{\"lesson_id\":\"1\",\"progress\":\"1\"},{\"lesson_id\":\"45\",\"progress\":\"1\"},{\"lesson_id\":\"18\",\"progress\":\"1\"}]', '[\"1\"]', NULL, '[{\"production_client_id\":\"\"}]', '[{\"public_live_key\":null,\"secret_live_key\":null}]', NULL, 1),
(3, 'Chef', 'Prasadmurthy', 'chef@chef.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '<p>I\'m Chef Prasadmurthy, i teach everything cool from IT to Cooking !!</p>', 2, 1560151512, NULL, '[]', '[\"3\"]', NULL, '[{\"production_client_id\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(4, 'John', 'Student', 'student@student.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1560156764, 1560162850, '[]', '[]', NULL, '[{\"production_client_id\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(5, 'Ramesh', 'Garg', 'ramesh@ramesh.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '{\"facebook\":\"https:\\/\\/www.facebook.com\\/ramesh.yerewar\",\"twitter\":\"https:\\/\\/twitter.com\\/rameshlaus\",\"linkedin\":\"\"}', '<p>I\'m Ramesh Garg, i teach everything cool from IT to HR !!</p>', 2, 1560156824, 1561548969, '[]', '[]', NULL, '[{\"production_client_id\":null}]', '[{\"public_live_key\":null,\"secret_live_key\":null}]', NULL, 1),
(6, 'Charles', 'Bennet', 'charles@charles.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1560156870, 1560162957, '[]', '[\"3\"]', NULL, '[{\"production_client_id\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(7, 'Peter', 'Pan', 'peter@peter.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '<p>Hello all I\'m Peter pan. I\'m a fictional character created by Scottish novelist and playwright J. M. Barrie. I\'m a free-spirited and mischievous young boy who can fly and never grows up.</p>\r\n<p>I wear an outfit that consists of a short-sleeved green tunic and tights made of cloth, and a cap with a red feather in it. I\'ve pointed elf-like ears, brown eyes and my hair is red.</p>', 2, 1560157286, 1566903026, '[]', '[\"1\",\"5\",\"7\",\"2\",\"3\"]', NULL, '[{\"production_client_id\":null}]', '[{\"public_live_key\":null,\"secret_live_key\":null}]', NULL, 1),
(8, 'Mia', 'Grace', 'mia@mia.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1560157358, 1560162980, '[]', '[]', NULL, '[{\"production_client_id\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', NULL, 1),
(9, 'Bipin', 'Khanduri', 'bipin@bipin.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '<p><br></p>', 2, 1562568746, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\"}]', '[{\"public_live_key\":null,\"secret_live_key\":null}]', NULL, 1),
(14, 'Madaiah', 'Udiyanda', 'ukmadaiah@gmail.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '', 2, 1563511502, 1568195242, '[]', '[]', NULL, '[{\"production_client_id\":\"\"}]', '[{\"public_live_key\":null,\"secret_live_key\":null}]', '9f690a0b649ed453f0d3fd23daba7bfd', 1),
(15, 'Robin', 'Hood', 'robinhood@robinhood.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', NULL, 2, 1563686640, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', 'ac472055d6380bce8b7f3333d0b25e0b', 1),
(16, 'Aditya', 'Kumar', 'aditya@aditya.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', NULL, 2, 1564376734, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', '3c1a099a4af005e4444f568ebbc0608c', 1),
(17, 'test', 'user', 'test@test.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', NULL, 2, 1565609351, NULL, '[]', '[]', NULL, '[{\"production_client_id\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', '734887c465587e5f28445c5d7c7cce2b', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ccavenue`
--
ALTER TABLE `ccavenue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enrol`
--
ALTER TABLE `enrol`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frontend_settings`
--
ALTER TABLE `frontend_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hits`
--
ALTER TABLE `hits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`phrase_id`);

--
-- Indexes for table `lesson`
--
ALTER TABLE `lesson`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `message_thread`
--
ALTER TABLE `message_thread`
  ADD PRIMARY KEY (`message_thread_id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT for table `ccavenue`
--
ALTER TABLE `ccavenue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;
--
-- AUTO_INCREMENT for table `enrol`
--
ALTER TABLE `enrol`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `frontend_settings`
--
ALTER TABLE `frontend_settings`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `hits`
--
ALTER TABLE `hits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `phrase_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=594;
--
-- AUTO_INCREMENT for table `lesson`
--
ALTER TABLE `lesson`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `message_thread`
--
ALTER TABLE `message_thread`
  MODIFY `message_thread_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rating`
--
ALTER TABLE `rating`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `section`
--
ALTER TABLE `section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `tag`
--
ALTER TABLE `tag`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
