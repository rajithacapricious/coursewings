<div class="row">
	<div class="col-12">
		<ol class="breadcrumb yellow mt-2">
			<li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="mdi mdi-view-dashboard mr-1"></i><?php echo get_phrase('admin_dashboard'); ?></a></li>
			<li class="breadcrumb-item"><a href="<?php echo site_url('admin/categories'); ?>"><i class="mdi mdi-shape mr-1"></i><?php echo get_phrase('categories'); ?></a></li>
			<li class="breadcrumb-item active"><i class="mdi mdi-plus-box mr-1"></i><?php echo get_phrase('add_new_category'); ?></li>
		</ol>
	</div>
</div>

<div class="row justify-content-center">
    <div class="col-xl-12">
        <div class="card">
			<div class="card-header purple">
				<h4 class="header-title mt-1"><i class="mdi mdi-shape mr-1"></i><?php echo get_phrase('category_add_form'); ?></small></h4>
			</div>
            <div class="card-body">
              <div class="col-lg-12">
                <form class="required-form" action="<?php echo site_url('admin/categories/add'); ?>" method="post" enctype="multipart/form-data">
					<div class="row">
						

						<div class="form-group col-lg-6">
							<label for="name"><?php echo get_phrase('category_title'); ?><span class="required">*</span></label>
							<input type="text" class="form-control" id="name" name = "name" required>
						</div>
						<div class="form-group col-lg-6">
							<label for="parent"><?php echo get_phrase('parent'); ?>&nbsp; Category</label>
							<select class="form-control select2" data-toggle="select2" name="parent" id="parent" onchange="checkCategoryType(this.value)">
							  <option value="0"><?php echo get_phrase('none'); ?></option>
							  <?php foreach ($categories as $category): ?>
								  <?php if ($category['parent'] == 0): ?>
									  <option value="<?php echo $category['id']; ?>"><?php echo $category['name']; ?></option>
								  <?php endif; ?>
							  <?php endforeach; ?>
							</select>
						</div>
						<div class="form-group col-lg-12" id = "icon-picker-area">
							<label for="font_awesome_class"><?php echo get_phrase('icon_picker'); ?></label>
							<input type="text" id ="font_awesome_class" name="font_awesome_class" class="form-control icon-picker" autocomplete="off">
						</div>

						
						<div class="form-group col-lg-12">
							<label for="code"><?php echo get_phrase('category_code'); ?><small><i>(Automatic Code, Don't enter anything)</i></small></label>
							<input type="text" class="form-control bg-light" id="code" name = "code" value="<?php echo substr(md5(rand(0, 1000000)), 0, 10); ?>" readonly>
						</div>
                    

						<div class="form-group col-lg-12" id = "thumbnail-picker-area" hidden>
							<label> <?php echo get_phrase('category_thumbnail'); ?> <small>(<?php echo get_phrase('the_image_size_should_be'); ?>: 400 X 255)</small> </label>
							<div class="input-group">
								<div class="custom-file">
									<input type="file" class="custom-file-input" id="category_thumbnail" name="category_thumbnail" accept="image/*" onchange="changeTitleOfImageUploader(this)">
									<label class="custom-file-label" for="category_thumbnail"><?php echo get_phrase('choose_thumbnail'); ?></label>
								</div>
							</div>
						</div>
						<div class="col-lg-12">
							<button type="button" class="btn btn-block btn-primary mt-4" onclick="checkRequiredFields()"><i class="mdi mdi-content-save mr-1"></i><?php echo get_phrase("submit"); ?></button>
						</div>
					</div>
                </form>
              </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<script type="text/javascript">
    function checkCategoryType(category_type) {
        if (category_type > 0) {
            $('#thumbnail-picker-area').hide();
            $('#icon-picker-area').hide();
        }else {
            $('#thumbnail-picker-area').show();
            $('#icon-picker-area').show();
        }
    }
</script>
