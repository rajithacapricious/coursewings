<?php
$course_details = $this->crud_model->get_course_by_id($course_id)->row_array();
?>

<div class="row">
	<div class="col-12">
		<ol class="breadcrumb yellow mt-2">
			<li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="mdi mdi-view-dashboard mr-1"></i><?php echo get_phrase('admin_dashboard'); ?></a></li>
			<li class="breadcrumb-item"><a><i class="mdi mdi-table-edit mr-1"></i><?php echo get_phrase('edit_course'); ?></a></li>
			<li class="breadcrumb-item active text-dark"><i class="mdi mdi-lead-pencil mr-1"></i><?php echo get_phrase('editing').': '.$course_details['title']; ?></li>
		</ol>
	</div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
			<div class="card-header purple">
			
				<div class="row">
					<div class="col-6 col-xl-8">
						<h4 class="header-title mt-1"><i class="mdi mdi-lead-pencil mr-1"></i><?php echo get_phrase('you_are_editing').': '.$course_details['title']; ?></h4>                         
					</div>
					<div class="col-6 col-xl-4">
						<div class="text-lg-right mt-1">
							
							<a href="<?php echo site_url('admin/courses'); ?>" class="btn btn-xs bg-yellow mb-0"><i class="mdi mdi-backspace mr-1"></i><?php echo get_phrase('back_to_course_list'); ?></a>
							<a href="<?php echo site_url('admin/preview/'.$course_id); ?>" class="btn btn-xs bg-yellow mb-0" target="_blank"><?php echo get_phrase('view_on_frontend'); ?><i class="mdi mdi-airplay ml-1"></i></a>
						</div>
					</div><!-- end col-->
				</div>
			</div>
			
            <div class="card-body">
                

                <div class="row">
                    <div class="col-xl-12">
                        <form class="required-form" action="<?php echo site_url('admin/course_actions/edit/'.$course_id); ?>" method="post" enctype="multipart/form-data">
                            <div id="basicwizard">
								<ul class="nav nav-pills nav-justified form-wizard-header mx-0 mb-3">
                                    <li class="nav-item mr-1">
                                       <a href="#curriculum" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                           <i class="mdi mdi-library-plus mr-1"></i>
                                           <span class="d-none d-sm-inline"><?php echo get_phrase('add/edit_sections_&_Lessons'); ?></span>
                                       </a>
                                   </li>
                                    <li class="nav-item ml-1">
                                        <a href="#basic" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                            <i class="mdi mdi-lead-pencil mr-1"></i>
                                            <span class="d-none d-sm-inline"><?php echo get_phrase('edit_basic_course_information'); ?></span>
                                        </a>
                                    </li>
                                    
                                </ul>
							
                                

								<div class="tab-content b-0 mb-0">
									<div class="tab-pane" id="curriculum">
										<?php include 'curriculum.php'; ?>
									</div>
									
									<div class="tab-pane" id="basic">
                                    <div class="row justify-content-center">
                                        <div class="col-xl-8">
                                            <div class="form-group row mb-1">
                                                <label class="col-md-12 col-form-label" for="course_title"><?php echo get_phrase('course_title'); ?><span class="required">*</span></label>
                                                <div class="col-md-12">
                                                    <input type="text" class="form-control" id="course_title" name = "title" placeholder="<?php echo get_phrase('enter_course_title'); ?>" value="<?php echo $course_details['title']; ?>" required>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-1">
                                                <label class="col-md-12 col-form-label" for="short_description"><?php echo get_phrase('short_course_description'); ?></label>
                                                <div class="col-md-12">
                                                    <textarea name="short_description" id = "short_description" placeholder="Enter a short course description" class="form-control"><?php echo $course_details['short_description']; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-1">
                                                <label class="col-md-12 col-form-label" for="description"><?php echo get_phrase('description'); ?></label>
                                                <div class="col-md-12">
                                                    <textarea name="description" id = "description" class="form-control"><?php echo $course_details['description']; ?></textarea>
                                                </div>
                                            </div>
                                            
                                            
											
											
											<div>
												<div class="form-group row mb-1">
													<label class="col-md-12 col-form-label" for="requirements"><?php echo get_phrase('course_requirements'); ?></label>
													<div class="col-md-12">
														<div id = "requirement_area">
															<?php if (count(json_decode($course_details['requirements'])) > 0): ?>
																<?php
																$counter = 0;
																foreach (json_decode($course_details['requirements']) as $requirement):?>
																<?php if ($counter == 0):
																	$counter++; ?>
																	<div class="d-flex mt-2">
																		<div class="flex-grow-1 pl-0 pr-3">
																			<div class="form-group">
																				<input type="text" class="form-control" name="requirements[]" id="requirements" placeholder="<?php echo get_phrase('provide_requirements_click_on_plus_icon_to_add_more'); ?>" value="<?php echo $requirement; ?>">
																			</div>
																		</div>
																		<div class="">
																			<button type="button" class="btn bg-blue text-white btn-sm"  style="min-height: 36px;margin-top: 1px;" name="button" onclick="appendRequirement()"> <i class="fa fa-plus-circle"></i> </button>
																		</div>
																	</div>
																<?php else: ?>
																	<div class="d-flex mt-2">
																		<div class="flex-grow-1 pl-0 pr-3">
																			<div class="form-group">
																				<input type="text" class="form-control" name="requirements[]" id="requirements" placeholder="<?php echo get_phrase('provide_requirements_click_on_plus_icon_to_add_more'); ?>" value="<?php echo $requirement; ?>">
																			</div>
																		</div>
																		<div class="">
																			<button type="button" class="btn btn-danger btn-sm" style="min-height: 36px;margin-top: 1px;" name="button" onclick="removeRequirement(this)"> <i class="fa fa-trash-alt"></i> </button>
																		</div>
																	</div>
																<?php endif; ?>
															<?php endforeach; ?>
														<?php else: ?>
															<div class="d-flex mt-2">
																<div class="flex-grow-1 pl-0 pr-3">
																	<div class="form-group">
																		<input type="text" class="form-control" name="requirements[]" id="requirements" placeholder="<?php echo get_phrase('provide_requirements_click_on_plus_icon_to_add_more'); ?>">
																	</div>
																</div>
																<div class="">
																	<button type="button" class="btn bg-blue text-white btn-sm" style="min-height: 36px;margin-top: 1px;" name="button" onclick="appendRequirement()"> <i class="fa fa-plus-circle"></i> </button>
																</div>
															</div>
														<?php endif; ?>

															<div id = "blank_requirement_field">
																<div class="d-flex mt-2">
																	<div class="flex-grow-1 pl-0 pr-3">
																		<div class="form-group">
																			<input type="text" class="form-control" name="requirements[]" id="requirements" placeholder="What qualifications do students need to take this course? Please click the plus icon to add more.">
																		</div>
																	</div>
																	<div class="">
																		<button type="button" class="btn btn-danger btn-sm" style="min-height: 36px;margin-top: 1px;" name="button" onclick="removeRequirement(this)"> <i class="fa fa-trash-alt"></i> </button>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											
											<div>
												<div class="form-group row mb-1">
													<label class="col-md-12 col-form-label" for="outcomes"><?php echo get_phrase('course_outcomes'); ?></label>
													<div class="col-md-12">
														<div id = "outcomes_area">
															<?php if (count(json_decode($course_details['outcomes'])) > 0): ?>
																<?php
																$counter = 0;
																foreach (json_decode($course_details['outcomes']) as $outcome):?>
																<?php if ($counter == 0):
																	$counter++; ?>
																	<div class="d-flex mt-2">
																		<div class="flex-grow-1 pl-0 pr-3">
																			<div class="form-group">
																				<input type="text" class="form-control" name="outcomes[]" placeholder="What will students learn after this course? Click on the plus icon to add more." value="<?php echo $outcome; ?>">
																			</div>
																		</div>
																		<div class="">
																			<button type="button" class="btn text-white bg-blue btn-sm" style="min-height: 36px;margin-top: 1px;" name="button" onclick="appendOutcome()"> <i class="fa fa-plus-circle"></i> </button>
																		</div>
																	</div>
																<?php else: ?>
																	<div class="d-flex mt-2">
																		<div class="flex-grow-1 pl-0 pr-3">
																			<div class="form-group">
																				<input type="text" class="form-control" name="outcomes[]"  placeholder="<?php echo get_phrase('provide_outcomes'); ?>" value="<?php echo $outcome; ?>">
																			</div>
																		</div>
																		<div class="">
																			<button type="button" class="btn btn-danger btn-sm" style="min-height: 36px;margin-top: 1px;" name="button" onclick="removeOutcome(this)"> <i class="fas fa-trash-alt"></i> </button>
																		</div>
																	</div>
																<?php endif; ?>
															<?php endforeach; ?>
														<?php else: ?>
															<div class="d-flex mt-2">
																<div class="flex-grow-1 pl-0 pr-3">
																	<div class="form-group">
																		<input type="text" class="form-control" name="outcomes[]" placeholder="<?php echo get_phrase('provide_outcomes'); ?>">
																	</div>
																</div>
																<div class="">
																	<button type="button" class="btn text-white bg-blue btn-sm" style="min-height: 36px;margin-top: 1px;" name="button" onclick="appendOutcome()"> <i class="fa fa-plus-circle"></i> </button>
																</div>
															</div>
														<?php endif; ?>
															<div id = "blank_outcome_field">
																<div class="d-flex mt-2">
																	<div class="flex-grow-1 pl-0 pr-3">
																		<div class="form-group">
																			<input type="text" class="form-control" name="outcomes[]" id="outcomes" placeholder="<?php echo get_phrase('provide_outcomes'); ?>">
																		</div>
																	</div>
																	<div class="">
																		<button type="button" class="btn btn-danger btn-sm" style="min-height: 36px;margin-top: 1px;" name="button" onclick="removeOutcome(this)"> <i class="fas fa-trash-alt"></i> </button>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											
											<div class="tab-pane" id="seo">
												<div class="form-group row mb-1">
													<label class="col-md-12 col-form-label" for="website_keywords"><?php echo get_phrase('meta_keywords'); ?><small class="mr-1"><i>(for S.E.O)&nbsp;&nbsp;(Press Enter after typing keyword)</i></small></label>
													<div class="col-md-12">
														<input type="text" class="form-control bootstrap-tag-input" id = "meta_keywords" name="meta_keywords" data-role="tagsinput" style="width: 100%;" placeholder="Eg. For Marketing related course - use keywords like Marketing, Sales, Strategies, Business Plan etc." value="<?php echo $course_details['meta_keywords']; ?>"/>
													</div>
												</div>
											
												<div class="form-group row mb-1">
													<label class="col-md-12 col-form-label" for="meta_description"><?php echo get_phrase('meta_description'); ?><small class="mr-1"><i>(for S.E.O)</i></small></label>
													<div class="col-md-12">
														<textarea name="meta_description"  placeholder="Write a short description of the course for search pages" class="form-control"><?php echo $course_details['meta_description']; ?></textarea>
													</div>
												</div>
											</div>
											
											
											
											
											
											
											<div class="col pr-0 pl-0 mt-3 mb-2">
												<div class="card widget-flat bg-yellow text-white mb-1">
													<div class="card-body">
														
															<div class="col-xl-12 justify-content-center">
																	<div class="form-group row mb-0">
																		<div class="offset-md-12">
																			<div class="custom-control custom-checkbox">
																				<input type="checkbox" class="custom-control-input" name="is_top_course" id="is_top_course" value="1" <?php if($course_details['is_top_course'] == 1) echo 'checked'; ?>>
																				<label class="custom-control-label" for="is_top_course"><?php echo get_phrase('make_this_course_appear_in_popular_course_on_home_page'); ?></label>
																				
																			</div>
																			
																		</div>
																	</div>
																
															</div>
															<div class="text-center font-weight-bold text-uppercase mt-2">
																<?php if($course_details['is_top_course'] == 1) echo 'this course is already in popular courses'; ?>
															</div>
													</div>
												</div>
											</div>
											
											
											
                                        </div> <!-- end col -->
										
										<div class="col-lg-4">
											<div class="form-group row mb-1">
                                                <label class="col-md-12 col-form-label" for="sub_category_id"><?php echo get_phrase('category'); ?><span class="required">*</span></label>
                                                <div class="col-md-12">
                                                    <select class="form-control select2" data-toggle="select2" name="sub_category_id" id="sub_category_id" required>
                                                        <option value=""><?php echo get_phrase('select_a_category'); ?></option>
                                                        <?php foreach ($categories->result_array() as $category): ?>
                                                            <optgroup label="<?php echo $category['name']; ?>">
                                                                <?php $sub_categories = $this->crud_model->get_sub_categories($category['id']);
                                                                foreach ($sub_categories as $sub_category): ?>
                                                                <option value="<?php echo $sub_category['id']; ?>" <?php if($sub_category['id'] == $course_details['sub_category_id']) echo 'selected'; ?>><?php echo $sub_category['name']; ?></option>
                                                            <?php endforeach; ?>
                                                        </optgroup>
                                                    <?php endforeach; ?>
													</select>
												</div>
											</div>
											<div class="form-group row mb-1">
												<label class="col-md-12 col-form-label" for="level"><?php echo get_phrase('level'); ?></label>
												<div class="col-md-12">
													<select class="form-control select2" data-toggle="select2" name="level" id="level">
														<option value="beginner" <?php if($course_details['level'] == "beginner") echo 'selected'; ?>><?php echo get_phrase('beginner'); ?></option>
														<option value="advanced" <?php if($course_details['level'] == "advanced") echo 'selected'; ?>><?php echo get_phrase('advanced'); ?></option>
														<option value="intermediate" <?php if($course_details['level'] == "intermediate") echo 'selected'; ?>><?php echo get_phrase('intermediate'); ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-1">
                                                <label class="col-md-12 col-form-label" for="language_made_in"><?php echo get_phrase('language_made_in'); ?></label>
                                                <div class="col-md-12">
                                                    <select class="form-control select2" data-toggle="select2" name="language_made_in" id="language_made_in">
                                                        <?php foreach ($languages as $language): ?>
                                                            <option value="<?php echo $language; ?>" <?php if ($course_details['language'] == $language)echo 'selected';?>><?php echo ucfirst($language); ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
											
											<div>
												<div class="col pr-0 pl-0 mt-3 mb-2">
													<div class="card widget-flat bg-yellow text-purple mb-1">
														<div class="card-body">
															<div class="row justify-content-center">
																<div class="col-xl-12">
																	<div class="form-group row mb-2">
																		<div class="col-md-12">
																			<div class="custom-control custom-checkbox">
																				<input type="checkbox" class="custom-control-input" name="is_free_course" id="is_free_course" value="1" <?php if($course_details['is_free_course'] == 1) echo 'checked'; ?> onclick="togglePriceFields(this.id)">
																				<label class="custom-control-label" for="is_free_course">Click the box if this is a free course<br><small>(Free Courses help you build a following and reviews)</label>
																			</div>
																		</div>
																	</div>

																	<div class="paid-course-stuffs">
																		<div class="form-group row mb-2">
																			<label class="col-md-12 col-form-label" for="price"><?php echo get_phrase('course_price').' ('.currency_code_and_symbol().')'; ?></label>
																			<div class="col-md-12">
																				<input type="number" class="form-control" id="price" name = "price" min="0" placeholder="<?php echo get_phrase('enter_course_course_price'); ?>" value="<?php echo $course_details['price']; ?>" >
																			</div>
																		</div>

																		<div class="form-group row mb-2">
																			<div class="col-md-12">
																				<div class="custom-control custom-checkbox">
																					<input type="checkbox" class="custom-control-input" name="discount_flag" id="discount_flag" value="1" <?php if($course_details['discount_flag'] == 1) echo 'checked'; ?>>
																					<label class="custom-control-label" for="discount_flag">Click the box if this course has a discount</label>
																				</div>
																			</div>
																		</div>

																		<div class="form-group row mb-1">
																			<label class="col-md-12 col-form-label" for="discounted_price"><?php echo get_phrase('discounted_price').' ('.currency_code_and_symbol().')'; ?></label>
																			<div class="col-md-12">
																				<input type="number" class="form-control" name="discounted_price" id="discounted_price" onkeyup="calculateDiscountPercentage(this.value)" value="<?php echo $course_details['discounted_price']; ?>" min="0">
																				
																				<small class="text-purple"><?php echo get_phrase('this_course_has'); ?> <span id = "discounted_percentage text-light" class="badge badge-danger">0%</span> <?php echo get_phrase('discount'); ?></small>
																				
																				
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												
											</div>
											
											<div>
												<div class="col pr-0 pl-0 mt-3">
													<div class="card widget-flat bg-info text-white mb-1">
														<div class="card-body pt-2 pb-2">
															<div class="row justify-content-center">
																<div class="col-xl-12">
																	<div class="form-group row mb-1">
																		<label class="col-md-12 col-form-label" for="course_overview_provider"><?php echo get_phrase('course_preview_provider'); ?><br><small>This preview is viewable by all before course purchase</small></label>
																		<div class="col-md-12">
																			<select class="form-control select2" data-toggle="select2" name="course_overview_provider" id="course_overview_provider">
																				<option value="youtube" <?php if ($course_details['course_overview_provider'] == 'youtube')echo 'selected';?>><?php echo get_phrase('youtube'); ?></option>
																				<option value="vimeo" <?php if ($course_details['course_overview_provider'] == 'vimeo')echo 'selected';?>><?php echo get_phrase('vimeo'); ?></option>
																				<option value="html5"><?php echo get_phrase('HTML5'); ?></option>
																			</select>
																		</div>
																	</div>
																	<div class="form-group row mb-1">
																		<label class="col-md-12 col-form-label" for="course_overview_url"><?php echo get_phrase('course_overview_url'); ?></label>
																		<div class="col-md-12">
																			<input type="text" class="form-control" name="course_overview_url" id="course_overview_url" placeholder="E.g: https://www.youtube.com/watch?v=YOUTUBE-VIDEO-CODE" value="<?php echo $course_details['video_url'] ?>">
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>

													<!-- Course media content edit file starts -->
													<?php include 'course_media_edit.php'; ?>
													<!-- Course media content edit file ends -->
												
											</div>
											<?php /*
											<div class="card cta-box bg-danger text-white">
												<div class="card-body">
													<div class="text-center">
														<h3 class="m-0 font-weight-normal cta-box-title"><b>IMPORTANT</b></h3>
														<i class="mdi mdi-alert-octagon mdi-48px"></i><br>
														<a class="font-weight-bold">After Updating/Editing your Course, it will be marked as Pending<br><span class="mt-2 mb-0 text-uppercase font-weight-bold" style="color:#f7a823">The Course would have to be Re-approved by the Admin</span></a>
													</div>
												</div>
											</div>
											*/ ?>
										</div>
                                    </div> <!-- end row -->
									<div class="row">
                                    
										<div class="col-12">
											<div class="text-center">
												<h2 class="mt-0"><i class="mdi mdi-check-decagram mdi-48px"></i></h2>
												<div class="mb-3 mt-3">
													<button type="button" class="btn btn-primary btn-block text-center" onclick="checkRequiredFields()"><i class="mdi mdi-content-save mr-2"></i><?php echo get_phrase('submit'); ?></button>
												</div>
											</div>
										</div> 
									
									</div> <!-- end FinishID -->
                                </div> <!-- end tab pane -->

                                
						
                        <ul class="list-inline mb-0 wizard text-center">
                            <li class="previous list-inline-item float-left" style="min-width: 150px;">
                                <a href="javascript::" class="btn btn-info"><i class="mdi mdi-arrow-left-bold mr-1"></i>Prev</a>
                            </li>
                            <li class="next list-inline-item float-right" style="min-width: 150px;">
                                <a href="javascript::" class="btn btn-info">Next<i class="mdi mdi-arrow-right-bold ml-1"></i></a>
                            </li>
                        </ul>

                    </div> <!-- tab-content -->
                </div>

                                

                                
                        
                       
                       

                       

								</div> <!-- tab-content -->
							</div> <!-- end #progressbarwizard-->
						</form>
					</div>
				</div><!-- end row-->
			</div> <!-- end card-body-->
		</div> <!-- end card-->
	</div>
</div>

<script type="text/javascript">
  $(document).ready(function () {
    initSummerNote(['#description']);
  });
</script>

<script type="text/javascript">
var blank_outcome = jQuery('#blank_outcome_field').html();
var blank_requirement = jQuery('#blank_requirement_field').html();
jQuery(document).ready(function() {
    jQuery('#blank_outcome_field').hide();
    jQuery('#blank_requirement_field').hide();
    calculateDiscountPercentage($('#discounted_price').val());
});
function appendOutcome() {
    jQuery('#outcomes_area').append(blank_outcome);
}
function removeOutcome(outcomeElem) {
    jQuery(outcomeElem).parent().parent().remove();
}

function appendRequirement() {
    jQuery('#requirement_area').append(blank_requirement);
}
function removeRequirement(requirementElem) {
    jQuery(requirementElem).parent().parent().remove();
}

function ajax_get_sub_category(category_id) {
    console.log(category_id);
    $.ajax({
        url: '<?php echo site_url('admin/ajax_get_sub_category/');?>' + category_id ,
        success: function(response)
        {
            jQuery('#sub_category_id').html(response);
        }
    });
}

function priceChecked(elem){
    if (jQuery('#discountCheckbox').is(':checked')) {

        jQuery('#discountCheckbox').prop( "checked", false );
    }else {

        jQuery('#discountCheckbox').prop( "checked", true );
    }
}

function topCourseChecked(elem){
    if (jQuery('#isTopCourseCheckbox').is(':checked')) {

        jQuery('#isTopCourseCheckbox').prop( "checked", false );
    }else {

        jQuery('#isTopCourseCheckbox').prop( "checked", true );
    }
}

function isFreeCourseChecked(elem) {

    if (jQuery('#'+elem.id).is(':checked')) {
        $('#price').prop('required',false);
    }else {
        $('#price').prop('required',true);
    }
}

function calculateDiscountPercentage(discounted_price) {
    if (discounted_price > 0) {
        var actualPrice = jQuery('#price').val();
        if ( actualPrice > 0) {
            var reducedPrice = actualPrice - discounted_price;
            var discountedPercentage = (reducedPrice / actualPrice) * 100;
            if (discountedPercentage > 0) {
                jQuery('#discounted_percentage').text(discountedPercentage.toFixed(2) + "%");

            }else {
                jQuery('#discounted_percentage').text('<?php echo '0%'; ?>');
            }
        }
    }
}

$('.on-hover-action').mouseenter(function() {
    var id = this.id;
    $('#widgets-of-'+id).show();
});
$('.on-hover-action').mouseleave(function() {
    var id = this.id;
    $('#widgets-of-'+id).hide();
});

//next previous hide/show
    $(document).ready(function() {
        $('li.previous').hide();
        $('#basicwizard').bootstrapWizard({
            'onNext': function(tab, navigation, index) {
                $('li.next').hide();
                $('li.previous').show();
            },
            'onPrevious': function(tab, navigation, index) {
                $('li.next').show();
                $('li.previous').hide();
            }
        });
    });
</script>
