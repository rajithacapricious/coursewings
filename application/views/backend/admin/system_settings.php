<div class="row">
	<div class="col-12">
		<ol class="breadcrumb yellow mt-2">
			<li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="mdi mdi-view-dashboard mr-1"></i><?php echo get_phrase('admin_dashboard'); ?></a></li>
			<li class="breadcrumb-item active "><a class="text-dark" href="#"><i class="mdi mdi-keyboard-settings mr-1"></i><?php echo get_phrase('global_settings'); ?></a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xl-12">
		<div class="card">
			<div class="card-header purple">

				<div class="row">
					<div class="col-12 col-xl-12">
						<h4 class="header-title mt-1"><i class="mdi mdi-keyboard-settings mr-1"></i><?php echo get_phrase('global_settings'); ?></h4>
					</div>
				</div>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-8">
						<form class="required-form" action="<?php echo site_url('admin/system_settings/system_update'); ?>" method="post" enctype="multipart/form-data">

							<div class="form-group">
								<label for="system_name"><?php echo get_phrase('website_name'); ?><span class="required">*</span></label>
								<input type="text" name="system_name" id="system_name" class="form-control" value="<?php echo get_settings('system_name');  ?>" required>
							</div>

							<div class="form-group">
								<label for="system_title"><?php echo get_phrase('website_title'); ?><span class="required">*</span></label>
								<input type="text" name="system_title" id="system_title" class="form-control" value="<?php echo get_settings('system_title');  ?>" required>
							</div>

							<div class="form-group">
								<label for="slogan"><?php echo get_phrase('site_usp'); ?><span class="required">*</span></label>
								<input type="text" name="slogan" id="slogan" class="form-control" value="<?php echo get_settings('slogan');  ?>" required>
							</div>

							<div class="form-group">
								<label for="system_email"><?php echo get_phrase('website_email'); ?><span class="required">*</span></label>
								<input type="text" name="system_email" id="system_email" class="form-control" value="<?php echo get_settings('system_email');  ?>" required>
							</div>


							<div class="form-group">
								<label for="website_description"><?php echo get_phrase('website_description'); ?></label>
								<textarea name="website_description" id="website_description" class="form-control" rows="5"><?php echo get_settings('website_description');  ?></textarea>
							</div>

							<div class="form-group">
								<label for="address"><?php echo get_phrase('address'); ?></label>
								<textarea name="address" id="address" class="form-control" rows="5"><?php echo get_settings('address');  ?></textarea>
							</div>


							<div class="form-group">
								<label for="website_keywords"><?php echo get_phrase('website_keywords'); ?></label>
								<input type="text" class="form-control bootstrap-tag-input" id="website_keywords" name="website_keywords" data-role="tagsinput" style="width: 100%;" value="<?php echo get_settings('website_keywords');  ?>" />
							</div>

							<div class="form-group">
								<label for="language"><?php echo get_phrase('student_email_verification'); ?></label>
								<select class="form-control select2" data-toggle="select2" name="student_email_verification" id="student_email_verification">
									<option value="enable" <?php if (get_settings('student_email_verification') == "enable") echo 'selected'; ?>><?php echo get_phrase('enable'); ?></option>
									<option value="disable" <?php if (get_settings('student_email_verification') == "disable") echo 'selected'; ?>><?php echo get_phrase('disable'); ?></option>
								</select>
							</div>

							<div class="form-group" hidden>
								<label for="footer_link"><?php echo get_phrase('footer_link'); ?></label>
								<input type="text" name="footer_link" id="footer_link" class="form-control" value="<?php echo get_settings('footer_link');  ?>">
							</div>


							<div class="form-group">
								<label for="youtube_api_key"><?php echo get_phrase('youtube_API_key'); ?><span class="required">*</span> &nbsp; <a href="https://developers.google.com/youtube/v3/getting-started" target="_blank" style="color: #a7a4a4">(<?php echo get_phrase('get_YouTube_API_key'); ?>)</a></label>
								<input type="text" name="youtube_api_key" id="youtube_api_key" class="form-control" value="<?php echo get_settings('youtube_api_key');  ?>" required>
							</div>

							<div class="form-group mb-4">
								<label for="vimeo_api_key"><?php echo get_phrase('vimeo_API_key'); ?><span class="required">*</span> &nbsp; <a href="https://www.youtube.com/watch?v=Wwy9aibAd54" target="_blank" style="color: #a7a4a4">(<?php echo get_phrase('get_Vimeo_API_key'); ?>)</a></label>
								<input type="text" name="vimeo_api_key" id="vimeo_api_key" class="form-control" value="<?php echo get_settings('vimeo_api_key');  ?>" required>
							</div>



							<div class="row" hidden>
								<div class="form-group" hidden>
									<label for="author"><?php echo get_phrase('author'); ?></label>
									<input type="text" name="author" id="author" class="form-control" value="<?php echo get_settings('author');  ?>">
								</div>

								<div class="form-group" hidden>
									<label for="phone"><?php echo get_phrase('phone'); ?></label>
									<input type="text" name="phone" id="phone" class="form-control" value="<?php echo get_settings('phone');  ?>">
								</div>

								<div class="form-group" hidden>
									<label for="purchase_code"><?php echo get_phrase('purchase_code'); ?><span class="required">*</span></label>
									<input type="text" name="purchase_code" id="purchase_code" class="form-control" value="<?php echo get_settings('purchase_code');  ?>" required>
								</div>

								<div class="form-group" hidden>
									<label for="language"><?php echo get_phrase('system_language'); ?></label>
									<select class="form-control select2" data-toggle="select2" name="language" id="language">
										<?php foreach ($languages as $language) : ?>
											<option value="<?php echo $language; ?>" <?php if (get_settings('language') == $language) echo 'selected'; ?>><?php echo ucfirst($language); ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>


							<hr class="my-4">


							<button type="button" class="btn btn-primary btn-block mt-4" onclick="checkRequiredFields()"><i class="mdi mdi-content-save mr-1"></i><?php echo get_phrase('save_site_settings'); ?></button>
						</form>


					</div>

					<div class="col-lg-4">

						<div class="row">


							<div class="col-lg-12">
								<div class="card">
									<div class="card-header purple">
										<div class="row">
											<div class="col-12 col-xl-12">
												<h5 class="my-1"><i class="mdi mdi-image mr-1"></i><?php echo get_phrase('change_frontend_logo'); ?></h5>
											</div>
										</div>
									</div>
									<div class="card-body bg-blue">
										<div class="col-lg-12">
											<div class="row justify-content-center">
												<form action="<?php echo site_url('admin/system_settings/dark_logo'); ?>" method="post" enctype="multipart/form-data" style="text-align: center;">
													<div class="form-group mb-2">
														<div class="wrapper-image-preview">
															<div class="box" style="width: 100%;">
																<div class="js--image-preview" style="background-image: url(<?php echo base_url('uploads/system/logo-dark.png'); ?>); background-color: #F5F5F5;"></div>
																<div class="upload-options">
																	<label for="dark_logo" class="btn"> <i class="mdi mdi-camera"></i> <?php echo get_phrase('select_frontend_logo'); ?> <br> <small>(330 X 70)</small> </label>
																	<input id="dark_logo" style="visibility:hidden;" type="file" class="image-upload" name="dark_logo" accept="image/*">
																</div>
															</div>
														</div>
													</div>
													<button type="submit" class="btn btn-primary btn-block"><i class="mdi mdi-content-save mr-1"></i><?php echo get_phrase('save_frontend_logo'); ?></button>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-lg-12">
								<div class="card">
									<div class="card-header purple">
										<div class="row">
											<div class="col-12 col-xl-12">
												<h5 class="my-1"><i class="mdi mdi-image mr-1"></i><?php echo get_phrase('change_backend_logo'); ?></h5>
											</div>
										</div>
									</div>
									<div class="card-body bg-blue">
										<div class="col-xl-12">
											<div class="row justify-content-center">
												<form action="<?php echo site_url('admin/system_settings/light_logo'); ?>" method="post" enctype="multipart/form-data" style="text-align: center;">
													<div class="form-group mb-2">
														<div class="wrapper-image-preview">
															<div class="box" style="width: 100%;">
																<div class="js--image-preview" style="background-image: url(<?php echo base_url('uploads/system/logo-light.png'); ?>); background-color: #F5F5F5;"></div>
																<div class="upload-options">
																	<label for="light_logo" class="btn"> <i class="mdi mdi-camera"></i> <?php echo get_phrase('select_backend_logo'); ?> <br> <small>(330 X 70)</small> </label>
																	<input id="light_logo" style="visibility:hidden;" type="file" class="image-upload" name="light_logo" accept="image/*">
																</div>
															</div>
														</div>
													</div>
													<button type="submit" class="btn btn-primary btn-block"><i class="mdi mdi-content-save mr-1"></i><?php echo get_phrase('save_backend_logo'); ?></button>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-lg-12">
								<div class="card">
									<div class="card-header purple">
										<div class="row">
											<div class="col-12 col-xl-12">
												<h5 class="my-1"><i class="mdi mdi-image mr-1"></i><?php echo get_phrase('update_mobile_logo'); ?></h5>
											</div>
										</div>
									</div>
									<div class="card-body bg-blue">
										<div class="col-lg-12">
											<div class="row justify-content-center">
												<form action="<?php echo site_url('admin/system_settings/small_logo'); ?>" method="post" enctype="multipart/form-data" style="text-align: center;">
													<div class="form-group mb-2">
														<div class="wrapper-image-preview">
															<div class="box" style="width: 100%;">
																<div class="js--image-preview" style="background-image: url(<?php echo base_url('uploads/system/logo-light-sm.png'); ?>); background-color: #F5F5F5;"></div>
																<div class="upload-options">
																	<label for="small_logo" class="btn"> <i class="mdi mdi-camera"></i> <?php echo get_phrase('select_mobile_logo'); ?> <br> <small>(49 X 58)</small> </label>
																	<input id="small_logo" style="visibility:hidden;" type="file" class="image-upload" name="small_logo" accept="image/*">
																</div>
															</div>
														</div>
													</div>
													<button type="submit" class="btn btn-primary btn-block"><i class="mdi mdi-content-save mr-1"></i><?php echo get_phrase('save_mobile_logo'); ?></button>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>




						</div>

					</div>

					<!-- #DK -->
					<!-- vimeo_api_key -->
					<?php $vimeo = $this->crud_model->get_vimeo_key('vimeo'); ?>
					<div class="col-lg-8">
						<form class="required-form-2" action="<?php echo site_url('admin/system_settings/update_vimeo_upload_key'); ?>" method="post">
							<h2>Vimeo Uploading Keys</h2>
							<div class="form-group">
								<label for="vimeo_client_id"><?php echo get_phrase('vimeo_client_id'); ?><span class="required">*</span></label>
								<input type="text" name="vimeo_client_id" id="vimeo_client_id" class="form-control" value="<?php echo $vimeo[0]->vimeo_client_id; ?>" required>
							</div>
							<div class="form-group">
								<label for="vimeo_client_secret"><?php echo get_phrase('vimeo_client_secret'); ?><span class="required">*</span></label>
								<input type="text" name="vimeo_client_secret" id="vimeo_client_secret" class="form-control" value="<?php echo $vimeo[0]->vimeo_client_secret; ?>" required>
							</div>
							<div class="form-group">
								<label for="vimeo_access_token"><?php echo get_phrase('vimeo_access_token'); ?><span class="required">*</span></label>
								<input type="text" name="vimeo_access_token" id="vimeo_access_token" class="form-control" value="<?php echo $vimeo[0]->vimeo_access_token; ?>" required>
							</div>
							<hr class="my-4">
							<button type="submit" class="btn btn-primary btn-block mt-4"><i class="mdi mdi-content-save mr-1"></i><?php echo get_phrase('save_vimeo_settings'); ?></button>
						</form>
					</div>

				</div>
			</div> <!-- end card body-->
		</div> <!-- end card -->
	</div><!-- end col-->

	<?php /*
    <div class="col-xl-5">
        <div class="card">
            <div class="card-body">
                <div class="col-lg-12">
                    <h4 class="mb-3 header-title"><?php echo get_phrase('update_product');?></h4>

                    <form action="<?php echo site_url('updater/update'); ?>" method="post" enctype="multipart/form-data">
                        <div class="form-group mb-2">
                            <label><?php echo get_phrase('file'); ?></label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="file_name" name="file_name" required onchange="changeTitleOfImageUploader(this)">
                                    <label class="custom-file-label" for="file_name"><?php echo get_phrase('update_product'); ?></label>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary"><?php echo get_phrase('update'); ?></button>
                    </form>
                </div>
            </div> <!-- end card body-->
        </div>
    </div>
	*/ ?>
</div>