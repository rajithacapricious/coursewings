<?php
$course_media_files = themeConfiguration(get_frontend_settings('theme'), 'course_media_files');
$course_media_placeholders = themeConfiguration(get_frontend_settings('theme'), 'course_media_placeholders');
foreach ($course_media_files as $course_media => $size): ?>
<div class="col pr-0 pl-0 mt-3 mb-2">
	<div class="card widget-flat bg-success text-white mb-1">
		<div class="card-body pt-2 pb-2">
			<div class="row justify-content-center">
				<div class="col-xl-12">
				  <div class="form-group row mb-3 text-center">
					<label class="col-md-12 col-form-label text-center pt-0 mb-1" for="<?php echo $course_media; ?>_label"><h5 class="my-0"><?php echo get_phrase($course_media); ?></label></h5>
					<div class="col-md-12 text-center">
					  <div class="wrapper-image-preview d-inline-flex" style="">
						<div class="box" style="width: 250px;">
						  <div class="js--image-preview" style="background-image: url(<?php echo $this->crud_model->get_course_thumbnail_url($course_details['id'], $course_media);?>); background-color: #F5F5F5;"></div>
						  <div class="upload-options">
							<label for="<?php echo $course_media; ?>" class="btn"> <i class="mdi mdi-camera"></i> <?php echo get_phrase($course_media); ?> <br> <small>(<?php echo $size; ?>px)</small> </label>
							<input id="<?php echo $course_media; ?>" style="visibility:hidden;" type="file" class="image-upload" name="<?php echo $course_media; ?>" accept="image/*">
						  </div>
						</div>
					  </div>
					</div>
				  </div>
				</div>
				</div>
					</div>
				  </div>
				</div>
<?php endforeach; ?>
