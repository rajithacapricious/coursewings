<div class="row">
	<div class="col-12">
		<ol class="breadcrumb yellow mt-2">
			<li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="mdi mdi-view-dashboard mr-1"></i><?php echo get_phrase('admin_dashboard'); ?></a></li>
			<li class="breadcrumb-item active "><a class="text-dark" href="#"><i class="mdi mdi-account-group mr-1"></i><?php echo get_phrase('site_members'); ?></a></li>
		</ol>
	</div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
			<div class="card-header purple">
			
				<div class="row">
					<div class="col-6 col-xl-8">
						<h4 class="header-title mt-1"><i class="mdi mdi-account-group mr-1"></i><?php echo get_phrase('site_members'); ?></h4>                     
					</div>
					<div class="col-6 col-xl-4">
						<div class="text-lg-right mt-1">
							<a href="<?php echo site_url('admin/user_form/add_user_form'); ?>" class="btn btn-xs bg-yellow mb-0">
								<i class="mdi mdi-account-plus mr-1"></i><?php echo get_phrase('add_site_member'); ?>
							</a>
						</div>
					</div><!-- end col-->
				</div>
			</div>
            <div class="card-body">
				<div class="table-responsive-sm mt-1">
                <table id="basic-datatable-users" class="table table-striped table-centered dt-responsive mb-0" width="100%">
                  <thead class="bg-yellow">
                    <tr>
                      <th>Sr.</th>
                      <th>Member Name</th>
                      <th class="text-center"><?php echo get_phrase('join-date'); ?></th>
                      <th class="text-center"><?php echo get_phrase('enrolled_courses'); ?></th>
                      <th class="text-center"><?php echo get_phrase('actions'); ?></th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php
                       foreach ($users->result_array() as $key => $user): ?>
                          <tr>
								<td class="text-center">
									<?php echo $key+1; ?>
								</td>
								<td>
									<div class="media">
										<img src="<?php echo $this->user_model->get_user_image_url($user['id']);?>" alt="" height="50" width="50" class="img-fluid img-thumbnail authorimg mr-1">
										<p class="media-body pb-3 mb-0">
											<strong class="d-block"><a><?php echo $user['first_name'].' '.$user['last_name']; ?></a></strong>
											<a href="mailto:<?php echo $user['email']; ?>"><i class="mdi mdi-email-outline mr-1"></i><?php echo $user['email']; ?></a>
										</p>
									</div>
								</td>
								<td class="text-center">
									<?php echo date('D, d-M-Y', $user['date_added']); ?>
								</td>
								
								<td class="text-center">
									<?php
										$enrolled_courses = $this->crud_model->enrol_history_by_user_id($user['id']);
										$numberofenrolled_courses = $this->crud_model->enrol_history_by_user_id($user['id'])->num_rows(); 
									?>
									
										
										
										
										<?php if ($this->crud_model->enrol_history_by_user_id($user['id'])->num_rows() == "0"): ?>
                                            <div class="">
												<a class="btn btn-block bg-yellow">No Courses Subscribed</a>
											</div>
										<?php endif; ?>	
                                        <?php if ($this->crud_model->enrol_history_by_user_id($user['id'])->num_rows() != "0"): ?>
										<div class="dropdown">
											<button type="button" class="btn btn-block btn-primary dropdown-toggle" data-toggle="dropdown">
												<?php echo $numberofenrolled_courses; ?> Courses Subscribed
											</button>
                                            <div class="dropdown-menu bg-yellow">
												<?php foreach ($enrolled_courses->result_array() as $enrolled_course):
												$course_details = $this->crud_model->get_course_by_id($enrolled_course['course_id'])->row_array();?>
												<a class="dropdown-item"><?php echo $course_details['title']; ?></a>
												<?php endforeach; ?>
											</div>
										</div> 
                                        <?php endif; ?>
										
										
										
										
									
                              </td>
							  
                              <td class="text-center">
									<a class="btn btn-xs2 bg-blue text-light" href="<?php echo site_url('admin/user_form/edit_user_form/'.$user['id']) ?>">
										<i class="mdi mdi-pencil mr-1"></i><?php echo get_phrase('edit'); ?>&nbsp;Member
									</a>
									
									<a class="btn btn-xs2 btn-danger text-light" href="#" onclick="confirm_modal('<?php echo site_url('admin/users/delete/'.$user['id']); ?>');">
										<i class="mdi mdi-delete mr-1"></i><?php echo get_phrase('delete'); ?>&nbsp;Member
									</a>
                              </td>
                          </tr>
                      <?php endforeach; ?>
                  </tbody>
              </table>
              </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
