<?php
  $homepage_banner = themeConfiguration(get_frontend_settings('theme'), 'homepage');
?>
<div class="row">
	<div class="col-12">
		<ol class="breadcrumb yellow mt-2">
			<li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="mdi mdi-view-dashboard mr-1"></i>Dashboard</a></li>
			<li class="breadcrumb-item active "><a class="text-dark" href="#"><i class="mdi mdi-airplay mr-1"></i><?php echo get_phrase('update_homepage'); ?></a></li>
		</ol>
	</div>
</div>

<div class="row justify-content-center">
    <div class="col-xl-8">
        <div class="card">
			<div class="card-header purple">
			
				<div class="row">
					<div class="col-12 col-xl-12">
						<h4 class="header-title mt-1"><i class="mdi mdi-airplay mr-1"></i><?php echo get_phrase('banner_settings');?></h4>                         
					</div>
				</div>
			</div>
            <div class="card-body">
                <form class="required-form" action="<?php echo site_url('admin/frontend_settings/frontend_update'); ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="banner_title"><?php echo get_phrase('banner_title'); ?><span class="required">*</span></label>
                        <input type="text" name = "banner_title" id = "banner_title" class="form-control" value="<?php echo get_frontend_settings('banner_title');  ?>" required>
                    </div>
                    <div class="form-group">
                        <label for="banner_sub_title"><?php echo get_phrase('banner_sub_title'); ?><span class="required">*</span></label>
                        <input type="text" name = "banner_sub_title" id = "banner_sub_title" class="form-control" value="<?php echo get_frontend_settings('banner_sub_title');  ?>" required>
                    </div>
                    

                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-primary btn-block" onclick="checkRequiredFields()"><i class="mdi mdi-content-save mr-1"></i><?php echo get_phrase('update_banner_text'); ?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
	
	<?php if (count($homepage_banner) > 0):
      if ($homepage_banner['homepage_banner_image']):?>
	<div class="col-xl-4">
        <div class="card">
			<div class="card-header purple">
			
				<div class="row">
					<div class="col-12 col-xl-12">
						<h4 class="header-title mt-1"><i class="mdi mdi-image mr-1"></i><?php echo get_phrase('change_banner_image');?></h4>                         
					</div>
				</div>
			</div>
            <div class="card-body bg-blue">
                <div class="col-xl-12">
                    <div class="row justify-content-center">
                        <form action="<?php echo site_url('admin/frontend_settings/banner_image_update'); ?>" method="post" enctype="multipart/form-data" style="text-align: center;">
                            <div class="form-group mb-2">
                                <div class="wrapper-image-preview">
                                    <div class="box" style="width: 100%;">
                                        <div class="js--image-preview" style="background-image: url(<?php echo base_url('uploads/system/home-banner.jpg');?>); background-color: #F5F5F5;"></div>
                                        <div class="upload-options">
                                            <label for="banner_image" class="btn"> <i class="mdi mdi-camera"></i> <?php echo get_phrase('upload_banner_image'); ?> <br> <small>(2000 X 1335)</small> </label>
                                            <input id="banner_image" style="visibility:hidden;" type="file" class="image-upload" name="banner_image" accept="image/*">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block"><i class="mdi mdi-content-save mr-1"></i><?php echo get_phrase('save_banner_image'); ?></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<?php endif; ?>
    <?php endif; ?>
</div>

<div class="row justify-content-center">
    
</div>

<script type="text/javascript">
//  $(document).ready(function () {
//    initSummerNote(['#about_us', '#terms_and_condition', '#privacy_policy']);
//  });
</script>
