<div class="row">
	<div class="col-12">
		<ol class="breadcrumb yellow mt-2">
			<li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="mdi mdi-view-dashboard mr-1"></i><?php echo get_phrase('admin_dashboard'); ?></a></li>
			<li class="breadcrumb-item active "><a class="text-dark" href="#"><i class="mdi mdi-tag-plus mr-1"></i><?php echo get_phrase('manually_subscribe_member'); ?></a></li>
		</ol>
	</div>
</div>

<div class="row justify-content-center">
    <div class="col-xl-12">
        <div class="card">
			<div class="card-header purple">
			
				<div class="row">
					<div class="col-12 col-xl-12">
						<h4 class="header-title mt-1"><i class="mdi mdi-cart-plus mr-1"></i><?php echo get_phrase('free_subscription_form'); ?></h4>                         
					</div>
				</div>
			</div>
            <div class="card-body">
              <div class="col-lg-12">
                <form class="required-form" action="<?php echo site_url('admin/enrol_student/enrol'); ?>" method="post" enctype="multipart/form-data">

                    <div class="form-group">
                        <label for="user_id"><?php echo get_phrase('site_member'); ?><span class="required">*</span> </label>
                        <select class="form-control select2" data-toggle="select2" name="user_id" id="user_id" required>
                            <option value=""><?php echo get_phrase('select_member'); ?></option>
                            <?php $user_list = $this->user_model->get_user()->result_array();
                                foreach ($user_list as $user):?>
                                <option value="<?php echo $user['id'] ?>"><?php echo $user['first_name'].' '.$user['last_name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="course_id"><?php echo get_phrase('allot_course'); ?><span class="required">*</span> </label>
                        <select class="form-control select2" data-toggle="select2" name="course_id" id="course_id" required>
                            <option value=""><?php echo get_phrase('select_a_course'); ?></option>
                            <?php $course_list = $this->crud_model->get_courses()->result_array();
                                foreach ($course_list as $course):
                                if ($course['status'] != 'active')
                                    continue;?>
                                <option value="<?php echo $course['id'] ?>"><?php echo $course['title']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <button type="button" class="btn btn-primary btn-block" onclick="checkRequiredFields()"><i class="mdi mdi-cart-plus mr-1"></i><?php echo get_phrase('subscribe_student_to_course'); ?></button>
                </form>
				<div class="text-center">
					Please note <span class="badge badge-danger">no payments</span> would be applicable for paid courses also 
				</div>
              </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
