<div class="row">
	<div class="col-xl-12">
		<ol class="breadcrumb yellow mt-2">
			<li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="mdi mdi-view-dashboard mr-1"></i><?php echo get_phrase('admin_dashboard'); ?></a></li>
			<li class="breadcrumb-item active text-purple"><i class="mdi mdi-book mr-1"></i><?php echo get_phrase('courses'); ?></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-sm-3 col-xl-3">
		<div class="card cta-box bg-yellow text-white">
			<div class="card-body">
				<div class="widget-title">
					<?php echo get_phrase('active_courses'); ?>
				</div>
				<div class="media align-items-center">
					
					<div class="media-body">
						<i class="fas fa-eye fa-6x bottomfaicon"></i>
					</div>
					<span class="mr-3" style="font-size: 3rem;margin-top: -10px;">
						<?php echo $status_wise_courses['active']->num_rows(); ?>
					</span>
				</div>
			</div>
			<!-- end card-body -->
		</div>
	</div>
	
	<div class="col-sm-3 col-xl-3">
		<div class="card cta-box bg-blue text-white">
			<div class="card-body">
				<div class="widget-title">
					<?php echo get_phrase('pending_courses'); ?>
				</div>
				<div class="media align-items-center">
					
					<div class="media-body">
						<i class="fas fa-eye-slash fa-6x bottomfaicon"></i>
					</div>
					<span class="mr-3" style="font-size: 3rem;margin-top: -10px;">
						<?php echo $status_wise_courses['pending']->num_rows(); ?>
					</span>
				</div>
			</div>
			<!-- end card-body -->
		</div>
	</div>
	
	<div class="col-sm-3 col-xl-3">
		<div class="card cta-box bg-success text-white">
			<div class="card-body">
				<div class="widget-title">
					<?php echo get_phrase('paid_courses'); ?>
				</div>
				<div class="media align-items-center">
					
					<div class="media-body">
						<i class="mdi mdi-currency-usd bottom45icon"></i>
					</div>
					<span class="mr-3" style="font-size: 3rem;margin-top: -10px;">
						<?php echo $this->crud_model->get_free_and_paid_courses('paid')->num_rows(); ?>
					</span>
				</div>
			</div>
			<!-- end card-body -->
		</div>
	</div>
	
	<div class="col-sm-3 col-xl-3">
		<div class="card cta-box bg-pink text-white">
			<div class="card-body">
				<div class="widget-title">
					<?php echo get_phrase('free_courses'); ?>
				</div>
				<div class="media align-items-center">
					
					<div class="media-body">
						<i class="mdi mdi-currency-usd-off bottom45icon"></i>
					</div>
					<span class="mr-3" style="font-size: 3rem;margin-top: -10px;">
						<?php echo $this->crud_model->get_free_and_paid_courses('free')->num_rows(); ?>
					</span>
				</div>
			</div>
			<!-- end card-body -->
		</div>
	</div>
</div>
<div class="row">
    <div class="col-xl-12">
        <div class="card">
			<div class="card-header purple">
			
				<div class="row">
					<div class="col-6 col-xl-8">
						<h4 class="header-title mt-1"><i class="mdi mdi-book mr-1"></i><?php echo get_phrase('course_list'); ?></h4>
						<small>Please be very careful while deleting courses as there may be subscribers for that course</small>
					</div>
					<div class="col-6 col-xl-4">
						<div class="text-lg-right">
							
							<a href="<?php echo site_url('admin/course_form/add_course'); ?>" class="btn btn-xs bg-yellow mb-0 mt-1"><i class="mdi mdi-plus"></i><?php echo get_phrase('add_new_course'); ?> as Admin</a>
						</div>
					</div><!-- end col-->
				</div>
			</div>
		
            <div class="card-body">
                <h4 class="mb-3 header-title"><?php echo get_phrase('course_list'); ?></h4>
				
                <form class="row justify-content-center" action="<?php echo site_url('admin/courses'); ?>" method="get">
                    <!-- Course Categories -->
                    <div class="col-xl-3">
                        <div class="form-group">
                            <label for="category_id"><?php echo get_phrase('categories'); ?></label>
                            <select class="form-control select2" data-toggle="select2" name="category_id" id="category_id">
                                <option value="<?php echo 'all'; ?>" <?php if($selected_category_id == 'all') echo 'selected'; ?>><?php echo get_phrase('all'); ?></option>
                                <?php foreach ($categories->result_array() as $category): ?>
                                    <optgroup label="<?php echo $category['name']; ?>">
                                        <?php $sub_categories = $this->crud_model->get_sub_categories($category['id']);
                                        foreach ($sub_categories as $sub_category): ?>
                                        <option value="<?php echo $sub_category['id']; ?>" <?php if($selected_category_id == $sub_category['id']) echo 'selected'; ?>><?php echo $sub_category['name']; ?></option>
                                    <?php endforeach; ?>
                                </optgroup>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <!-- Course Status -->
                <div class="col-xl-2">
                    <div class="form-group">
                        <label for="status"><?php echo get_phrase('status'); ?></label>
                        <select class="form-control select2" data-toggle="select2" name="status" id = 'status'>
                            <option value="all" <?php if($selected_status == 'all') echo 'selected'; ?>><?php echo get_phrase('all'); ?></option>
                            <option value="active" <?php if($selected_status == 'active') echo 'selected'; ?>><?php echo get_phrase('active'); ?></option>
                            <option value="pending" <?php if($selected_status == 'pending') echo 'selected'; ?>><?php echo get_phrase('pending'); ?></option>
                        </select>
                    </div>
                </div>

                <!-- Course Instructors -->
                <div class="col-xl-3">
                    <div class="form-group">
                        <label for="instructor_id"><?php echo get_phrase('instructor'); ?></label>
                        <select class="form-control select2" data-toggle="select2" name="instructor_id" id = 'instructor_id'>
                            <option value="all" <?php if($selected_instructor_id == 'all') echo 'selected'; ?>><?php echo get_phrase('all'); ?></option>
                            <?php foreach ($instructors as $instructor): ?>
                                <option value="<?php echo $instructor['id']; ?>" <?php if($selected_instructor_id == $instructor['id']) echo 'selected'; ?>><?php echo $instructor['first_name'].' '.$instructor['last_name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <!-- Course Price -->
                <div class="col-xl-2">
                    <div class="form-group">
                        <label for="price"><?php echo get_phrase('price'); ?></label>
                        <select class="form-control select2" data-toggle="select2" name="price" id = 'price'>
                            <option value="all"  <?php if($selected_price == 'all' ) echo 'selected'; ?>><?php echo get_phrase('all'); ?></option>
                            <option value="free" <?php if($selected_price == 'free') echo 'selected'; ?>><?php echo get_phrase('free'); ?></option>
                            <option value="paid" <?php if($selected_price == 'paid') echo 'selected'; ?>><?php echo get_phrase('paid'); ?></option>
                        </select>
                    </div>
                </div>

                <div class="col-xl-2">
                    <label for=".." class="text-white"><?php echo get_phrase('..'); ?></label>
                    <button type="submit" class="btn btn-primary btn-block" name="button"><?php echo get_phrase('filter'); ?></button>
                </div>
            </form>
			
            <div class="table-responsive-sm mt-4">
                <?php if (count($courses) > 0): ?>
                    <table id="course-datatable" class="table table-striped dt-responsive nowrap" width="100%" data-page-length='10'>
                        <thead class="bg-yellow">
                            <tr>
                                
                                <th><?php echo get_phrase('title'); ?></th>
								<th>Author</th>
								<th><?php echo get_phrase('price'); ?></th>
                                <th class="text-center">Admin <?php echo get_phrase('actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($courses as $key => $course):
                                $instructor_details = $this->user_model->get_all_user($course['user_id'])->row_array();
                                $category_details = $this->crud_model->get_category_details_by_id($course['sub_category_id'])->row_array();
                                $sections = $this->crud_model->get_section('course', $course['id']);
                                $lessons = $this->crud_model->get_lessons('course', $course['id']);
                                $enroll_history = $this->crud_model->enrol_history($course['id']);
                                if ($course['status'] == 'draft') {
                                    continue;
                                }
                            ?>
                                <tr>
                                    <td>
                                        <strong><a class="ml-1" href="<?php echo site_url('admin/course_form/course_edit/'.$course['id']); ?>">
											<?php echo ellipsis($course['title']); ?>
										</a></strong>
										<?php if ($course['status'] == 'pending'): ?>
                                            <span class="badge bg-yellow text-light"><?php echo get_phrase($course['status']); ?></span>
											
                                        <?php elseif ($course['status'] == 'active'):?>
                                            <span class="badge bg-success text-light"><?php echo get_phrase($course['status']); ?></span>
                                        <?php endif; ?>
										
										
										<?php /*
										<?php if ($course['is_top_course'] == '1'): ?>
                                            <span class="badge bg-yellow text-light"><?php echo get_phrase($course['featured']); ?></span>
											
                                        <?php elseif ($course['is_top_course'] == '0'):?>
                                            <span class="badge bg-success text-light"><?php echo get_phrase($course['unfeatured']); ?></span>
                                        <?php endif; ?>
										
										*/ ?>
										
										<br>
										<span>
											<?php if ($course['is_top_course'] == '1'): ?>
                                            <span class="badge bg-blue text-light mr-1">Featured</span>
											
                                        <?php elseif ($course['is_top_course'] == '0'):?>
                                            <span class="badge bg-yellow text-light mr-1">Not Featured</span>
                                        <?php endif; ?>
										</span>
										<?php
											
											if($sections->num_rows() == "0"){
												$textclass = "text-danger";
												
											} else{
												$textclass = "text-muted";
												
											}
										?>
										
										 
										
										<small class="<?php echo $textclass ;?> ml-1">
											<span><small><?php echo '<b>'.get_phrase('total_section').'</b>: '.$sections->num_rows(); ?> | 
											<?php echo '<b>'.get_phrase('total_lesson').'</b>: '.$lessons->num_rows(); ?></small></span>
										</small>
										
                                    </td>
									<td>
                                        <?php echo '<b>'.$instructor_details['first_name'].' '.$instructor_details['last_name'].'</b>'; ?><br>
										<span class="badge bg-blue text-light">Cat:&nbsp;<i><?php echo $category_details['name']; ?></i></span>
                                    </td>
									
									<td>
                                        <?php if ($course['is_free_course'] == null): ?>
                                            <?php if ($course['discount_flag'] == 1): ?>
                                                <span class="badge badge-primary" style="font-size:14px;"><?php echo currency($course['discounted_price']); ?></span>
                                            <?php else: ?>
                                                <span class="badge badge-primary" style="font-size:14px;"><?php echo currency($course['price']); ?></span>
                                            <?php endif; ?>
                                        <?php elseif ($course['is_free_course'] == 1):?>
                                            <span class="badge bg-pink text-light" style="font-size:14px;"><?php echo get_phrase('free'); ?></span>
                                        <?php endif; ?><br>
										<span class="badge badge-primary text-light">Subscribers:&nbsp;<b><?php echo $enroll_history->num_rows(); ?></b></span>
                                    </td>
                                    
                                    <td class="text-center">
										<?php if ($course['status'] == 'active'): ?>
											<?php if ($course['user_id'] != $this->session->userdata('user_id')): ?>
												<a class="btn btn-xs2 btn-warning" href="#" onclick="showAjaxModal('<?php echo base_url();?>modal/popup/mail_on_course_status_changing_modal/pending/<?php echo $course['id']; ?>/<?php echo $selected_category_id; ?>/<?php echo $selected_instructor_id; ?>/<?php echo $selected_price; ?>/<?php echo $selected_status;?>', '<?php echo get_phrase('inform_instructor'); ?>');">
												<i class="fas fa-eye-slash mr-1"></i><?php echo get_phrase('mark_as_pending');?>
											</a>
											<?php else: ?>
											<a class="btn btn-xs2 btn-warning" href="#" onclick="confirm_modal('<?php echo site_url();?>admin/change_course_status_for_admin/pending/<?php echo $course['id']; ?>/<?php echo $selected_category_id; ?>/<?php echo $selected_instructor_id; ?>/<?php echo $selected_price; ?>/<?php echo $selected_status;?>', '<?php echo get_phrase('inform_instructor'); ?>');">
												<i class="fas fa-eye-slash mr-1"></i><?php echo get_phrase('mark_as_pending');?>
											</a>
											<?php endif; ?>
											
											<?php else: ?>
											<?php if ($course['user_id'] != $this->session->userdata('user_id')): ?>
											<a class="btn btn-xs2 btn-success" href="#" onclick="showAjaxModal('<?php echo base_url();?>modal/popup/mail_on_course_status_changing_modal/active/<?php echo $course['id']; ?>/<?php echo $selected_category_id; ?>/<?php echo $selected_instructor_id; ?>/<?php echo $selected_price; ?>/<?php echo $selected_status;?>', '<?php echo get_phrase('inform_instructor'); ?>');">
												<i class="fas fa-eye mr-1"></i><?php echo get_phrase('mark_as_active');?>
											</a>
											<?php else: ?>
											
											<a class="btn btn-xs2 btn-success" href="#" onclick="confirm_modal('<?php echo site_url();?>admin/change_course_status_for_admin/active/<?php echo $course['id']; ?>/<?php echo $selected_category_id; ?>/<?php echo $selected_instructor_id; ?>/<?php echo $selected_price; ?>/<?php echo $selected_status;?>', '<?php echo get_phrase('inform_instructor'); ?>');">
												<i class="fas fa-eye mr-1"></i><?php echo get_phrase('mark_as_active');?>
											</a>
											<?php endif; ?>
											<?php endif; ?>
											
										<a class="btn btn-xs2 bg-blue text-light" href="<?php echo site_url('admin/course_form/course_edit/'.$course['id']); ?>">
											<i class="mdi mdi-pencil mr-1"></i>Edit
										</a>
										
										<a class="btn btn-xs2 btn-danger text-light" href="#" onclick="confirm_modal('<?php echo site_url('admin/course_actions/delete/'.$course['id']); ?>');">
											<i class="mdi mdi-delete mr-1"></i><?php echo get_phrase('delete'); ?>
										</a>
										
										<a class="btn btn-xs2 btn-primary pull-right hidden-xs" href="<?php echo site_url('home/course/'.slugify($course['title']).'/'.$course['id']); ?>" target="_blank">
											<i class="mdi mdi-airplay mr-1"></i>View
										</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
                <?php if (count($courses) == 0): ?>
                    <div class="img-fluid w-100 text-center">
                      <img style="opacity: 1; width: 100px;" src="<?php echo base_url('assets/backend/images/file-search.svg'); ?>"><br>
                      <?php echo get_phrase('no_data_found'); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
</div>
