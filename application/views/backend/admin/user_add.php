<div class="row">
	<div class="col-12">
		<ol class="breadcrumb yellow mt-2">
			<li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="mdi mdi-view-dashboard mr-1"></i>Dashboard</a></li>
			<li class="breadcrumb-item"><a href="<?php echo site_url('admin/users'); ?>"><i class="mdi mdi-account-group mr-1"></i><?php echo get_phrase('members'); ?></a></li>
			<li class="breadcrumb-item active "><a class="text-dark" href="#"><i class="mdi mdi-account-edit mr-1"></i><?php echo get_phrase('add_member'); ?></a></li>
		</ol>
	</div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
			<div class="card-header purple">
			
				<div class="row">
					<div class="col-12 col-xl-12">
						<h4 class="header-title mt-1"><i class="mdi mdi-account-edit mr-1"></i><?php echo get_phrase('manually_add_member'); ?></h4>                         
					</div>
				</div>
			</div>
            <div class="card-body">

                <form class="required-form" action="<?php echo site_url('admin/users/add'); ?>" enctype="multipart/form-data" method="post">
                    <div id="progressbarwizard">
					
						<ul class="nav nav-tabs nav-justified nav-bordered mb-3">
                            <li class="nav-item">
                                <a href="#basic_info" data-toggle="tab" class="nav-link rounded-0 trans text-primary pt-2">
                                    <i class="mdi mdi-account-circle mdi-18px mr-1"></i>
                                    <span class="d-none d-sm-inline">Member Info</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#social_information" data-toggle="tab" class="nav-link rounded-0 trans text-primary pt-2">
                                    <i class="mdi mdi-wan mdi-18px mr-1"></i>
                                    <span class="d-none d-sm-inline">Payment & Social Info</span>
                                </a>
                            </li>
							<li class="nav-item">
                                <a href="#finish" data-toggle="tab" class="nav-link rounded-0 trans text-primary pt-2">
                                    <i class="mdi mdi-clipboard-check mdi-18px mr-1"></i>
                                    <span class="d-none d-sm-inline"><?php echo get_phrase('finish'); ?></span>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content b-0 mb-0">

                            <div id="bar" class="progress mb-3" style="height: 20px;">
                                <div class="bar progress-bar progress-bar-striped progress-bar-animated bg-success"></div>
                            </div>

                            <div class="tab-pane" id="basic_info">
                                <div class="row">
									<div class="col-6">
										<div class="form-group row mb-1">
                                            <label class="col-md-12 col-form-label" for="first_name"><?php echo get_phrase('first_name'); ?><span class="required">*</span></label>
                                            <div class="col-md-12">
                                                <input type="text" class="form-control" id="first_name" name="first_name" required>
                                            </div>
                                        </div>
									</div>
									
									<div class="col-6">
										 <div class="form-group row mb-1">
                                            <label class="col-md-12 col-form-label" for="last_name"><?php echo get_phrase('last_name'); ?><span class="required">*</span></label>
                                            <div class="col-md-12">
                                                <input type="text" class="form-control" id="last_name" name="last_name" required>
                                            </div>
                                        </div>
									</div>
									
									<div class="col-6">
										<div class="form-group row mb-1">
                                            <label class="col-md-12 col-form-label" for="email"><?php echo get_phrase('email'); ?><span class="required">*</span></label>
                                            <div class="col-md-12">
                                                <input type="email" id="email" name="email" class="form-control" required>
                                            </div>
                                        </div>
									</div>
									
									<div class="col-6">
										<div class="form-group row mb-1">
                                            <label class="col-md-12 col-form-label" for="password"><?php echo get_phrase('password'); ?><span class="required">*</span></label>
                                            <div class="col-md-12">
                                                <input type="password" id="password" name="password" class="form-control" required>
                                            </div>
                                        </div>
									</div>
									
									<div class="col-12">
										<div class="form-group row mb-1">
                                            <label class="col-md-12 col-form-label" for="linkedin_link"><?php echo get_phrase('biography'); ?></label>
                                            <div class="col-md-12">
                                                <textarea name="biography" id = "summernote-basic" class="form-control"></textarea>
                                            </div>
                                        </div>
									</div>
									
									
                                    <div class="col-12">
                                        
                                       
                                        
                                        
                                    </div> <!-- end col -->
                                </div> <!-- end row -->
                            </div>

                            <div class="tab-pane" id="login_credentials">
                                <div class="row">
                                    <div class="col-12">
                                        
                                        
                                    </div> <!-- end col -->
                                </div> <!-- end row -->
                            </div>

                            <div class="tab-pane" id="social_information">
                                <div class="row">
								
									<div class="col-12">
									
									</div>
									
									<div class="col-12">
										<div class="card widget-flat bg-success text-white">
											<div class="card-body">
												<div class="form-group row mb-0">
													<label class="col-md-3 col-form-label" for="facebook_link"><i class="mdi mdi-paypal mdi-18px mr-1"></i><?php echo get_phrase('paypal_client_id'); ?></label>
													
													<div class="col-md-9 text-center">
														<input type="text" id="paypal_client_id" name="paypal_client_id" class="form-control mb-0">
														<small class=""><i>(<?php echo get_phrase("required_if_adding_member_as_instructor"); ?> for payouts)</i></small>
														
													</div>
												</div>
											</div>
										</div>
									</div>
									
									<div class="col-12">
										<div class="form-group row mb-3">
                                            <label class="col-md-3 col-form-label" for="user_image"><?php echo get_phrase('user_image'); ?></label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" id="user_image" name="user_image" accept="image/*" onchange="changeTitleOfImageUploader(this)">
                                                        <label class="custom-file-label" for="user_image"><?php echo get_phrase('choose_user_image'); ?></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
									</div>
									
									<div class="col-12">
										<div class="form-group row mb-3">
                                            <label class="col-md-3 col-form-label" for="facebook_link"> <?php echo get_phrase('facebook'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" id="facebook_link" name="facebook_link" class="form-control">
                                            </div>
                                        </div>
									</div>
									
									<div class="col-12">
										 <div class="form-group row mb-3">
                                            <label class="col-md-3 col-form-label" for="twitter_link"><?php echo get_phrase('twitter'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" id="twitter_link" name="twitter_link" class="form-control">
                                            </div>
                                        </div>
									</div>
									
									<div class="col-12">
										<div class="form-group row mb-3">
                                            <label class="col-md-3 col-form-label" for="linkedin_link"><?php echo get_phrase('linkedin'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" id="linkedin_link" name="linkedin_link" class="form-control">
                                            </div>
                                        </div>
									</div>
									
									<?php /*
									<div class="col-12">
                                        
                                        <div class="form-group row mb-3">
                                            <label class="col-md-3 col-form-label" for="stripe_public_key"><?php echo get_phrase('stripe_public_key'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" id="stripe_public_key" name="stripe_public_key" class="form-control">
                                                <small><?php echo get_phrase("required_for_instructor"); ?></small>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-3">
                                            <label class="col-md-3 col-form-label" for="stripe_secret_key"><?php echo get_phrase('stripe_secret_key'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" id="stripe_secret_key" name="stripe_secret_key" class="form-control">
                                                <small><?php echo get_phrase("required_for_instructor"); ?></small>
                                            </div>
                                        </div>
                                    </div>
									
									*/ ?>
                                </div> <!-- end row -->
                            </div>
                            <div class="tab-pane" id="finish">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="text-center">
                                            <h2 class="mt-0"><i class="mdi mdi-check-circle mdi-48px text-success"></i></h2>
                                            <h3 class="mt-0">Click Submit</h3>

                                            <p class="w-75 mb-2 mx-auto">and the user would be added to site</p>

                                            <div class="mb-3">
                                                <button type="button" class="btn btn-primary btn-block text-center" onclick="checkRequiredFields()" name="button">
													<i class="mdi mdi-content-save mr-2"></i><?php echo get_phrase('submit'); ?>
												</button>
                                            </div>
                                        </div>
                                    </div> <!-- end col -->
                                </div> <!-- end row -->
                            </div>

                            <ul class="list-inline mb-0 wizard text-center mt-3">
								<li class="previous list-inline-item float-left">
                                    <a href="javascript::" class="btn btn-info"><i class="mdi mdi-arrow-left-bold-box mdi-18px mr-1"></i>Previous</a>
                                </li>
                                <li class="next list-inline-item float-right">
                                    <a href="javascript::" class="btn btn-info">Next<i class="mdi mdi-arrow-right-bold-box mdi-18px ml-1"></i></a>
                                </li>
                            </ul>

                        </div> <!-- tab-content -->
                    </div> <!-- end #progressbarwizard-->
                </form>

            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div>
</div>
