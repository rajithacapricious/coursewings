<?php /*
  $curl_enabled = function_exists('curl_version');
*/ ?>


<div class="row">
		<div class="col-12">
			<ol class="breadcrumb yellow mt-2">
				<li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="mdi mdi-view-dashboard mr-1"></i><?php echo get_phrase('admin_dashboard'); ?></a></li>
				<li class="breadcrumb-item active "><a class="text-dark" href="#"><i class="mdi mdi-fountain-pen-tip mr-1"></i><?php echo get_phrase('update_statutory_pages'); ?></a></li>
			</ol>
		</div>
	</div>
  
  <div class="row justify-content-center">
    <div class="col-xl-12">
        <div class="card">
			<div class="card-header purple">
			
				<div class="row">
					<div class="col-12 col-xl-12">
						<h4 class="header-title mt-1"><i class="mdi mdi-airplay mr-1"></i><?php echo get_phrase('update_about_us_/_terms_&_conditions_/_privacy_policy_content');?></h4>                         
					</div>
				</div>
			</div>
            <div class="card-body">
                <form class="required-form" action="<?php echo site_url('admin/frontend_settings/frontend_update2'); ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <h4><label for="about_us"><?php echo get_phrase('about_us'); ?></label></h4>
                        <textarea name="about_us" id = "about_us" class="form-control" rows="5"><?php echo get_frontend_settings('about_us'); ?></textarea>
                    </div>
					<hr>
                    <div class="form-group">
                        <h4><label for="terms_and_condition"><?php echo get_phrase('terms_and_condition'); ?></label></h4>
                        <textarea name="terms_and_condition" id ="terms_and_condition" class="form-control" rows="5"><?php echo get_frontend_settings('terms_and_condition'); ?></textarea>
                    </div>
					<hr>
                    <div class="form-group">
                        <h4><label for="privacy_policy"><?php echo get_phrase('privacy_policy'); ?></label></h4>
                        <textarea name="privacy_policy" id = "privacy_policy" class="form-control" rows="10"><?php echo get_frontend_settings('privacy_policy'); ?></textarea>
                    </div>

                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-primary btn-block" onclick="checkRequiredFields()"><i class="mdi mdi-content-save mr-1"></i><?php echo get_phrase('update_statutory_pages'); ?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
  $(document).ready(function () {
    initSummerNote(['#about_us', '#terms_and_condition', '#privacy_policy']);
	height: 300
  });
</script>

<?php /*
  <!-- start page title -->
  <div class="row ">
    <div class="col-xl-12">
      <div class="card">
        <div class="card-body">
          <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo get_phrase('about_this_application'); ?></h4>
        </div> <!-- end card body-->
      </div> <!-- end card -->
    </div><!-- end col-->
  </div>

  <div class="row justify-content-center">
    <div class="col-xl-8">
      <div class="card cta-box">
        <div class="card-body">
          <div class="media align-items-center">
            <div class="media-body">
              <div class="chart-widget-list">
                <p>
                  <i class="mdi mdi-square"></i> <?php echo get_phrase('software_version'); ?>
                  <span class="float-right"><?php echo get_settings('version'); ?></span>
                </p>
                <p>
                  <i class="mdi mdi-square"></i> <?php echo get_phrase('check_update'); ?>
                  <span class="float-right">
                      <a href="https://etcnet.net/"
                        target="_blank" style="color: #343a40;">
                          <i class="mdi mdi-telegram"></i>
                            <?php echo get_phrase('check_update'); ?>
                      </a>
                  </span>
                </p>
                <p>
                  <i class="mdi mdi-square"></i> <?php echo get_phrase('php_version'); ?>
                  <span class="float-right"><?php echo phpversion(); ?></span>
                </p>
                <p class="mb-0">
                  <i class="mdi mdi-square"></i> <?php echo get_phrase('curl_enable') ?>
                  <span class="float-right">
                    <?php echo $curl_enabled ? '<span class="badge badge-success-lighten">'.get_phrase('enabled').'</span>' : '<span class="badge badge-danger-lighten">'.get_phrase('disabled').'</span>'; ?>
                  </span>
                </p>

                <p style="margin-top: 8px;">
                  <i class="mdi mdi-square"></i> <?php echo get_phrase('purchase_code'); ?>
                  <span class="float-right"><?php echo get_settings('purchase_code'); ?></span>
                </p>
                <p>
                  <i class="mdi mdi-square"></i> <?php echo get_phrase('purchase_code_status'); ?>
                  <span class="float-right">
                    <?php if (strtolower($application_details['purchase_code_status']) == 'expired'): ?>
                      <span class="badge badge-danger-lighten"><?php echo $application_details['purchase_code_status']; ?></span>
                    <?php elseif (strtolower($application_details['purchase_code_status']) == 'valid'): ?>
                      <span class="badge badge-success-lighten"><?php echo $application_details['purchase_code_status']; ?></span>
                    <?php else: ?>
                      <span class="badge badge-danger-lighten"><?php echo ucfirst($application_details['purchase_code_status']); ?></span>
                    <?php endif; ?>
                  </span>
                </p>
                <p>
                  <i class="mdi mdi-square"></i> <?php echo get_phrase('support_expiry_date'); ?>

                    <?php if ($application_details['support_expiry_date'] != "invalid"): ?>
                        <span class="float-right"><?php echo $application_details['support_expiry_date']; ?></span>
                    <?php else: ?>
                        <span class="float-right"><span class="badge badge-danger-lighten"><?php echo ucfirst($application_details['support_expiry_date']); ?></span></span>
                    <?php endif; ?>
                </p>
                <p class="mb-0">
                  <i class="mdi mdi-square"></i> <?php echo get_phrase('customer_name') ?>
                  <?php if ($application_details['customer_name'] != "invalid"): ?>
                      <span class="float-right"><?php echo $application_details['customer_name']; ?></span>
                  <?php else: ?>
                      <span class="float-right"><span class="badge badge-danger-lighten"><?php echo ucfirst($application_details['customer_name']); ?></span></span>
                  <?php endif; ?>
                </p>
                <p style="margin-top: 8px;">
                  <i class="mdi mdi-square"></i> <?php echo get_phrase('get_customer_support'); ?>
                  <span class="float-right"><a href="http://www.etcnet.net" target="_blank" style="color: #343a40;"> <i class="mdi mdi-telegram"></i> <?php echo get_phrase('support'); ?> </a> </span>
                </p>
              </div>
            </div>
            <img class="ml-3" src="<?php echo base_url('assets/backend/images/report.svg'); ?>" width="120" alt="Generic placeholder image">
          </div>
        </div>
      </div>
    </div>
  </div>
*/ ?>