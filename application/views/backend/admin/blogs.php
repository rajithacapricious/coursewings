<style>
/*switch checkbox*/
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
  margin-bottom: 0;
}
.switch input {display:none;}


.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}
.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}
input:checked + .slider {
  background-color: #009840;
}
input:focus + .slider {
  box-shadow: 0 0 1px #009840;
}
input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}
/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}
.slider.round:before {
  border-radius: 50%;
}
</style>
<div class="row">
    <div class="col-12">
        <ol class="breadcrumb yellow mt-2">
            <li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="mdi mdi-view-dashboard mr-1"></i><?php echo get_phrase('admin_dashboard'); ?></a></li>
            <li class="breadcrumb-item active "><a class="text-dark" href="#"><i class="mdi mdi-account-details mr-1"></i><?php echo get_phrase('testimonials'); ?></a></li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-header purple">

                <div class="row">
                    <div class="col-6 col-xl-8">
                        <h4 class="header-title mt-2"><i class="mdi mdi-book-open mr-1"></i><?php echo get_phrase('blogs'); ?></h4>
                    </div>
                    <div class="col-6 col-xl-4">
						<div class="text-lg-right">
							<a href="<?php echo base_url('admin/blog_form') ?>" class="btn btn-xs bg-yellow mb-0 mt-1"><i class="mdi mdi-plus"></i>Add New Blog</a>
						</div>
					</div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive1 mt-1">
                    <?php if (count($blogs->result_array()) > 0) : ?>
                        <table id="basic1-datatable" class="table table-striped dt-responsive1 nowrap1" width="100%" data-page-length='10'>
                            <thead class="bg-yellow">
                                <tr>
                                    <th data-orderable="false" style="width:2%"><?php echo get_phrase('ID'); ?></th>
                                    <th data-orderable="false" style="width:75%"><?php echo get_phrase('title'); ?></th>
                                    <th data-orderable="false"><?php echo get_phrase('published'); ?></th>
                                    <th data-orderable="false"  style="width:20%"><?php echo get_phrase('actions'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($blogs->result_array() as $enrol) :
                                    //$user_data = $this->db->get_where('users', array('id' => $enrol['user_id']))->row_array();
                                    ?>
                                    <tr class="gradeU">
                                        <td>
                                            <?php echo $enrol['id']; ?>
                                        </td>
                                        <td>
                                            <div class="media">
                                            <?php if(!empty($enrol['image'])): ?>    
                                                <img src="<?php echo base_url('uploads/blog_images/').$enrol['image']; ?>" alt="<?php echo $enrol['title']; ?>" width="200" class="img-fluid img-thumbnail authorimg mr-1">
                                            <?php else: ?>
                                                <?php $blog_media_placeholders = themeConfiguration(get_frontend_settings('theme'), 'blog_media_placeholders'); ?>
                                                <img src="<?php echo base_url().$blog_media_placeholders['blog_image_placeholder']; ?>" alt="<?php echo $enrol['title']; ?>" width="200" class="img-fluid img-thumbnail authorimg mr-1">
                                            <?php endif; ?>
                                                <p class="media-body mb-0">
                                                    <strong class="d-block"><a><?php echo $enrol['title']; ?></a></strong>
                                                    <small><strong>Created on: </strong><?php echo date('d-M-Y, h:i A',strtotime($enrol['created_date'])); ?></small>
                                                </p>
                                            </div>
                                        </td>
                                        <td>
                                        <?php $status = ($enrol['status']) ? 'checked' : ''; ?>
                                        <label class="switch">
                                            <input type="checkbox" <?php echo $status ?> name="status" class="blog_status" value="<?php echo $enrol['id']; ?>">
                                            <span class="slider round"></span>
                                        </label>
                                        </td>
										<td>
											<a class="btn btn-xs2 bg-blue text-light" href="<?php echo site_url('admin/blog_form/' . $enrol['id']); ?>">
												<i class="mdi mdi-pencil mr-1"></i><?php echo get_phrase('edit'); ?>
											</a>
											<button type="button" class="btn btn-xs2 btn-danger text-light" onclick="confirm_modal('<?php echo site_url('admin/blog_delete/' . $enrol['id']); ?>');">
                                                <i class="mdi mdi-delete mr-1"></i><?php echo get_phrase('delete'); ?>
                                            </button>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php endif; ?>
                    <?php if (count($blogs->result_array()) == 0) : ?>
                        <div class="img-fluid w-100 text-center">
                            <img style="opacity: 1; width: 100px;" src="<?php echo base_url('assets/backend/images/file-search.svg'); ?>"><br>
                            <?php echo get_phrase('no_data_found'); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<script>
    //client on off
    $( ".blog_status" ).change(function() {
    var value = 'id='+$(this).val();
      $.ajax({
            url: "<?php echo base_url('admin/blog_active_deactive') ?>",
            type: "GET",
            data: value ,
            success: function (response) {
               console.log(response);
            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log(textStatus, errorThrown);
            }
        });
  });
</script>