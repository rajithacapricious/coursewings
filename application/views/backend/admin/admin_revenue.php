<div class="row">
	<div class="col-12">
		<ol class="breadcrumb yellow mt-2">
			<li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="mdi mdi-view-dashboard mr-1"></i><?php echo get_phrase('admin_dashboard'); ?></a></li>
			<li class="breadcrumb-item active "><a class="text-dark" href="#"><i class="mdi mdi-cash-multiple mr-1"></i><?php echo get_phrase('coursewings_revenue'); ?></a></li>
		</ol>
	</div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
			<div class="card-header purple">
			
				<div class="row">
					<div class="col-12 col-xl-6">
						<h4 class="header-title mt-2"><i class="mdi mdi-account-details mr-1"></i><?php echo get_phrase('coursewings_revenue'); ?></h4>                         
					</div>
					<div class="col-12 col-xl-6">
						<div class="row text-lg-right mt-1">
							<form class="form-inline col-xl-12" action="<?php echo site_url('admin/admin_revenue/filter_by_date_range') ?>" method="get">
								<div class="col-xl-8">
									<div class="form-group">
										<div id="reportrange" class="form-control mb-0" data-toggle="date-picker-range" data-target-display="#selectedValue"  data-cancel-class="btn-light" style="width: 100%;">
											<i class="mdi mdi-calendar"></i>&nbsp;
											<span id="selectedValue"><?php echo date("F d, Y" , $timestamp_start) . " - " . date("F d, Y" , $timestamp_end);?></span> <i class="mdi mdi-menu-down"></i>
										</div>
										<input id="date_range" type="hidden" name="date_range" value="<?php echo date("d F, Y" , $timestamp_start) . " - " . date("d F, Y" , $timestamp_end);?>">
									</div>
								</div>
								<div class="col-xl-4">
									<button type="submit" class="btn btn-info btn-block mb-0" id="submit-button" onclick="update_date_range();"><i class="mdi mdi-air-filter mr-1"></i><?php echo get_phrase('filter');?></button>
								</div>
							</form>
						</div>
					</div><!-- end col-->
				</div>
			</div>
			
			
			
            <div class="card-body">
                <div class="table-responsive-sm mt-1">
                    <table id="basic-datatable" class="table table-striped dt-responsive nowrap" width="100%" data-page-length='10'>
                        <thead class="bg-yellow">
                            <tr>
                                <th><?php echo get_phrase('course_sold'); ?></th>
								<th><?php echo get_phrase('purchaser'); ?></th>
                                <th class="text-center"><?php echo get_phrase('total_amount'); ?></th>
                                <th class="text-center"><?php echo get_phrase('admin_revenue'); ?></th>
                                <th><?php echo get_phrase('purchase_date'); ?></th>
                                <th><?php echo get_phrase('actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($payment_history as $payment):
                                $user_data = $this->db->get_where('users', array('id' => $payment['user_id']))->row_array();
                                $course_data = $this->db->get_where('course', array('id' => $payment['course_id']))->row_array();
								$instructor_details = $this->user_model->get_all_user($course_data['user_id'])->row_array();
								?>
                                <tr class="gradeU">
                                    <td>
										<strong><a href="<?php echo site_url('admin/course_form/course_edit/'.$course_data['id']); ?>" target="_blank"><?php echo ellipsis($course_data['title']); ?></a></strong><br>
										<span class="badge bg-blue text-light mr-2"><i class="mdi mdi-teach mr-1"></i>Author:&nbsp;<?php echo $instructor_details['first_name'].' '.$instructor_details['last_name']; ?></span>
									</td>
									<td><?php echo $user_data['first_name'].' '.$user_data['last_name']; ?></td>
                                    <td class="text-center"><?php echo currency($payment['amount']); ?></td>
                                    <td class="text-center"><?php echo currency($payment['admin_revenue']); ?></td>
                                    <td><?php echo date('D, dS M, y', $payment['date_added']); ?></td>
                                    <td>
                                        <button type="button" class="btn btn-xs2 btn-danger text-light" onclick="confirm_modal('<?php echo site_url('admin/payment_history_delete/'.$payment['id'].'/admin_revenue'); ?>');"><i class="mdi mdi-delete mr-1"></i>Delete</button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
function update_date_range()
{
    var x = $("#selectedValue").html();
    $("#date_range").val(x);
}
</script>
