<div class="row">
	<div class="col-12">
		<ol class="breadcrumb yellow mt-2">
			<li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="mdi mdi-view-dashboard mr-1"></i><?php echo get_phrase('admin_dashboard'); ?></a></li>
			<li class="breadcrumb-item active"><i class="mdi mdi-shape mr-1"></i><?php echo get_phrase('categories'); ?></li>
		</ol>
	</div>
</div>


<div class="row">

	<div class="col-xl-12">
        <div class="card">
			<div class="card-header purple">
			
				<div class="row">
					<div class="col-6 col-xl-8">
						<h4 class="header-title mt-1"><i class="mdi mdi-shape mr-1"></i><?php echo get_phrase('categories'); ?></h4><small>Please be carefull while deleting as they may be courses & subscribers linked to a category or subcategory</small>                         
					</div>
					<div class="col-6 col-xl-4">
						<div class="text-lg-right mt-1">
							
							<a href="<?php echo site_url('admin/category_form/add_category'); ?>" class="btn btn-xs bg-yellow mb-0"><i class="mdi mdi-plus"></i><?php echo get_phrase('add_new_category'); ?></a>
						</div>
					</div><!-- end col-->
				</div>
			</div>
			
			<div class="card-body">
				<div class="row">
					<?php foreach ($categories->result_array() as $category):
						if($category['parent'] > 0)
						 continue;
						 $sub_categories = $this->crud_model->get_sub_categories($category['id']); ?>
					<div class="col-md-6 col-lg-6 col-xl-4" id = "<?php echo $category['id']; ?>">
						<div class="card d-block">
							<?php /*<img class="card-img-top d-none" src="echo base_url('uploads/thumbnails/category_thumbnails/'.$category['thumbnail']);" alt="Card image cap"> */ ?>
							<div class="card-body bg-yellow px-3 py-2">
								<h4 class="card-title text-primary mb-0"><i class="<?php echo $category['font_awesome_class']; ?> mr-1"></i> <?php echo $category['name']; ?></h4>
								<div>
									<a href = "<?php echo site_url('admin/category_form/edit_category/'.$category['id']); ?>" class="btn btn-icon btn-info btn-xs mb-0 mt-2" id = "category-edit-btn-<?php echo $category['id']; ?>" style="" style="margin-right:5px;" data-toggle="tooltip" data-placement="top" title="Edit Category">
										<i class="mdi mdi-wrench"></i> <?php echo get_phrase('edit'); ?>
									</a>
									<a href = "#" class="btn btn-icon bg-danger text-light btn-xs mb-0 mt-2" id = "category-delete-btn-<?php echo $category['id']; ?>"style="float: right;" onclick="confirm_modal('<?php echo site_url('admin/categories/delete/'.$category['id']); ?>');" style="margin-right:5px;" data-toggle="tooltip" data-placement="top" title="Delete Category">
										<i class="mdi mdi-delete"></i> <?php echo get_phrase('delete'); ?>
									</a>
								</div>
							</div>

							<ul class="list-group list-group-flush">
								<li class="list-group-item bg-primary text-light">
									<?php echo count($sub_categories).' '.get_phrase('sub_categories'); ?><i class="fas fa-level-down-alt ml-2"></i>
								</li>
								<?php foreach ($sub_categories as $sub_category): ?>
								<li class="list-group-item" id = "<?php echo $sub_category['id']; ?>">
									<span><i class="<?php echo $sub_category['font_awesome_class']; ?>"></i> <?php echo $sub_category['name']; ?></span>
									<span class="category-action" id = 'category-delete-btn-<?php echo $sub_category['id']; ?>' style="float: right; margin-left: 5px; height: 20px;">
										<a href="javascript::" class="btn bg-danger text-light btn-xs" onclick="confirm_modal('<?php echo site_url('admin/categories/delete/'.$sub_category['id']); ?>');" data-toggle="tooltip" data-placement="top" title="Delete Subcategory"> <i class="mdi mdi-delete" style="font-size: 18px;"></i></a>
									</span>
									<span class="category-action" id = 'category-edit-btn-<?php echo $sub_category['id']; ?>' style="float: right; height: 20px;">
										<a href="<?php echo site_url('admin/category_form/edit_category/'.$sub_category['id']); ?>" class="btn bg-info text-light btn-xs" data-toggle="tooltip" data-placement="top" title="Edit Subcategory"> <i class="mdi mdi-pencil" style="font-size: 18px;"></i></a>
									</span>
								</li>
								<?php endforeach; ?>
							</ul>
							<div class="card-body">
								<a href = "<?php echo site_url('admin/category_form/edit_category/'.$category['id']); ?>" class="btn btn-icon btn-outline-info btn-sm" id = "category-edit-btn-<?php echo $category['id']; ?>" style="display: none;" style="margin-right:5px;">
									<i class="mdi mdi-wrench"></i> <?php echo get_phrase('edit'); ?>
								</a>
								<a href = "#" class="btn btn-icon btn-outline-danger btn-sm" id = "category-delete-btn-<?php echo $category['id']; ?>"style="float: right; display: none;" onclick="confirm_modal('<?php echo site_url('admin/categories/delete/'.$category['id']); ?>');" style="margin-right:5px;">
									<i class="mdi mdi-delete"></i> <?php echo get_phrase('delete'); ?>
								</a>
							</div> <!-- end card-body-->
						</div> <!-- end card-->
					</div>
					<?php endforeach; ?>
				</div>
			
			</div>
		
		
		</div>
	</div>





    
</div>

<script type="text/javascript">
    $('.on-hover-action').mouseenter(function() {
        var id = this.id;
        $('#category-delete-btn-'+id).show();
        $('#category-edit-btn-'+id).show();
    });
    $('.on-hover-action').mouseleave(function() {
        var id = this.id;
        $('#category-delete-btn-'+id).hide();
        $('#category-edit-btn-'+id).hide();
    });
</script>
