<style>
/*switch checkbox*/
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
  margin-bottom: 0;
}
.switch input {display:none;}


.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}
.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}
input:checked + .slider {
  background-color: #009840;
}
input:focus + .slider {
  box-shadow: 0 0 1px #009840;
}
input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}
/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}
.slider.round:before {
  border-radius: 50%;
}
</style>
<div class="row">
    <div class="col-12">
        <ol class="breadcrumb yellow mt-2">
            <li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="mdi mdi-view-dashboard mr-1"></i><?php echo get_phrase('admin_dashboard'); ?></a></li>
            <li class="breadcrumb-item active "><a class="text-dark" href="#"><i class="mdi mdi-account-details mr-1"></i><?php echo get_phrase('testimonials'); ?></a></li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-header purple">

                <div class="row">
                    <div class="col-12 col-xl-7">
                        <h4 class="header-title mt-2"><i class="mdi mdi-book-open mr-1"></i><?php echo get_phrase('testimonials'); ?></h4>
						<small>Please publish only 4, as only 4 testimonials would display on home page.</small>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive1 mt-1">
                    <?php if (count($testimonials->result_array()) > 0) : ?>
                        <table id="basic1-datatable" class="table table-striped dt-responsive1 nowrap1" width="100%" data-page-length='10'>
                            <thead class="bg-yellow">
                                <tr>
                                    <th data-orderable="false" style="width:25%"><?php echo get_phrase('user_name'); ?></th>
                                    <th data-orderable="false"><?php echo get_phrase('text'); ?></th>
									<th data-orderable="false"><?php echo get_phrase('status'); ?></th>
                                    <th data-orderable="false"  style="width:15%"><?php echo get_phrase('actions'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($testimonials->result_array() as $enrol) :
                                    $user_data = $this->db->get_where('users', array('id' => $enrol['user_id']))->row_array();
                                    ?>
                                    <tr class="gradeU">
                                        <td>
                                            <div class="media">
                                                <img src="<?php echo $this->user_model->get_user_image_url($enrol['user_id']); ?>" alt="" width="50" class="img-fluid img-thumbnail authorimg mr-1">
                                                <p class="media-body mb-0">
                                                    <strong class="d-block"><a><?php echo $user_data['first_name'] . ' ' . $user_data['last_name']; ?></a></strong>
                                                    <a href="mailto:<?php echo $user_data['email']; ?>"><i class="mdi mdi-email-outline mr-1"></i><?php echo $user_data['email']; ?></a> <br>
                                                    <small><?php echo date('d-M-Y, h:i A',strtotime($enrol['last_modified_at'])); ?></small>
                                                </p>
                                            </div>
                                        </td>
                                        <td>
                                            <?php echo nl2br(urldecode(strip_slashes($enrol['text']))); ?>
                                        </td>
                                        <td>
                                        <?php $status = ($enrol['status']) ? 'checked' : ''; ?>
                                        <label class="switch">
                                            <input type="checkbox" <?php echo $status ?> name="status" class="testimonial_status" value="<?php echo $enrol['id']; ?>">
                                            <span class="slider round"></span>
                                        </label>
                                            <?php /* ?>
                                            <button type="button" class="btn btn-danger btn-icon btn-sm" onclick="confirm_modal('<?php echo site_url('admin/enrol_history_delete/' . $enrol['id']); ?>');">
                                                <i class="mdi mdi-delete-empty mr-1"></i><?php echo get_phrase('cancel_subsciption'); ?>
                                            </button>
                                            <?php */ ?>
                                        </td>
										<td>
											<a class="btn btn-xs2 bg-blue text-light" href="<?php echo site_url('admin/testimonial_edit/' . $enrol['id']); ?>">
												<i class="mdi mdi-pencil mr-1"></i><?php echo get_phrase('edit'); ?>
											</a>
											<button type="button" class="btn btn-xs2 btn-danger text-light" onclick="confirm_modal('<?php echo site_url('admin/testimonial_delete/' . $enrol['id']); ?>');">
                                                <i class="mdi mdi-delete mr-1"></i><?php echo get_phrase('delete'); ?>
                                            </button>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php endif; ?>
                    <?php if (count($testimonials->result_array()) == 0) : ?>
                        <div class="img-fluid w-100 text-center">
                            <img style="opacity: 1; width: 100px;" src="<?php echo base_url('assets/backend/images/file-search.svg'); ?>"><br>
                            <?php echo get_phrase('no_data_found'); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<script>
    //client on off
    $( ".testimonial_status" ).change(function() {
    var value = 'id='+$(this).val();
      $.ajax({
            url: "<?php echo base_url('admin/testimonial_active_deactive') ?>",
            type: "GET",
            data: value ,
            success: function (response) {
               console.log(response);
            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log(textStatus, errorThrown);
            }
        });
  });
</script>