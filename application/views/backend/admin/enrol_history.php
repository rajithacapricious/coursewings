<div class="row">
	<div class="col-12">
		<ol class="breadcrumb yellow mt-2">
			<li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="mdi mdi-view-dashboard mr-1"></i><?php echo get_phrase('admin_dashboard'); ?></a></li>
			<li class="breadcrumb-item active "><a class="text-dark" href="#"><i class="mdi mdi-account-details mr-1"></i><?php echo get_phrase('subscription_history'); ?></a></li>
		</ol>
	</div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
			<div class="card-header purple">
			
				<div class="row">
					<div class="col-12 col-xl-6">
						<h4 class="header-title mt-2"><i class="mdi mdi-account-details mr-1"></i><?php echo get_phrase('subscription_history'); ?></h4>                         
					</div>
					<div class="col-12 col-xl-6">
						<div class="row text-lg-right mt-1">
							<form class="form-inline col-xl-12" action="<?php echo site_url('admin/enrol_history/filter_by_date_range') ?>" method="get">
								  <div class="col-xl-8">
									  <div class="form-group">
										  <div id="reportrange" class="form-control mb-0 text-center" data-toggle="date-picker-range" data-target-display="#selectedValue"  data-cancel-class="btn-light" style="width: 100%;">
											  <i class="mdi mdi-calendar"></i>&nbsp;
											  <span id="selectedValue"><?php echo date("F d, Y" , $timestamp_start) . " - " . date("F d, Y" , $timestamp_end);?></span>
										  </div>
										  <input id="date_range" type="hidden" name="date_range" value="<?php echo date("d F, Y" , $timestamp_start) . " - " . date("d F, Y" , $timestamp_end);?>">
									  </div>
								  </div>
								  <div class="col-xl-4">
									  <button type="submit" class="btn btn-info btn-block mb-0" id="submit-button" onclick="update_date_range();"><i class="mdi mdi-air-filter mr-1"></i><?php echo get_phrase('filter');?></button>
								  </div>
								  
							  </form>
						</div>
					</div><!-- end col-->
				</div>
			</div>
            <div class="card-body">
              <div class="table-responsive mt-1">
                  <?php if (count($enrol_history->result_array()) > 0): ?>
                      <table  id="basic-datatable" class="table table-striped dt-responsive nowrap" width="100%" data-page-length='10'>
                          <thead class="bg-yellow">
                              <tr>
                                  <th><?php echo get_phrase('subscriber_name'); ?></th>
                                  <th><?php echo get_phrase('subscribed_course'); ?></th>
                                  <th><?php echo get_phrase('actions'); ?></th>
                              </tr>
                          </thead>
                          <tbody>
                              <?php foreach ($enrol_history->result_array() as $enrol):
                                  $user_data = $this->db->get_where('users', array('id' => $enrol['user_id']))->row_array();
                                  $course_data = $this->db->get_where('course', array('id' => $enrol['course_id']))->row_array();?>
                                  <tr class="gradeU">
                                      <td>
										<div class="media">
											<img src="<?php echo $this->user_model->get_user_image_url($enrol['user_id']); ?>" alt="" height="50" width="50" class="img-fluid img-thumbnail authorimg mr-1">
											<p class="media-body mb-0">
												<strong class="d-block"><a><?php echo $user_data['first_name'].' '.$user_data['last_name']; ?></a></strong>
												<a href="mailto:<?php echo $user_data['email']; ?>"><i class="mdi mdi-email-outline mr-1"></i><?php echo $user_data['email']; ?></a>
											</p>
										</div>
                                      </td>
										<td>
											<strong>
												<a href="<?php echo site_url('admin/course_form/course_edit/'.$course_data['id']); ?>" target="_blank"><?php echo ellipsis($course_data['title']); ?></a>
											</strong><br>
											<span><?php echo date('D, d-M-y', $enrol['date_added']); ?></span>
											<span>|</span>
											<?php if ($course_data['is_free_course'] == null): ?>
                                            <?php if ($course_data['discount_flag'] == 1): ?>
                                                <span class="badge badge-primary" style="font-size:14px;"><?php echo currency($course_data['discounted_price']); ?></span>
                                            <?php else: ?>
                                                <span class="badge badge-primary" style="font-size:14px;"><?php echo currency($course_data['price']); ?></span>
                                            <?php endif; ?>
												<?php elseif ($course_data['is_free_course'] == 1):?>
													<span class="badge bg-pink text-light" style="font-size:14px;"><?php echo get_phrase('free'); ?></span>
											<?php endif; ?>
											
										</td>
                                     
                                      <td>
											<button type="button" class="btn btn-danger btn-icon btn-sm" onclick="confirm_modal('<?php echo site_url('admin/enrol_history_delete/'.$enrol['id']); ?>');"> 
												<i class="mdi mdi-delete-empty mr-1"></i><?php echo get_phrase('cancel_subsciption'); ?>
											</button>
                                      </td>
                                  </tr>
                              <?php endforeach; ?>
                          </tbody>
                      </table>
                  <?php endif; ?>
                  <?php if (count($enrol_history->result_array()) == 0): ?>
                      <div class="img-fluid w-100 text-center">
                        <img style="opacity: 1; width: 100px;" src="<?php echo base_url('assets/backend/images/file-search.svg'); ?>"><br>
                        <?php echo get_phrase('no_data_found'); ?>
                      </div>
                  <?php endif; ?>
              </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<script type="text/javascript">
    function update_date_range()
    {
        var x = $("#selectedValue").html();
        $("#date_range").val(x);
    }
</script>
<script type="text/javascript">

</script>