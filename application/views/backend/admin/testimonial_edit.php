<div class="row">
  <div class="col-12">
    <ol class="breadcrumb yellow mt-2">
      <li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="mdi mdi-view-dashboard mr-1"></i><?php echo get_phrase('admin_dashboard'); ?></a></li>
      <li class="breadcrumb-item active "><a class="text-dark" href="<?php echo site_url('admin/testimonials') ?>"><i class="mdi mdi-fountain-pen-tip mr-1"></i><?php echo get_phrase('testimonials'); ?></a></li>
      <li class="breadcrumb-item active "><a class="text-dark" href="#"><i class="mdi mdi-fountain-pen-tip mr-1"></i><?php echo get_phrase('testimonial_edit'); ?></a></li>
    </ol>
  </div>
</div>

<?php $user_data = $this->db->get_where('users', array('id' => $result['user_id']))->row_array(); ?>
<div class="row justify-content-center">
  <div class="col-xl-12">
    <div class="card">
      <div class="card-header purple">
        <div class="row">
          <div class="col-12 col-xl-12">
            <h4 class="header-title mt-1"><i class="mdi mdi-airplay mr-1"></i><?php echo get_phrase('update /_edit_testimonials'); ?></h4>
          </div>
        </div>
      </div>
      <div class="card-body">
        <form class="required-form" action="<?php echo site_url('admin/testimonail_store/'.$result['id']); ?>" method="post" enctype="multipart/form-data">
          <div class="form-group">
            
            <div class="row mb-1">
              <div class="col-sm-12">
                <div class="media">
                  <img src="<?php echo $this->user_model->get_user_image_url($result['user_id']); ?>" alt="" width="50" class="img-fluid img-thumbnail authorimg mr-1">
                  <p class="media-body mb-0">
                    <strong class="d-block"><a><?php echo $user_data['first_name'] . ' ' . $user_data['last_name']; ?></a></strong>
                    <a href="mailto:<?php echo $user_data['email']; ?>"><i class="mdi mdi-email-outline mr-1"></i><?php echo $user_data['email']; ?></a>
                  </p>
                </div>
              </div>
            </div>
            <textarea rows="10" name="testimonial_text" id="testimonial_text" maxlength="1000" class="form-control" style="height: auto;" required="required"><?php echo urldecode(strip_slashes($result['text'])); ?></textarea>
          </div>

          <div class="row justify-content-center">
            <div class="col-md-12">
              <button type="submit" class="btn btn-primary btn-block"><i class="mdi mdi-content-save mr-1"></i><?php echo get_phrase('update_testimonial'); ?></button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
