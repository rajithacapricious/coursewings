<div class="row">
    <div class="col-12">
        <ol class="breadcrumb yellow mt-2">
            <li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="mdi mdi-view-dashboard mr-1"></i><?php echo get_phrase('admin_dashboard'); ?></a></li>
            <li class="breadcrumb-item"><a href="<?php echo site_url('admin/blogs'); ?>"><i class="mdi mdi-shape mr-1"></i><?php echo get_phrase('blogs'); ?></a></li>
            <li class="breadcrumb-item active"><i class="mdi mdi-plus-box mr-1"></i><?php echo get_phrase('add_new_blog'); ?></li>
        </ol>
    </div>
</div>

<div class="row justify-content-center">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-header purple">
                <h4 class="header-title mt-1"><i class="mdi mdi-shape mr-1"></i><?php echo get_phrase('blog_add_form'); ?></small></h4>
            </div>
            <div class="card-body">
                <?php
                $attributes = array('name' => 'blog-form', 'class' => 'required-form', 'enctype' => 'multipart/form-data');
                if (!empty($result)) {
                    $hidden = array('modified_by' => $this->session->userdata('user_id'));
                    echo form_open('admin/blog_store/' . $result['id'], $attributes, $hidden);
                } else {
                    $hidden = array('created_by' => $this->session->userdata('user_id'));
                    echo form_open('admin/blog_store', $attributes, $hidden);
                }
                ?>
                <div class="row">
                    <div class="col-lg-8">
                        <div class="form-group row mb-1">
                            <label class="col-lg-12 col-form-label" for="title">Blog Title <span class="required">*</span></label>
                            <div class="col-lg-12">
                                <input type="text" class="form-control mb-0" id="title" name="title" maxlength="200" required value="<?php echo !empty($result) ? $result['title'] : ''; ?>">
                            </div>
                        </div>
                        <div class="form-group row mb-1">
                            <label class="col-lg-12 col-form-label" for="short_description">Short Description <span class="required">*</span></label>
                            <div class="col-lg-12">
                                <textarea name="short_description" id="short_description" class="form-control" required><?php echo !empty($result) ? html_entity_decode($result['short_description']) : ''; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group row mb-1">
                            <label class="col-lg-12 col-form-label" for="description">Description</label>
                            <div class="col-lg-12">
                                <textarea name="description" id="description" class="form-control"><?php echo !empty($result) ? html_entity_decode($result['description']) : ''; ?></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="col pr-0 pl-0 mb-1">
                            <div class="card widget-flat bg-purple text-white mb-1">
                                <div class="card-body">
                                    <div class="col-xl-12 justify-content-center">
                                        <div class="form-group row mb-0">
                                            <div class="offset-md-12">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="status" id="status" value="1" <?php echo !empty($result) ? (!empty($result['status']) ? 'checked' : '') : ''; ?>>
                                                    <label class="custom-control-label" for="status"><?php echo get_phrase('mark_this_if_you_want_to_publish'); ?></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row mb-1">
                            <label class="col-md-12 col-form-label" for="meta_keywords"><?php echo get_phrase('meta_keywords'); ?><small><i>&nbsp;&nbsp;(Press Enter after typing keyword)</i></small></label>
                            <div class="col-md-12">
                                <input type="text" class="form-control bootstrap-tag-input" id="meta_keywords" name="meta_keywords" value="<?php echo !empty($result) ? $result['meta_keywords'] : ''; ?>" placeholder="Eg. Marketing, Sales, Strategies etc." data-role="tagsinput" style="width: 100%;" />
                            </div>
                        </div>
                        <div class="form-group row mb-1">
                            <label class="col-md-12 col-form-label" for="meta_description"><?php echo get_phrase('meta_description'); ?></label>
                            <div class="col-md-12">
                                <textarea name="meta_description" placeholder="Write a short description of the course for search pages" class="form-control"><?php echo !empty($result) ? html_entity_decode($result['meta_description']) : ''; ?></textarea>
                            </div>
                        </div>
                        <!-- this portion will be generated theme wise from the theme-config.json file Starts-->
                        <?php include 'blog_media.php'; ?>
                        <!-- this portion will be generated theme wise from the theme-config.json file Ends-->
                    </div>
                    <div class="col-lg-12">
                        <button type="button" class="btn btn-block btn-primary mt-4" onclick="checkRequiredFields()"><i class="mdi mdi-content-save mr-1"></i><?php echo get_phrase("submit"); ?></button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<script type="text/javascript">
    $(document).ready(function() {
        initSummerNote(['#description']);
    });
</script>