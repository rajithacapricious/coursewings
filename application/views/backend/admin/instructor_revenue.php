<div class="row">
    <div class="col-12">
        <ol class="breadcrumb yellow mt-2">
            <li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="mdi mdi-view-dashboard mr-1"></i><?php echo get_phrase('admin_dashboard'); ?></a></li>
            <li class="breadcrumb-item active "><a class="text-dark" href="#"><i class="mdi mdi-cash mr-1"></i><?php echo get_phrase('instructor_revenue'); ?></a></li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-header purple">

                <div class="row">
                    <div class="col-12 col-xl-12">
                        <h4 class="header-title mt-1"><i class="mdi mdi-cart-plus mr-1"></i><?php echo get_phrase('instructor_revenue'); ?></h4>                         
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive-sm mt-4">
                    <table id="basic-datatable" class="table table-striped table-centered mb-0">
                        <thead class="bg-yellow">
                            <tr>
                                <th><?php echo get_phrase('subscribed_course'); ?></th>
                                <th><?php echo get_phrase('subscriber'); ?></th>
                                <th><?php echo get_phrase('instructor'); ?></th>
                                <th class="text-center"><?php echo get_phrase('instructor_revenue'); ?></th>
                                <th class="text-center"><?php echo get_phrase('payment_status'); ?></th>
                                <th class="text-center"><?php echo get_phrase('actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($payment_history as $payment):
                                $course_data = $this->db->get_where('course', array('id' => $payment['course_id']))->row_array();
                                $user_data = $this->db->get_where('users', array('id' => $course_data['user_id']))->row_array();

                                $buyer_data = $this->db->get_where('users', array('id' => $payment['user_id']))->row_array();
                                ?>


                                <?php
                                $paypal_keys = json_decode($user_data['paypal_keys'], true);
                                $stripe_keys = json_decode($user_data['stripe_keys'], true);
                                ?>
                                <tr class="gradeU">
                                    <td>
                                        <strong><a href="<?php echo site_url('home/course/' . slugify($course_data['title']) . '/' . $course_data['id']); ?>" target="_blank"><?php echo ellipsis($course_data['title']); ?></a></strong><br>
                                        <small class="text-muted"><?php echo get_phrase('gross_price') . ': ' . currency($payment['amount']); ?> | <?php echo get_phrase('net_price') . ': ' . currency($payment['instructor_revenue']); ?></small>
                                    </td>
                                    <td>
                                        <strong><?php echo $buyer_data['first_name'] . ' ' . $buyer_data['last_name']; ?><br></strong>
                                        <small class="text-muted"><?php echo get_phrase('buy_date') . ': ' . date('D, d-M-Y', $payment['date_added']); ?></small>
                                    </td>
                                    <td>
                                        <strong><?php echo $user_data['first_name'] . ' ' . $user_data['last_name']; ?><br></strong>
                                        <small><a href="mailto:<?php echo $user['email']; ?>"><i class="mdi mdi-email-outline mr-1"></i><?php echo $user_data['email']; ?></a></small>
                                    </td>
                                    <td class="text-center">
                                        <strong><?php echo currency($payment['instructor_revenue']); ?></strong>
                                    </td>
                                    <td style="text-align: center;">
                                        <?php if ($payment['instructor_payment_status'] == 0): ?>
                                            <div class="badge bg-danger text-white"><h6 class="my-0 mx-1"><?php echo get_phrase('pending'); ?></h6></div>
                                        <?php elseif ($payment['instructor_payment_status'] == 1): ?>
                                            <div class="badge bg-success text-white"><h6 class="my-0 mx-1"><?php echo get_phrase('paid'); ?></h6></div>
                                        <?php endif; ?>
                                    </td>
                                    <td style="text-align: center;">
                                        <?php if ($payment['instructor_payment_status'] == 0): ?>
                                            <?php if ($paypal_keys[0]['production_client_id'] != ""): ?>

                                                <?php if ($payment['instructor_revenue'] >= 100) { ?>
                                        <button type="button" class = "btn bg-yellow btn-sm text-black" onclick="alert('<?php echo str_replace(array("\n", "\r"), ' : ', $paypal_keys[0]['bank_details']!=''?$paypal_keys[0]['bank_details']:'Bank Details Empty!'); ?>');">Bank Details</button> 
                                                <?php } if ($payment['instructor_revenue'] >= 5) { ?>
                                                    <form action="<?php echo site_url('admin/paypal_checkout_for_instructor_revenue'); ?>" method="post">
                                                        <input type="hidden" name="amount_to_pay"        value="<?php echo $payment['instructor_revenue']; ?>">
                                                        <input type="hidden" name="payment_id"           value="<?php echo $payment['id']; ?>">
                                                        <input type="hidden" name="instructor_name"      value="<?php echo $user_data['first_name'] . ' ' . $user_data['last_name']; ?>">
                                                        <input type="hidden" name="course_title"         value="<?php echo $course_data['title']; ?>">
                                                        <input type="hidden" name="production_client_id" value="<?php echo $paypal_keys[0]['production_client_id']; ?>">
                                                        <input type="submit" class="btn bg-blue btn-sm text-white"  value="<?php echo get_phrase('pay_with_paypal'); ?>">
                                                    </form>
                                                <?php } else { ?>
                                                    Revenue must higher than 5USD
                                                <?php } ?>

                                            <?php else: ?>
                                                <?php if ($payment['instructor_revenue'] >= 100) { ?>
                                                    <button type="button" class = "btn bg-yellow btn-sm text-black" onclick="alert('<?php echo str_replace(array("\n", "\r"), '', $paypal_keys[0]['bank_details']!=''?$paypal_keys[0]['bank_details']:'Bank Details Empty!'); ?>');">Bank Details</button> 
                                                <?php } if ($payment['instructor_revenue'] >= 5) { ?>
                                                    <button type="button" class = "btn bg-blue btn-sm text-white" name="button" onclick="alert('<?php echo get_phrase('this_instructor_has_not_provided_valid_paypal_client_id'); ?>')"><i class="mdi mdi-paypal mr-1"></i><?php echo get_phrase('pay_with_paypal'); ?></button>
                                                    <?php } else { ?>
                                                    Revenue must higher than 5USD
                                                <?php } ?>
                                            <?php endif; ?>
                                            <?php /*
                                              <?php if ($stripe_keys[0]['public_live_key'] != "" && $stripe_keys[0]['secret_live_key']): ?>
                                              <form action="<?php echo site_url('admin/stripe_checkout_for_instructor_revenue'); ?>" method="post">
                                              <input type="hidden" name="amount_to_pay"   value="<?php echo $payment['instructor_revenue']; ?>">
                                              <input type="hidden" name="payment_id"      value="<?php echo $payment['id']; ?>">
                                              <input type="hidden" name="instructor_name" value="<?php echo $user_data['first_name'].' '.$user_data['last_name']; ?>">
                                              <input type="hidden" name="course_title"    value="<?php echo $course_data['title']; ?>">
                                              <input type="hidden" name="public_live_key" value="<?php echo $stripe_keys[0]['public_live_key']; ?>">
                                              <input type="hidden" name="secret_live_key" value="<?php echo $stripe_keys[0]['secret_live_key']; ?>">
                                              <input type="submit" class="btn btn-outline-info btn-sm btn-rounded"   value="<?php echo get_phrase('pay_with_stripe'); ?>">
                                              </form>
                                              <?php else: ?>
                                              <button type="button" class = "btn btn-outline-danger btn-sm btn-rounded" name="button" onclick="alert('<?php echo get_phrase('this_instructor_has_not_provided_valid_public_key_or_secret_key'); ?>')"><?php echo get_phrase('pay_with_stripe'); ?></button>
                                              <?php endif; ?>
                                              <?php else: ?>
                                              <a href="<?php echo site_url('admin/invoice/'.$payment['id']); ?>" class="btn btn-outline-primary btn-rounded btn-sm"><i class="mdi mdi-printer-settings"></i></a>
                                             */ ?>
                                        <?php endif; ?>

                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
