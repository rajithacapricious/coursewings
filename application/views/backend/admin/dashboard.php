<?php
    $status_wise_courses = $this->crud_model->get_status_wise_courses();
    $number_of_courses = $status_wise_courses['pending']->num_rows() + $status_wise_courses['active']->num_rows();
    $number_of_lessons = $this->crud_model->get_lessons()->num_rows();
    $number_of_enrolment = $this->crud_model->enrol_history()->num_rows();
    $number_of_students = $this->user_model->get_user()->num_rows();
?>


<div class="row">
	<div class="col-12">
		<ol class="breadcrumb yellow mt-2">
			<li class="breadcrumb-item active text-purple"><i class="mdi mdi-view-dashboard mr-1"></i><?php echo get_phrase('admin_dashboard'); ?></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xl-6">
		<div class="row">
			<div class="col-sm-6 col-xl-6">
				<a href="<?php echo site_url('admin/courses'); ?>">
					<div class="card cta-box bg-success text-white">
						<div class="card-body">
							<div class="widget-title">
								<?php echo get_phrase('total_courses'); ?>
							</div>
							<div class="media align-items-center">
								
								<div class="media-body">
									<i class="fas fa-book fa-6x bottomfaicon"></i>
								</div>
								<span class="mr-3" style="font-size: 3rem;margin-top: -10px;">
									<?php echo $number_of_courses; ?>
								</span>
							</div>
						</div>
					</div>
				</a>
			</div>
			
			<div class="col-sm-6 col-xl-6">
				<a href="<?php echo site_url('admin/courses'); ?>">
					<div class="card cta-box bg-pink text-white">
						<div class="card-body">
							<div class="widget-title">
								<?php echo get_phrase('number_of_lessons'); ?>
							</div>
							<div class="media align-items-center">
								
								<div class="media-body">
									<i class="mdi mdi-book-open-page-variant bottom45icon"></i>
								</div>
								<span class="mr-3" style="font-size: 3rem;margin-top: -10px;">
									<?php echo $number_of_lessons; ?>
								</span>
							</div>
						</div>
					</div>
				</a>
			</div>
			
			<div class="col-sm-6 col-xl-6">
				<a href="<?php echo site_url('admin/users'); ?>">
					<div class="card cta-box bg-primary text-white">
						<div class="card-body">
							<div class="widget-title">
								<?php echo get_phrase('number_of_student'); ?>
							</div>
							<div class="media align-items-center">
								
								<div class="media-body">
									<i class="fas fa-users fa-6x bottomfaicon"></i>
								</div>
								<span class="mr-3" style="font-size: 3rem;margin-top: -10px;">
									<?php echo $number_of_students; ?>
								</span>
							</div>
						</div>
					</div>
				</a>
			</div>
			
			<div class="col-sm-6 col-xl-6">
				<a href="<?php echo site_url('admin/enrol_history'); ?>">
					<div class="card cta-box bg-dark text-white">
						<div class="card-body">
							<div class="widget-title">
								<?php echo get_phrase('number_of_enrollment'); ?>
							</div>
							<div class="media align-items-center">
								
								<div class="media-body">
									<i class="fas fa-user-graduate fa-6x bottomfaicon"></i>
								</div>
								<span class="mr-3" style="font-size: 3rem;margin-top: -10px;">
									<?php echo $number_of_enrolment; ?>
								</span>
							</div>
						</div>
					</div>
				</a>
			</div>
			
			<div class="col-sm-6 col-xl-6">
				<a href="<?php echo site_url('admin/courses'); ?>">
					<div class="card cta-box bg-yellow text-white">
						<div class="card-body">
							<div class="widget-title">
								<?php echo get_phrase('active_courses'); ?>
							</div>
							<div class="media align-items-center">
								
								<div class="media-body">
									<i class="fas fa-eye fa-6x bottomfaicon"></i>
								</div>
								<span class="mr-3" style="font-size: 3rem;margin-top: -10px;">
									<?php echo $status_wise_courses['active']->num_rows(); ?>
								</span>
							</div>
						</div>
					</div>
				</a>
			</div>
			
			<div class="col-sm-6 col-xl-6">
				<a href="<?php echo site_url('admin/courses'); ?>" class="text-secondary">
					<div class="card cta-box bg-blue text-white">
						<div class="card-body">
							<div class="widget-title">
								<?php echo get_phrase('pending_courses'); ?>
							</div>
							<div class="media align-items-center">
								
								<div class="media-body">
									<i class="fas fa-eye-slash fa-6x bottomfaicon"></i>
								</div>
								<span class="mr-3" style="font-size: 3rem;margin-top: -10px;">
									<?php echo $status_wise_courses['pending']->num_rows(); ?>
								</span>
							</div>
						</div>
					</div>
				</a>
			</div>
		</div>
	
	</div>
	<div class="col-xl-6">
		<div class="card">
			<div class="card-header purple">
				<h4 class="header-title mb-1 mt-0"><i class="mdi mdi-cash mdi-24px mr-1"></i><?php echo get_phrase('total_coursewings_revenue'); ?><small><i>(Admin Revenue Share)</i></small></h4>
			</div>
            <div class="card-body">
				
                <div class="mt-3 chartjs-chart" style="height: 320px;">
                    <canvas id="task-area-chart"></canvas>
                </div>
            </div>
        </div>
	</div>
</div>



<div class="row">
    <div class="col-xl-12">
        <div class="card" id = 'unpaid-instructor-revenue'>
			<div class="card-header bg-yellow">
				<div class="row">
					<div class="col-6 col-xl-8">
						<h4 class="header-title mt-1"><i class="mdi mdi-currency-usd-off mr-1"></i><?php echo get_phrase('pending_revenue_payouts'); ?></h4>                         
					</div>
					<div class="col-6 col-xl-4">
						<div class="text-lg-right mt-1">
							
							<a href="<?php echo site_url('admin/instructor_revenue'); ?>" class="btn btn-xs bg-purple mb-0"><i class="mdi mdi-plus"></i><?php echo get_phrase('view_all'); ?></a>
						</div>
					</div>
				</div>
			</div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table table-striped table-centered table-hover mb-0">
						<thead class="thead-purple">
							<tr>
                                <th><?php echo get_phrase('course_title'); ?></th>
                                <th><?php echo get_phrase('student'); ?></th>
                                <th><?php echo get_phrase('course_author'); ?></th>
                                <th class="text-center"><?php echo get_phrase('instructor_revenue'); ?></th>
                            </tr>
						</thead>
                        <tbody>

                            <?php
                                $this->db->where('instructor_payment_status', 0);
                                $this->db->where('instructor_revenue >', 0);
                                $unpaid_instructor_revenues = $this->db->get('payment')->result_array();
                                foreach ($unpaid_instructor_revenues as $key => $row):
                                $course_details = $this->crud_model->get_course_by_id($row['course_id'])->row_array();
                                $instructor_details = $this->user_model->get_all_user($course_details['user_id'])->row_array();
								//Test below out for multiple pending payments
								$unpaid_instructor_revenues2 = $this->db->get('payment')->row_array();
								$purchaser_details = $this->user_model->get_all_user($unpaid_instructor_revenues2['user_id'])->row_array();
								//$this->db->get_where('users', array('id' => $unpaid_instructor_revenues2['user_id']))->row_array();
                            ?>
                            <tr>
                                <td>
                                    <h5 class="font-14 my-1"><a href="<?php echo site_url('home/course/'.slugify($course_details['title']).'/'.$course_details['id']) ?>" class="text-body" target="_blank"><?php echo $course_details['title']; ?></a></h5>
                                </td>
								<td>
									<h5 class="font-14 my-1"><a href="javascript:void(0);" class="text-body"><?php echo $purchaser_details['first_name'].' '.$purchaser_details['last_name']; ?></a></h5>
									<small><?php echo get_phrase('purchase_date'); ?>: <span class="font-13"><?php echo date('D, d-M-Y', $unpaid_instructor_revenues2['date_added']); ?></span></small>
								</td>
                                <td>
                                    <h5 class="font-14 my-1"><a href="javascript:void(0);" class="text-body" style="cursor: auto;"><?php echo $instructor_details['first_name'].' '.$instructor_details['last_name']; ?></a></h5>
                                    <small><?php echo get_phrase('email'); ?>: <span class="text-muted font-13"><?php echo $instructor_details['email']; ?></span></small>
                                </td>
                                <td class="text-center">
                                    <h5 class="font-14 my-1"><a href="javascript:void(0);" class="text-body" style="cursor: auto;"><?php echo currency($row['instructor_revenue']); ?></a></h5>
                                </td>
                            </tr>
                            <?php endforeach; ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#unpaid-instructor-revenue').mouseenter(function() {
        $('#go-to-instructor-revenue').show();
    });
    $('#unpaid-instructor-revenue').mouseleave(function() {
        $('#go-to-instructor-revenue').hide();
    });
</script>
