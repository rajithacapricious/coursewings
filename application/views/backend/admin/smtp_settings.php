<div class="row">
	<div class="col-12">
		<ol class="breadcrumb yellow mt-2">
			<li class="breadcrumb-item"><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="mdi mdi-view-dashboard mr-1"></i>Dashboard</a></li>
			<li class="breadcrumb-item active "><a class="text-dark" href="#"><i class="mdi mdi-email-open mr-1"></i><?php echo get_phrase('email_smtp_settings'); ?></a></li>
		</ol>
	</div>
</div>

<div class="row justify-content-center">
    <div class="col-xl-12">
        <div class="card">
			<div class="card-header purple">
			
				<div class="row">
					<div class="col-12 col-xl-12">
						<h4 class="header-title mt-1"><i class="mdi mdi-email-open mr-1"></i><?php echo get_phrase('edit_email_smtp_settings'); ?></h4>                         
					</div>
				</div>
			</div>
            <div class="card-body">
                <div class="col-lg-12">
                    <h4 class="mb-3 header-title"><?php echo get_phrase('smtp_settings');?></h4>

                    <form class="required-form" action="<?php echo site_url('admin/smtp_settings/update'); ?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="smtp_protocol"><?php echo get_phrase('protocol'); ?><span class="required">*</span></label>
                            <input type="text" name = "protocol" id = "smtp_protocol" class="form-control" value="<?php echo get_settings('protocol');  ?>" required>
                        </div>

                        <div class="form-group">
                            <label for="smtp_host"><?php echo get_phrase('smtp_host'); ?><span class="required">*</span></label>
                            <input type="text" name = "smtp_host" id = "smtp_host" class="form-control" value="<?php echo get_settings('smtp_host');  ?>" required>
                        </div>

                        <div class="form-group">
                            <label for="smtp_port"><?php echo get_phrase('smtp_port'); ?><span class="required">*</span></label>
                            <input type="text" name = "smtp_port" id = "smtp_port" class="form-control" value="<?php echo get_settings('smtp_port');  ?>" required>
                        </div>

                        <!-- <div class="form-group">
                            <label for="smtp_user"><?php //echo get_phrase('smtp_username'); ?><span class="required">*</span></label>
                            <input type="text" name = "smtp_user" id = "smtp_user" class="form-control" value="<?php //echo get_settings('smtp_user');  ?>" required>
                        </div> -->

                        <div class="form-group">
                            <label for="smtp_pass"><?php echo get_phrase('smtp_password'); ?><span class="required">*</span></label>
                            <input type="text" name = "smtp_pass" id = "smtp_pass" class="form-control" value="<?php echo get_settings('smtp_pass');  ?>" required>
                        </div>

                        <button type="button" class="btn btn-primary btn-block" onclick="checkRequiredFields()"><i class="mdi mdi-content-save mr-1"></i><?php echo get_phrase('save_email_settings'); ?></button>
						<p class="mt-2"><small>* Please note that any error in settings would result in no emails going out from coursewings</br>Please get SMTP host name and Port number from the email service provider.</br>If using Gmail/Gsuite make sure that access to <a style="color:red" href="https://www.google.com/settings/security/lesssecureapps" target="_blank" rel="noopener">"Less secure apps" is turned on in your gmail account settings.</a></small></p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
