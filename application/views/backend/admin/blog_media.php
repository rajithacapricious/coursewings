<?php
  $blog_media_files = themeConfiguration(get_frontend_settings('theme'), 'blog_media_files');
  $blog_media_placeholders = themeConfiguration(get_frontend_settings('theme'), 'blog_media_placeholders');
  foreach ($blog_media_files as $blog_media => $size): ?>
  <div class="col pr-0 pl-0 mt-3 mb-2">
	<div class="card widget-flat bg-success text-white mb-1">
		<div class="card-body pt-2 pb-2">
			<div class="row justify-content-center">
			  <div class="col-xl-12">
				<div class="form-group row mb-3 text-center">
				  <label class="col-md-12 col-form-label text-center pt-0 mb-1" for="<?php echo $blog_media.'_label' ?>">
				  	<h5 class="my-0"><?php echo get_phrase($blog_media); ?></h5>
				  </label>
				  <div class="col-md-12 text-center">
					<div class="wrapper-image-preview d-inline-flex" style="">
					  <div class="box" style="width: 250px;">
				<?php if (empty($result) || empty($result['image'])) { ?>
					<div class="js--image-preview" style="background-image: url(<?php echo base_url().$blog_media_placeholders[$blog_media.'_placeholder']; ?>); background-color: #F5F5F5;"></div>
				<?php } else {?>	
					<div class="js--image-preview" style="background-image: url(<?php echo base_url('uploads/blog_images/'.$result['image']); ?>); background-color: #F5F5F5;"></div>
				<?php } ?>
						<div class="upload-options">
						  <label for="<?php echo $blog_media; ?>" class="btn"> <i class="mdi mdi-camera"></i> <?php echo get_phrase($blog_media); ?> <br> <small>(1045x300px)</small> </label>
						  <input id="<?php echo $blog_media; ?>" style="visibility:hidden;" type="file" class="image-upload" name="<?php echo $blog_media; ?>" accept="image/*">
						</div>
					  </div>
					</div>
				  </div>
				</div>
			  </div>
			</div>
		</div>
	</div>
</div>
<?php endforeach; ?>
