<?php
$course_details = $this->crud_model->get_course_by_id($course_id)->row_array();
?>
<div class="row">
	<div class="col-12">
		<ol class="breadcrumb yellow mt-2">
			<li class="breadcrumb-item"><a href="<?php echo site_url(); ?>"><i class="mdi mdi-home mr-1"></i></a></li>
			<li class="breadcrumb-item"><a href="<?php echo site_url('user/courses'); ?>"><i class="mdi mdi-view-dashboard mr-1"></i>Dashboard</a></li>
			<li class="breadcrumb-item"><a><i class="mdi mdi-table-edit mr-1"></i><?php echo get_phrase('edit_course'); ?></a></li>
			<li class="breadcrumb-item active"><a><i class="mdi mdi-lead-pencil mr-1"></i><?php echo get_phrase('editing') . ': ' . $course_details['title']; ?></a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xl-12">
		<div class="card">
			<div class="card-header purple">

				<div class="row">
					<div class="col-6 col-xl-8">
						<h4 class="header-title mt-1"><i class="mdi mdi-lead-pencil mr-1"></i><?php echo get_phrase('you_are_editing') . ': ' . $course_details['title']; ?></h4>
					</div>
					<div class="col-6 col-xl-4">
						<div class="text-lg-right mt-1">

							<a href="<?php echo site_url('user/courses'); ?>" class="btn btn-xs bg-yellow mb-0"><i class="mdi mdi-backspace mr-1"></i><?php echo get_phrase('back_to_course_list'); ?></a>
							<a href="<?php echo site_url('user/preview/' . $course_id); ?>" class="btn btn-xs bg-yellow mb-0" target="_blank"><?php echo get_phrase('view_on_frontend'); ?><i class="mdi mdi-airplay ml-1"></i></a>
						</div>
					</div><!-- end col-->
				</div>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-xl-12">
						<form class="required-form" action="<?php echo site_url('user/course_actions/edit/' . $course_id); ?>" method="post" enctype="multipart/form-data">
							<div id="basicwizard" class="">
								<ul class="nav nav-pills nav-justified form-wizard-header mx-0 mb-3">
									<li class="nav-item mr-1">
										<a href="#curriculum" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
											<i class="mdi mdi-folder-edit mr-1 mdi-18px"></i>
											<span class="d-none d-sm-inline"><?php echo get_phrase('add/edit_sections_&_Lessons'); ?></span>
										</a>
									</li>
									<li class="nav-item ml-1">
										<a href="#basic" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
											<i class="mdi mdi-square-edit-outline mr-1 mdi-18px"></i>
											<span class="d-none d-sm-inline"><?php echo get_phrase('edit_basic_course_information'); ?></span>
										</a>
									</li>
									<?php /*
                                    <li class="nav-item">
                                        <a href="#requirements" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                            <i class="mdi mdi-bell-alert mr-1"></i>
                                            <span class="d-none d-sm-inline"><?php echo get_phrase('requirements'); ?></span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#outcomes" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                            <i class="mdi mdi-camera-control mr-1"></i>
                                            <span class="d-none d-sm-inline"><?php echo get_phrase('outcomes'); ?></span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#pricing" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                            <i class="mdi mdi-currency-cny mr-1"></i>
                                            <span class="d-none d-sm-inline"><?php echo get_phrase('pricing'); ?></span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#media" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                            <i class="mdi mdi-library-video mr-1"></i>
                                            <span class="d-none d-sm-inline"><?php echo get_phrase('media'); ?></span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#seo" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                            <i class="mdi mdi-tag-multiple mr-1"></i>
                                            <span class="d-none d-sm-inline"><?php echo get_phrase('seo'); ?></span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#finish" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2">
                                            <i class="mdi mdi-checkbox-marked-circle-outline mr-1"></i>
                                            <span class="d-none d-sm-inline"><?php echo get_phrase('finish'); ?></span>
                                        </a>
                                    </li>
									*/ ?>
								</ul>

								<div class="tab-content b-0 mb-0">
									<div class="tab-pane" id="curriculum">
										<?php include 'curriculum.php'; ?>
									</div>

									<div class="tab-pane" id="basic">
										<div class="row justify-content-center">
											<div class="col-xl-8">
												<div class="form-group row mb-1">
													<label class="col-md-12 col-form-label" for="course_title"><?php echo get_phrase('course_title'); ?><span class="required">*</span></label>
													<div class="col-md-12">
														<input type="text" class="form-control" id="course_title" name="title" placeholder="<?php echo get_phrase('enter_course_title'); ?>" value="<?php echo $course_details['title']; ?>" required>
													</div>
												</div>
												<div class="form-group row mb-1">
													<label class="col-md-12 col-form-label" for="short_description"><?php echo get_phrase('short_course_description'); ?></label>
													<div class="col-md-12">
														<textarea name="short_description" id="short_description" placeholder="Enter a short course description" class="form-control"><?php echo $course_details['short_description']; ?></textarea>
													</div>
												</div>
												<div class="form-group row mb-1">
													<label class="col-md-12 col-form-label" for="description"><?php echo get_phrase('description'); ?></label>
													<div class="col-md-12">
														<textarea name="description" id="description" class="form-control"><?php echo $course_details['description']; ?></textarea>
													</div>
												</div>
												<?php /*
                                            <div class="form-group row mb-1">
                                                <div class="offset-md-2 col-md-10">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" name="is_top_course" id="is_top_course" value="1" <?php if($course_details['is_top_course'] == 1) echo 'checked'; ?>>
                                                        <label class="custom-control-label" for="is_top_course"><?php echo get_phrase('check_if_this_course_is_top_course'); ?></label>
                                                    </div>
                                                </div>
                                            </div>
											*/ ?>

												<div>
													<div class="form-group row mb-1">
														<label class="col-md-12 col-form-label" for="requirements"><?php echo get_phrase('course_requirements'); ?></label>
														<div class="col-md-12">
															<div id="requirement_area">
																<?php if (count(json_decode($course_details['requirements'])) > 0) : ?>
																	<?php
																		$counter = 0;
																		foreach (json_decode($course_details['requirements']) as $requirement) : ?>
																		<?php if ($counter == 0) :
																					$counter++; ?>
																			<div class="d-flex mt-2">
																				<div class="flex-grow-1 pl-0 pr-3">
																					<div class="form-group">
																						<input type="text" class="form-control" name="requirements[]" id="requirements" placeholder="<?php echo get_phrase('provide_requirements_click_on_plus_icon_to_add_more'); ?>" value="<?php echo $requirement; ?>">
																					</div>
																				</div>
																				<div class="">
																					<button type="button" class="btn bg-blue text-white btn-sm" style="min-height: 36px;margin-top: 1px;" name="button" onclick="appendRequirement()"> <i class="fa fa-plus-circle"></i> </button>
																				</div>
																			</div>
																		<?php else : ?>
																			<div class="d-flex mt-2">
																				<div class="flex-grow-1 pl-0 pr-3">
																					<div class="form-group">
																						<input type="text" class="form-control" name="requirements[]" id="requirements" placeholder="<?php echo get_phrase('provide_requirements_click_on_plus_icon_to_add_more'); ?>" value="<?php echo $requirement; ?>">
																					</div>
																				</div>
																				<div class="">
																					<button type="button" class="btn btn-danger btn-sm" style="min-height: 36px;margin-top: 1px;" name="button" onclick="removeRequirement(this)"> <i class="fa fa-trash-alt"></i> </button>
																				</div>
																			</div>
																		<?php endif; ?>
																	<?php endforeach; ?>
																<?php else : ?>
																	<div class="d-flex mt-2">
																		<div class="flex-grow-1 pl-0 pr-3">
																			<div class="form-group">
																				<input type="text" class="form-control" name="requirements[]" id="requirements" placeholder="<?php echo get_phrase('provide_requirements_click_on_plus_icon_to_add_more'); ?>">
																			</div>
																		</div>
																		<div class="">
																			<button type="button" class="btn bg-blue text-white btn-sm" style="min-height: 36px;margin-top: 1px;" name="button" onclick="appendRequirement()"> <i class="fa fa-plus-circle"></i> </button>
																		</div>
																	</div>
																<?php endif; ?>

																<div id="blank_requirement_field">
																	<div class="d-flex mt-2">
																		<div class="flex-grow-1 pl-0 pr-3">
																			<div class="form-group">
																				<input type="text" class="form-control" name="requirements[]" id="requirements" placeholder="What qualifications do students need to take this course? Please click the plus icon to add more.">
																			</div>
																		</div>
																		<div class="">
																			<button type="button" class="btn btn-danger btn-sm" style="min-height: 36px;margin-top: 1px;" name="button" onclick="removeRequirement(this)"> <i class="fa fa-trash-alt"></i> </button>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>

												<div>
													<div class="form-group row mb-1">
														<label class="col-md-12 col-form-label" for="outcomes"><?php echo get_phrase('course_outcomes'); ?></label>
														<div class="col-md-12">
															<div id="outcomes_area">
																<?php if (count(json_decode($course_details['outcomes'])) > 0) : ?>
																	<?php
																		$counter = 0;
																		foreach (json_decode($course_details['outcomes']) as $outcome) : ?>
																		<?php if ($counter == 0) :
																					$counter++; ?>
																			<div class="d-flex mt-2">
																				<div class="flex-grow-1 pl-0 pr-3">
																					<div class="form-group">
																						<input type="text" class="form-control" name="outcomes[]" placeholder="What will students learn after this course? Click on the plus icon to add more." value="<?php echo $outcome; ?>">
																					</div>
																				</div>
																				<div class="">
																					<button type="button" class="btn text-white bg-blue btn-sm" style="min-height: 36px;margin-top: 1px;" name="button" onclick="appendOutcome()"> <i class="fa fa-plus-circle"></i> </button>
																				</div>
																			</div>
																		<?php else : ?>
																			<div class="d-flex mt-2">
																				<div class="flex-grow-1 pl-0 pr-3">
																					<div class="form-group">
																						<input type="text" class="form-control" name="outcomes[]" placeholder="<?php echo get_phrase('provide_outcomes'); ?>" value="<?php echo $outcome; ?>">
																					</div>
																				</div>
																				<div class="">
																					<button type="button" class="btn btn-danger btn-sm" style="min-height: 36px;margin-top: 1px;" name="button" onclick="removeOutcome(this)"> <i class="fas fa-trash-alt"></i> </button>
																				</div>
																			</div>
																		<?php endif; ?>
																	<?php endforeach; ?>
																<?php else : ?>
																	<div class="d-flex mt-2">
																		<div class="flex-grow-1 pl-0 pr-3">
																			<div class="form-group">
																				<input type="text" class="form-control" name="outcomes[]" placeholder="<?php echo get_phrase('provide_outcomes'); ?>">
																			</div>
																		</div>
																		<div class="">
																			<button type="button" class="btn text-white bg-blue btn-sm" style="min-height: 36px;margin-top: 1px;" name="button" onclick="appendOutcome()"> <i class="fa fa-plus-circle"></i> </button>
																		</div>
																	</div>
																<?php endif; ?>
																<div id="blank_outcome_field">
																	<div class="d-flex mt-2">
																		<div class="flex-grow-1 pl-0 pr-3">
																			<div class="form-group">
																				<input type="text" class="form-control" name="outcomes[]" id="outcomes" placeholder="<?php echo get_phrase('provide_outcomes'); ?>">
																			</div>
																		</div>
																		<div class="">
																			<button type="button" class="btn btn-danger btn-sm" style="min-height: 36px;margin-top: 1px;" name="button" onclick="removeOutcome(this)"> <i class="fas fa-trash-alt"></i> </button>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>

												<div class="tab-pane" id="seo">
													<div class="form-group row mb-1">
														<label class="col-md-12 col-form-label" for="website_keywords"><?php echo get_phrase('meta_keywords'); ?><small class="mr-1"><i class="mr-1">(for S.E.O)</i> Press 'Enter' After Every Word.</small><a class="ml-1" data-toggle="tooltip" title="Meta Keywords are a specific type of meta tag that appear in the HTML code of a Web page and help tell search engines what the topic of the page is."><i class="mdi mdi-help-box"></i></a></label>
														<div class="col-md-12">
															<input type="text" class="form-control bootstrap-tag-input" id="meta_keywords" name="meta_keywords" placeholder="Eg. For Marketing related course - use keywords like Marketing, Sales, Strategies, Business Plan etc." data-role="tagsinput" style="width: 100%;" value="<?php echo $course_details['meta_keywords']; ?>" />
														</div>
													</div>

													<div class="form-group row mb-1">
														<label class="col-md-12 col-form-label" for="meta_description"><?php echo get_phrase('meta_description'); ?><small class="mr-1"><i>(for S.E.O)</i></small><a class="ml-1" data-toggle="tooltip" title="The meta description is an HTML attribute that provides a brief summary of a web page. Search engines such as Google often display the meta description in search results where they can highly influence user click-through rates."><i class="mdi mdi-help-box"></i></a></label>
														<div class="col-md-12">
															<textarea name="meta_description" placeholder="Write a short description of the course for search pages" class="form-control"><?php echo $course_details['meta_description']; ?></textarea>
														</div>
													</div>
												</div>


											</div> <!-- end col -->

											<div class="col-lg-4">
												<div class="form-group row mb-1">
													<label class="col-md-12 col-form-label" for="sub_category_id"><?php echo get_phrase('category'); ?><span class="required">*</span></label>
													<div class="col-md-12">
														<select class="form-control select2" data-toggle="select2" name="sub_category_id" id="sub_category_id" required>
															<option value=""><?php echo get_phrase('select_a_category'); ?></option>
															<?php foreach ($categories->result_array() as $category) : ?>
																<optgroup label="<?php echo $category['name']; ?>">
																	<?php $sub_categories = $this->crud_model->get_sub_categories($category['id']);
																		foreach ($sub_categories as $sub_category) : ?>
																		<option value="<?php echo $sub_category['id']; ?>" <?php if ($sub_category['id'] == $course_details['sub_category_id']) echo 'selected'; ?>><?php echo $sub_category['name']; ?></option>
																	<?php endforeach; ?>
																</optgroup>
															<?php endforeach; ?>
														</select>
													</div>
												</div>
												<div class="form-group row mb-1">
													<label class="col-md-12 col-form-label" for="level"><?php echo get_phrase('level'); ?></label>
													<div class="col-md-12">
														<select class="form-control select2" data-toggle="select2" name="level" id="level">
															<option value="beginner" <?php if ($course_details['level'] == "beginner") echo 'selected'; ?>><?php echo get_phrase('beginner'); ?></option>
															<option value="advanced" <?php if ($course_details['level'] == "advanced") echo 'selected'; ?>><?php echo get_phrase('advanced'); ?></option>
															<option value="intermediate" <?php if ($course_details['level'] == "intermediate") echo 'selected'; ?>><?php echo get_phrase('intermediate'); ?>
														</select>
													</div>
												</div>
												<div class="form-group row mb-1">
													<label class="col-md-12 col-form-label" for="language_made_in"><?php echo get_phrase('language_made_in'); ?></label>
													<div class="col-md-12">
														<select class="form-control select2" data-toggle="select2" name="language_made_in" id="language_made_in">
															<?php foreach ($languages as $language) : ?>
																<option value="<?php echo $language; ?>" <?php if ($course_details['language'] == $language) echo 'selected'; ?>><?php echo ucfirst($language); ?></option>
															<?php endforeach; ?>
														</select>
													</div>
												</div>

												<div>
													<div class="col pr-0 pl-0 mt-3 mb-2">
														<div class="card widget-flat bg-yellow text-purple mb-1">
															<div class="card-body">
																<div class="row justify-content-center">
																	<div class="col-xl-12">
																		<div class="form-group row mb-2">
																			<div class="col-md-12">
																				<div class="custom-control custom-checkbox">
																					<input type="checkbox" class="custom-control-input" name="is_free_course" id="is_free_course" value="1" <?php if ($course_details['is_free_course'] == 1) echo 'checked'; ?> onclick="togglePriceFields(this.id)">
																					<label class="custom-control-label" for="is_free_course">Click the box if this is a free course<br><small>(A free course increases your followers and reviews)</label>
																				</div>
																			</div>
																		</div>

																		<div class="paid-course-stuffs">
																			<div class="form-group row mb-2">
																				<label class="col-md-12 col-form-label" for="price"><?php echo get_phrase('course_price') . ' (' . currency_code_and_symbol() . ')'; ?></label>
																				<div class="col-md-12">
																					<input type="number" class="form-control" id="price" name="price" min="0" placeholder="<?php echo get_phrase('enter_course_course_price'); ?>" value="<?php echo $course_details['price']; ?>">
																				</div>
																			</div>

																			<div class="form-group row mb-2">
																				<div class="col-md-12">
																					<div class="custom-control custom-checkbox">
																						<input type="checkbox" class="custom-control-input" name="discount_flag" id="discount_flag" value="1" <?php if ($course_details['discount_flag'] == 1) echo 'checked'; ?>>
																						<label class="custom-control-label" for="discount_flag">Click the box if this course has a discount</label>
																					</div>
																				</div>
																			</div>

																			<div class="form-group row mb-3">
																				<label class="col-md-12 col-form-label" for="discounted_price"><?php echo get_phrase('discounted_price') . ' (' . currency_code_and_symbol() . ')'; ?></label>
																				<div class="col-md-12">
																					<input type="number" class="form-control" name="discounted_price" id="discounted_price" onkeyup="calculateDiscountPercentage(this.value)" value="<?php echo $course_details['discounted_price']; ?>" min="0">
																					<small class="text-purple"><?php echo get_phrase('this_course_has'); ?> <span id="discounted_percentage" class="badge badge-danger">0%</span> <?php echo get_phrase('discount'); ?></small>
																				</div>
																			</div>
																			<div class="form-group row mb-0 text-center">
																				<div class="col-md-12">
																					<span class="badge badge-secondary">Please Note: Your revenue share would be: <?php echo get_settings('instructor_revenue'); ?>%</span>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>

												</div>

												<div>
													<div class="col pr-0 pl-0 mt-3">
														<div class="card widget-flat bg-info text-white mb-1">
															<div class="card-body pt-2 pb-2">
																<div class="row justify-content-center">
																	<div class="col-xl-12">
																		<div class="form-group row mb-1">
																			<label class="col-md-12 col-form-label text-center pt-0 mb-1" for="course_overview_provider">
																				<h5 class="my-0"><?php echo get_phrase('course_preview_provider'); ?></h5>
																				<small>This preview is viewable by all before course is purchased</small>
																			</label>
																			<!-- #DK -->
																			<div class="col-md-12">
																				<select class="form-control select2" data-toggle="select2" name="course_overview_provider" id="course_overview_provider" onchange="c_check_video_provider(this.value)">
																					<option value="youtube" <?php if ($course_details['course_overview_provider'] == 'youtube') echo 'selected'; ?>><?php echo get_phrase('youtube'); ?></option>
																					<option value="vimeo" <?php if ($course_details['course_overview_provider'] == 'vimeo') echo 'selected'; ?>><?php echo get_phrase('vimeo'); ?></option>
																					<option value="html5" <?php if ($course_details['course_overview_provider'] == 'html5') echo 'selected'; ?>><?php echo get_phrase('your_hosted_video_url'); ?></option>
																					<option value="upload" <?php if ($course_details['course_overview_provider'] == 'upload') echo 'selected'; ?>><?php echo get_phrase('upload_video'); ?></option>
																					<option value="vimeo_upload" <?php if ($course_details['course_overview_provider'] == 'vimeo_upload') echo 'selected'; ?>><?php echo get_phrase('vimeo_upload'); ?></option>
																				</select>
																			</div>
																		</div>

																		<!-- #DK  -->
																		<div id="c_others" class="form-group row mb-1" <?php echo ($course_details['course_overview_provider'] == 'upload') ? 'style="display: none;"' : ''; ?>>
																			<label class="col-md-12 col-form-label" for="course_overview_url"><?php echo get_phrase('course_overview_url'); ?></label>
																			<div class="col-md-12">
																				<input type="text" class="form-control" name="course_overview_url" id="course_overview_url" placeholder="E.g: https://www.youtube.com/watch?v=YOUTUBE-VIDEO-CODE" value="<?php echo ($course_details['course_overview_provider'] == 'upload') ? basename($course_details['video_url']) : $course_details['video_url'] ?>">
																			</div>
																		</div>

																		<!-- #DK [START] -->
																		<div class="" id="c_upload" <?php echo ($course_details['course_overview_provider'] != 'upload') ? 'style="display: none;"' : ''; ?>>
																			<!-- #DK [uploaded video preview]-->
																			<?php
																			if ($course_details['course_overview_provider'] == 'upload') :
																				// $lesson_thumbnail_url = $this->crud_model->get_lesson_thumbnail_url($course_details['id']);
																				$video_url = $course_details['video_url'];
																				?>
																				<div class="col-sm-12 px-0">
																					<div class="card">
																						<div class="card-header bg-primary text-white">Currently Uploaded File</div>
																						<div class="card-body p-1">
																							<!------------- PLYR.IO ------------>
																							<link rel="stylesheet" href="<?php echo base_url(); ?>assets/global/plyr/plyr.css">
																							<video width="300px" id="cplayer" playsinline controls>
																								<!-- <video width="300px" poster="<?php echo $lesson_thumbnail_url; ?>" id="player" playsinline controls> -->
																								<?php if (get_video_extension($video_url) == 'mp4') : ?>
																									<source src="<?php echo $video_url; ?>" type="video/mp4">
																								<?php elseif (get_video_extension($video_url) == 'webm') : ?>
																									<source src="<?php echo $video_url; ?>" type="video/webm">
																								<?php else : ?>
																									<h4><?php get_phrase('video_url_is_not_supported'); ?></h4>
																								<?php endif; ?>
																							</video>
																							<script src="<?php echo base_url(); ?>assets/global/plyr/plyr.js"></script>
																							<script>
																								const cplayer = new Plyr('#cplayer');
																							</script>
																							<!------------- PLYR.IO ------------>
																							<?php /* <p class="text-dark"><?php echo basename($course_details['video_url']); ?></p> */ ?>
																						</div>
																					</div>
																				</div>
																			<?php endif; ?>
																			<!-- #DK [uploaded video preview] [END]-->

																			<div class="form-group">
																				<label> <?php echo get_phrase('upload_a_new_video_file'); ?><small> (*will overwrite the old video)</small></label>
																				<div class="input-group">
																					<div class="custom-file">
																						<input type="file" class="custom-file-input" id="c_upload_video" name="upload_video" onchange="changeTitleOfImageUploader(this)">
																						<label class="custom-file-label" for="c_upload_video"><?php echo get_phrase('upload_mp4_video_file_here'); ?></label>
																					</div>
																				</div>
																				<div class="badge badge-danger">
																					Max File size accepted is 256mb.
																				</div>
																				<div id="dk_c_file_upload_content" class="blueimp">
																					<input type="hidden" name="uploaded_file_name" class="w-100" id="c_uploaded_file_name" value="<?php echo basename($course_details['video_url']); ?>">
																					<div class="row">
																						<div class="col-sm-12 mb-1">
																							<div id="c_process_msg" class="mt-1"></div> <!-- process msg display here -->
																							<div id="progress_bar" class="progress" style="display: none;">
																								<div id="progress" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="46" aria-valuemin="0" aria-valuemax="100" style="width: 0%">&nbsp;0%</div>
																							</div>
																						</div>
																					</div>
																					<!-- The file list will be shown here -->
																					<div id="c_video_preview"></div>
																				</div>
																			</div>
																		</div>

																		<!-- vimeo upload -->
																		<div class="" id="c_vimeo_upload" <?php echo ($course_details['course_overview_provider'] != 'vimeo_upload') ? 'style="display: none;"' : ''; ?>>
																			<!-- preview -->
																			<?php
																			if ($course_details['course_overview_provider'] == 'vimeo_upload') :
																				$video_details = $this->video_model->getVideoDetails($course_details['video_url']);
																				$video_id = $video_details['video_id']; ?>
																				<!------------- PLYR.IO ------------>
																				<link rel="stylesheet" href="<?php echo base_url(); ?>assets/global/plyr/plyr.css">
																				<div class="plyr__video-embed" id="player">
																					<iframe height="150" src="https://player.vimeo.com/video/<?php echo $video_id; ?>?loop=false&amp;byline=false&amp;portrait=false&amp;title=false&amp;speed=true&amp;transparent=0&amp;gesture=media" allowfullscreen allowtransparency allow="autoplay"></iframe>
																				</div>
																				<script src="<?php echo base_url(); ?>assets/global/plyr/plyr.js"></script>
																				<script>
																					const player = new Plyr('#player');
																				</script>
																				<!------------- PLYR.IO ------------>
																			<?php endif; ?>
																			<!-- preview end -->
																			<div class="form-group">
																				<label> <?php echo get_phrase('upload_video_file'); ?></label>
																				<div class="input-group">
																					<div class="custom-file">
																						<input type="file" class="custom-file-input" id="c_vimeo_upload_video" name="vimeo_upload_video" onchange="changeTitleOfImageUploader(this)">
																						<label class="custom-file-label" for="c_vimeo_upload_video"><?php echo get_phrase('upload_video_file_here_for_vimeo'); ?></label>
																					</div>
																				</div>
																				<div id="dk_vimeo_file_upload_content" class="blueimp">
																					<input type="hidden" name="vimeo_uploaded_file_name" class="w-100" id="c_vimeo_uploaded_file_name">
																					<div class="row">
																						<div class="col-sm-12 mb-1">
																							<div id="c_vimeo_process_msg" class="mt-1"></div> <!-- process msg display here -->
																							<div id="c_vimeo_progress_bar" class="progress" style="display: none;">
																								<div id="c_vimeo_progress" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="46" aria-valuemin="0" aria-valuemax="100" style="width: 0%">&nbsp;0%</div>
																							</div>
																						</div>
																					</div>
																					<!-- The file list will be shown here -->
																					<div id="c_vimeo_video_preview"></div>
																				</div>
																			</div>
																		</div>
																		<?php $vimeo_key = $this->crud_model->get_vimeo_key('vimeo'); ?>
																		<input type="hidden" id="accessToken" class="form-control" placeholder="Vimeo access token" value="<?php echo base64_encode($vimeo_key[0]->vimeo_access_token); ?>" required autofocus />
																		<!-- #DK [END] -->

																		<div class="form-group row mb-1">
																			<div class="col-md-12">
																				<small><?php echo ucfirst(strtolower(get_phrase('Please_ensure_to_upload_a_powerfull_&_enticing_promo_video_of_approx_2_minutes,_which_gives_a_good_preview_about_your_course_and_why_someone_should_buy_your_course.'))); ?></small>
																			</div>
																		</div>

																	</div>
																</div>
															</div>
														</div>
													</div>

													<!-- Course media content edit file starts -->
													<?php include 'course_media_edit.php'; ?>
													<!-- Course media content edit file ends -->

												</div>
												<div class="card cta-box bg-danger text-white">
													<div class="card-body">
														<div class="text-center">
															<h3 class="m-0 font-weight-normal cta-box-title"><b>IMPORTANT</b></h3>
															<i class="mdi mdi-alert-octagon mdi-48px"></i><br>
															<a class="font-weight-bold">After updating/editing your course, it will be marked as pending.<br><span class="mt-2 mb-0 text-uppercase font-weight-bold" style="color:#f7a823">The course will have to be Re-approved by the Admin</span></a>
														</div>
													</div>
													<!-- end card-body -->
												</div>

											</div>
										</div> <!-- end row -->
										<div class="row">

											<div class="col-12">
												<div class="text-center">
													<h2 class="mt-0"><i class="mdi mdi-check-decagram mdi-48px"></i></h2>
													<div class="mb-3 mt-3">
														<button id="submit_course_btn" type="button" class="btn btn-primary btn-block text-center" onclick="checkRequiredFields()"><i class="mdi mdi-content-save mr-2"></i><?php echo get_phrase('submit'); ?></button>
													</div>
												</div>
											</div>

										</div> <!-- end FinishID -->
									</div> <!-- end tab pane -->



									<ul class="list-inline mb-0 wizard text-center">
										<li class="previous list-inline-item float-left" style="min-width: 150px;">
											<a href="javascript::" class="btn btn-info"><i class="mdi mdi-arrow-left-bold mr-1"></i>Prev</a>
										</li>
										<li class="next list-inline-item float-right" style="min-width: 150px;">
											<a href="javascript::" class="btn btn-info">Next<i class="mdi mdi-arrow-right-bold ml-1"></i></a>
										</li>
									</ul>

								</div> <!-- tab-content -->
							</div> <!-- end #progressbarwizard-->
						</form>
					</div>
				</div><!-- end row-->
			</div> <!-- end card-body-->
		</div> <!-- end card-->
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		initSummerNote(['#description']);
		togglePriceFields('is_free_course');
	});
</script>

<script type="text/javascript">
	var blank_outcome = jQuery('#blank_outcome_field').html();
	var blank_requirement = jQuery('#blank_requirement_field').html();
	jQuery(document).ready(function() {
		jQuery('#blank_outcome_field').hide();
		jQuery('#blank_requirement_field').hide();
		calculateDiscountPercentage($('#discounted_price').val());
	});

	function appendOutcome() {
		jQuery('#outcomes_area').append(blank_outcome);
	}

	function removeOutcome(outcomeElem) {
		jQuery(outcomeElem).parent().parent().remove();
	}

	function appendRequirement() {
		jQuery('#requirement_area').append(blank_requirement);
	}

	function removeRequirement(requirementElem) {
		jQuery(requirementElem).parent().parent().remove();
	}

	function ajax_get_sub_category(category_id) {
		console.log(category_id);
		$.ajax({
			url: '<?php echo site_url('user/ajax_get_sub_category/'); ?>' + category_id,
			success: function(response) {
				jQuery('#sub_category_id').html(response);
			}
		});
	}

	function priceChecked(elem) {
		if (jQuery('#discountCheckbox').is(':checked')) {

			jQuery('#discountCheckbox').prop("checked", false);
		} else {

			jQuery('#discountCheckbox').prop("checked", true);
		}
	}

	function topCourseChecked(elem) {
		if (jQuery('#isTopCourseCheckbox').is(':checked')) {

			jQuery('#isTopCourseCheckbox').prop("checked", false);
		} else {

			jQuery('#isTopCourseCheckbox').prop("checked", true);
		}
	}

	function isFreeCourseChecked(elem) {

		if (jQuery('#' + elem.id).is(':checked')) {
			$('#price').prop('required', false);
		} else {
			$('#price').prop('required', true);
		}
	}

	function calculateDiscountPercentage(discounted_price) {
		if (discounted_price > 0) {
			var actualPrice = jQuery('#price').val();
			if (actualPrice > 0) {
				var reducedPrice = actualPrice - discounted_price;
				var discountedPercentage = (reducedPrice / actualPrice) * 100;
				if (discountedPercentage > 0) {
					jQuery('#discounted_percentage').text(discountedPercentage.toFixed(2) + "%");

				} else {
					jQuery('#discounted_percentage').text('<?php echo '0%'; ?>');
				}
			}
		}
	}

	$('.on-hover-action').mouseenter(function() {
		var id = this.id;
		$('#widgets-of-' + id).show();
	});
	$('.on-hover-action').mouseleave(function() {
		var id = this.id;
		$('#widgets-of-' + id).hide();
	});
</script>
<!-- #DK [start] -->
<script src="<?php echo site_url('assets/global/blueimp/jquery.ui.widget.js'); ?>"></script>
<script src="<?php echo site_url('assets/global/blueimp/jquery.fileupload.js'); ?>"></script>

<script>
	function c_check_video_provider(provider) {
		$('#submit_course_btn').prop('disabled', false);
		if (provider === 'upload') {
			$('#upload').show();
			$('#others,#c_vimeo_upload').hide();
			$('#submit_course_btn').prop('disabled', true);
		} else if (provider === 'vimeo_upload') {
			$('#c_vimeo_upload').show();
			$('#others,#upload').hide();
			$('#submit_course_btn').prop('disabled', true);
		} else {
			$('#others').show();
			$('#upload,#c_vimeo_upload').hide();
		}
	}
	$('#c_process_msg').html('');
	$('#c_upload_video').fileupload({
		url: '<?php echo base_url('user/course_video_upload'); ?>',
		type: 'POST',
		autoUpload: false,
		replaceFileInput: false,
		acceptFileTypes: '/(\.|\/)(mp4)$/i',
		maxChunkSize: 10000000, // 10 MB
		// This function is called when a file is added to the queue
		add: function(e, data) {
			var fileType = data.files[0].name.split('.').pop(),
				allowdtypes = 'mp4';
			if (allowdtypes.indexOf(fileType) < 0) {
				$('#c_process_msg').html('<strong class="text-danger">Invalid file type. Only Accept mp4 Video format.</strong>');
				return false;
			}
			$('#c_upload_video').prop('disabled', true);
			//This area will contain file list and progress information.
			// var tpl = $('<div class="row working" id="v_name">' + '<div class="col-sm-4"><div id="c_upload_actions"><button type="button" class="btn btn-primary upload">Upload</button> <button type="button" class="btn btn-primary cancel">cancel</button></div></div></div>');
			var tpl = $('<div class="working"><div id="c_v_name">' + '</div></div>');
			// Append the videp name and size and preview
			tpl.find('#c_v_name').append('<div class="row">' + '<div class="col-sm-12"><video id="preview" width="270" src="' + URL.createObjectURL(data.files[0]) + '" controls/></div>' + '<div class="col-sm-12"><label> File Size: <strong class="d-block">' + formatFileSize(data.files[0].size) + '</strong></label></div>' + '<div class="col-sm-12"><div id="c_upload_actions"><button type="button" class="btn btn-primary upload">Upload</button> <button type="button" class="btn btn-primary cancel">Cancel</button></div></div>' + '</div>');
			// Add the HTML to the UL element
			data.context = tpl.appendTo('div#c_video_preview');
			//cancel file
			tpl.find('button.cancel').click(function() {
				tpl.fadeOut(function() {
					$('#c_progress_bar').hide();
					$('#c_process_msg').html('');
					$('#c_upload_video').prop('disabled', false);
					tpl.remove();
				});
			});
			//upload file
			tpl.find('button.upload').click(function() {
				var jqXHR = data.submit();
			});
		},
		progress: function(e, data) {
			// $('#c_process_msg').html('<strong class="text-warning">Uploading...</strong>');
			$('#c_process_msg').html('');
			$('#c_progress_bar').show();
			$('#c_upload_actions').remove();
			// Calculate the completion percentage of the upload
			var progress = parseInt(data.uploadedBytes / data.total * 100, 10);
			$('#c_progress_bar > div').text(progress + '%').animate({
				width: progress + '%'
			}, 5);
			// Update the hidden input field and trigger a change
			// so that the jQuery knob plugin knows to update the dial
			data.context.find('input').val(progress).change();
			if (progress == 100) {
				data.context.removeClass('working');
			}
		},
		done: function(e, data) {
			if (data.textStatus == 'success') {
				$('#c_process_msg').html('<strong class="text-warning">File Uploaded Successfully.</strong>');
				var obj = JSON.parse(data.result);
				$('#c_uploaded_file_name').val(obj.upload_video[0].name);
				$('#course_overview_url').val(obj.upload_video[0].name);
				// $('#c_uploaded_file_name').val(obj.files.file_name);
				// $('#course_overview_url').val(obj.files.file_name);
				$('#c_progress_bar').hide();
				$('#submit_course_btn').prop('disabled', false);
			} else {
				$('#c_process_msg').html('<strong class="text-warning">Try again something went wrong</strong>');
				$('#c_upload_video').prop('disabled', true);
			}
		}
	});

	// <!------------------ vimeo upload script -----------------!>
	$('#c_vimeo_process_msg').html('');
	$('#c_vimeo_upload_video').fileupload({
		url: '<?php echo base_url('user/vimeo_video_upload'); ?>',
		type: 'POST',
		autoUpload: false,
		replaceFileInput: false,
		acceptFileTypes: /(\.|\/)(mp4)$/i,
		// This function is called when a file is added to the queue
		add: function(e, data) {
			var fileType = data.files[0].name.split('.').pop(),
				allowdtypes = 'mp4';
			if (allowdtypes.indexOf(fileType) < 0) {
				$('#c_vimeo_process_msg').html('<strong class="text-danger">Invalid file type. Only Accept mp4 Video format.</strong>');
				return false;
			}
			// $('#c_vimeo_upload_video').prop('disabled', true);
			//This area will contain file list and progress information.
			var tpl = $('<div class="working"><div id="c_vimeo_name">' + '</div></div>');
			// Append the videp name and size and preview
			tpl.find('#c_vimeo_name').append('<div class="row">' + '<div class="col-sm-12"><video id="vimeo_preview" width="270" src="' + URL.createObjectURL(data.files[0]) + '" controls/></div>' + '<div class="col-sm-12"><label> File Size: <strong class="d-block">' + formatFileSize(data.files[0].size) + '</strong></label></div>' + '<div class="col-sm-12"><div id="c_vimeo_upload_actions"><button type="button" class="btn btn-primary upload">Upload</button> <button type="button" class="btn btn-primary cancel">Cancel</button></div></div>' + '</div>');
			// Add the HTML to the UL element
			data.context = tpl.appendTo('div#c_vimeo_video_preview');
			//cancel file
			tpl.find('button.cancel').click(function() {
				tpl.fadeOut(function() {
					$('#c_vimeo_process_msg').html('');
					$('#c_vimeo_upload_video').prop('disabled', false);
					tpl.remove();
				});
			});
			//upload file
			tpl.find('button.upload').click(function() {
				// var jqXHR = data.submit();
				var browse = document.getElementById('c_vimeo_upload_video');
				browse.addEventListener('click', handleFileSelect, false);
				$('#c_vimeo_upload_video').trigger('click');
				$('#c_vimeo_upload_video').prop('disabled', true);
			});
		},
		progress: function(e, data) {
			$('#c_vimeo_process_msg').html('<strong class="text-warning"><i class="mdi mdi-spin mdi-loading"></i> Uploading under process...</strong>');
			$('#c_vimeo_upload_actions').remove();
		},
		done: function(e, data) {
			if (data.textStatus == 'success') {
				$('#c_vimeo_process_msg').html('<strong class="text-success">File Uploaded Successfully.</strong>');
				var obj = JSON.parse(data.result);
				$('#c_vimeo_uploaded_file_name').val(obj.response);
				$('#course_overview_url').val(obj.response);
				$('#submit_course_btn').prop('disabled', false);
			} else {
				$('#c_vimeo_process_msg').html('<strong class="text-warning">Try again something went wrong</strong>');
				$('#c_vimeo_upload_video').prop('disabled', true);
			}
		}
	});


	//Helper function for calculation of progress
	function formatFileSize(bytes) {
		if (typeof bytes !== 'number') {
			return '';
		}
		if (bytes >= 1000000000) {
			return (bytes / 1000000000).toFixed(2) + ' GB';
		}

		if (bytes >= 1000000) {
			return (bytes / 1000000).toFixed(2) + ' MB';
		}
		return (bytes / 1000).toFixed(2) + ' KB';
	}

	//next previous hide/show
	$(document).ready(function() {
		$('li.previous').hide();
		$('#basicwizard').bootstrapWizard({
			'onNext': function(tab, navigation, index) {
				$('li.next').hide();
				$('li.previous').show();
			},
			'onPrevious': function(tab, navigation, index) {
				$('li.next').show();
				$('li.previous').hide();
			}
		});
	});
</script>

<script src="<?php echo site_url('assets/global/vimeo/vimeo-upload.js'); ?>"></script>
<script>
	/**
	 * vimeo upload using javasrcipt, script called when click on upload button.
	 */
	function handleFileSelect(evt) {
		evt.stopPropagation()
		evt.preventDefault()

		var files = evt.dataTransfer ? evt.dataTransfer.files : $(this).get(0).files;
		var results = document.getElementById('c_vimeo_process_msg');
		/* Clear the results div */
		while (results.hasChildNodes()) results.removeChild(results.firstChild)

		/* Rest the progress bar and show it */
		updateProgress(0)
		document.getElementById('c_vimeo_progress_bar').style.display = 'block';		
		$('#c_vimeo_upload_actions').remove();

		/* Instantiate Vimeo Uploader */
		;
		(new VimeoUpload({
			name: document.getElementById('course_title').value,
			// description: document.getElementById('videoDescription').value,
			// private: document.getElementById('make_private').checked,
			file: files[0],
			token: atob(document.getElementById('accessToken').value),
			// upgrade_to_1080: document.getElementById('upgrade_to_1080').checked,
			onError: function(data) {
				showMessage('<strong>Error</strong>: ' + JSON.parse(data).error, 'danger')
			},
			onProgress: function(data) {
				updateProgress(data.loaded / data.total)
			},
			onComplete: function(videoId, index) {
				var url = 'https://vimeo.com/' + videoId				

				if (index > -1) {
					/* The metadata contains all of the uploaded video(s) details see: https://developer.vimeo.com/api/endpoints/videos#/{video_id} */
					url = this.metadata[index].link //
					/* add stringify the json object for displaying in a text area */
					// var pretty = JSON.stringify(this.metadata[index], null, 2)
					// console.log(pretty) /* echo server data */
				}
				$('#c_vimeo_uploaded_file_name').val(btoa(url));
				$('#course_overview_url').val(btoa(url));
				$('#submit_course_btn').prop('disabled', false);
				showMessage('<strong>Upload Successful</strong>: Please note that after successfull upload we would transcode this video for web streaming and it might take upto 30 minutes depending on your uploaded file size. Video would be available on coursewings after it has been successfully transcoded. Untill then you would see Striped video');
			}
		})).upload();

		/* local function: show a user message */
		function showMessage(html, type) {
			/* hide progress bar */
			document.getElementById('c_vimeo_progress_bar').style.display = 'none'

			/* display alert message */
			var element = document.createElement('div')
			element.setAttribute('class', 'alert alert-' + (type || 'success'))
			element.innerHTML = html
			results.appendChild(element)
		}
	}
	/**
	 * Updat progress bar.
	 */
	function updateProgress(progress) {
		progress = Math.floor(progress * 100)
		var element = document.getElementById('c_vimeo_progress')
		element.setAttribute('style', 'width:' + progress + '%')
		element.innerHTML = '&nbsp;' + progress + '%'
	}
</script>
<!-- #DK [end] -->