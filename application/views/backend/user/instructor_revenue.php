<?php
$payment_histories = $this->crud_model->get_instructor_wise_payment_history($this->session->userdata('user_id'));
?>


<div class="row">
    <div class="col-12">
        <ol class="breadcrumb yellow mt-2">
            <li class="breadcrumb-item"><a href="<?php echo site_url(); ?>"><i class="mdi mdi-home mr-1"></i></a></li>
            <li class="breadcrumb-item"><a href="<?php echo site_url('user/courses'); ?>"><i class="mdi mdi-view-dashboard mr-1"></i>Dashboard</a></li>
            <li class="breadcrumb-item active"><a href="#"><i class="mdi mdi-cash-multiple mr-1"></i><?php echo get_phrase('instructor_revenue'); ?></a></li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-header purple">
                <div class="row">
                    <div class="col-xl-12 col-sm-12">
                        <h4 class="header-title mt-1"><i class="mdi mdi-lead-pencil mr-1"></i><?php echo get_phrase('instructor_revenue'); ?></h4>                         
                    </div>
                    <?php /*
                      <div class="col-xl-6 col-sm-12">
                      <div class="text-lg-right">

                      <form class="form-inline" action="<?php echo site_url('user/instructor_revenue/filter_by_date_range') ?>" method="get">
                      <div class="col-xl-8">
                      <div class="form-group">
                      <div id="reportrange" class="form-control mb-0 text-center" data-toggle="date-picker-range" data-target-display="#selectedValue"  data-cancel-class="btn-light" style="width: 100%;">
                      <i class="mdi mdi-calendar"></i>&nbsp;
                      <span id="selectedValue"><?php echo date("F d, Y" , $timestamp_start) . " - " . date("F d, Y" , $timestamp_end);?></span> <i class="mdi mdi-menu-down"></i>
                      </div>
                      <input id="date_range" type="hidden" name="date_range" value="<?php echo date("d F, Y" , $timestamp_start) . " - " . date("d F, Y" , $timestamp_end);?>">
                      </div>
                      </div>
                      <div class="col-xl-4">
                      <button type="submit" class="btn btn-info btn-block mb-0" id="submit-button" onclick="update_date_range();"> <?php echo get_phrase('filter');?></button>
                      </div>
                      </form>
                      </div>
                      </div><!-- end col-->
                     */ ?>
                </div>
            </div>
            <div class="card-body">

                <div class="table-responsive-sm mt-2">
                    <table id="basic-datatable" class="table table-striped dt-responsive nowrap" width="100%" data-page-length='10'>
                        <thead class="thead-purple">
                            <tr>
                                <th><?php echo get_phrase('subscribed_course_name'); ?></th>
                                <th><?php echo get_phrase('purchaser'); ?></th>
                                <th><?php echo get_phrase('date'); ?></th>
                                <th class="text-center"><?php echo get_phrase('selling_price'); ?></th>
                                <th class="text-center"><?php echo get_phrase('instructor_revenue'); ?></th>
                                <th class="text-center"><?php echo get_phrase('status'); ?></th>
                                <?php /* <th class="text-center"><?php echo get_phrase('option'); ?></th> */ ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($payment_history as $payment):
                                $course_data = $this->db->get_where('course', array('id' => $payment['course_id']))->row_array();
                                $user_data = $this->db->get_where('users', array('id' => $course_data['user_id']))->row_array();
                                $buyer_data = $this->db->get_where('users', array('id' => $payment['user_id']))->row_array();
                                ?>
                                <?php
                                $paypal_keys = json_decode($user_data['paypal_keys'], true);
                                $stripe_keys = json_decode($user_data['stripe_keys'], true);
                                ?>
                                <tr class="gradeU">
                                    <td>
                                        <strong><a href="<?php echo site_url('home/course/' . slugify($course_data['title']) . '/' . $course_data['id']); ?>" target="_blank"><?php echo ellipsis($course_data['title']); ?></a></strong><br>

                                    </td>
                                    <td>
                                        <strong><?php echo $buyer_data['first_name'] . ' ' . $buyer_data['last_name']; ?></strong><br>
                                    </td>
                                    <td>
                                        <?php echo date('D, d-M-Y', $payment['date_added']); ?>
                                    </td>
                                    <td class="text-center">
                                        <?php echo currency($payment['amount']); ?>
                                    </td>
                                    <td class="text-center">
                                        <?php echo currency($payment['instructor_revenue']); ?>
                                    </td>
                                    <td style="text-align: center;">
                                        <?php if ($payment['instructor_payment_status'] == 0): ?>
                                            <div class="btn btn-sm btn-danger"><i class="mdi mdi-av-timer mr-1"></i><?php echo get_phrase('pending_disbursement'); ?></div>
                                        <?php elseif ($payment['instructor_payment_status'] == 1): ?>
                                            <div class="btn btn-sm btn-success"><i class="mdi mdi-briefcase-check mr-1"></i><?php echo get_phrase('paid'); ?></div>
                                    <?php endif; ?>
                                    </td>
                                    <?php /*
                                      <td class="text-center">
                                      <a href="<?php echo site_url('user/invoice/'.$payment['id']); ?>" class="btn btn-outline-primary btn-rounded btn-sm"><i class="mdi mdi-printer-settings"></i></a>
                                      </td>
                                     */ ?>
                                </tr>
                    <?php endforeach; ?>
                        </tbody>
                    </table>
                        <?php if (empty($payment_history)): ?>
                        <div class="img-fluid w-100 text-center">
                            <img style="opacity: 1; width: 100px;" src="<?php echo base_url('assets/backend/images/social_share.png'); ?>"><br>
                        <?php echo get_phrase('to_increase_your_sales,_please_promote_your_course_page_on_your_facebook_/_twitter_/_linkedin_pages'); ?>
                        </div>
<?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function update_date_range()
    {
        var x = $("#selectedValue").html();
        $("#date_range").val(x);
    }
</script>
