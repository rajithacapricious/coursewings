<?php
	$status_wise_courses = $this->crud_model->get_status_wise_courses();
 ?>
<!-- ========== Left Sidebar Start ========== -->
<div class="left-side-menu left-side-menu-detached">
	<div class="leftbar-user">
		<a>
			<img src="<?php echo $this->user_model->get_user_image_url($this->session->userdata('user_id')); ?>" alt="user-image" height="42" class="rounded-circle shadow-sm">
			<?php
			$user_details = $this->user_model->get_all_user($this->session->userdata('user_id'))->row_array();
			?>
			<span class="leftbar-user-name"><?php echo get_phrase('welcome_back'); ?>, <?php echo $user_details['first_name'].' '.$user_details['last_name']; ?></span>
		</a>
	</div>

	<!--- Sidemenu -->
		<ul class="metismenu side-nav in">

			<li class="side-nav-title side-nav-item mb-0"><i class="mdi mdi-compass mr-2"></i><?php echo get_phrase('instructor_menu'); ?></li>
			
			<li class="side-nav-item" style="background:#fff!important;text-align: center;">
				<a class="btn btn-xs btn-primary" href="<?php echo site_url('user/howtobecomeinstructor'); ?>" >
					<span>How To Become An Instructor</span>
				</a>
			</li>

			<li class="side-nav-item">
				<a href="<?php echo site_url('user/courses'); ?>" class="side-nav-link <?php if ($page_name == 'courses' || $page_name == 'course_edit')echo 'active';?>">
					<i class="mdi mdi-library-books mr-2"></i>
					<span><?php echo get_phrase('courses'); ?></span>
				</a>
			</li>
			<li class="side-nav-item">
				<a href="<?php echo site_url('user/course_form/add_course'); ?>" class="side-nav-link <?php if ($page_name == 'course_add')echo 'active';?>">
					<i class="mdi mdi-book-plus mr-2"></i>
					<span><?php echo get_phrase('add_new_course'); ?></span>
				</a>
			</li>
			<li class="side-nav-item">
				<a href="<?php echo site_url('user/instructor_revenue'); ?>" class="side-nav-link <?php if ($page_name == 'report' || $page_name == 'invoice')echo 'active';?>">
					<i class="mdi mdi-cash-multiple mr-2"></i>
					<span><?php echo get_phrase('instructor_revenue'); ?></span>
				</a>
			</li>
			
			<li class="side-nav-item">
				<a href="<?php echo site_url('user/payment_settings'); ?>" class="side-nav-link <?php if ($page_name == 'system_settings' || $page_name == 'frontend_settings' || $page_name == 'payment_settings' || $page_name == 'instructor_settings' || $page_name == 'smtp_settings' || $page_name == 'manage_language' ): ?> active <?php endif; ?>">
					<i class="mdi mdi-settings mr-2"></i>
					<span><?php echo get_phrase('payment_settings'); ?></span>
				</a>
			</li>
			<li class="side-nav-item">
				<a href="<?php echo site_url(strtolower($this->session->userdata('role')).'/manage_profile'); ?>" class="side-nav-link">
				<i class="mdi mdi-account mr-2"></i><span><?php echo get_phrase('manage_profile'); ?></span></a>
			</li>
			<li class="side-nav-item">
				<a href="<?php echo site_url();?>" class="side-nav-link">
				<i class="mdi mdi-airplay mr-2"></i><span class="blink_me"><strong>RETURN TO FRONTEND</strong></span></a>
			</li>
			<li class="side-nav-item">
				<a href="<?php echo site_url('login/logout'); ?>" class="side-nav-link">
				<i class="mdi mdi-power mr-2"></i><span><?php echo get_phrase('logout'); ?></span></a>
			</li>
			<?php /*
			<li class="side-nav-item">
				<a href="javascript: void(0);" class="side-nav-link <?php if ($page_name == 'system_settings' || $page_name == 'frontend_settings' || $page_name == 'payment_settings' || $page_name == 'instructor_settings' || $page_name == 'smtp_settings' || $page_name == 'manage_language' ): ?> active <?php endif; ?>">
					<i class="dripicons-toggles"></i>
					<span> <?php echo get_phrase('settings'); ?> </span>
					<span class="menu-arrow"></span>
				</a>
				<ul class="side-nav-second-level" aria-expanded="false">
					<li class = "<?php if($page_name == 'payment_settings') echo 'active'; ?>">
						<a href="<?php echo site_url('user/payment_settings'); ?>"><?php echo get_phrase('payment_settings'); ?></a>
					</li>
				</ul>
			</li>
			*/ ?>
	    </ul>
</div>
