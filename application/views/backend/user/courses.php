<div class="row">
	<div class="col-12">
		<ol class="breadcrumb yellow mt-2">
			<li class="breadcrumb-item"><a href="<?php echo site_url();?>"><i class="mdi mdi-home mr-1"></i></a></li>
			<li class="breadcrumb-item active text-purple"><i class="mdi mdi-view-dashboard mr-1"></i>Dashboard/<?php echo get_phrase('course_list'); ?></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col">
		<div class="card cta-box bg-dark text-white">
			<div class="card-body">
				<div class="widget-title">
					<?php echo get_phrase('active_courses'); ?>
				</div>
				<div class="media align-items-center">
					
					<div class="media-body">
						<i class="mdi mdi-eye bottom45icon"></i>
					</div>
					<span class="mr-3" style="font-size: 3rem;margin-top: -10px;">
						<?php
							$active_courses = $this->crud_model->get_status_wise_courses_for_instructor('active');
							echo $active_courses->num_rows();
						 ?>
					</span>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col">
		<div class="card cta-box bg-dark text-white">
			<div class="card-body">
				<div class="widget-title">
					<?php echo get_phrase('pending_approval'); ?>
				</div>
				<div class="media align-items-center">
					
					<div class="media-body">
						<i class="mdi mdi-eye-off bottom45icon"></i>
					</div>
					<span class="mr-3" style="font-size: 3rem;margin-top: -10px;">
						<?php
							$pending_courses = $this->crud_model->get_status_wise_courses_for_instructor('pending');
							echo $pending_courses->num_rows();
						?>
					</span>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col">
		<div class="card cta-box bg-dark text-white">
			<div class="card-body">
				<div class="widget-title">
					<?php echo get_phrase('draft_courses'); ?>
				</div>
				<div class="media align-items-center">
					
					<div class="media-body">
						<i class="mdi mdi-book bottom45icon"></i>
					</div>
					<span class="mr-3" style="font-size: 3rem;margin-top: -10px;">
						<?php
							$draft_courses = $this->crud_model->get_status_wise_courses_for_instructor('draft');
							echo $draft_courses->num_rows();
						?>
					</span>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col">
		<div class="card cta-box bg-dark text-white">
			<div class="card-body">
				<div class="widget-title">
					<?php echo get_phrase('free_courses'); ?>
				</div>
				<div class="media align-items-center">
					
					<div class="media-body">
						<i class="mdi mdi-tag bottom45icon"></i>
					</div>
					<span class="mr-3" style="font-size: 3rem;margin-top: -10px;">
						<?php echo $this->crud_model->get_free_and_paid_courses('free', $this->session->userdata('user_id'))->num_rows(); ?>
					</span>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col">
		<div class="card cta-box bg-dark text-white">
			<div class="card-body">
				<div class="widget-title">
					<?php echo get_phrase('paid_courses'); ?>
				</div>
				<div class="media align-items-center">
					
					<div class="media-body">
						<i class="mdi mdi-shopping bottom45icon"></i>
					</div>
					<span class="mr-3" style="font-size: 3rem;margin-top: -10px;">
						<?php echo $this->crud_model->get_free_and_paid_courses('paid', $this->session->userdata('user_id'))->num_rows(); ?>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <div class="col-xl-12">
        <div class="card">
			<div class="card-header purple">
			
				<div class="row">
					<div class="col-6 col-xl-8">
						<h4 class="header-title mt-1"><i class="mdi mdi-library-books mr-1"></i><?php echo get_phrase('course_list'); ?></h4>                         
					</div>
					<div class="col-6 col-xl-4">
						<div class="text-lg-right">
							
							<a href="<?php echo site_url('user/course_form/add_course'); ?>" class="btn btn-xs bg-yellow mb-0 mt-1"><i class="mdi mdi-plus"></i><?php echo get_phrase('add_a_new_course'); ?></a>
						</div>
					</div><!-- end col-->
				</div>
			</div>
            <div class="card-body">
                <form class="row justify-content-center" action="<?php echo site_url('user/courses'); ?>" method="get">
                    <!-- Course Categories -->
                    <div class="col-xl-3">
                        <div class="form-group">
                            <label for="category_id"><?php echo get_phrase('categories'); ?></label>
                            <select class="form-control select2" data-toggle="select2" name="category_id" id="category_id">
                                <option value="<?php echo 'all'; ?>" <?php if($selected_category_id == 'all') echo 'selected'; ?>><?php echo get_phrase('all'); ?></option>
                                <?php foreach ($categories->result_array() as $category): ?>
                                    <optgroup label="<?php echo $category['name']; ?>">
                                        <?php $sub_categories = $this->crud_model->get_sub_categories($category['id']);
                                        foreach ($sub_categories as $sub_category): ?>
                                        <option value="<?php echo $sub_category['id']; ?>" <?php if($selected_category_id == $sub_category['id']) echo 'selected'; ?>><?php echo $sub_category['name']; ?></option>
                                    <?php endforeach; ?>
                                </optgroup>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <!-- Course Status -->
                <div class="col-xl-3">
                    <div class="form-group">
                        <label for="status"><?php echo get_phrase('status'); ?></label>
                        <select class="form-control select2" data-toggle="select2" name="status" id = 'status'>
                            <option value="all" <?php if($selected_status == 'all') echo 'selected'; ?>><?php echo get_phrase('all'); ?></option>
                            <option value="active" <?php if($selected_status == 'active') echo 'selected'; ?>><?php echo get_phrase('active'); ?></option>
                            <option value="pending" <?php if($selected_status == 'pending') echo 'selected'; ?>><?php echo get_phrase('pending'); ?></option>
                        </select>
                    </div>
                </div>

                <!-- Course Price -->
                <div class="col-xl-3">
                    <div class="form-group">
                        <label for="price"><?php echo get_phrase('price'); ?></label>
                        <select class="form-control select2" data-toggle="select2" name="price" id = 'price'>
                            <option value="all"  <?php if($selected_price == 'all' ) echo 'selected'; ?>><?php echo get_phrase('all'); ?></option>
                            <option value="free" <?php if($selected_price == 'free') echo 'selected'; ?>><?php echo get_phrase('free'); ?></option>
                            <option value="paid" <?php if($selected_price == 'paid') echo 'selected'; ?>><?php echo get_phrase('paid'); ?></option>
                        </select>
                    </div>
                </div>

                <div class="col-xl-3">
                    <label for=".." class="text-white"><?php echo get_phrase('..'); ?></label>
                    <button type="submit" class="btn bg-purple btn-block" name="button"><i class="mdi mdi-filter mr-1"></i><?php echo get_phrase('filter'); ?></button>
                </div>
            </form>

            <div class="table-responsive-sm mt-4">
                <?php if (count($courses) > 0): ?>
                    <table id="course-datatable" class="table table-striped dt-responsive nowrap" width="100%" data-page-length='10'>
                        <thead class="thead-purple">
                            <tr>
                                
                                <th class="hidden-xs"><?php echo get_phrase('title'); ?></th>
                                <th class="text-center hidden-xs"><?php echo get_phrase('lesson_&_section'); ?></th>
                                <th class="text-center hidden-xs"><?php echo get_phrase('enrolled_student'); ?></th>
                                <th class="text-center hidden-xs"><?php echo get_phrase('status'); ?></th>
                                <th class="text-center hidden-xs"><?php echo get_phrase('actions'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($courses as $key => $course):
                                $instructor_details = $this->user_model->get_all_user($course['user_id'])->row_array();
                                $category_details = $this->crud_model->get_category_details_by_id($course['sub_category_id'])->row_array();
                                $sections = $this->crud_model->get_section('course', $course['id']);
                                $lessons = $this->crud_model->get_lessons('course', $course['id']);
                                $enroll_history = $this->crud_model->enrol_history($course['id']);
                            ?>
                                <tr>
                                    
                                    <td>
                                        <strong><span class="text-primary"><?php echo ++$key; ?>)&nbsp;</span><a href="<?php echo site_url('user/course_form/course_edit/'.$course['id']); ?>"><?php echo ellipsis($course['title']); ?></a></strong><br>
                                        <span class="text-secondary"><?php echo get_phrase('instructor').': <b>'.$instructor_details['first_name'].' '.$instructor_details['last_name'].'</b>'; ?></span><br>
                                        <span class="text-secondary"><?php echo get_phrase('category').': <b class="badge badge-primary">'.$category_details['name'].'</b>'; ?></span>
										<?php if ($course['is_free_course'] == null): ?>
                                            <?php if ($course['discount_flag'] == 1): ?>
                                                <span class="badge badge-dark"><?php echo currency($course['discounted_price']); ?></span>
                                            <?php else: ?>
                                                <span class="badge badge-dark"><?php echo currency($course['price']); ?></span>
                                            <?php endif; ?>
                                        <?php elseif ($course['is_free_course'] == 1):?>
                                            <span class="badge badge-success"><?php echo get_phrase('free'); ?></span>
                                        <?php endif; ?>
                                    </td>
                                    
                                    <td class="text-center">
                                        <span class="text-secondary mr-1"><?php echo '<b>'.get_phrase('topics').'</b>: '.$sections->num_rows(); ?></span>|<span class="text-secondary ml-1"><?php echo '<b>'.get_phrase('chapters').'</b>: '.$lessons->num_rows(); ?></span>
										
                                    </td>
                                    <td class="text-center">
                                        <span class="text-secondary"><?php echo '<b>'.get_phrase('total_enrolment').'</b>: '.$enroll_history->num_rows(); ?></span>
                                    </td>
                                    <td class="text-center">
                                        <?php if ($course['status'] == 'pending'): ?>
                                            <i class="mdi mdi-circle text-warning mr-1" style="font-size: 12px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo get_phrase($course['status']); ?>"></i><small class="text-secondary"><b><?php echo get_phrase($course['status']); ?></b></small>
                                        <?php elseif ($course['status'] == 'active'):?>
                                            <i class="mdi mdi-circle text-success mr-1" style="font-size: 12px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo get_phrase($course['status']); ?>"></i><small class="text-secondary"><b><?php echo get_phrase($course['status']); ?></b></small>
                                        <?php elseif ($course['status'] == 'draft'):?>
                                            <i class="mdi mdi-circle text-secondary mr-1" style="font-size: 12px;" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo get_phrase($course['status']); ?>"></i><small class="text-secondary"><b><?php echo get_phrase($course['status']); ?></b></small>
                                        <?php endif; ?><br>
										<a class="btn btn-warning btn-xs mt-1 btn-block mb-0" href="<?php echo site_url('home/course/'.slugify($course['title']).'/'.$course['id']); ?>" target="_blank"><small class="text-secondary"><i class="mdi mdi-airplay mdi-10px "></i>&nbsp; View on Frontend</small></a>
                                    </td>
                                    <td class="text-center">
										<div class="col">
											<?php if ($course['status'] == 'active' || $course['status'] == 'pending'): ?>
                                                  <small><b><a class="btn btn-info btn-xs btn-block" href="#" onclick="confirm_modal('<?php echo site_url('user/course_actions/draft/'.$course['id']); ?>');"><i class="mdi mdi-eye-off mr-1"></i><?php echo get_phrase('mark_as_drafted'); ?></a></b></small>
                                              <?php else: ?>
                                                  <small><b><a class="btn btn-info btn-xs btn-block" href="#" onclick="confirm_modal('<?php echo site_url('user/course_actions/publish/'.$course['id']); ?>');"><i class="mdi mdi-eye mr-1"></i><?php echo get_phrase('publish_this_course'); ?></a></b></small>
                                              <?php endif; ?>
										</div>
										
										<div class="col">
											<a class="btn btn-primary btn-xs btn-block mb-0" href="<?php echo site_url('user/course_form/course_edit/'.$course['id']); ?>"><i class="mdi mdi-table-edit mr-1"></i><?php echo get_phrase('edit_this_course');?></a>
											<?php /*
											<a class="btn btn-danger btn-xs btn-block mb-0" href="#" onclick="confirm_modal('<?php echo site_url('user/course_actions/delete/'.$course['id']); ?>');"><i class="mdi mdi-delete mr-1"></i><?php echo get_phrase('delete_this_course'); ?></a>
											*/ ?>
										</div>
										
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
                <?php if (count($courses) == 0): ?>
                    <div class="img-fluid w-100 text-center">
                      <img style="opacity: 1; width: 100px;" src="<?php echo base_url('assets/backend/images/file-search.svg'); ?>"><br>
                      <?php echo get_phrase('no_data_found'); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
</div>
