<?php
// $param2 = course id
$course_details = $this->crud_model->get_course_by_id($param2)->row_array();
$sections       = $this->crud_model->get_section('course', $param2)->result_array();
?>
<form action="<?php
                echo site_url('user/lessons/' . $param2 . '/add');
                ?>" method="post" enctype="multipart/form-data">

    <div class="form-group">
        <label><?php echo get_phrase('chapter_title'); ?></label>
        <input type="text" name="title" id="lesson_title" class="form-control" required>
    </div>

    <input type="hidden" name="course_id" value="<?php echo $param2; ?>">
    <div class="row">
        <div class="form-group col-md-6">
            <label for="section_id"><?php echo get_phrase('topic'); ?></label>
            <select class="form-control select2" data-toggle="select2" name="section_id" id="section_id" required>
                <?php foreach ($sections as $section) : ?>
                    <option value="<?php echo $section['id']; ?>"><?php echo $section['title']; ?></option>
                <?php endforeach; ?>
            </select>
        </div>

        <div class="form-group col-md-6">
            <label for="section_id"><?php echo get_phrase('chapter_type'); ?></label>
            <select class="form-control select2" data-toggle="select2" name="lesson_type" id="lesson_type" required onchange="show_lesson_type_form(this.value)">
                <option value=""><?php echo get_phrase('select_type_of_chapter'); ?></option>
                <option value="video-url"><?php echo get_phrase('video'); ?></option>
                <option value="other-txt"><?php echo get_phrase('text_file'); ?></option>
                <option value="other-pdf"><?php echo get_phrase('pdf_file'); ?></option>
                <option value="other-doc"><?php echo get_phrase('document_file'); ?></option>
                <option value="other-img"><?php echo get_phrase('image_file'); ?></option>
            </select>
        </div>
    </div>
    <div class="" id="video" style="display: none;">

        <div class="form-group">
            <label for="lesson_provider"><?php echo get_phrase('chapter_provider'); ?></label>
            <select class="form-control select2" data-toggle="select2" name="lesson_provider" id="lesson_provider" onchange="check_video_provider(this.value)">
                <option value=""><?php echo get_phrase('select_chapter_provider'); ?></option>
                <option value="vimeo_upload"><?php echo get_phrase('upload_video'); ?></option>
                <option value="youtube"><?php echo get_phrase('youtube_link'); ?></option>
                <option value="vimeo"><?php echo get_phrase('vimeo_link'); ?></option>
                <option value="html5"><?php echo get_phrase('your_hosted_video_link'); ?></option>
                <?php /* <option value="upload"><?php echo get_phrase('upload_video'); ?></option> */ ?>
            </select>
        </div>



        <div class="" id="youtube_vimeo" style="display: none;">
            <div class="row">
                <div class="form-group col-md-8">
                    <label><?php echo get_phrase('video_url'); ?></label>
                    <input type="text" id="video_url" name="video_url" class="form-control" onblur="ajax_get_video_details(this.value)" placeholder="Upload to Youtube or Vimeo & Enter the video URL">
                    <label class="form-label" id="perloader" style="margin-top: 4px; display: none;"><i class="mdi mdi-spin mdi-loading">&nbsp;</i><?php echo get_phrase('analyzing_the_url'); ?></label>
                    <label class="form-label" id="invalid_url" style="margin-top: 4px; color: red; display: none;"><?php echo get_phrase('invalid_url') . '. ' . get_phrase('your_video_source_has_to_be_either_youtube_or_vimeo'); ?></label>
                </div>

                <div class="form-group col-md-4">
                    <label><?php echo get_phrase('duration'); ?></label>
                    <input type="text" name="duration" id="duration" class="form-control">
                </div>
            </div>
        </div>

        <div class="" id="html5" style="display: none;">
            <div class="row">
                <div class="form-group col-md-8">
                    <label><?php echo get_phrase('video_url'); ?></label>
                    <input type="text" id="html5_video_url" name="html5_video_url" class="form-control" placeholder="Enter your valid custom video URL link">
                </div>

                <div class="form-group col-md-4">
                    <label><?php echo get_phrase('duration'); ?></label>
                    <input type="text" class="form-control" data-toggle='timepicker' data-minute-step="5" name="html5_duration" id="html5_duration" data-show-meridian="false" value="00:00:00">
                </div>
            </div>

            <div class="form-group">
                <label><?php echo get_phrase('thumbnail_980x550px'); ?> <small>(For custom video URL, please enter a thumbnail for video preplay)</small> </label>
                <div class="input-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="thumbnail" name="thumbnail" onchange="changeTitleOfImageUploader(this)">
                        <label class="custom-file-label" for="thumbnail"><?php echo get_phrase('thumbnail'); ?></label>
                    </div>
                </div>
            </div>
        </div>

        <!-- #DK [START] -->
        <div class="" id="upload" style="display: none;">
            <div class="form-group">
                <label> <?php echo get_phrase('upload_video_file'); ?></label>
                <div class="badge badge-danger ml-1">
                    Max File size accepted is 256mb.
                </div>
                <div class="input-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="upload_video" name="upload_video" onchange="changeTitleOfImageUploader(this)">
                        <label class="custom-file-label" for="upload_video"><?php echo get_phrase('upload_mp4_video_file_here'); ?></label>
                    </div>

                </div>
                <div id="dk_file_upload_content" class="blueimp">
                    <input type="hidden" name="uploaded_file_name" class="w-100" id="uploaded_file_name">
                    <div class="row">
                        <div class="col-sm-12 mb-1">
                            <div id="process_msg" class="mt-1"></div> <!-- process msg display here -->
                            <div id="upload_video_progress_bar" class="progress" style="display: none;">
                                <div id="upload_video_progress" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="46" aria-valuemin="0" aria-valuemax="100" style="width: 0%">&nbsp;0%</div>
                            </div>
                        </div>
                    </div>
                    <!-- The file list will be shown here -->
                    <div id="video_preview"></div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label><?php echo get_phrase('duration'); ?></label>
                    <input type="text" class="form-control" data-toggle='timepicker' data-minute-step="5" name="upload_video_duration" id="upload_video_duration" data-show-meridian="false" value="00:00:00">
                </div>
                <div class="form-group col-md-8">
                    <label><?php echo get_phrase('thumbnail_980x550px'); ?><small> (For video upload, please enter a thumbnail for video preplay)</small></label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="thumbnail" name="upload_video_thumbnail" onchange="changeTitleOfImageUploader(this)">
                            <label class="custom-file-label" for="thumbnail"><?php echo get_phrase('thumbnail'); ?></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- vimeo upload -->
        <div class="" id="vimeo_upload" style="display: none;">
            <div class="form-group">
                <label> <?php echo get_phrase('upload_video_file'); ?></label>
                <div class="input-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="vimeo_upload_video" name="vimeo_upload_video" onchange="changeTitleOfImageUploader(this)">
                        <label class="custom-file-label" for="upload_video"><?php echo get_phrase('select_your_video_file_here'); ?></label>
                    </div>
                </div>
                <div id="dk_vimeo_file_upload_content" class="blueimp">
                    <input type="hidden" name="vimeo_uploaded_file_name" class="w-100" id="vimeo_uploaded_file_name">
                    <div class="row">
                        <div class="col-sm-12 mb-1">
                            <div id="vimeo_process_msg" class="mt-1"></div> <!-- process msg display here -->
                            <div id="vimeo_progress_bar" class="progress" style="display: none;">
                                <div id="vimeo_progress" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="46" aria-valuemin="0" aria-valuemax="100" style="width: 0%">&nbsp;0%</div>
                            </div>
                        </div>
                    </div>
                    <!-- The file list will be shown here -->
                    <div id="vimeo_video_preview"></div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label><?php echo get_phrase('duration'); ?></label>
                    <input type="text" class="form-control" data-toggle='timepicker' data-minute-step="5" name="vimeo_upload_video_duration" id="vimeo_upload_video_duration" data-show-meridian="false" value="00:00:00">
                </div>
                <div class="form-group col-md-8">
                    <label><?php echo get_phrase('thumbnail_980x550px'); ?><small> (For video upload, please enter a thumbnail for video preplay)</small></label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="thumbnail" name="vimeo_upload_video_thumbnail" onchange="changeTitleOfImageUploader(this)">
                            <label class="custom-file-label" for="thumbnail"><?php echo get_phrase('thumbnail'); ?></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $vimeo_key = $this->crud_model->get_vimeo_key('vimeo'); ?>
        <input type="hidden" id="accessToken" class="form-control" placeholder="Vimeo access token" value="<?php echo base64_encode($vimeo_key[0]->vimeo_access_token); ?>" required autofocus />
        <!-- #DK [END] -->
    </div>

    <div class="" id="other" style="display: none;">
        <div class="form-group">
            <label> <?php echo get_phrase('attachment'); ?></label>
            <div class="input-group">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="attachment" name="attachment" onchange="changeTitleOfImageUploader(this)">
                    <label class="custom-file-label" for="attachment"><?php echo get_phrase('attachment'); ?></label>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label><?php echo get_phrase('summary'); ?></label>
        <textarea name="summary" class="form-control"></textarea>
    </div>

    <div class="text-center">
        <button id="submit_lesson_btn" class="btn btn-primary btn-block" type="submit" name="button"><?php echo get_phrase('add_chapter'); ?></button>
    </div>
</form>
<script type="text/javascript">
    $(document).ready(function() {
        initSelect2(['#section_id', '#lesson_type', '#lesson_provider']);
        initTimepicker();
    });

    function ajax_get_video_details(video_url) {
        $('#perloader').show();
        if (checkURLValidity(video_url)) {
            $.ajax({
                url: '<?php echo site_url('user/ajax_get_video_details'); ?>',
                type: 'POST',
                data: {
                    video_url: video_url
                },
                success: function(response) {
                    jQuery('#duration').val(response);
                    $('#perloader').hide();
                    $('#invalid_url').hide();
                }
            });
        } else {
            $('#invalid_url').show();
            $('#perloader').hide();
            jQuery('#duration').val('');

        }
    }

    function checkURLValidity(video_url) {
        var youtubePregMatch = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
        var vimeoPregMatch = /^(http\:\/\/|https\:\/\/)?(www\.)?(vimeo\.com\/)([0-9]+)$/;
        if (video_url.match(youtubePregMatch)) {
            return true;
        } else if (vimeoPregMatch.test(video_url)) {
            return true;
        } else {
            return false;
        }
    }

    function show_lesson_type_form(param) {
        var checker = param.split('-');
        var lesson_type = checker[0];
        if (lesson_type === "video") {
            $('#other').hide();
            $('#video').show();
        } else if (lesson_type === "other") {
            $('#video').hide();
            $('#other').show();
        } else {
            $('#video').hide();
            $('#other').hide();
        }
    }

    function check_video_provider(provider) {
        $('#submit_lesson_btn').prop('disabled', false);
        if (provider === 'youtube' || provider === 'vimeo') {
            $('#youtube_vimeo').show();
            $('#html5,#upload,#vimeo_upload').hide();
        } else if (provider === 'html5') {
            $('#html5').show();
            $('#youtube_vimeo,#upload,#vimeo_upload').hide();
        } else if (provider === 'upload') {
            $('#upload').show();
            $('#submit_lesson_btn').prop('disabled', true);
            $('#youtube_vimeo,#html5,#vimeo_upload').hide();
        } else if (provider === 'vimeo_upload') {
            $('#vimeo_upload').show();
            $('#submit_lesson_btn').prop('disabled', true);
            $('#upload,#youtube_vimeo,#html5').hide();
        } else {
            $('#vimeo_upload,#upload,#youtube_vimeo,#html5').hide();
        }
    }
</script>
<!-- #DK -->
<script src="<?php echo site_url('assets/global/blueimp/jquery.ui.widget.js'); ?>"></script>
<script src="<?php echo site_url('assets/global/blueimp/jquery.fileupload.js'); ?>"></script>
<script>
    $(document).ready(function() {
        $('#process_msg').html('');
        $('#upload_video').fileupload({
            url: '<?php echo base_url('user/lession_video_upload'); ?>',
            type: 'POST',
            autoUpload: false,
            replaceFileInput: false,
            acceptFileTypes: /(\.|\/)(mp4)$/i,
            maxChunkSize: 1000000, // 10 MB Earlier 10000000, // 10 MB Removed one 0
            // This function is called when a file is added to the queue
            add: function(e, data) {
                $('.modal').css('overflow-y', 'auto');
                var fileType = data.files[0].name.split('.').pop(),
                    allowdtypes = 'mp4';
                if (allowdtypes.indexOf(fileType) < 0) {
                    $('#process_msg').html('<strong class="text-danger">Invalid file type. Only Accept mp4 Video format.</strong>');
                    return false;
                }
                $('#upload_video').prop('disabled', true);
                //This area will contain file list and progress information.
                // var tpl = $('<div class="row working" id="v_name">' + '<div class="col-sm-4"><div id="upload_actions"><button type="button" class="btn btn-primary upload">Upload</button> <button type="button" class="btn btn-primary cancel">cancel</button></div></div></div>');
                var tpl = $('<div class="working"><div id="v_name">' + '</div></div>');
                // Append the videp name and size and preview
                tpl.find('#v_name').append('<div class="row">' + '<div class="col-sm-6"><video id="preview" width="320" src="' + URL.createObjectURL(data.files[0]) + '" controls/></div>' + '<div class="col-sm-6"><div class="row"><div class="col-sm-12 check"><label> File Size: <strong>' + formatFileSize(data.files[0].size) + ' (Please do not refresh/close this page till you get Upload Successful message above.)</strong></label></div>' + '<div class="col-sm-12"><div id="upload_actions"><button type="button" class="btn btn-block btn-primary upload">Upload</button> <button type="button" class="btn btn-block btn-danger cancel">Cancel</button></div></div></div></div>' + '</div>');
                // Add the HTML to the UL element
                data.context = tpl.appendTo('div#video_preview');
                //cancel file
                tpl.find('button.cancel').click(function() {
                    tpl.fadeOut(function() {
                        $('#upload_video_progress_bar').hide();
                        $('#process_msg').html('');
                        $('#upload_video').prop('disabled', false);
                        tpl.remove();
                    });
                });
                //upload file
                tpl.find('button.upload').click(function() {
                    var jqXHR = data.submit();
                });
            },
            progress: function(e, data) {
                // $('#process_msg').html('<strong class="text-warning">Uploading...</strong>');
                $('#process_msg').html('');
                $('#upload_video_progress_bar').show();
                $('#upload_actions').remove();
                // Calculate the completion percentage of the upload
                var progress = parseInt(data.uploadedBytes / data.total * 100, 10);
                $('#upload_video_progress').text(progress + '%').animate({
                    width: progress + '%'
                }, 5);
                // Update the hidden input field and trigger a change
                // so that the jQuery knob plugin knows to update the dial
                data.context.find('input').val(progress).change();
                if (progress == 100) {
                    data.context.removeClass('working');
                }
            },
            done: function(e, data) {
                if (data.textStatus == 'success') {
                    $('#process_msg').html('<strong class="text-success">File Uploaded Successfully.</strong>');
                    var obj = JSON.parse(data.result);
                    $('#uploaded_file_name').val(obj.upload_video[0].name);
                    // $('#uploaded_file_name').val(obj.files.file_name);
                    $('#upload_video_progress_bar').hide();
                    $('#submit_lesson_btn').prop('disabled', false);
                } else {
                    $('#process_msg').html('<strong class="text-warning">Try again something went wrong</strong>');
                    $('#upload_video').prop('disabled', true);
                }
            }
        });
    });
    //Helper function for calculation of progress
    function formatFileSize(bytes) {
        if (typeof bytes !== 'number') {
            return '';
        }
        if (bytes >= 1000000000) {
            return (bytes / 1000000000).toFixed(2) + ' GB';
        }

        if (bytes >= 1000000) {
            return (bytes / 1000000).toFixed(2) + ' MB';
        }
        return (bytes / 1000).toFixed(2) + ' KB';
    }
    //set video duration on uploading
    document.getElementById('upload_video').onchange = function() {
        var myVideos = [];
        var files = this.files;
        myVideos.push(files[0]);
        var video = document.createElement('video');
        video.preload = 'metadata';

        video.onloadedmetadata = function() {
            window.URL.revokeObjectURL(video.src);
            var duration = video.duration;
            let totalSeconds = duration;
            let hours = Math.floor(totalSeconds / 3600);
            totalSeconds %= 3600;
            let minutes = Math.floor(totalSeconds / 60);
            let seconds = Math.floor(totalSeconds % 60);

            minutes = String(minutes).padStart(2, "0");
            hours = String(hours).padStart(2, "0");
            seconds = String(seconds).padStart(2, "0");
            $('#upload_video_duration').val(hours + ":" + minutes + ":" + seconds);
        }
        video.src = URL.createObjectURL(files[0]);
    }

    // <!------------------ vimeo upload script -----------------!>
    $(document).ready(function() {
        $('#vimeo_process_msg').html('');
        $('#vimeo_upload_video').fileupload({
            url: '<?php echo base_url('user/vimeo_video_upload'); ?>',
            type: 'POST',
            autoUpload: false,
            replaceFileInput: false,
            acceptFileTypes: /(\.|\/)(mp4)$/i,
            // This function is called when a file is added to the queue
            add: function(e, data) {
                var fileType = data.files[0].name.split('.').pop(),
                    allowdtypes = 'mp4';
                if (allowdtypes.indexOf(fileType) < 0) {
                    $('#vimeo_process_msg').html('<strong class="text-danger">Invalid file type. Only Accept mp4 Video format.</strong>');
                    return false;
                }
                // $('#vimeo_upload_video').prop('disabled', true);
                //This area will contain file list and progress information.
                var tpl = $('<div class="working"><div id="vimeo_name">' + '</div></div>');
                // Append the videp name and size and preview
                tpl.find('#vimeo_name').append('<div class="row">' + '<div class="col-sm-6"><video id="vimeo_preview" width="320" src="' + URL.createObjectURL(data.files[0]) + '" controls/></div>' + '<div class="col-sm-6"><div class="row"><div class="col-sm-12"><label> File Size: <strong>' + formatFileSize(data.files[0].size) + ' (Please do not refresh/close this page till you get Upload Successful message above.)</strong></label></div>' + '<div class="col-sm-12"><div id="vimeo_upload_actions"><button type="button" class="btn btn-primary btn-block upload">Upload</button> <button type="button" class="btn btn-block btn-danger cancel">Cancel</button></div></div></div></div>' + '</div>');
                // Add the HTML to the UL element
                data.context = tpl.appendTo('div#vimeo_video_preview');
                //cancel file
                tpl.find('button.cancel').click(function() {
                    tpl.fadeOut(function() {
                        $('#vimeo_process_msg').html('');
                        $('#vimeo_upload_video').prop('disabled', false);
                        tpl.remove();
                    });
                });
                //upload file
                tpl.find('button.upload').click(function() {
                    // var jqXHR = data.submit();
                    var browse = document.getElementById('vimeo_upload_video');
                    browse.addEventListener('click', handleFileSelect, false);
                    $('#vimeo_upload_video').trigger('click');
                    $('#vimeo_upload_video').prop('disabled', true);
                });
            },
            progress: function(e, data) {
                $('#vimeo_process_msg').html('<strong class="text-warning"><i class="mdi mdi-spin mdi-loading"></i> Uploading under process...</strong>');
                $('#vimeo_upload_actions').remove();
            },
            done: function(e, data) {
                if (data.textStatus == 'success') {
                    $('#vimeo_process_msg').html('<strong class="text-success">File Uploaded Successfully.</strong>');
                    var obj = JSON.parse(data.result);
                    $('#vimeo_uploaded_file_name').val(obj.response);
                    $('#submit_lesson_btn').prop('disabled', false);
                } else {
                    $('#vimeo_process_msg').html('<strong class="text-warning">Try again something went wrong</strong>');
                    $('#vimeo_upload_video').prop('disabled', true);
                }
            }
        });
    });

    //set video duration on uploading
    document.getElementById('vimeo_upload_video').onchange = function() {
        var myVideos = [];
        var files = this.files;
        myVideos.push(files[0]);
        var video = document.createElement('video');
        video.preload = 'metadata';

        video.onloadedmetadata = function() {
            window.URL.revokeObjectURL(video.src);
            var duration = video.duration;
            let totalSeconds = duration;
            let hours = Math.floor(totalSeconds / 3600);
            totalSeconds %= 3600;
            let minutes = Math.floor(totalSeconds / 60);
            let seconds = Math.floor(totalSeconds % 60);

            minutes = String(minutes).padStart(2, "0");
            hours = String(hours).padStart(2, "0");
            seconds = String(seconds).padStart(2, "0");
            $('#vimeo_upload_video_duration').val(hours + ":" + minutes + ":" + seconds);
        }
        video.src = URL.createObjectURL(files[0]);
    }
</script>

<script src="<?php echo site_url('assets/global/vimeo/vimeo-upload.js'); ?>"></script>
<script>
	/**
	 * vimeo upload using javasrcipt, script called when click on upload button.
	 */
	function handleFileSelect(evt) {
		evt.stopPropagation()
		evt.preventDefault()

		var files = evt.dataTransfer ? evt.dataTransfer.files : $(this).get(0).files;
        var results = document.getElementById('vimeo_process_msg');
		/* Clear the results div */
		while (results.hasChildNodes()) results.removeChild(results.firstChild)

		/* Rest the progress bar and show it */
		updateProgress(0)
		document.getElementById('vimeo_progress_bar').style.display = 'block';		
		$('#vimeo_upload_actions').remove();

		/* Instantiate Vimeo Uploader */
		;
		(new VimeoUpload({
			name: document.getElementById('lesson_title').value,
			// description: document.getElementById('videoDescription').value,
			// private: document.getElementById('make_private').checked,
			file: files[0],
			token: atob(document.getElementById('accessToken').value),
			// upgrade_to_1080: document.getElementById('upgrade_to_1080').checked,
			onError: function(data) {
				showMessage('<strong>Error</strong>: ' + JSON.parse(data).error, 'danger')
			},
			onProgress: function(data) {
				updateProgress(data.loaded / data.total)
			},
			onComplete: function(videoId, index) {
				var url = 'https://vimeo.com/' + videoId				

				if (index > -1) {
					/* The metadata contains all of the uploaded video(s) details see: https://developer.vimeo.com/api/endpoints/videos#/{video_id} */
					url = this.metadata[index].link //
					/* add stringify the json object for displaying in a text area */
					// var pretty = JSON.stringify(this.metadata[index], null, 2)
					// console.log(pretty) /* echo server data */
				}
				$('#vimeo_uploaded_file_name').val(btoa(url));
                $('#submit_lesson_btn').prop('disabled', false);
				showMessage('<strong>Upload Successful</strong>: Please note that after successfull upload we would transcode this video for web streaming and it might take upto 30 minutes depending on your uploaded file size. Video would be available on coursewings after it has finished transcoding.');
			}
		})).upload();

		/* local function: show a user message */
		function showMessage(html, type) {
			/* hide progress bar */
			document.getElementById('vimeo_progress_bar').style.display = 'none'

			/* display alert message */
			var element = document.createElement('div')
			element.setAttribute('class', 'alert alert-' + (type || 'success'))
			element.innerHTML = html
			results.appendChild(element)
		}
	}
	/**
	 * Updat progress bar.
	 */
	function updateProgress(progress) {
		progress = Math.floor(progress * 100)
		var element = document.getElementById('vimeo_progress')
		element.setAttribute('style', 'width:' + progress + '%')
		element.innerHTML = '&nbsp;' + progress + '%'
	}
</script>