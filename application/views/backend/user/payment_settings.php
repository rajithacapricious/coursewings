<?php
$user_data = $this->user_model->get_user($this->session->userdata('user_id'))->row_array();
$paypal_keys = json_decode($user_data['paypal_keys'], true);
$stripe_keys = json_decode($user_data['stripe_keys'], true);
?>
<div class="row">
    <div class="col-12">
        <ol class="breadcrumb yellow mt-2">
            <li class="breadcrumb-item"><a href="<?php echo site_url(); ?>"><i class="mdi mdi-home mr-1"></i></a></li>
            <li class="breadcrumb-item"><a href="<?php echo site_url('user/courses'); ?>"><i class="mdi mdi-view-dashboard mr-1"></i>Dashboard</a></li>
            <li class="breadcrumb-item active"><a href="#"><i class="mdi mdi-credit-card mr-1"></i><?php echo get_phrase('setup_your_payment_informations'); ?></a></li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-header purple">

                <div class="row">
                    <div class="col-xl-12 col-sm-12">
                        <h4 class="header-title mt-1"><i class="mdi mdi-credit-card mr-1"></i><?php echo get_phrase('your_payment_information'); ?></h4>                         
                    </div>
                </div>
            </div>

            <div class="card-body row">
                <div class="col-xl-12 col-sm-12">
                    <div class="card cta-box bg-info text-white">
                        <div class="card-body">
                            <div class="">

                                <form class="" action="<?php echo site_url('user/payment_settings/paypal_settings'); ?>" method="post" enctype="multipart/form-data">


                                    <!--<h3 class="m-0 font-weight-normal cta-box-title"><b>Setup bank details</b></h3>-->

                                    <br>
                                    <!--<div class="form-group">
                                        <label>Bank Details</label>
                                        <textarea name="bank_details" class="form-control" /><?php if(isset($paypal_keys[0]['bank_details'])) echo $paypal_keys[0]['bank_details']; else echo ""; ?></textarea>
                                        <div class="">Insert your Account holder name, Bank name, Account number, Branch , IBAN/IFSC Etc</div>
                                    </div>-->


                                    <h3 class="m-0 font-weight-normal cta-box-title"><b><?php echo get_phrase('setup_paypal_settings'); ?></b></h3>

                                    <br>
                                    <div class="form-group">
                                        <label>Paypal Email</label>
                                        <input type="text" name="paypal_email" class="form-control" value="<?php if(isset($paypal_keys[0]['paypal_email'])) echo $paypal_keys[0]['paypal_email']; else echo ""; ?>" required />
                                    </div>
                                    <div class="form-group">
                                        <!-- <label> echo get_phrase('paste_your_client_') . ' ID' . get_phrase('_here') . ''; ?></label> -->
										<label>
											Please see below on how to generate 'Client ID'
										</label>
                                        <input type="text" name="paypal_client_id" class="form-control" value="<?php echo $paypal_keys[0]['production_client_id']; ?>" required />
                                    </div>
                                    <div class="row justify-content-md-center">
                                        <div class="form-group col-md-12">
                                            <button class="btn btn-block btn-primary" type="submit"><i class="fab fa-paypal mr-1"></i><?php echo get_phrase('update_paypal_keys'); ?></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <?php /*
                  <div class="col-xl-6 col-sm-12">
                  <div class="card cta-box bg-success text-white">
                  <div class="card-body">
                  <div class="media align-items-center">
                  <div class="media-body">
                  <h2 class="mt-0"><i class="mdi mdi-bullhorn-outline mdi-48px"></i></h2>
                  <h3 class="m-0 font-weight-normal cta-box-title">Offline <b>Payments</b> Coming Soon <i class="mdi mdi-arrow-right-bold-outline"></i></h3>
                  </div>
                  <img class="ml-3" src="<?php echo site_url('assets/backend/images/cheque.png'); ?>" alt="Generic placeholder image" width="285">
                  </div>
                  </div>
                  </div>

                  </div>
                 */ ?>
                <div class="col-xl-12 col-sm-12">
                    <p>Prior to configuring the PayPal Payment Gateway integration, the author must have a PayPal business or personal account. Paypal Client ID value is required for recieving payments from Course Wings.</p>

                    <p>Please follow below steps to get paypal client ID. (This allows Course Wings system to authenticate with PayPal)</p>

                    <ol>
                        <li>Go to: <strong><a href="https://developer.paypal.com/developer/applications/" style="color:#078edf !important" target="_blank">https://developer.paypal.com/developer/applications/</a></strong></li>
                        <li>Log into Dashboard on the top right with your PayPal credentials. (Please note that your Course Wings account email ID & Paypal email ID should be the same)</li>
                        <li>Navigate to the My Apps & Credentials tab.</li>
                        <li>Please click on the Live Button.</li>
                        <li>Click the Create App button in the REST API Apps section.</li>
                        <li>Give any name to your app and click on Create App button.</li>
                        <li>Copy the client ID from paypal.</li>
                        <li>Go to your instructors panel in Course Wings and paste this client ID in "Payment Settings Page" and press Update PayPal Keys. On this screen the Client ID is on top.</li>
                    </ol>
                    <p>Please note the Live client ID should be posted in Course Wings Payment Settings (Not Sandbox client ID, as sandbox is just for testing purposes)</p>
                </div>
            </div>
        </div>
    </div>
    <?php /*
      <div class="row">
      <div class="col-md-7" style="padding: 0;">
      <div class="col-md-12">
      <div class="card">

      <div class="card-body">
      <h4 class="header-title"><p><?php echo get_phrase('setup_paypal_settings'); ?></p></h4>
      <form class="" action="<?php echo site_url('user/payment_settings/paypal_settings'); ?>" method="post" enctype="multipart/form-data">

      <div class="form-group">
      <label><?php echo get_phrase('client_id').' ('.get_phrase('production').')'; ?></label>
      <input type="text" name="paypal_client_id" class="form-control" value="<?php echo $paypal_keys[0]['production_client_id']; ?>" required />
      </div>

      <div class="row justify-content-md-center">
      <div class="form-group col-md-6">
      <button class="btn btn-block btn-primary" type="submit"><?php echo get_phrase('update_paypal_keys'); ?></button>
      </div>
      </div>
      </form>
      </div>
      </div>
      </div>

      <div class="col-md-12">
      <div class="card">
      <div class="card-body">
      <h4 class="header-title"><p><?php echo get_phrase('setup_stripe_settings'); ?></p></h4>
      <form class="" action="<?php echo site_url('user/payment_settings/stripe_settings'); ?>" method="post" enctype="multipart/form-data">
      <div class="form-group">
      <label><?php echo get_phrase('live_secret_key'); ?></label>
      <input type="text" name="stripe_secret_key" class="form-control" value="<?php echo $stripe_keys[0]['secret_live_key']; ?>" required />
      </div>

      <div class="form-group">
      <label><?php echo get_phrase('live_public_key'); ?></label>
      <input type="text" name="stripe_public_key" class="form-control" value="<?php echo $stripe_keys[0]['public_live_key']; ?>" required />
      </div>

      <div class="row justify-content-md-center">
      <div class="form-group col-md-6">
      <button class="btn btn-block btn-primary" type="submit"><?php echo get_phrase('update_stripe_keys'); ?></button>
      </div>
      </div>
      </form>
      </div>
      </div>
      </div>
      </div>
      </div>
     */ ?>
