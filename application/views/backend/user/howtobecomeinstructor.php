<link rel="stylesheet" href="<?php echo base_url();?>assets/global/plyr/plyr.css">
<div class="row">
	<div class="col-12">
		<ol class="breadcrumb yellow mt-2">
			<li class="breadcrumb-item active text-purple"><i class="mdi mdi-view-dashboard mr-1"></i><?php echo get_phrase('how_to_become_instructor'); ?></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xl-12">
		<div class="plyr__video-embed" id="player3">
		  <iframe height="500" src="https://player.vimeo.com/video/378469843?loop=false&amp;byline=false&amp;portrait=false&amp;title=false&amp;speed=true&amp;transparent=0&amp;gesture=media" allowfullscreen allowtransparency allow="autoplay"></iframe>
		</div>
	</div>
</div>



<style media="screen">
	.plyr__video-embed {
	  height: 450px;
	}
</style>

<script src="<?php echo base_url();?>assets/global/plyr/plyr.js"></script>
<script>
	const player = new Plyr('#player3');
</script>