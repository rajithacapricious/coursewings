<div class="row">
	<div class="col-12">
		<ol class="breadcrumb yellow mt-2">
			<li class="breadcrumb-item"><a href="<?php echo site_url(); ?>"><i class="mdi mdi-home mr-1"></i></a></li>
			<li class="breadcrumb-item text-purple"><a href="<?php echo site_url('user/courses'); ?>"><i class="mdi mdi-view-dashboard mr-1"></i>Dashboard</a></li>
			<li class="breadcrumb-item active text-purple"><a href="#"><i class="mdi mdi-book-plus mr-1"></i><?php echo get_phrase('add_new_course'); ?></a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xl-12">
		<div class="card">
			<div class="card-header purple">

				<div class="row">
					<div class="col-6 col-xl-8">
						<h4 class="header-title mt-1"><i class="mdi mdi-book-plus mr-1"></i><?php echo get_phrase('create_a_new_course_'); ?>:<?php echo get_phrase('_basic_details'); ?></h4>
					</div>
					<div class="col-6 col-xl-4">
						<div class="text-lg-right mt-1">

							<a href="<?php echo site_url('user/courses'); ?>" class="btn btn-xs bg-yellow mb-0"><i class="mdi mdi-backspace mr-1"></i><?php echo get_phrase('back_to_course_list'); ?></a>
						</div>
					</div>
				</div>
			</div>
			<div class="card-body">

				<div class="row">
					<div class="col-xl-12">
						<form class="required-form" action="<?php echo site_url('user/course_actions/add'); ?>" method="post" enctype="multipart/form-data">
							<div class="row justify-content-center">
								<div class="col-lg-8">
									<div>
										<div class="form-group row mb-1">
											<label class="col-md-12 col-form-label" for="course_title"><?php echo get_phrase('enter_a_nice_title_for_your_course'); ?> <span class="required">*</span> </label>
											<div class="col-md-12">
												<input type="text" class="form-control" id="course_title" name="title" placeholder="<?php echo get_phrase('add_course_title'); ?>" required>
											</div>
										</div>
										<div class="form-group row mb-1">
											<label class="col-md-12 col-form-label" for="short_description"><?php echo get_phrase('Short_course_description'); ?></label>
											<div class="col-md-12">
												<textarea name="short_description" id="short_description" class="form-control" placeholder="Enter a short course description"></textarea>
											</div>
										</div>
										<div class="form-group row mb-1">
											<label class="col-md-12 col-form-label" for="description"><?php echo get_phrase('detailed_course_description'); ?></label>
											<div class="col-md-12">

												<textarea name="description" id="description" class="form-control"></textarea>
											</div>
										</div>

										<?php /*
										<div class="form-group row mb-1">
											<div class="offset-md-2 col-md-10">
												<div class="custom-control custom-checkbox">
													<input type="checkbox" class="custom-control-input" name="is_top_course" id="is_top_course" value="1">
													<label class="custom-control-label" for="is_top_course"><?php echo get_phrase('check_if_this_course_is_top_course'); ?></label>
												</div>
											</div>
										</div>
										*/ ?>

									</div> <!-- End BasicID -->

									<div>
										<div class="form-group row mb-1">
											<label class="col-md-12 col-form-label" for="requirements"><?php echo get_phrase('course_requirements'); ?></label>
											<div class="col-md-12">
												<div id="requirement_area">
													<div class="d-flex mt-2">
														<div class="flex-grow-1 pl-0 pr-3">
															<div class="form-group">
																<input type="text" class="form-control" name="requirements[]" id="requirements" placeholder="What qualifications do students need to take this course? Please click the plus icon to add more.">
															</div>
														</div>
														<div class="">
															<button type="button" class="btn bg-blue text-white btn-sm" style="min-height: 36px;margin-top: 1px;" name="button" onclick="appendRequirement()"> <i class="fa fa-plus-circle"></i> </button>
														</div>
													</div>
													<div id="blank_requirement_field">
														<div class="d-flex mt-2">
															<div class="flex-grow-1 pl-0 pr-3">
																<div class="form-group">
																	<input type="text" class="form-control" name="requirements[]" id="requirements" placeholder="<?php echo get_phrase('what_qualification_do_students_need_to_take_this_course._click_the_plus_icon_to_add_more'); ?>">
																</div>
															</div>
															<div class="">
																<button type="button" class="btn btn-danger btn-sm" style="min-height: 36px;margin-top: 1px;" name="button" onclick="removeRequirement(this)"> <i class="fas fa-trash-alt"></i> </button>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div> <!-- End RequirementsID -->

									<div>
										<div class="form-group row mb-1">
											<label class="col-md-12 col-form-label" for="outcomes"><?php echo get_phrase('course_outcomes'); ?></label>
											<div class="col-md-12">
												<div id="outcomes_area">
													<div class="d-flex mt-2">
														<div class="flex-grow-1 pl-0 pr-3">
															<div class="form-group">
																<input type="text" class="form-control" name="outcomes[]" id="outcomes" placeholder="What will students learn after this course? Click on the plus icon to add more.">
															</div>
														</div>
														<div class="">
															<button type="button" class="btn text-white bg-blue btn-sm" style="min-height: 36px;margin-top: 1px;" name="button" onclick="appendOutcome()"> <i class="fa fa-plus-circle"></i> </button>
														</div>
													</div>
													<div id="blank_outcome_field">
														<div class="d-flex mt-2">
															<div class="flex-grow-1 pl-0 pr-3">
																<div class="form-group">
																	<input type="text" class="form-control" name="outcomes[]" id="outcomes" placeholder="<?php echo get_phrase('what_will_students_learn_after_this_course?_click_on_plus_icon_to_add_more'); ?>">
																</div>
															</div>
															<div class="">
																<button type="button" class="btn btn-danger btn-sm" style="min-height: 36px;margin-top: 1px;" name="button" onclick="removeOutcome(this)"> <i class="fas fa-trash-alt"></i> </button>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div> <!-- End OutcomesID -->





									<div>
										<div class="form-group row mb-1">
											<label class="col-md-12 col-form-label" for="website_keywords"><?php echo get_phrase('meta_keywords'); ?><small class="mr-1"><i class="mr-1">(for S.E.O)</i> Press 'Enter' After Every Word.</small><a class="ml-1" data-toggle="tooltip" title="Meta Keywords are a specific type of meta tag that appear in the HTML code of a Web page and help tell search engines what the topic of the page is."><i class="mdi mdi-help-box"></i></a></label>
											<div class="col-md-12">
												<input type="text" class="form-control bootstrap-tag-input" placeholder="Eg. For Marketing related course - use keywords like Marketing, Sales, Strategies, Business Plan etc." id="meta_keywords" name="meta_keywords" data-role="tagsinput" style="width: 100%;" />
											</div>
										</div>

										<div class="form-group row mb-1">
											<label class="col-md-12 col-form-label" for="meta_description"><?php echo get_phrase('meta_description'); ?><small class="mr-1"><i>(for S.E.O)</i></small><a class="ml-1" data-toggle="tooltip" title="The meta description is an HTML attribute that provides a brief summary of a web page. Search engines such as Google often display the meta description in search results where they can highly influence user click-through rates."><i class="mdi mdi-help-box"></i></a></label>
											<div class="col-md-12">
												<textarea name="meta_description" placeholder="Write a short description of the course for search pages" class="form-control"></textarea>
											</div>
										</div>
									</div> <!-- end SEOID -->

									<div class="card cta-box bg-blue text-white">
										<div class="card-body">
											<div class="text-center">
												<h3 class="m-0 font-weight-normal cta-box-title"><b>IMPORTANT</b></h3>
												<i class="mdi mdi-alert-octagon mdi-48px"></i><br>
												<a class="font-weight-bold">After submitting basic course details, you will be taken to add section & lesson page.<br><span class="mt-2 mb-0 font-weight-bold" style="color:#f7a823">Please make sure to add sections and lessons</span></a>
											</div>
										</div>
										<!-- end card-body -->
									</div>


									<?php /*
									<ul class="list-inline mb-0 wizard text-center">
										<li class="previous list-inline-item">
											<a href="javascript::" class="btn btn-info"> <i class="mdi mdi-arrow-left-bold"></i> </a>
										</li>
										<li class="next list-inline-item">
											<a href="javascript::" class="btn btn-info"> <i class="mdi mdi-arrow-right-bold"></i> </a>
										</li>
									</ul>
									*/ ?>
								</div>

								<div class="col-lg-4">
									<div>
										<div class="form-group row mb-1">
											<label class="col-md-12 col-form-label" for="sub_category_id"><?php echo get_phrase('course_category'); ?><span class="required">*</span></label>
											<div class="col-md-12">
												<select class="form-control select2" data-toggle="select2" name="sub_category_id" id="sub_category_id" required>
													<option value=""><?php echo get_phrase('select_a_category'); ?></option>
													<?php foreach ($categories->result_array() as $category) : ?>
														<optgroup label="<?php echo $category['name']; ?>">
															<?php $sub_categories = $this->crud_model->get_sub_categories($category['id']);
																foreach ($sub_categories as $sub_category) : ?>
																<option value="<?php echo $sub_category['id']; ?>"><?php echo $sub_category['name']; ?></option>
															<?php endforeach; ?>
														</optgroup>
													<?php endforeach; ?>
												</select>
											</div>
										</div>
										<div class="form-group row mb-1">
											<label class="col-md-12 col-form-label" for="level"><?php echo get_phrase('course_level'); ?></label>
											<div class="col-md-12">
												<select class="form-control select2" data-toggle="select2" name="level" id="level">
													<option value="beginner"><?php echo get_phrase('beginner'); ?></option>
													<option value="advanced"><?php echo get_phrase('advanced'); ?></option>
													<option value="intermediate"><?php echo get_phrase('intermediate'); ?></option>
												</select>
											</div>
										</div>
										<div class="form-group row mb-1">
											<label class="col-md-12 col-form-label" for="language_made_in"><?php echo get_phrase('course_language'); ?></label>
											<div class="col-md-12">
												<select class="form-control select2" data-toggle="select2" name="language_made_in" id="language_made_in">
													<?php foreach ($languages as $language) : ?>
														<option value="<?php echo $language; ?>"><?php echo ucfirst($language); ?></option>
													<?php endforeach; ?>
												</select>
											</div>
										</div>
									</div>

									<div>
										<div class="col pr-0 pl-0 mt-3 mb-2">
											<div class="card widget-flat bg-purple text-white mb-1">
												<div class="card-body">
													<div class="row justify-content-center">
														<div class="col-xl-12">
															<div class="form-group row mb-1">
																<div class="col-md-12">
																	<div class="custom-control custom-checkbox">
																		<input type="checkbox" class="custom-control-input" name="is_free_course" id="is_free_course" value="1" onclick="togglePriceFields(this.id)">
																		<label class="custom-control-label" for="is_free_course">Click the box if this is a free course<br><small>(A free course increases your followers and reviews)</small></label>
																	</div>
																</div>
															</div>

															<div class="paid-course-stuffs">
																<div class="form-group row mb-1">
																	<label class="col-md-12 col-form-label" for="price"><?php echo get_phrase('course_price') . ' (' . currency_code_and_symbol() . ')'; ?></label>
																	<div class="col-md-12">
																		<input type="number" class="form-control" id="price" name="price" placeholder="<?php echo get_phrase('enter_course_course_price'); ?>" min="0">
																	</div>
																</div>

																<div class="form-group row mb-1">
																	<div class="col-md-12">
																		<div class="custom-control custom-checkbox">
																			<input type="checkbox" class="custom-control-input" name="discount_flag" id="discount_flag" value="1">
																			<label class="custom-control-label" for="discount_flag">Click the box if this course has a discount</label>
																		</div>
																	</div>
																</div>

																<div class="form-group row mb-1">
																	<label class="col-md-12 col-form-label" for="discounted_price"><?php echo get_phrase('enter_selling_price_after_discount') . ' (' . currency_code_and_symbol() . ')'; ?></label>
																	<div class="col-md-12">
																		<input type="number" class="form-control" name="discounted_price" id="discounted_price" onkeyup="calculateDiscountPercentage(this.value)" min="0">

																		<small class="text-light"><?php echo get_phrase('this_course_has'); ?></small> <span id="discounted_percentage" class="badge badge-danger" style="font-size: 12px;">0%</span><small class="text-light"> <?php echo get_phrase('discount'); ?></small>
																	</div>
																</div>
																<div class="form-group row mb-0 text-center">
																	<div class="col-md-12">
																		<span class="badge badge-secondary">Please Note: Your revenue share would be: <?php echo get_settings('instructor_revenue'); ?>%</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>

									</div>

									<div>
										<div class="col pr-0 pl-0 mt-3">
											<div class="card widget-flat bg-info text-white mb-1">
												<div class="card-body pt-2 pb-2">
													<div class="row justify-content-center">
														<div class="col-xl-12">
															<div class="form-group row mb-1">
																<label class="col-md-12 col-form-label text-center pt-0 mb-1" for="course_overview_provider">
																	<h5 class="my-0"><?php echo get_phrase('course_preview_provider'); ?></h5>
																	<small>This preview is viewable by all before course is purchased</small>
																</label>
																<!-- #DK [start] -->
																<div class="col-md-12">
																	<select class="form-control select2" data-toggle="select2" name="course_overview_provider" id="course_overview_provider" onchange="c_check_video_provider(this.value)">
																		<option value="youtube"><?php echo get_phrase('youtube'); ?></option>
																		<option value="vimeo"><?php echo get_phrase('vimeo'); ?></option>
																		<option value="html5"><?php echo get_phrase('your_hosted_video_url'); ?></option>
																		<option value="upload"><?php echo get_phrase('upload_video'); ?></option>
																		<option value="vimeo_upload"><?php echo get_phrase('vimeo_upload_video'); ?></option>
																	</select>
																</div>
																<!-- #DK [end] -->
															</div>
															<div id="others" class="form-group row mb-1">
																<label class="col-md-12 col-form-label" for="course_overview_url"><?php echo get_phrase('course_preview_url'); ?></label>
																<div class="col-md-12">
																	<input type="text" class="form-control" name="course_overview_url" id="course_overview_url" placeholder="E.g: https://www.[youtube][Vimeo].com/watch?v=YOUTUBE-VIDEO-CODE">
																</div>
															</div>
															<!-- #DK [START] -->
															<div class="" id="upload" style="display: none;">
																<div class="form-group">
																	<label> <?php echo get_phrase('upload_video_file'); ?></label>
																	<div class="input-group">
																		<div class="custom-file">
																			<input type="file" class="custom-file-input" id="upload_video" name="upload_video" onchange="changeTitleOfImageUploader(this)">
																			<label class="custom-file-label" for="upload_video"><?php echo get_phrase('upload_video_file_here'); ?></label>
																		</div>
																	</div>
																	<div id="dk_file_upload_content" class="blueimp">
																		<input type="hidden" name="uploaded_file_name" class="w-100" id="uploaded_file_name">
																		<div class="row">
																			<div class="col-sm-12 mb-1">
																				<div id="process_msg" class="mt-1"></div> <!-- process msg display here -->
																				<div id="progress_bar" class="progress" style="display: none;">
																					<div class="progress-bar progress-bar-animated bg-warning" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
																				</div>
																			</div>
																		</div>
																		<!-- The file list will be shown here -->
																		<div id="video_preview"></div>
																	</div>
																</div>
															</div>
															<!-- vimeo upload -->
															<div class="" id="c_vimeo_upload" style="display: none;">
																<div class="form-group">
																	<label> <?php echo get_phrase('upload_video_file'); ?></label>
																	<div class="input-group">
																		<div class="custom-file">
																			<input type="file" class="custom-file-input" id="c_vimeo_upload_video" name="vimeo_upload_video" onchange="changeTitleOfImageUploader(this)">
																			<label class="custom-file-label" for="c_vimeo_upload_video"><?php echo get_phrase('upload_video_file_here_for_vimeo'); ?></label>
																		</div>
																	</div>
																	<div id="dk_vimeo_file_upload_content" class="blueimp">
																		<input type="hidden" name="vimeo_uploaded_file_name" class="w-100" id="c_vimeo_uploaded_file_name">
																		<div class="row">
																			<div class="col-sm-12 mb-1">
																				<div id="c_vimeo_process_msg" class="mt-1"></div> <!-- process msg display here -->
																			</div>
																		</div>
																		<!-- The file list will be shown here -->
																		<div id="c_vimeo_video_preview"></div>
																	</div>
																</div>
															</div>
															<!-- #DK [END] -->
															<div class="form-group row mb-1">
																<div class="col-md-12">
																	<small>Please ensure to upload a powerfull & enticing promo video of approx 2 minutes, which gives a good preview about your course and why someone should buy your course.</small>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>

										<!-- this portion will be generated theme wise from the theme-config.json file Starts-->
										<?php include 'course_media_add.php'; ?>
										<!-- this portion will be generated theme wise from the theme-config.json file Ends-->

									</div> <!-- end MediaID -->
								</div>

							</div>
							<div class="row">

								<div class="col-12">
									<div class="text-center">
										<h2 class="mt-0"><i class="mdi mdi-check-decagram mdi-48px"></i></h2>
										<div class="mb-3 mt-3">
											<button id="submit_course_btn"type="button" class="btn btn-primary btn-block text-center" onclick="checkRequiredFields()"><i class="mdi mdi-content-save mr-2"></i><?php echo get_phrase('submit'); ?></button>
										</div>
									</div>
								</div>

							</div> <!-- end FinishID -->


						</form>
					</div>
				</div><!-- end row-->
			</div> <!-- end card-body-->
		</div> <!-- end card-->
	</div>
</div>


<script type="text/javascript">
	$(document).ready(function() {
		initSummerNote(['#description']);
	});
</script>

<script type="text/javascript">
	var blank_outcome = jQuery('#blank_outcome_field').html();
	var blank_requirement = jQuery('#blank_requirement_field').html();
	jQuery(document).ready(function() {
		jQuery('#blank_outcome_field').hide();
		jQuery('#blank_requirement_field').hide();
	});

	function appendOutcome() {
		jQuery('#outcomes_area').append(blank_outcome);
	}

	function removeOutcome(outcomeElem) {
		jQuery(outcomeElem).parent().parent().remove();
	}

	function appendRequirement() {
		jQuery('#requirement_area').append(blank_requirement);
	}

	function removeRequirement(requirementElem) {
		jQuery(requirementElem).parent().parent().remove();
	}

	function priceChecked(elem) {
		if (jQuery('#discountCheckbox').is(':checked')) {

			jQuery('#discountCheckbox').prop("checked", false);
		} else {

			jQuery('#discountCheckbox').prop("checked", true);
		}
	}

	function topCourseChecked(elem) {
		if (jQuery('#isTopCourseCheckbox').is(':checked')) {

			jQuery('#isTopCourseCheckbox').prop("checked", false);
		} else {

			jQuery('#isTopCourseCheckbox').prop("checked", true);
		}
	}

	function isFreeCourseChecked(elem) {

		if (jQuery('#' + elem.id).is(':checked')) {
			$('#price').prop('required', false);
		} else {
			$('#price').prop('required', true);
		}
	}

	function calculateDiscountPercentage(discounted_price) {
		if (discounted_price > 0) {
			var actualPrice = jQuery('#price').val();
			if (actualPrice > 0) {
				var reducedPrice = actualPrice - discounted_price;
				var discountedPercentage = (reducedPrice / actualPrice) * 100;
				if (discountedPercentage > 0) {
					jQuery('#discounted_percentage').text(discountedPercentage.toFixed(2) + '%');

				} else {
					jQuery('#discounted_percentage').text('<?php echo '0%'; ?>');
				}
			}
		}
	}
</script>
<!-- #DK [start] -->
<script src="<?php echo site_url('assets/global/blueimp/jquery.ui.widget.js'); ?>"></script>
<script src="<?php echo site_url('assets/global/blueimp/jquery.fileupload.js'); ?>"></script>

<script>
	function c_check_video_provider(provider) {
		$('#submit_course_btn').prop('disabled', false);
		if (provider === 'upload') {
			$('#upload').show();
			$('#others,#c_vimeo_upload').hide();
			$('#submit_course_btn').prop('disabled', true);
		} else if (provider === 'vimeo_upload') {
			$('#c_vimeo_upload').show();
			$('#others,#upload').hide();
			$('#submit_course_btn').prop('disabled', true);
		} else {
			$('#others').show();
			$('#upload,#c_vimeo_upload').hide();
		}
	}
	$('#process_msg').html('');
	$('#upload_video').fileupload({
		url: '<?php echo base_url('user/course_video_upload'); ?>',
		type: 'POST',
		autoUpload: false,
		replaceFileInput: false,
		acceptFileTypes: '/(\.|\/)(mp4)$/i',
		// This function is called when a file is added to the queue
		add: function(e, data) {
			var fileType = data.files[0].name.split('.').pop(),
				allowdtypes = 'mp4';
			if (allowdtypes.indexOf(fileType) < 0) {
				$('#process_msg').html('<strong class="text-danger">Invalid file type. We Only Accept mp4 Video format.</strong>');
				return false;
			}
			$('#upload_video').prop('disabled', true);
			//This area will contain file list and progress information.
			// var tpl = $('<div class="row working" id="v_name">' + '<div class="col-sm-4"><div id="upload_actions"><button type="button" class="btn btn-primary upload">Upload</button> <button type="button" class="btn btn-primary cancel">cancel</button></div></div></div>');
			var tpl = $('<div class="working"><div id="v_name">' + '</div></div>');
			// Append the videp name and size and preview
			tpl.find('#v_name').append('<div class="row">' + '<div class="col-sm-12"><video id="preview" width="270" src="' + URL.createObjectURL(data.files[0]) + '" controls/></div>' + '<div class="col-sm-12"><label> File Size: <strong class="d-block">' + formatFileSize(data.files[0].size) + '</strong></label></div>' + '<div class="col-sm-12"><div id="upload_actions"><button type="button" class="btn btn-primary upload">Upload</button> <button type="button" class="btn btn-primary cancel">Cancel</button></div></div>' + '</div>');
			// Add the HTML to the UL element
			data.context = tpl.appendTo('div#video_preview');
			//cancel file
			tpl.find('button.cancel').click(function() {
				tpl.fadeOut(function() {
					$('#progress_bar').hide();
					$('#process_msg').html('');
					$('#upload_video').prop('disabled', false);
					tpl.remove();
				});
			});
			//upload file
			tpl.find('button.upload').click(function() {
				var jqXHR = data.submit();
			});
		},
		progress: function(e, data) {
			// $('#process_msg').html('<strong class="text-warning">Uploading...</strong>');
			$('#process_msg').html('');
			$('#progress_bar').show();
			$('#upload_actions').remove();
			// Calculate the completion percentage of the upload
			var progress = parseInt(data.loaded / data.total * 100, 10);
			$('#progress_bar > div').text(progress + '%').animate({
				width: progress + '%'
			}, 50);
			// Update the hidden input field and trigger a change
			// so that the jQuery knob plugin knows to update the dial
			data.context.find('input').val(progress).change();
			if (progress == 100) {
				data.context.removeClass('working');
			}
		},
		done: function(e, data) {
			if (data.textStatus == 'success') {
				$('#process_msg').html('<strong class="text-warning">File Uploaded Successfully.</strong>');
				var obj = JSON.parse(data.result);
				$('#uploaded_file_name').val(obj.files.file_name);
				$('#course_overview_url').val(obj.files.file_name);
				$('#progress_bar').hide();
				$('#submit_course_btn').prop('disabled', false);
			} else {
				$('#process_msg').html('<strong class="text-warning">Try again something went wrong</strong>');
				$('#upload_video').prop('disabled', true);
			}
		}
	});

	// <!------------------ vimeo upload script -----------------!>
	$('#c_vimeo_process_msg').html('');
	$('#c_vimeo_upload_video').fileupload({
		url: '<?php echo base_url('user/vimeo_video_upload'); ?>',
		type: 'POST',
		autoUpload: false,
		replaceFileInput: false,
		acceptFileTypes: /(\.|\/)(mp4)$/i,
		// This function is called when a file is added to the queue
		add: function(e, data) {
			var fileType = data.files[0].name.split('.').pop(),
				allowdtypes = 'mp4';
			if (allowdtypes.indexOf(fileType) < 0) {
				$('#c_vimeo_process_msg').html('<strong class="text-danger">Invalid file type. Only Accept mp4 Video format.</strong>');
				return false;
			}
			$('#c_vimeo_upload_video').prop('disabled', true);
			//This area will contain file list and progress information.
			var tpl = $('<div class="working"><div id="c_vimeo_name">' + '</div></div>');
			// Append the videp name and size and preview
			tpl.find('#c_vimeo_name').append('<div class="row">' + '<div class="col-sm-12"><video id="vimeo_preview" width="270" src="' + URL.createObjectURL(data.files[0]) + '" controls/></div>' + '<div class="col-sm-12"><label> File Size: <strong class="d-block">' + formatFileSize(data.files[0].size) + '</strong></label></div>' + '<div class="col-sm-12"><div id="c_vimeo_upload_actions"><button type="button" class="btn btn-primary upload">Upload</button> <button type="button" class="btn btn-primary cancel">Cancel</button></div></div>' + '</div>');
			// Add the HTML to the UL element
			data.context = tpl.appendTo('div#c_vimeo_video_preview');
			//cancel file
			tpl.find('button.cancel').click(function() {
				tpl.fadeOut(function() {
					$('#c_vimeo_process_msg').html('');
					$('#c_vimeo_upload_video').prop('disabled', false);
					tpl.remove();
				});
			});
			//upload file
			tpl.find('button.upload').click(function() {
				var jqXHR = data.submit();
			});
		},
		progress: function(e, data) {
			$('#c_vimeo_process_msg').html('<strong class="text-warning"><i class="mdi mdi-spin mdi-loading"></i> Uploading under process...</strong>');
			$('#c_vimeo_upload_actions').remove();
		},
		done: function(e, data) {
			if (data.textStatus == 'success') {
				$('#c_vimeo_process_msg').html('<strong class="text-success">File Uploaded Successfully.</strong>');
				var obj = JSON.parse(data.result);
				$('#c_vimeo_uploaded_file_name').val(obj.response);
				$('#course_overview_url').val(obj.response);
				$('#submit_course_btn').prop('disabled', false);
			} else {
				$('#c_vimeo_process_msg').html('<strong class="text-warning">Try again something went wrong</strong>');
				$('#c_vimeo_upload_video').prop('disabled', true);
			}
		}
	});

	//Helper function for calculation of progress
	function formatFileSize(bytes) {
		if (typeof bytes !== 'number') {
			return '';
		}
		if (bytes >= 1000000000) {
			return (bytes / 1000000000).toFixed(2) + ' GB';
		}

		if (bytes >= 1000000) {
			return (bytes / 1000000).toFixed(2) + ' MB';
		}
		return (bytes / 1000).toFixed(2) + ' KB';
	}

	//next previous hide/show
	$(document).ready(function() {
		$('li.previous').hide();
		$('#basicwizard').bootstrapWizard({
			'onNext': function(tab, navigation, index) {
				$('li.next').hide();
				$('li.previous').show();
			},
			'onPrevious': function(tab, navigation, index) {
				$('li.next').show();
				$('li.previous').hide();
			}
		});
	});
</script>
<!-- #DK [end] -->
<style media="screen">
	body {
		overflow-x: hidden;
	}
</style>