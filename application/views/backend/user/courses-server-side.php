<div class="row">
	<div class="col-12">
		<ol class="breadcrumb yellow mt-2">
			<li class="breadcrumb-item"><a href="<?php echo site_url();?>"><i class="mdi mdi-home mr-1"></i></a></li>
			<li class="breadcrumb-item active text-purple"><i class="mdi mdi-view-dashboard mr-1"></i>Dashboard/<?php echo get_phrase('course_list'); ?></li>
		</ol>
	</div>
</div>
<div class="row">
	<div class="col">
		<div class="card cta-box bg-dark text-white">
			<div class="card-body">
				<div class="widget-title">
					<?php echo get_phrase('active_courses'); ?>
				</div>
				<div class="media align-items-center">
					
					<div class="media-body">
						<i class="mdi mdi-eye bottom45icon"></i>
					</div>
					<span class="mr-3" style="font-size: 3rem;margin-top: -10px;">
						<?php
							$active_courses = $this->crud_model->get_status_wise_courses_for_instructor('active');
							echo $active_courses->num_rows();
						 ?>
					</span>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col">
		<div class="card cta-box bg-dark text-white">
			<div class="card-body">
				<div class="widget-title">
					<?php echo get_phrase('pending_approval'); ?>
				</div>
				<div class="media align-items-center">
					
					<div class="media-body">
						<i class="mdi mdi-eye-off bottom45icon"></i>
					</div>
					<span class="mr-3" style="font-size: 3rem;margin-top: -10px;">
						<?php
							$pending_courses = $this->crud_model->get_status_wise_courses_for_instructor('pending');
							echo $pending_courses->num_rows();
						?>
					</span>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col">
		<div class="card cta-box bg-dark text-white">
			<div class="card-body">
				<div class="widget-title">
					<?php echo get_phrase('draft_courses'); ?>
				</div>
				<div class="media align-items-center">
					
					<div class="media-body">
						<i class="mdi mdi-book bottom45icon"></i>
					</div>
					<span class="mr-3" style="font-size: 3rem;margin-top: -10px;">
						<?php
							$draft_courses = $this->crud_model->get_status_wise_courses_for_instructor('draft');
							echo $draft_courses->num_rows();
						?>
					</span>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col">
		<div class="card cta-box bg-dark text-white">
			<div class="card-body">
				<div class="widget-title">
					<?php echo get_phrase('free_courses'); ?>
				</div>
				<div class="media align-items-center">
					
					<div class="media-body">
						<i class="mdi mdi-tag bottom45icon"></i>
					</div>
					<span class="mr-3" style="font-size: 3rem;margin-top: -10px;">
						<?php echo $this->crud_model->get_free_and_paid_courses('free', $this->session->userdata('user_id'))->num_rows(); ?>
					</span>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col">
		<div class="card cta-box bg-dark text-white">
			<div class="card-body">
				<div class="widget-title">
					<?php echo get_phrase('paid_courses'); ?>
				</div>
				<div class="media align-items-center">
					
					<div class="media-body">
						<i class="mdi mdi-shopping bottom45icon"></i>
					</div>
					<span class="mr-3" style="font-size: 3rem;margin-top: -10px;">
						<?php echo $this->crud_model->get_free_and_paid_courses('paid', $this->session->userdata('user_id'))->num_rows(); ?>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <div class="col-xl-12">
        <div class="card">
			<div class="card-header purple">
			
				<div class="row">
					<div class="col-6 col-xl-8">
						<h4 class="header-title mt-1"><i class="mdi mdi-library-books mr-1"></i><?php echo get_phrase('course_list'); ?></h4>                         
					</div>
					<div class="col-6 col-xl-4">
						<div class="text-lg-right">
							
							<a href="<?php echo site_url('user/course_form/add_course'); ?>" class="btn btn-xs bg-yellow mb-0 mt-1"><i class="mdi mdi-plus"></i><?php echo get_phrase('add_a_new_course'); ?></a>
						</div>
					</div><!-- end col-->
				</div>
			</div>
            <div class="card-body">
                
                <form class="row justify-content-center" action="<?php echo site_url('user/courses'); ?>" method="get">
                    <!-- Course Categories -->
                    <div class="col-xl-3">
                        <div class="form-group">
                            <label for="category_id"><?php echo get_phrase('categories'); ?></label>
                            <select class="form-control select2" data-toggle="select2" name="category_id" id="category_id">
                                <option value="<?php echo 'all'; ?>" <?php if($selected_category_id == 'all') echo 'selected'; ?>><?php echo get_phrase('all'); ?></option>
                                <?php foreach ($categories->result_array() as $category): ?>
                                    <optgroup label="<?php echo $category['name']; ?>">
                                        <?php $sub_categories = $this->crud_model->get_sub_categories($category['id']);
                                        foreach ($sub_categories as $sub_category): ?>
                                        <option value="<?php echo $sub_category['id']; ?>" <?php if($selected_category_id == $sub_category['id']) echo 'selected'; ?>><?php echo $sub_category['name']; ?></option>
                                    <?php endforeach; ?>
                                </optgroup>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <!-- Course Status -->
                <div class="col-xl-3">
                    <div class="form-group">
                        <label for="status"><?php echo get_phrase('status'); ?></label>
                        <select class="form-control select2" data-toggle="select2" name="status" id = 'status'>
                            <option value="all" <?php if($selected_status == 'all') echo 'selected'; ?>><?php echo get_phrase('all'); ?></option>
                            <option value="active" <?php if($selected_status == 'active') echo 'selected'; ?>><?php echo get_phrase('active'); ?></option>
                            <option value="pending" <?php if($selected_status == 'pending') echo 'selected'; ?>><?php echo get_phrase('pending'); ?></option>
                        </select>
                    </div>
                </div>

                <!-- Course Price -->
                <div class="col-xl-3">
                    <div class="form-group">
                        <label for="price"><?php echo get_phrase('price'); ?></label>
                        <select class="form-control select2" data-toggle="select2" name="price" id = 'price'>
                            <option value="all"  <?php if($selected_price == 'all' ) echo 'selected'; ?>><?php echo get_phrase('all'); ?></option>
                            <option value="free" <?php if($selected_price == 'free') echo 'selected'; ?>><?php echo get_phrase('free'); ?></option>
                            <option value="paid" <?php if($selected_price == 'paid') echo 'selected'; ?>><?php echo get_phrase('paid'); ?></option>
                        </select>
                    </div>
                </div>

                <div class="col-xl-3">
                    <label for=".." class="text-white"><?php echo get_phrase('..'); ?></label>
                    <button type="submit" class="btn btn-primary btn-block" name="button"><i class="fas fa-filter mr-2"></i><?php echo get_phrase('filter'); ?></button>
                </div>
            </form>

            <div class="table-responsive-sm mt-4">
                <?php if (count($courses) > 0): ?>
                    <table id="course-datatable-server-side" class="table table-striped dt-responsive nowrap" width="100%" data-page-length='10'>
                        <thead>
                            <tr>
                                <th><?php echo get_phrase('title'); ?></th>
                                <th><?php echo get_phrase('author'); ?></th>
                                <th><?php echo get_phrase('price'); ?></th>
                                <th><?php echo get_phrase('subscribers'); ?></th>
                                <th><?php echo get_phrase('status'); ?></th>
                                <th><?php echo get_phrase('edit'); ?></th>
                                <th><?php echo get_phrase('publish_/_draft'); ?></th>
                                <th><?php echo get_phrase('view'); ?></th>
                            </tr>
                        </thead>
                    </table>
                <?php endif; ?>
                <?php if (count($courses) == 0): ?>
                    <div class="img-fluid w-100 text-center">
                      <img style="opacity: 1; width: 100px;" src="<?php echo base_url('assets/backend/images/file-search.svg'); ?>"><br>
                      <?php echo get_phrase('no_data_found'); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
</div>
