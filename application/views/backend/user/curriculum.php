<div class="row justify-content-center">
    <div class="col-xl-4 mb-4 text-center">
        <a href="javascript::void(0)" class="btn btn-success btn-block ml-1" onclick="showAjaxModal('<?php echo site_url('modal/popup/section_add/'.$course_id); ?>', '<?php echo get_phrase('add_new_topic'); ?>')"><i class="mdi mdi-playlist-plus mr-1"></i> <?php echo get_phrase('add_topic'); ?></a>
		
        <a href="javascript::void(0)" class="btn btn-info btn-block ml-1" onclick="showLargeModal('<?php echo site_url('modal/popup/lesson_add/'.$course_id); ?>', '<?php echo get_phrase('add_new_chapter'); ?>')"><i class="mdi mdi-folder-plus-outline"></i> <?php echo get_phrase('add_chapter'); ?></a>
		<?php /*
        <a href="javascript::void(0)" class="btn btn-outline-primary btn-rounded btn-sm ml-1" onclick="showAjaxModal('<?php echo site_url('modal/popup/quiz_add/'.$course_id); ?>', '<?php echo get_phrase('add_new_quiz'); ?>')"><i class="mdi mdi-plus"></i> <?php echo get_phrase('add_quiz'); ?></a>
		*/ ?>
        <a href="javascript::void(0)" class="btn btn-dark btn-block ml-1" onclick="showLargeModal('<?php echo site_url('modal/popup/sort_section/'.$course_id); ?>', '<?php echo get_phrase('sort_topics'); ?>')"><i class="mdi mdi-sort-variant"></i> <?php echo get_phrase('sort_topics'); ?></a>
    </div>

    <div class="col-xl-8">
        <div class="row">
            <?php
            $lesson_counter = 0;
            $quiz_counter   = 0;
            $sections = $this->crud_model->get_section('course', $course_id)->result_array();
            foreach ($sections as $key => $section):?>
            <div class="col-xl-12">
                <div class="card bg-light  bg-yellow" id = "section-<?php echo $section['id']; ?>">
                    <div class="card-body">
                        <h5 class="card-title" class="mb-3" style="min-height: 35px;"><span class="font-weight-light"><?php echo get_phrase('topic').' '.++$key; ?></span>: <?php echo $section['title']; ?>
                            <div class="row justify-content-center alignToTitle float-right" id = "widgets-of-section-<?php echo $section['id']; ?>">
                                <button type="button" class="btn btn-primary btn-xs ml-1" name="button" onclick="showLargeModal('<?php echo site_url('modal/popup/sort_lesson/'.$section['id']); ?>', '<?php echo get_phrase('sort_chapters'); ?>')" >
									<i class="mdi mdi-sort-variant mr-1"></i> <?php echo get_phrase('sort_chapter'); ?>
								</button>
                                <button type="button" class="btn btn-info btn-xs ml-1 text-white" name="button" onclick="showAjaxModal('<?php echo site_url('modal/popup/section_edit/'.$section['id'].'/'.$course_id); ?>', '<?php echo get_phrase('update_topic'); ?>')" >
									<i class="mdi mdi-pencil-outline mr-1"></i><?php echo get_phrase('edit_topic'); ?>
								</button>
                                <button type="button" class="btn btn-danger btn-xs ml-1" name="button" onclick="confirm_modal('<?php echo site_url('user/sections/'.$course_id.'/delete'.'/'.$section['id']); ?>');">
									<i class="mdi mdi-delete mr-1"></i><?php echo get_phrase('delete_topic'); ?>
								</button>
                            </div>
                        </h5>
                        <div class="clearfix"></div>
                        <?php
                        $lessons = $this->crud_model->get_lessons('section', $section['id'])->result_array();
                        foreach ($lessons as $index => $lesson):?>
						<div class="col-md-12">
							<div class="chart-widget-list text-white bg-purple p-2 mb-2" id = "<?php echo 'lesson-'.$lesson['id']; ?>">
								<p>
									<span class="font-weight-light">
										<?php
                                            if ($lesson['lesson_type'] == 'quiz') {
                                                $quiz_counter++; // Keeps track of number of quiz
                                                $lesson_type = $lesson['lesson_type'];
                                            }else {
                                                $lesson_counter++; // Keeps track of number of lesson
                                                if ($lesson['attachment_type'] == 'txt' || $lesson['attachment_type'] == 'pdf' || $lesson['attachment_type'] == 'doc' || $lesson['attachment_type'] == 'img') {
                                                    $lesson_type = $lesson['attachment_type'];
                                                }else {
                                                    $lesson_type = 'video';
                                                }
                                            }
                                        ?>
                                        <img class="mr-1" src="<?php echo base_url('assets/backend/lesson_icon/'.$lesson_type.'.png'); ?>" alt="" height = "18">
                                            <?php echo $lesson['lesson_type'] == 'quiz' ? get_phrase('quiz').' '.$quiz_counter : get_phrase('chapter').' '.$lesson_counter; ?>
                                    </span>:<span> <?php echo $lesson['title']; ?></span>
									
									<span class="float-right" id = "widgets-of-lesson-<?php echo $lesson['id']; ?>">
                                        <?php if ($lesson['lesson_type'] == 'quiz'): ?>
                                            <a href="javascript::" onclick="showLargeModal('<?php echo site_url('modal/popup/quiz_questions/'.$lesson['id']); ?>', '<?php echo get_phrase('manage_quiz_questions'); ?>')">
												<i class="mdi mdi-comment-question-outline"></i>
											</a>
                                            <a href="javascript::" onclick="showAjaxModal('<?php echo site_url('modal/popup/quiz_edit/'.$lesson['id'].'/'.$course_id); ?>', '<?php echo get_phrase('update_quiz_information'); ?>')">
												<i class="mdi mdi-pencil-outline"></i>
											</a>
                                        <?php else: ?>
                                            <a class="btn btn-icon btn-xs btn-info text-white" href="javascript::" onclick="showLargeModal('<?php echo site_url('modal/popup/lesson_edit/'.$lesson['id'].'/'.$course_id); ?>', '<?php echo get_phrase('update_chapter'); ?>')">
												<i class="mdi mdi-pencil-outline"></i>
											</a>
                                        <?php endif; ?>
											<a class="btn btn-icon btn-xs btn-danger text-white" href="javascript::" onclick="confirm_modal('<?php echo site_url('user/lessons/'.$course_id.'/delete'.'/'.$lesson['id']); ?>');">
												<i class="mdi mdi-delete"></i>
											</a>
                                    </span>
								</p>
							</div>
						</div>
                        
                    <?php endforeach; ?>
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div>
    <?php endforeach; ?>
</div>
</div>
</div>
