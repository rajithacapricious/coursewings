<!-- Topbar Start -->
<div class="navbar-custom topnav-navbar topnav-navbar-dark">
    <div class="container-fluid">

        <!-- LOGO -->
        <a href="<?php echo site_url($this->session->userdata('role')); ?>" class="topnav-logo" style = "min-width: unset;">
            <span class="topnav-logo-lg">
                <img src="<?php echo base_url('uploads/system/logo-light.png');?>" alt="logo" height="40">
            </span>
            <span class="topnav-logo-sm">
                <img src="<?php echo base_url('uploads/system/logo-light-sm.png');?>" alt="logo" height="40">
            </span>
        </a>

        <ul class="list-unstyled topbar-right-menu float-right mb-0 dropdown notification-list">
					
            <li class="">
                <a class="nav-link nav-user mr-0">
                <span class="account-user-avatar">
                    <img src="<?php echo $this->user_model->get_user_image_url($this->session->userdata('user_id')); ?>" alt="user-image" class="rounded-circle">
                </span>
                <span  style="color: #fff;">
                    <?php
                    $logged_in_user_details = $this->user_model->get_all_user($this->session->userdata('user_id'))->row_array();;
                    ?>
                    <span class="account-user-name"><?php echo $logged_in_user_details['first_name'].' '.$logged_in_user_details['last_name'];?></span>
                    <span class="account-position"><?php echo strtolower($this->session->userdata('role')) == 'user' ? get_phrase('instructor') : get_phrase('admin'); ?></span>
					
				</a>
					
                </span>
				
				</a>
				
			</li>
			
			<?php if (strtolower($this->session->userdata('role')) == 'admin'): ?>
			<li style="margin-top: 12px;margin-right: 10px;">
				<a href="#" class="nav-link dropdown-toggle arrow-none mr-0 text-warning" data-toggle="dropdown" id="topbar-userdrop"
                href="#" role="button" aria-haspopup="true" aria-expanded="false">
					<i class="mdi mdi-settings mdi-30px mr-1"></i>
				</a>
				<div class="dropdown-menu dropdown-menu-right dropdown-menu-animated topbar-dropdown-menu profile-dropdown"
				aria-labelledby="topbar-userdrop">
					<!-- item-->
					<?php /*
					<div class=" dropdown-header noti-title">
						<h6 class="text-overflow m-0"><?php echo get_phrase('welcome'); ?> !</h6>
					</div>

					<!-- Account -->
					<a href="<?php echo site_url(strtolower($this->session->userdata('role')).'/manage_profile'); ?>" class="dropdown-item notify-item">
						<i class="mdi mdi-account-circle mr-1"></i>
						<span><?php echo get_phrase('my_account'); ?></span>
					</a>
					*/  ?>
					
						<!-- settings-->
						<a href="<?php echo site_url('admin/system_settings'); ?>" class="dropdown-item notify-item">
							<i class="mdi mdi-settings mr-1"></i>
							<span><?php echo get_phrase('global_settings'); ?></span>
						</a>
						
						<!-- Frontend settings-->
						<a href="<?php echo site_url('admin/frontend_settings'); ?>" class="dropdown-item notify-item <?php if($page_name == 'frontend_settings') echo 'active'; ?>">
							<i class="mdi mdi-web mr-1"></i>
							<span><?php echo get_phrase('website_settings'); ?></span>
						</a>
						<!-- Payment settings-->
						<a href="<?php echo site_url('admin/payment_settings'); ?>" class="dropdown-item notify-item <?php if($page_name == 'payment_settings') echo 'active'; ?>">
							<i class="mdi mdi-cash-usd mr-1"></i>
							<span><?php echo get_phrase('payment_settings'); ?></span>
						</a>
						<!-- Instructor settings-->
						<a href="<?php echo site_url('admin/instructor_settings'); ?>" class="dropdown-item notify-item <?php if($page_name == 'instructor_settings') echo 'active'; ?>">
							<i class="mdi mdi-paper-cut-vertical mr-1"></i>
							<span><?php echo get_phrase('revenue_share'); ?></span>
						</a>
						<!-- Email SMTP settings-->
						<a href="<?php echo site_url('admin/smtp_settings'); ?>" class="dropdown-item notify-item <?php if($page_name == 'smtp_settings') echo 'active'; ?>">
							<i class="mdi mdi-email mr-1"></i>
							<span><?php echo get_phrase('smtp_settings'); ?></span>
						</a>

					

					 <?php /*
					<!-- Logout-->
					<a href="<?php echo site_url('login/logout'); ?>" class="dropdown-item notify-item">
						<i class="mdi mdi-logout mr-1"></i>
						<span><?php echo get_phrase('logout'); ?></span>
					</a>
					
					*/  ?>
				</div>
			</li>
			<?php endif; ?>
		
		
			<li style="padding-top: 8px;">
				<span class="logot2">
					<a href="<?php echo site_url('login/logout'); ?>" class="text-warning">
					<i class="mdi mdi-power mr-1"></i>
				</span>
			</li>

</ul>
<a class="button-menu-mobile disable-btn">
    <div class="lines">
        <span></span>
        <span></span>
        <span></span>
    </div>
</a>

<div class="app-search" style="text-align: center;color: #fff;font-size: 24px;text-transform: uppercase;font-weight:bold;">
    <?php echo $this->db->get_where('settings' , array('key'=>'system_name'))->row()->value; ?>
	<?php echo strtolower($this->session->userdata('role')) == 'user' ? get_phrase('instructor') : get_phrase('admin'); ?>&nbsp;&nbsp;Console
</div>
</div>
</div>
<!-- end Topbar -->
