<div class="row"
  style="margin-top: 30px;">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <div class="panel panel-default" data-collapsed="0"
          style="border-color: #dedede;">
          <!-- panel body -->
          <div class="panel-body" style="font-size: 14px;">
            <center>
              <i class="entypo-thumbs-up" style="font-size: 32px;"></i>
              <h3>Congratulations!! Course Wings Website is now Installed</h3>
            </center>
            <br>
            <center>
              <strong>
                Set up Administrator for coursewings.<br>Please Remember the administrator login credentials which you will need for signing into your account.
              </strong>
            </center>
            <br>
            <div class="row">
              <div class="col-md-12">
                <form class="form-horizontal form-groups" method="post"
                  action="<?php echo site_url('install/finalizing_setup/setup_admin');?>">
                  <div class="row">
                  <div class="form-group col-md-12" hidden>
						<label class="col-md-12 control-label">System Name</label>
						<div class="col-md-12">
							<input type="text" class="form-control" name="system_name" placeholder="" value="Course Wings" required autofocus>
						</div>
					</div>
                  <hr>
                  <div class="form-group col-md-6">
            				<label class="col-md-12 control-label">Admin First Name</label>
            				<div class="col-md-12">
            					<input type="text" class="form-control" name="first_name" placeholder="" required>
            				</div>
                    <div class="col-md-12" style="font-size: 12px;">
                      Full name of Administrator
                    </div>
            	</div>
                  <div class="form-group col-md-6">
            				<label class="col-md-12 control-label">Admin Last Name</label>
            				<div class="col-md-12">
            					<input type="text" class="form-control" name="last_name" placeholder="" required>
            				</div>
                    <div class="col-md-12" style="font-size: 12px;">
                      Full name of Administrator
                    </div>
            	</div>
                  <hr>
                  <div class="form-group col-md-6">
            				<label class="col-md-12 control-label">Admin Email</label>
            				<div class="col-md-12">
            					<input type="email" class="form-control" name="email" placeholder="" required>
            				</div>
                    <div class="col-md-12" style="font-size: 12px;">
                      Email address for administrator login
                    </div>
            	</div>
                  <hr>
                  <div class="form-group col-md-6">
            				<label class="col-md-12 control-label">Password</label>
            				<div class="col-md-12">
            					<input type="password" class="form-control" name="password" placeholder=""
                        required>
            				</div>
                    <div class="col-md-12" style="font-size: 12px;">
                      Admin login password
                    </div>
            	</div>
                  <hr>
                  <div class="form-group col-md-12">
            				<label class="col-md-12 control-label"></label>
            				<div class="col-md-12">
            					<button type="submit" class="btn btn-block btn-primary">Finish Installation</button>
            				</div>
            			</div>
				</div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
