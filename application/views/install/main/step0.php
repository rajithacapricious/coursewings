<div class="row" style="margin-top: 30px;">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <div class="panel panel-default" data-collapsed="0"
          style="border-color: #dedede;">
    			<!-- panel body -->
    			<div class="panel-body" style="font-size: 14px;">
            <p style="font-size: 14px;">
              Welcome to Coursewings Installation. You will need to know the following items before
              proceeding.
            </p>
            <ol>
              <li>Database Name</li>
              <li>Database Username</li>
              <li>Database Password</li>
              <li>Database Hostname</li>
            </ol>
            
            <p style="font-size: 14px;">
              Collect and keep the above mentioned information ready before hitting the start installation button.
            </p>
            <br>
            <p>
              <a href="<?php echo site_url('install/step1');?>" class="btn btn-block btn-warning">
                Start Installation Process
              </a>
            </p>
    			</div>
    		</div>
      </div>
  </div>
</div>
</div>
