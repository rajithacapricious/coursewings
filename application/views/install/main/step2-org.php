<?php if(isset($error)) { ?>
  <div class="row"
    style="margin-top: 20px;">
    <div class="col-md-8 col-md-offset-2">
      <div class="alert alert-danger">
        <strong><?php echo $error; ?></strong>
      </div>
    </div>
  </div>
<?php } ?>
<div class="row"
  style="margin-top: 30px;">
  <div class="col-md-12">
    <div class="panel panel-default" data-collapsed="0"
      style="border-color: #dedede;">
      <!-- panel body -->
      <div class="panel-body" style="font-size: 14px;">
        <p style="font-size: 14px;">
          Continue with your <strong>Installation</strong>
        </p>
        <br>
        <div class="row">
          <div class="col-md-12">
            <form class="form-horizontal form-groups" method="post"
              action="<?php echo site_url('install/validate_install');?>">
              <div class="form-group">
        				<label class="control-label">Please keep your database information ready for next step.</label>
        				<div class="" >
        					<input type="text" class="form-control" name="purchase_code" placeholder="Continue"
                    required autofocus autocomplete="off" value="267af801-ddb2-4d89-9044-456679754aa2">
        				</div>
        			</div>
              <div class="form-group">
        				<label class="col-sm-3 control-label"></label>
        				<div class="col-sm-7">
        					<button type="submit" class="btn btn-block btn-primary">Continue</button>
        				</div>
        			</div>
            </form>
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
