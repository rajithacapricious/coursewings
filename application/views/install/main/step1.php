<?php
  $db_file_write_perm = is_writable('application/config/database.php');
  $routes_file_write_perm = is_writable('application/config/routes.php');
  $curl_enabled = function_exists('curl_version');
  if ($db_file_write_perm == false || $routes_file_write_perm == false || $curl_enabled == false) {
    $valid = false;
  } else {
    $valid = true;
  }
?>
<div class="row"
  style="margin-top: 30px;">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <div class="panel panel-default" data-collapsed="0"
          style="border-color: #dedede;">
    			<!-- panel body -->
    			<div class="panel-body" style="font-size: 14px;">
				<h3>Server Check!</h3>
            <p style="font-size: 14px;">
              Review and resolve the items that have a red mark on it.<br>If everything is green, Proceed to the next step.
            </p>
            <br>
			<ol>
              
            <li style="font-size: 14px;">
              <?php if ($db_file_write_perm == true) { ?>
                <i class="dripicons-checkmark" style="color: #5ac52d;"></i>
              <?php } else { ?>
                <i class="dripicons-cross" style="color: #f12828"></i>
              <?php } ?>
              <strong>application/config/database.php </strong>: file has write permission
            </li>
			
            <li style="font-size: 14px;">
              <?php if ($routes_file_write_perm == true) { ?>
                <i class="dripicons-checkmark" style="color: #5ac52d;"></i>
              <?php } else { ?>
                <i class="dripicons-cross" style="color: #f12828"></i>
              <?php } ?>
              <strong>application/config/routes.php </strong>: file has write permission
            </li>
            <li style="font-size: 14px;">
              <?php if ($curl_enabled == true) { ?>
                <i class="dripicons-checkmark" style="color: #5ac52d;"></i>
              <?php } else { ?>
                <i class="dripicons-cross" style="color: #f12828"></i>
              <?php } ?>
              <strong>Curl Enabled</strong>
            </li>
			</ol>
            <p style="font-size: 14px;">
              <strong>To continue the installation process, all the above points are needed to be checked and green</strong>
            </p>
            <br>
            <?php if ($valid == true) { ?>
              <p>
                <?php if ($_SERVER['SERVER_NAME'] == 'localhost' || $_SERVER['SERVER_NAME'] == '127.0.0.1') { ?>
                  <a href="<?php echo site_url('install/step3');?>" class="btn btn-block btn-primary">
                    Continue
                  </a>
                <?php } else { ?>
                  <a href="<?php echo site_url('install/step2');?>" class="btn btn-block btn-primary">
                    Continue
                  </a>
                <?php } ?>
              </p>
            <?php } ?>

            <?php if ($valid != true) { ?>
              <p>
                <?php if ($_SERVER['SERVER_NAME'] == 'localhost' || $_SERVER['SERVER_NAME'] == '127.0.0.1') { ?>
                  <a href="<?php echo site_url('install/step3');?>" class="btn btn-primary" disabled>
                    Continue
                  </a>
                <?php } else { ?>
                  <a href="<?php echo site_url('install/step2');?>" class="btn btn-primary" disabled>
                    Continue
                  </a>
                <?php } ?>
                <a href="<?php echo site_url('install/step1');?>" class="btn btn-info" >
                  <i class="mdi mdi-refresh"></i>Reload
                </a>
              </p>
            <?php } ?>
    			</div>
    		</div>
      </div>
    </div>
  </div>
</div>
