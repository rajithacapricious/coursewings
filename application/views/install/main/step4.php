<?php if(isset($error)) { ?>
  <div class="row"
    style="margin-top: 20px;">
    <div class="col-md-8 col-md-offset-2">
      <div class="alert alert-danger">
        <strong><?php echo $error; ?></strong>
      </div>
    </div>
  </div>
<?php } ?>
<div class="row"
  style="margin-top: 30px;">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <div class="panel panel-default" data-collapsed="0"
          style="border-color: #dedede;">
          <!-- panel body -->
          <div class="panel-body" style="font-size: 14px;">
            <p style="font-size: 14px;">
              <strong>We have connected to your server's database successfully</strong>. <br>To continue <strong>press the 'Install' button, Sit Back and enjoy your coffee till next step ;-)</strong>
            </p>
			<p style="font-size: 14px;">
				We'll populate datase with dummy data for Course wings. <br><strong>The data is Dummy!</strong> Please delete this data from Course Wings admin panel or with help of PhpMyAdmin.
				<br>
				
				<br>
			</p>
				<div class="card card-body bg-light">
				<strong>Steps for resurrecting Coursewings</strong>
				<ol>
					<li>Proceed with this installation.</li>
					<li>After Installation reupload your backed up "uploads" folder to the root of the site. (This folder will contain all videos/files/images etc..)</li>
					<li>After uploading "uploads" folder go to phpmyadmin and overwrite the database with your backup database.</li>
				</ol>
				</div>
			
            <br>
            <div class="row">
              <div class="col-md-12">
                <button type="button" id="install_button" class="btn btn-block btn-primary">
                    <i class="entypo-check"></i> &nbsp; Install
                </button>
                <div id="loader" style="margin-top: 20px;">
                  &nbsp; Importing database with dummy content....
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $('#loader').hide();
    $('#install_button').click(function() {
      $('#loader').fadeIn();
      setTimeout(
      function()
      {
        window.location.href = '<?php echo site_url('install/step4/confirm_install');?>';
      }, 5000);
    });
  });
</script>
