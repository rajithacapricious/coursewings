<div class="row"
  style="margin-top: 30px;">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <div class="panel panel-default" data-collapsed="0"
          style="border-color: #dedede;">
    			<!-- panel body -->
    			<div class="panel-body" style="font-size: 14px;">
            <h3>Course Wings website is now installed!!</h3>
            <br>
            <br>
            <table>
              <tbody>
                <tr>
                  <td style="padding: 12px;"><strong>Administrator Email :</strong></td>
                  <td style="padding: 12px;">Your chosen Email ID</td>
                </tr>
                <tr>
                  <td style="padding: 12px;"><strong>Password :</strong></td>
                  <td style="padding: 12px;">Your chosen Password</td>
                </tr>
              </tbody>
            </table>
            <br>
            <p>
              <a href="<?php echo site_url('install/success/login');?>" class="btn btn-block btn-primary">
                <i class="entypo-login"></i> &nbsp; Take me to login page
              </a>
            </p>
    			</div>
    		</div>
      </div>
    </div>
  </div>
</div>
