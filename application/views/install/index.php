<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Script Installation" />
	<meta name="author" content="ETC" />

	<title>Installation | Coursewings</title>
	<?php include 'styles.php'; ?>
</head>
<body class="page-body">

<div class="container horizontal-menu">


	<header class="navbar navbar-fixed-top" style="min-height: 80px; background:#6f2d87 !important;">
		<div class="navbar-inner">
			<!-- logo -->
			<div class="navbar-brand">
				<a href="#">
					<img src="<?php echo base_url('uploads/system/logo-light.png');?>"  style="max-height:40px;"/>
				</a>
			</div>
	    	<div class="navbar-brand pull-right" style="margin-top: 13px;color:#fff!important">
	        	Website Installation
	      	</div>
		</div>
	</header>
	<div class="main-content" style="border:2px solid #6f2d87;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
          			<?php include 'main/'.$page_name.'.php'; ?>
				</div>
			</div>

		</div>
	</div>

<?php include 'scripts.php'; ?>

</body>
</html>
