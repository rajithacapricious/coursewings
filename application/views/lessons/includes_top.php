<link rel="favicon" href="<?php echo base_url().'assets/frontend/default/img/icons/favicon.ico' ?>">
<link rel="apple-touch-icon" href="<?php echo base_url().'assets/frontend/default/img/icons/icon.png'; ?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/frontend/default/css/jquery.webui-popover.min.css'; ?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/frontend/default/css/select2.min.css'; ?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/frontend/default/css/slick.css'; ?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/frontend/default/css/slick-theme.css'; ?>">
<!-- font awesome 5 -->
<link rel="stylesheet" href="<?php echo base_url().'assets/frontend/default/css/fontawesome-all.min.css'; ?>">

<!--New Src-->
<script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/6c68ca46d8e5e6abb71f5c9ec/4b894cc492ee1c5078e2bdfcc.js");</script>

<link rel="stylesheet" href="<?php echo base_url().'assets/frontend/default/css/bootstrap.min.css'; ?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/frontend/default/css/bootstrap-tagsinput.css'; ?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/frontend/default/css/main.css'; ?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/frontend/default/css/customfront.css'; ?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/frontend/default/css/responsive.css'; ?>">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url().'assets/global/toastr/toastr.css' ?>">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/nestable2/1.6.0/jquery.nestable.min.css" />
<!-- <link rel="stylesheet" href="<?php /* echo base_url().'application/views/lessons/asset/lesson_player.css'; */ ?>" /> -->
<link rel="stylesheet" href="<?php echo base_url().'assets/lessons/css/custom.css' ?>">
<script src="<?php echo base_url('assets/backend/js/jquery-3.3.1.min.js'); ?>"></script>

<!-- Lesson page specific styles are here -->
<style type="text/css">
body {
    background-color: #fff !important;
}
.card {
    border-radius: 3px !important;
	background-color: #fff !important;
	border: 1px solid #ddd !important;
}
.course_card {
    padding: 0px;
    background-color: #F7F8FA;
}
.course_container {
    background-color: #fff !important;
}
.course_col {
    padding: 0px;
}
.course_header_col {
    background-color: #fff;
    color: #6f2d87;
    padding: 15px 10px 10px;
	box-shadow: 0 0 1px 1px rgba(20,23,28,.1), 0 3px 1px 0 rgba(20,23,28,.1);
}
.course_header_col2 {
    background-color: #6f2d87;
	color: #f7a823 !important;
    padding: 10px;
}
.course_header_col img {
    padding: 0px 0px;
	height: 50px !important;
	margin-left: 25px;
}
.course_btn {
    color: #95979a;
    border: 1px solid #95979a;
    padding: 7px 10px;
}
.course_btn:hover {
    color: #fff;
    border:1px solid #fff;
}
.lesson_duration{
    border-radius: 5px;
    padding-top: 8px;
    color: #5C5D61;
    font-size: 13px;
    font-weight: 100;
}
.quiz-card {
    border: 1px solid #dcdddf !important;
}
.bg-quiz-result-info {
    background-color: #007791 !important;
    padding: 13px !important;
}
</style>
