<!-- hit count -->
<?php $this->db->set('hits', 'hits+1', FALSE)->where('id', $result['id'])->update('blogs'); ?>
<link rel="stylesheet" href="<?php echo base_url('assets/frontend/default/rrssb/css/rrssb.css'); ?>" />
<section class="category-header-area">
    <div class="container-lg">
        <div class="row">
            <div class="col">
                <h1 class="category-name">
                    <i class="fas fa-pen-square mr-2"></i><?php echo $page_title; ?>
                </h1>
            </div>
        </div>
    </div>
</section>

<section class="category-course-list-area blogs">
	<div class="container-lg">
		<div class="row">
			<div class="col-md-9">
			<div class="blog_left_section">
                <div class="blog_section">
				<?php if (!empty($result)) : ?>
				<div class="cw_blog_image">
						<?php if ($result['image']): ?>
							<img src="<?php echo base_url('uploads/blog_images/'.$result['image']) ?>" alt="<?php echo $result['title']; ?>" class="img-fluid">
						<?php else: ?>
							<?php $blog_media_placeholders = themeConfiguration(get_frontend_settings('theme'), 'blog_media_placeholders');?>
							<img src="<?php echo base_url().$blog_media_placeholders['blog_image_placeholder']; ?>" alt="<?php echo $result['title']; ?>" class="img-fluid">
						<?php endif; ?>
						<?php if ($result['created_date']) : ?>
							<span class="cw_blog_date"><?php echo date('M d, Y', strtotime($result['created_date'])); ?></span>
							<span class="cw_blog_date_right"><i class="far fa-eye mr-2" aria-hidden="true"></i><?php echo $result['hits'] . ' Views' ?></span>
						<?php endif; ?>
				</div>
				<?php endif; ?>
				<div class="cw_blog_content">
					<?php if (!empty($result)) : ?>
					<h1><?php echo $result['title']; ?></h1>
					<?php endif ?>
					
					<?php /*
					<?php if ($result['short_description']) : ?>
						<h6><strong><?php echo $result['short_description']; ?></strong></h6>
					<?php endif ?>
					*/ ?>
					
					<div class="row">
						<div class="col-md-3">
							<div class="blog_user">
								<div class="user_name">
									<?php if ($result['created_by']) :
										$user_details = $this->db->select(array('first_name', 'last_name'))->where('id', $result['created_by'])->get('users')->row_array();
										?>
										<img class="blog-item-avtar img-fluid" src="<?php echo $this->user_model->get_user_image_url($result['created_by']); ?>" alt="blog Author">
											<a>By: <?php echo $user_details['first_name'] . ' ' . $user_details['last_name']; ?></a>
									<?php endif ?>
								</div>
							</div>
						</div>
				
						<div class="col-md-9 blog_user">
							<ul class="rrssb-buttons clearfix">
								<li class="rrssb-email">
									<a href="mailto:?Subject=<?php echo $result['title']; ?>&body=<?php echo current_url() ?>">
										<span class="rrssb-icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 28">
												<path d="M20.11 26.147c-2.335 1.05-4.36 1.4-7.124 1.4C6.524 27.548.84 22.916.84 15.284.84 7.343 6.602.45 15.4.45c6.854 0 11.8 4.7 11.8 11.252 0 5.684-3.193 9.265-7.398 9.3-1.83 0-3.153-.934-3.347-2.997h-.077c-1.208 1.986-2.96 2.997-5.023 2.997-2.532 0-4.36-1.868-4.36-5.062 0-4.75 3.503-9.07 9.11-9.07 1.713 0 3.7.4 4.6.972l-1.17 7.203c-.387 2.298-.115 3.3 1 3.4 1.674 0 3.774-2.102 3.774-6.58 0-5.06-3.27-8.994-9.304-8.994C9.05 2.87 3.83 7.545 3.83 14.97c0 6.5 4.2 10.2 10 10.202 1.987 0 4.09-.43 5.647-1.245l.634 2.22zM16.647 10.1c-.31-.078-.7-.155-1.207-.155-2.572 0-4.596 2.53-4.596 5.53 0 1.5.7 2.4 1.9 2.4 1.44 0 2.96-1.83 3.31-4.088l.592-3.72z" /></svg></span>
										<span class="rrssb-text">email</span>
									</a>
								</li>
								<li class="rrssb-facebook">
									<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo current_url() ?>" class="popup">
										<span class="rrssb-icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 29 29">
												<path d="M26.4 0H2.6C1.714 0 0 1.715 0 2.6v23.8c0 .884 1.715 2.6 2.6 2.6h12.393V17.988h-3.996v-3.98h3.997v-3.062c0-3.746 2.835-5.97 6.177-5.97 1.6 0 2.444.173 2.845.226v3.792H21.18c-1.817 0-2.156.9-2.156 2.168v2.847h5.045l-.66 3.978h-4.386V29H26.4c.884 0 2.6-1.716 2.6-2.6V2.6c0-.885-1.716-2.6-2.6-2.6z" /></svg></span>
										<span class="rrssb-text">facebook</span>
									</a>
								</li>
								<li class="rrssb-twitter">
									<a href="https://twitter.com/intent/tweet?text=<?php echo current_url() ?>" class="popup">
										<span class="rrssb-icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 28">
												<path d="M24.253 8.756C24.69 17.08 18.297 24.182 9.97 24.62a15.093 15.093 0 0 1-8.86-2.32c2.702.18 5.375-.648 7.507-2.32a5.417 5.417 0 0 1-4.49-3.64c.802.13 1.62.077 2.4-.154a5.416 5.416 0 0 1-4.412-5.11 5.43 5.43 0 0 0 2.168.387A5.416 5.416 0 0 1 2.89 4.498a15.09 15.09 0 0 0 10.913 5.573 5.185 5.185 0 0 1 3.434-6.48 5.18 5.18 0 0 1 5.546 1.682 9.076 9.076 0 0 0 3.33-1.317 5.038 5.038 0 0 1-2.4 2.942 9.068 9.068 0 0 0 3.02-.85 5.05 5.05 0 0 1-2.48 2.71z" /></svg></span>
										<span class="rrssb-text">twitter</span>
									</a>
								</li>
								<li class="rrssb-linkedin">
									<a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo current_url() ?>&amp;summary=<?php echo $result['title']; ?>" class="popup">
										<span class="rrssb-icon">
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 28">
												<path d="M25.424 15.887v8.447h-4.896v-7.882c0-1.98-.71-3.33-2.48-3.33-1.354 0-2.158.91-2.514 1.802-.13.315-.162.753-.162 1.194v8.216h-4.9s.067-13.35 0-14.73h4.9v2.087c-.01.017-.023.033-.033.05h.032v-.05c.65-1.002 1.812-2.435 4.414-2.435 3.222 0 5.638 2.106 5.638 6.632zM5.348 2.5c-1.676 0-2.772 1.093-2.772 2.54 0 1.42 1.066 2.538 2.717 2.546h.032c1.71 0 2.77-1.132 2.77-2.546C8.056 3.593 7.02 2.5 5.344 2.5h.005zm-2.48 21.834h4.896V9.604H2.867v14.73z" /></svg>
										</span>
										<span class="rrssb-text">linkedin</span>
									</a>
								</li>
								<li class="rrssb-whatsapp">
									<a href="whatsapp://send?text=<?php echo current_url() ?>" data-action="share/whatsapp/share">
										<span class="rrssb-icon">
											<svg xmlns="http://www.w3.org/2000/svg" width="90" height="90" viewBox="0 0 90 90">
												<path d="M90 43.84c0 24.214-19.78 43.842-44.182 43.842a44.256 44.256 0 0 1-21.357-5.455L0 90l7.975-23.522a43.38 43.38 0 0 1-6.34-22.637C1.635 19.63 21.415 0 45.818 0 70.223 0 90 19.628 90 43.84zM45.818 6.983c-20.484 0-37.146 16.535-37.146 36.86 0 8.064 2.63 15.533 7.076 21.61l-4.64 13.688 14.274-4.537A37.122 37.122 0 0 0 45.82 80.7c20.48 0 37.145-16.533 37.145-36.857S66.3 6.983 45.818 6.983zm22.31 46.956c-.272-.447-.993-.717-2.075-1.254-1.084-.537-6.41-3.138-7.4-3.495-.993-.36-1.717-.54-2.438.536-.72 1.076-2.797 3.495-3.43 4.212-.632.72-1.263.81-2.347.27-1.082-.536-4.57-1.672-8.708-5.332-3.22-2.848-5.393-6.364-6.025-7.44-.63-1.076-.066-1.657.475-2.192.488-.482 1.084-1.255 1.625-1.882.543-.628.723-1.075 1.082-1.793.363-.718.182-1.345-.09-1.884-.27-.537-2.438-5.825-3.34-7.977-.902-2.15-1.803-1.793-2.436-1.793-.63 0-1.353-.09-2.075-.09-.722 0-1.896.27-2.89 1.344-.99 1.077-3.788 3.677-3.788 8.964 0 5.288 3.88 10.397 4.422 11.113.54.716 7.49 11.92 18.5 16.223 11.01 4.3 11.01 2.866 12.996 2.686 1.984-.18 6.406-2.6 7.312-5.107.9-2.513.9-4.664.63-5.112z" /></svg>
										</span>
										<span class="rrssb-text">Whatsapp</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<!--<hr class="bloghr"> -->
					<div class="box2">
						<div class="box-sm2 red2"></div>
						<div class="box-sm2 orange2"></div>
						<div class="box-sm2 yellow2 "></div>
						<div class="box-sm2 green2 "></div>
						<div class="box-sm2 blue2 "></div>
						<div class="box-sm2 purple2"></div>
					</div>
				<!-- Buttons end here -->
				<?php if ($result['description']) : ?>
					<div class="description">
						<?php echo html_entity_decode($result['description']) ?>
					</div>
				<?php endif ?>
					<div class="sidebar_block mt-4">
                        <div class="widget widget-author">
                            <div class="author-card blogside">
								<div class="sidebar_heading">
									<h3>Tags</h3>
								</div>
								<div class="sidebar_tags mt-0">
									<ul>
									<?php if ($result['meta_keywords']) : 
										$meta_keywords = explode(',',$result['meta_keywords']); 
										foreach ($meta_keywords as $key => $value) : ?>	
											<li><a><?php echo $value; ?></a></li>
									<?php endforeach;
									endif ?>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				
				<a href="javascript:history.back()" class="btn btn-primary rounded25"><span><i class="fas fa-arrow-circle-left mr-2" aria-hidden="true"></i></span>Go Back</a>
			</div>
			</div>
			</div>
			
			<div class="col-md-3">
				<div class="blog_sidebar">
					<div class="sidebar_block">
                        <div class="widget widget-author">
                            <!--<div class="sidebar_heading">
                                <h3>about me</h3>
                                <img src="../assets/images/footer_underline.png" alt="image">
                            </div>-->
                            <div class="author-card">
                                <div class="author-card-cover" style="background-image: url(<?php echo site_url('uploads/system/bloggerheader.jpg'); ?>);"></div>
                                    <div class="author-card-profile">
									<?php 
											$user_details = $this->db->select(array('first_name', 'last_name'))->where('id', 4)->get('users')->row_array();
											?>
                                        <div class="author-card-avatar">
											
											<img src="<?php echo $this->user_model->get_user_image_url(4); ?>" alt="image">
											
                                        </div>
                                        <div class="author-card-details">
                                        <h5 class="author-card-name"><?php echo $user_details['first_name'] . ' ' . $user_details['last_name']; ?></h5><span class="author-card-position">Founder Course Wings</span>
                                        </div>
                                    </div>
                                <p class="author-card-info">Blogs on latest trends & insights concerning Industries, Business, E-Learning & User experience.</p>
                                <div class="author-card-social-bar-wrap">
                                    <div class="author-card-social-bar">
                                        <a class="social-btn sb-style-1 sb-facebook" href="https://www.facebook.com/CourseWings/" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                        <a class="social-btn sb-style-1 sb-twitter" href="https://twitter.com/CourseWings" target="_blank"><i class="fab fa-twitter"></i></a>
                                        <a class="social-btn sb-style-1 sb-instagram" href="https://www.instagram.com/coursewings_videos/" target="_blank"><i class="fab fa-instagram"></i></a>
                                        <a class="social-btn sb-style-1 sb-linkedin" href="https://www.linkedin.com/company/course-wings/" target="_blank"><i class="fab fa-linkedin"></i></a>
                                        <a class="social-btn sb-style-1 sb-youtube" href="https://www.youtube.com/channel/UCG1ovYGI3fWScL2SF3Gg1XQ?view_as=subscriber" target="_blank"><i class="fab fa-youtube"></i></a>
                                    </div>
                                </div>
                            </div>
                      </div>
                    </div>
				
				
					<?php
					$result = $this->db->where('hits != ', 0)->where('status', 1)->order_by('hits', 'DESC')->limit(3)->get('blogs')->result_array();
					if (!empty($result)) : ?>
                    <div class="sidebar_block">
                        <div class="widget widget-author">
							<div class="author-card blogside">
								<div class="sidebar_heading">
									<h3>popular posts</h3>
								</div>
								<div class="sidebar_post">
									<ul>
									<?php foreach ($result as $key => $value) : ?>
										<li>
											<div class="post_content">
												<p><?php echo date('M d, Y', strtotime($value['created_date'])) ?></p>
												<a href="<?php echo base_url('home/blog/' . $value['slug']); ?>"><?php echo $value['title'] ?></a>
											</div>
										</li>
									<?php endforeach; ?>
									</ul>
								</div>
							</div>
                        </div>
                    </div>
					<?php endif; ?>
                    
					<?php
					$result = $this->db->where('status', 1)->order_by('created_date', 'DESC')->limit(3)->get('blogs')->result_array();
					if (!empty($result)) : ?>
                    <div class="sidebar_block">
                        <div class="widget widget-author">
							<div class="author-card blogside">
								<div class="sidebar_heading">
									<h3>recent posts</h3>
								</div>
								<div class="sidebar_post">
									<ul>
									<?php foreach ($result as $key => $value) : ?>
										<li>
											<div class="post_content">
												<p><?php echo date('M d, Y', strtotime($value['created_date'])) ?></p>
												<a href="<?php echo base_url('home/blog/' . $value['slug']); ?>"><?php echo $value['title'] ?></a>
											</div>
										</li>
									<?php endforeach; ?>
									</ul>
									
								</div>
							</div>
                        </div>
                    </div>
					<?php endif; ?>
                    <!-- TAGS ECHO
                    <div class="sidebar_block">
                        <div class="widget widget-author">
                            <div class="author-card blogside">
                                <div class="sidebar_heading">
                                    <h3>tags</h3>
                                </div>
                                <div class="sidebar_tags">
                                    <ul>
                                        <li><a href="javascript:;">Agriculture</a></li>
                                        <li><a href="javascript:;">farm</a></li>
                                        <li><a href="javascript:;">Organic</a></li>
                                        <li><a href="javascript:;">egg</a></li>
                                        <li><a href="javascript:;">Permaculture</a></li>
                                        <li><a href="javascript:;">Green</a></li>
                                        <li><a href="javascript:;">garden</a></li>
                                        <li><a href="javascript:;">milk</a></li>
                                        <li><a href="javascript:;">dairy farm</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    -->
                    
                </div>
			</div>
			
		</div>
	</div>
</section>
<script src="<?php echo base_url('assets/frontend/default/rrssb/js/rrssb.min.js'); ?>"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {

		$('.rrssb-buttons').rrssb({
			// required:
			title: '<?php echo $result['title']; ?>',
			url: '<?php echo current_url() ?>',
		});
	});
</script>