<?php
	$this->db->where('user_id', $this->session->userdata('user_id'));
	$purchase_history = $this->db->get('payment',$per_page, $this->uri->segment(3));
?>
<section class="page-header-area my-course-area">
    <div class="container-lg">
        <div class="row">
            <div class="col">
                <h1 class="page-title"><i class="fas fa-cart-plus mr-2"></i><?php echo get_phrase('purchase_history'); ?></h1>
                <ul>
					<li class=""><a href="<?php echo site_url('home/my_courses'); ?>"><i class="fas fa-book-reader mr-1"></i><?php echo get_phrase('subscribed_courses'); ?></a></li>
					<li><a href="<?php echo site_url('home/my_wishlist'); ?>"><i class="fas fa-hand-holding-heart mr-1"></i><?php echo get_phrase('my_wishlists'); ?></a></li>
					<?php /* <li><a href="<?php echo site_url('home/my_messages'); ?>"><?php echo get_phrase('my_messages'); ?></a></li> */ ?>
					<li class="active"><a href="<?php echo site_url('home/purchase_history'); ?>"><i class="fas fa-cart-plus mr-1"></i><?php echo get_phrase('purchase_history'); ?></a></li>
					<?php /* <li><a href="<?php echo site_url('home/profile/user_profile'); ?>"><?php echo get_phrase('user_profile'); ?></a></li> */ ?>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user-edit mr-1"></i><?php echo get_phrase('my_profile'); ?></a>
						<div class="dropdown-menu profile">
							<a class="dropdown-item px-3 py-2" href="<?php echo site_url('home/profile/user_profile'); ?>">
								<i class="fas fa-user-edit mr-1"></i><?php echo get_phrase('edit_profile'); ?>
							</a>
							<a class="dropdown-item px-3 py-2" href="<?php echo site_url('home/profile/user_credentials'); ?>">
								<i class="fas fa-key mr-1"></i><?php echo get_phrase('change_password'); ?>
							</a>
							<a class="dropdown-item px-3 py-2" href="<?php echo site_url('home/profile/user_photo'); ?>">
								<i class="far fa-image mr-1"></i><?php echo get_phrase('edit_photo'); ?>
							</a>
						</div>
					</li>
                </ul>
            </div>
        </div>
    </div>
</section>


<section class="purchase-history-list-area mt-3">
    <div class="container-lg">
		<div class="row">
			
            <div class="col-md-9">
				<?php if ($purchase_history->num_rows() > 0):
					foreach($purchase_history->result_array() as $each_purchase):
						$course_details = $this->crud_model->get_course_by_id($each_purchase['course_id'])->row_array();?>
				<div class="row no-gutters purchasehist">
					<div class="col-lg-3">
						<div class="course-box-wrap mb-2">
							<div class="course-box">
								<a class="purchase-history-course-title" href="<?php echo site_url('home/lesson/'.slugify($course_details['title']).'/'.$course_details['id']); ?>" >
								<div class="course-image">
									<img src="<?php echo $this->crud_model->get_course_thumbnail_url($each_purchase['course_id']);?>" class="img-fluid">
								</div>
								</a>
							
							</div>
							<div class="course-details text-center" style="background:#6f2d87;padding:5px;">
								<a class="" href="<?php echo site_url('home/course/'.slugify($course_details['title']).'/'.$course_details['id']); ?>" >
									<?php echo $course_details['title']; ?>
								</a>
							</div>
							
							<div class="ratenow mycourses">
								<div>
									<span class="lsnhrswish float-left"><?php echo get_phrase('pur._date'); ?></span>
									
									<span class="lsnhrswish">
										<p class="price text-right"><?php echo date('dS F, Y', $each_purchase['date_added']); ?></p>
									</span>
									
								</div>
								<div>
									<span class="lsnhrswish float-left"><?php echo get_phrase('payment_mode'); ?></span>
									
									<span class="lsnhrswish">
										<p class="price text-right"><?php echo ucfirst($each_purchase['payment_type']); ?></p>
									</span>
								</div>
								
								<div>
									<span class="lsnhrswish float-left"><?php echo get_phrase('price_paid'); ?></span>
									
									<span class="lsnhrswish">
										<p class="price text-right"><?php echo currency($each_purchase['amount']); ?></p>
									</span>
								</div>
							</div>
							<?php /*
							<div class="ratenow mycourses paytype">
								<a href="<?php echo site_url('home/invoice/'.$each_purchase['id']); ?>" target="_blank" class="btn btn-receipt"><?php echo get_phrase('invoice'); ?></a>
							</div>
							*/ ?>
						</div>
					</div>
				</div>
					

					<?php endforeach; ?>
				<?php else: ?>
					<div class="row" style="text-align: center;">
						<?php echo get_phrase('no_records_found'); ?>
					</div>
				<?php endif; ?>		
			</div>
			
			<div class="col-md-3">
				<div class="course-sidebar-text-box side purhist mt-4 bg-orange">	
					<div class="promo">
						<ul class="list-group list-group-flush">
							<li class="list-group-item bg-orange">
								<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fas fa-desktop"></i></div>
								<div>
									<p class="txt-purple font-weight-bold mb-0">
									<?php 
										$status_wise_courses = $this->crud_model->get_status_wise_courses();
										$number_of_courses = $status_wise_courses['active']->num_rows();
										echo get_phrase('online_courses'); 
									?>
									</p>
									
									<p><?php echo $number_of_courses.' '.get_phrase('topics_to_learn_from'); ?></p>
								</div>
							</li>
							<li class="list-group-item bg-orange">
								<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fa fa-key"></i></div>
								<div>
									<p class="txt-purple font-weight-bold mb-0"><?php echo get_phrase('lifetime_access'); ?></p>
									<p><?php echo get_phrase('learn_as_per_your_convenience'); ?></p>
								</div>
							</li>
							<li class="list-group-item bg-orange">
								<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fas fa-chalkboard-teacher"></i></div>
								<div>
									<p class="txt-purple font-weight-bold mb-0"><?php echo get_phrase('inspire_students'); ?></p>
									<p><?php echo get_phrase('help_people_learn_new_skills'); ?></p>
								</div>
							</li>
							<li class="list-group-item bg-orange">
								<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fas fa-user-graduate"></i></div>
								<div>
									<p class="txt-purple font-weight-bold mb-0"><?php echo get_phrase('course_completion_acknowledgement'); ?></p>
									<p><?php echo get_phrase('get_proof_of_course_completion'); ?></p>
								</div>
							</li>
							
						</ul>
					</div>
						
				</div>
			</div>
			
			
		</div>
        
    </div>
</section>
<nav>
    <?php echo $this->pagination->create_links(); ?>
</nav>
