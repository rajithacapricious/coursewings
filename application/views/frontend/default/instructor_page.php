<?php
$instructor_details = $this->user_model->get_all_user($instructor_id)->row_array();
$social_links  = json_decode($instructor_details['social_links'], true);
$course_ids = $this->crud_model->get_instructor_wise_courses($instructor_id, 'simple_array');
?>
<section class="instructor-header-area">
    <div class="container-lg">
        <div class="row">
            <div class="col">
                <h1 class="instructor-name"><?php echo $instructor_details['first_name'].' '.$instructor_details['last_name']; ?></h1>
                <h2 class="instructor-title"><?php echo ucfirst(get_phrase('course_wings_instructor')); ?></h2>
				<?php /* <h2 class="instructor-title"><?php echo $instructor_details['title']; ?></h2>  */?>
            </div>
        </div>
    </div>
</section>

<section class="instructor-details-area">
    <div class="container-lg">
        <div class="row">
			<div class="col-lg-3">
				<div class="cart-sidebar mb-3">
                    <div class="user-box text-center">
						<img src="<?php echo $this->user_model->get_user_image_url($instructor_details['id']);?>" alt="" class="img-fluid img-thumbnail">
						<div class="name text-center mt-3">
							<div class="name"><?php echo $instructor_details['first_name'].' '.$instructor_details['last_name']; ?></div>
						</div>
					</div>
					
					
                    <?php if ($social_links['twitter']!=''): ?>
						<a class="btn bg-twitter btn-block mt-3" href="<?php echo $social_links['twitter']; ?>" target="_blank">
							<i class="fab fa-twitter mr-2"></i> My Twitter Page
						</a>
					<?php endif; ?>
					<?php if ($social_links['twitter']==''): ?>
						<a class="btn bg-twitter btn-block mt-3 active" data-toggle="modal" data-target="#notwittermodal"><i class="fab fa-twitter mr-2"></i>My Twitter Page</a>
					<?php endif; ?>
					
					
					<?php if ($social_links['facebook']!=''): ?>
						<a class="btn bg-facebook btn-block mt-3" href="<?php echo $social_links['facebook']; ?>" target="_blank">
							<i class="fab fa-facebook mr-2"></i> My Facebook Page 
						</a>
					<?php endif; ?>
					<?php if ($social_links['facebook']==''): ?>
						<a class="btn bg-facebook btn-block mt-3 active" data-toggle="modal" data-target="#nofacebookmodal"><i class="fab fa-facebook-f mr-2"></i>My Facebook Page</a>
					<?php endif; ?>
					
					
                    <?php if ($social_links['linkedin']!=''): ?>
						<a class="btn bg-linkedin btn-block mt-3" href="<?php echo $social_links['linkedin']; ?>" target="_blank">
							<i class="fab fa-linkedin-in mr-2"></i> My Linked In Page
						</a>
					<?php endif; ?>
					<?php if ($social_links['linkedin']==''): ?>
						<a class="btn bg-linkedin btn-block mt-3 active" data-toggle="modal" data-target="#nolinkedinmodal"><i class="fab fa-linkedin-in mr-2"></i>My Linked In Page</a>
					<?php endif; ?>
					
                </div>
				
				
				
			</div>
		
		
		
		
		
		
			
            
            <div class="col-lg-9">
				
				<div class="row mb-2">
					<div class="col-md-4 mb-2">
						<div class="card bg-info card-img-holder text-white">
							<div class="card-body">
								<img src="<?php echo site_url('assets/frontend/default/img/circle.svg'); ?>" class="card-img-absolute" alt="circle-image">
								<h4 class="font-weight-normal mb-3"><?php echo get_phrase('total_students'); ?>
								<i class="fas fa-user-graduate fa-3x mr-1 float-right"></i>
								</h4>
								<h2 class="mb-1">
									<?php
                                    $this->db->select('user_id');
                                    $this->db->distinct();
                                    $this->db->where_in('course_id', $course_ids);
                                    echo $this->db->get('enrol')->num_rows();?>
								</h2>
							</div>
						</div>
					</div>
					
					<div class="col-md-4 mb-2">
						<div class="card bg-purple card-img-holder text-white">
							<div class="card-body">
								<img src="<?php echo site_url('assets/frontend/default/img/circle.svg'); ?>" class="card-img-absolute" alt="circle-image">
								<h4 class="font-weight-normal mb-3"><?php echo get_phrase('courses'); ?>
								<i class="fas fa-book-open fa-3x mr-1 float-right"></i>
								</h4>
								<h2 class="mb-1">
									<?php echo sizeof($course_ids); ?>
								</h2>
							</div>
						</div>
					</div>
					
					<div class="col-md-4 mb-2">
						<div class="card bg-yellow card-img-holder text-white">
							<div class="card-body">
								<img src="<?php echo site_url('assets/frontend/default/img/circle.svg'); ?>" class="card-img-absolute" alt="circle-image">
								<h4 class="font-weight-normal mb-3"><?php echo get_phrase('reviews'); ?>
								<i class="fas fa-star-half-alt fa-3x mr-1 float-right"></i>
								</h4>
								<h2 class="mb-1">
									<?php echo $this->crud_model->get_instructor_wise_course_ratings($instructor_id, 'course')->num_rows(); ?>
								</h2>
							</div>
						</div>
					</div>
				</div>
				
				
				
                <div class="course-sidebar-text-box side mt-3">
					<div class="includes">
					<div class="title"><b>About Me:</b></div>

                    <div class="biography-content-box view-more-parent">
                        <!-- <div class="view-more" onclick="viewMore(this,'hide')"><b><?php echo get_phrase('show_full_biography'); ?></b></div> -->
                        <div class="biography-content">
                            <?php echo $instructor_details['biography']; ?>
                        </div>
                    </div>
					</div>
                </div>
				
				<div class="course-sidebar-text-box side mt-3">
					<div class="includes">
						<div class="title"><b>Courses By Me</b></div>
							<ul class="requirements__list">
								<?php
									$author_courses = $this->crud_model->get_instructor_wise_courses($instructor_id, 'course')->result_array();
									foreach ($author_courses as $author_courses):
									  if($author_courses['id'] != $course_details['id'] && $author_courses['status'] == 'active'): 
								?>
									<li><i class="iconbuttonsuffix mr-1"></i><a href="<?php echo site_url('home/course/'.slugify($author_courses['title']).'/'.$author_courses['id']); ?>"><?php echo $author_courses['title']; ?></a></li>
								<?php endif; ?>
							  <?php endforeach; ?>
							</ul>
					</div>
				</div>
				
				
				
            </div>
			
			
			
			
        </div>
    </div>
</section>
<div class="modal fade" id="notwittermodal">
	<div class="modal-dialog">
		<div class="modal-content">
		
		<div class="modal-body bg-warning text-center">
			<i class="fas fa-frown fa-3x"></i>
			<h3>Sorry!</h3>
			Instructor has not provided his Twitter page<br>
			<button type="button" class="btn btn-danger mt-2" data-dismiss="modal">Close</button>
		</div>
	</div>
  </div>
</div>
<div class="modal fade" id="nofacebookmodal">
	<div class="modal-dialog">
		<div class="modal-content">
		
		<div class="modal-body bg-warning text-center">
			<i class="fas fa-frown fa-3x"></i>
			<h3>Sorry!</h3>
			Instructor has not provided his Facebook page<br>
			<button type="button" class="btn btn-danger mt-2" data-dismiss="modal">Close</button>
		</div>
	</div>
  </div>
</div>
<div class="modal fade" id="nolinkedinmodal">
	<div class="modal-dialog">
		<div class="modal-content">
		
		<div class="modal-body bg-warning text-center">
			<i class="fas fa-frown fa-3x"></i>
			<h3>Sorry!</h3>
			Instructor has not provided his LinkedIn page<br>
			<button type="button" class="btn btn-danger mt-2" data-dismiss="modal">Close</button>
		</div>
	</div>
  </div>
</div>