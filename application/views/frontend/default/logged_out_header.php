<section class="menu-area sticky">
  <div class="container-xl">
    <div class="row">
      <div class="col">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">

          <ul class="mobile-header-buttons">
            <li><a class="mobile-nav-trigger" href="#mobile-primary-nav">Menu<span></span></a></li>
            <li><a class="mobile-search-trigger" href="#mobile-search">Search<span></span></a></li>
          </ul>
			
          <a href="<?php echo site_url(''); ?>" class="navbar-brand" href="#"><img src="<?php echo base_url().'uploads/system/logo-dark.png'; ?>" alt="" height="25"></a>
		  <?php /* ?> <a href="<?php echo site_url(''); ?>" class="navbar-brand" href="#"><i class="fas fa-home pl-2 txt-purple"></i></a> <?php */ ?>
          <?php include 'menu.php'; ?>

          <form class="inline-form" action="<?php echo site_url('home/search'); ?>" method="get" style="width: 100%;" autocomplete="off">
            <div class="input-group search-box mobile-search">
              <input id="autosearchbar" type="text" name = 'query' class="form-control" placeholder="Search for the course you want to learn!" style="z-index: 0;">
              <div class="input-group-append menusearch1" style="position: absolute;right: 6px;top: 50%;transform: translateY(-50%);">
                <button class="btn" type="submit"><i class="fas fa-search" style="margin-left: -5px !important;"></i></button>
              </div>
            </div>
          </form>

          <?php if ($this->session->userdata('admin_login')): ?>
            <div class="instructor-box menu-icon-box">
              <div class="icon admin">
                <a href="<?php echo site_url('admin'); ?>" style="background: #6f2d87; color:#fff; margin: 10px; font-size: 14px; width: 100%; border-radius: 20px; min-width: 100px;"><?php echo get_phrase('admin_panel'); ?></a>
              </div>
            </div>
          <?php endif; ?>

          

          <span class="signin-box-move-desktop-helper"></span>
          <div class="sign-in-box btn-group">
			<button type="button" class="btn btn-sign-in plain" data-toggle="modal" data-target="#signInModal"><?php echo get_phrase('log_in'); ?></button>
			<?php /*
            <a href="<?php echo site_url('home/login'); ?>" class="btn btn-sign-in"><?php echo get_phrase('log_in'); ?></a>
			*/ ?>

            <a href="<?php echo site_url('home/sign_up'); ?>" class="btn btn-sign-up"><?php echo get_phrase('sign_up'); ?></a>

          </div>
		  
		  <div class="cart-box menu-icon-box" id = "cart_items">
            <?php include 'cart_items.php'; ?>
          </div>
        </nav>
      </div>
    </div>
  </div>
</section>
