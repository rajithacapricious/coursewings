<section class="category-header-area">
    <div class="container-lg">
        <div class="row">
            <div class="col">
                <h1 class="category-name">
                    <i class="fas fa-user-plus mr-2"></i><?php echo get_phrase('register_yourself'); ?>
                </h1>
            </div>
        </div>
    </div>
</section>

<section class="category-course-list-area">
    <div class="container-lg">
        <div class="row justify-content-center">
            <div class="col-lg-9">
              <div class="user-dashboard-box mt-4">
                  <div class="user-dashboard-content w-100 login-form hidden">
                      <div class="content-title-box bg-yellow">
                          <div class="title"><i class="fas fa-sign-in-alt mr-2"></i><?php echo get_phrase('login'); ?></div>
                          <div class="subtitle"><?php echo get_phrase('provide_your_valid_login_credentials'); ?>.</div>
                      </div>
                      <form action="<?php echo site_url('login/validate_login/user'); ?>" method="post">
                          <div class="content-box">
                              <div class="basic-group">
                                  <div class="form-group">
                                      <label for="login-email"><span class="input-field-icon"><i class="fas fa-envelope"></i></span> <?php echo get_phrase('email'); ?>:</label>
                                      <input type="email" class="form-control" name = "email" id="login-email" placeholder="<?php echo get_phrase('email'); ?>" required>
                                  </div>
                                  <div class="form-group">
                                      <label for="login-password"><span class="input-field-icon"><i class="fas fa-lock"></i></span> <?php echo get_phrase('password'); ?>:</label>
                                      <input type="password" class="form-control" name = "password" placeholder="<?php echo get_phrase('password'); ?>" required>
                                  </div>
                              </div>
                          </div>
                          <div class="content-update-box">
                              <button type="submit" class="btn btn-block"><?php echo get_phrase('login'); ?></button>
                          </div>
                          <div class="forgot-pass text-center">
                              <span>or</span>
                              <a href="javascript::" onclick="toggoleForm('forgot_password')"><?php echo get_phrase('forgot_password'); ?></a>
                          </div>
                          <div class="account-have text-center">
                              <?php echo get_phrase('do_not_have_an_account'); ?>? <a href="javascript::" onclick="toggoleForm('registration')"><?php echo get_phrase('sign_up'); ?></a>
                          </div>
                      </form>
					  <div class="agreement-text text-center pt-3">
                            <small><?php echo get_phrase('by_signing_in_you_agree_to_our'); ?> <a href="<?php echo site_url('home/terms_and_condition'); ?>"><?php echo get_phrase('terms_of_use'); ?></a> <?php echo get_phrase('and'); ?> <a href="<?php echo site_url('home/privacy_policy'); ?>"><?php echo get_phrase('privacy_policy'); ?></a>.</small>
                      </div>
                  </div>
                  <div class="user-dashboard-content w-100 register-form">
                      <div class="content-title-box bg-yellow">
                          <div class="title"><i class="fab fa-wpforms mr-2"></i><?php echo get_phrase('registration_form'); ?></div>
                          <div class="subtitle">Sign up and start Teaching or Learning.</div>
                      </div>
                      <form action="<?php echo site_url('login/register'); ?>" method="post">
                          <div class="content-box">
                              <div class="basic-group">
                                  <div class="form-group">
                                      <label for="first_name"><span class="input-field-icon"><i class="fas fa-user"></i></span> <?php echo get_phrase('first_name'); ?>:</label>
                                      <input type="text" class="form-control" name = "first_name" id="first_name" placeholder="<?php echo get_phrase('first_name'); ?>" value="" required>
                                  </div>
                                  <div class="form-group">
                                      <label for="last_name"><span class="input-field-icon"><i class="fas fa-user"></i></span> <?php echo get_phrase('last_name'); ?>:</label>
                                      <input type="text" class="form-control" name = "last_name" id="last_name" placeholder="<?php echo get_phrase('last_name'); ?>" value="" required>
                                  </div>
                                  <div class="form-group">
                                      <label for="registration-email"><span class="input-field-icon"><i class="fas fa-envelope"></i></span> <?php echo get_phrase('email'); ?>:</label><p style="color:red; display: none;"><small>NOTE: For Instructors, please open a PayPal Account & make sure your below Email ID is same as PayPal email ID to receive payments.</small></p>
                                      <input type="email" class="form-control" name = "email" id="registration-email" placeholder="<?php echo get_phrase('email'); ?>" value="" required>
                                  </div>
                                  <div class="form-group">
                                      <label for="registration-password"><span class="input-field-icon"><i class="fas fa-lock"></i></span> <?php echo get_phrase('password'); ?>:</label>
                                      <input type="password" class="form-control" name = "password" id="registration-password" placeholder="<?php echo get_phrase('password'); ?>" value="" required>
                                  </div>
                              </div>
                          </div>
                          <div class="content-update-box">
                              <button type="submit" class="btn btn-block"><?php echo get_phrase('sign_up'); ?></button>
                          </div>
                          <div class="account-have text-center">
                              <?php echo get_phrase('already_have_an_account'); ?>? <a href="javascript::" onclick="toggoleForm('login')"><?php echo get_phrase('login'); ?></a>
                          </div>
                      </form>
					  <div class="agreement-text text-center pt-3">
                            <small><?php echo get_phrase('by_signing_in_you_agree_to_our'); ?> <a href="<?php echo site_url('home/terms_and_condition'); ?>"><?php echo get_phrase('terms_of_use'); ?></a> <?php echo get_phrase('and'); ?> <a href="<?php echo site_url('home/privacy_policy'); ?>"><?php echo get_phrase('privacy_policy'); ?></a>.</small>
                      </div>
                  </div>

                  <div class="user-dashboard-content w-100 forgot-password-form hidden">
                      <div class="content-title-box bg-yellow">
                          <div class="title"><i class="fas fa-unlock-alt mr-2"></i><?php echo get_phrase('forgot_password'); ?></div>
                          <div class="subtitle"><?php echo get_phrase('provide_your_email_address_to_get_password'); ?>.</div>
                      </div>
                      <form action="<?php echo site_url('login/forgot_password/frontend'); ?>" method="post">
                          <div class="content-box">
                              <div class="basic-group">
                                  <div class="form-group">
                                      <label for="forgot-email"><span class="input-field-icon"><i class="fas fa-envelope"></i></span> <?php echo get_phrase('email'); ?>:</label>
                                      <input type="email" class="form-control" name = "email" id="forgot-email" placeholder="<?php echo get_phrase('email'); ?>" value="" required>
                                      <small class="form-text text-muted"><?php echo get_phrase('provide_your_email_address_to_get_password'); ?>.</small>
                                  </div>
                              </div>
                          </div>
                          <div class="content-update-box">
                              <button type="submit" class="btn btn-block"><?php echo get_phrase('reset_password'); ?></button>
                          </div>
                          <div class="forgot-pass text-center">
                              <?php echo get_phrase('want_to_go_back'); ?>? <a href="javascript::" onclick="toggoleForm('login')"><?php echo get_phrase('login'); ?></a>
                          </div>
                      </form>
                  </div>
              </div>
            </div>
			
			<div class="col-lg-3">
				<div class="course-sidebar-text-box side mt-4 bg-orange">	
					<div class="promo">
						<ul class="list-group list-group-flush">
							<li class="list-group-item bg-orange">
								<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fas fa-desktop"></i></div>
								<div>
									<p class="txt-purple font-weight-bold mb-0">
									<?php 
										$status_wise_courses = $this->crud_model->get_status_wise_courses();
										$number_of_courses = $status_wise_courses['active']->num_rows();
										echo get_phrase('online_courses'); 
									?>
									</p>
									
									<p><?php echo $number_of_courses.' '.get_phrase('topics_to_learn_from'); ?></p>
								</div>
							</li>
							<li class="list-group-item bg-orange">
								<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fa fa-key"></i></div>
								<div>
									<p class="txt-purple font-weight-bold mb-0"><?php echo get_phrase('lifetime_access'); ?></p>
									<p><?php echo get_phrase('learn_as_per_your_convenience'); ?></p>
								</div>
							</li>
							<li class="list-group-item bg-orange">
								<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fas fa-chalkboard-teacher"></i></div>
								<div>
									<p class="txt-purple font-weight-bold mb-0"><?php echo get_phrase('inspire_students'); ?></p>
									<p><?php echo get_phrase('help_people_learn_new_skills'); ?></p>
								</div>
							</li>
							<li class="list-group-item bg-orange">
								<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fas fa-user-graduate"></i></div>
								<div>
									<p class="txt-purple font-weight-bold mb-0"><?php echo get_phrase('course_completion_acknowledgement'); ?></p>
									<p><?php echo get_phrase('get_proof_of_course_completion'); ?></p>
								</div>
							</li>
							
						</ul>
					</div>
						
				</div>
			
			</div>
			
        </div>
    </div>
</section>

<script type="text/javascript">
  function toggoleForm(form_type) {
    if (form_type === 'login') {
      $('.login-form').show();
      $('.forgot-password-form').hide();
      $('.register-form').hide();
    }else if (form_type === 'registration') {
      $('.login-form').hide();
      $('.forgot-password-form').hide();
      $('.register-form').show();
    }else if (form_type === 'forgot_password') {
      $('.login-form').hide();
      $('.forgot-password-form').show();
      $('.register-form').hide();
    }
  }
</script>
