<section class="category-header-area">
    <div class="container-lg">
        <div class="row">
            <div class="col">
                <h1 class="category-name">
                    <i class="fas fa-file-signature mr-2"></i><?php echo $page_title; ?>
                </h1>
            </div>
        </div>
    </div>
</section>

<section class="category-course-list-area">
    <div class="container-lg">
        <div class="row">
            <div class="col-md-9" style="padding: 35px 35px 35px 15px;">
                <p>
                    <b>1. Cookies:</b><br />
                    We employ the use of cookies. By accessing Course Wings, you agreed to use cookies in agreement with the Course Wings Privacy Policy.<br />
                    <br />
                    Most interactive websites use cookies to let us retrieve the user’s details for each visit. Cookies are used by our website to enable the functionality of certain areas to make it easier for people visiting our website. Some of our
                    affiliate/advertising partners may also use cookies.<br />
                    <br />
                    <b>
                        <br />
                        2. License, Trademark &amp; Communication with the Users:
                    </b>
                    <br />
                    Unless otherwise specified, Course Wings Portal and/or its licensors own the intellectual property rights for all materials published on Course Wings Portal. All intellectual property rights are reserved. <br />
                    <br />
                    <i>You may not: </i><br />
                    • Republish material/videos from Course Wings website <br />
                    • Sell, rent or sub-license material from Course Wings website <br />
                    • Reproduce, duplicate or copy material from Course Wings website<br />
                    • Redistribute content from Course Wings website<br />
                    • Tamper with Course Wing Site and its contents<br />
                    • You can’t use Course Wings Name, Logo and Domain Name to represent your business, as this is a registered name in the UAE.<br />
                    <br />
                    All communication will be done electronically and if you sign-up with us it is understood that you accept this mode of communication. <br />
                    <br />
                    <i>Point of Agreement: </i><br />
                    • As a student, when you open an account with Course Wings either to view a course or for any other reason/s, you are granted the permission to access/view our courses. <br />
                    • As soon as you open an account (either as a student or lecturer) you agree to all the terms and conditions.<br />
                    • As a student, in most cases, you will have lifetime access to most of our course. However, we have the right to cancel your account at any given point of time without notice. <br />
                    <br />
                    <br />
                    <b>3. Course-related comments and offensive comments: </b><br />
                    Parts of this website offer an opportunity for experts to post videos and exchange opinions and information. Course Wings does not filter, edit, publish or review comments prior to their presence on the website. Comments (from anyone)
                    do not reflect the views and opinions of Course Wings, its agents and or its affiliates. Comments reflect the views and opinions of the person who post the content. To the extent permitted by applicable laws, Course Wings shall not be
                    liable for the Comments or for any liability, damages or expenses caused and/or suffered as a result of any use of and/or posting of and/or appearance of the Comments on this website. <br />
                    <br />
                    <br />
                    <b>4. Terms and Conditions Updates: </b><br />
                    On a regular basis, we will be updating these terms and conditions section and Course Wings reserves the right to make any changes. Hence, it is advisable for users to view the T&amp;C regularly. Note: The Terms and Conditions will be
                    in effect from the time it is posted.<br />
                </p>

            </div>
            <div class="col-md-3">
                <div class="course-sidebar-text-box side mt-4 bg-orange">	
                    <div class="promo">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item bg-orange">
                                <div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fas fa-desktop"></i></div>
                                <div>
                                    <p class="txt-purple font-weight-bold mb-0">
                                        <?php
                                        $status_wise_courses = $this->crud_model->get_status_wise_courses();
                                        $number_of_courses = $status_wise_courses['active']->num_rows();
                                        echo get_phrase('online_courses');
                                        ?>
                                    </p>

                                    <p><?php echo $number_of_courses . ' ' . get_phrase('topics_to_learn_from'); ?></p>
                                </div>
                            </li>
                            <li class="list-group-item bg-orange">
                                <div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fa fa-key"></i></div>
                                <div>
                                    <p class="txt-purple font-weight-bold mb-0"><?php echo get_phrase('lifetime_access'); ?></p>
                                    <p><?php echo get_phrase('learn_as_per_your_convenience'); ?></p>
                                </div>
                            </li>
                            <li class="list-group-item bg-orange">
                                <div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fas fa-chalkboard-teacher"></i></div>
                                <div>
                                    <p class="txt-purple font-weight-bold mb-0"><?php echo get_phrase('inspire_students'); ?></p>
                                    <p><?php echo get_phrase('help_people_learn_new_skills'); ?></p>
                                </div>
                            </li>
                            <li class="list-group-item bg-orange">
                                <div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fas fa-user-graduate"></i></div>
                                <div>
                                    <p class="txt-purple font-weight-bold mb-0"><?php echo get_phrase('course_completion_acknowledgement'); ?></p>
                                    <p><?php echo get_phrase('get_proof_of_course_completion'); ?></p>
                                </div>
                            </li>

                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
