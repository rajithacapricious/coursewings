<ul>

    <?php foreach($courses as $course):
     $instructor_details = $this->user_model->get_all_user($course['user_id'])->row_array();?>

<div class="container">
    <div class="row">
        <div class="col-sm-12 px-0 py-0">
            <div class="card mb-3">
                <div class="card-body lng">
                    <div class="row">
						<div class="col-sm-3">
							<a href="<?php echo site_url('home/course/'.slugify($course['title']).'/'.$course['id']) ?>">
								<img src="<?php echo $this->crud_model->get_course_thumbnail_url($course['id']); ?>" alt="" class="img-fluid">
							</a>
						</div>
						<div class="col-sm-9">
							<h6 class="card-title listtit">
								<a href="<?php echo site_url('home/course/'.slugify($course['title']).'/'.$course['id']); ?>" class="course-title">
									<u><?php echo $course['title']; ?></u>
								</a>
							</h6>
							<div class="row rating">
								<div class="col-sm-5">
									<div class="course-price">
										<?php if ($course['is_free_course'] == 1): ?>
											<span class="current-price"><?php echo get_phrase('free'); ?></span>
										<?php else: ?>
											<?php if($course['discount_flag'] == 1): ?>
												<span class="current-price"><?php echo currency($course['discounted_price']); ?></span>
												<span class="discount cross prc"><?php echo currency($course['price']); ?></span>
											<?php else: ?>
												<span class="current-price"><?php echo currency($course['price']); ?></span>
											<?php endif; ?>
										<?php endif; ?>
									</div>
								</div>
								<div class="col-sm-7 float-right">
									<?php
										$total_rating =  $this->crud_model->get_ratings('course', $course['id'], true)->row()->rating;
										$number_of_ratings = $this->crud_model->get_ratings('course', $course['id'])->num_rows();
										if ($number_of_ratings > 0) {
											$average_ceil_rating = ceil($total_rating / $number_of_ratings);
										}else {
											$average_ceil_rating = 0;
										}

										for($i = 1; $i < 6; $i++):?>
										<?php if ($i <= $average_ceil_rating): ?>
										<span class="float-right"><i class="fas fa-star filled"></i>
										<?php else: ?>
										<span class="float-right"><i class="fas fa-star"></i>
										<?php endif; ?>
										<?php endfor; ?>
									
										<span class="d-inline-block average-rating"><?php echo $average_ceil_rating; ?>&nbsp;Stars</span>
										<span class="font-italic ml-2">
											<kbd>(<?php echo $this->crud_model->get_ratings('course', $course['id'])->num_rows().' '.get_phrase('ratings'); ?>)</kbd>
										</span>
									</span>
								</div>
							</div>
							
							<p class="card-text listdesc mt-2"><?php echo $course['short_description']; ?></p>
							
							<div class="media">
								
									<a href="<?php echo site_url('home/instructor_page/'.$instructor_details['id']) ?>">
									<img src="<?php echo $this->user_model->get_user_image_url($instructor_details['id']); ?>" alt="" class="img-fluid img-thumbnail authorimg">
									</a>
									<p class="media-body pb-3 mb-0">
										<a href="<?php echo site_url('home/instructor_page/'.$instructor_details['id']) ?>" class="course-instructor"><strong class="d-block"><?php echo $instructor_details['first_name'].' '.$instructor_details['last_name']; ?></strong></a>
										Course Author
									</p>
								
							</div>
						</div>
						
						<div class="col-sm-12 mt-3">
							<div class="breadcrumb lstbrd">
								<span class="col text-center"><i class="fas fa-chalkboard-teacher mr-1"></i>
									<?php
										$number_of_lessons = $this->crud_model->get_lessons('course', $course['id'])->num_rows();
										echo $number_of_lessons.' '.get_phrase('chapters');
									 ?>
								</span>
								<span class="col text-center"><i class="fab fa-youtube mr-1"></i>
									<?php echo $this->crud_model->get_total_duration_of_lesson_by_course_id($course['id']); ?>
								</span>
								<span class="col text-center">
									<i class="fas fa-language mr-1"></i><?php echo ucfirst($course['language']); ?>
								</span>
								<span class="col text-center">
									<i class="fas fa-cubes mr-1"></i><?php echo ucfirst($course['level']); ?>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


    
<?php endforeach; ?>
</ul>
