<?php
$course_details = $this->crud_model->get_course_by_id($course_id)->row_array();
$instructor_details = $this->user_model->get_all_user($course_details['user_id'])->row_array();
?>
<section class="course-header-area">
	  <div class="container-lg">
		<div class="row align-items-end">
			<div class="col-lg-8">
				<div class="course-header-wrap">
				  <h1 class="title"><?php echo $course_details['title']; ?></h1>
					
					<div class="rating-row">
						<?php /* <span class="course-badge best-seller"><?php echo ucfirst($course_details['level']); ?></span> */ ?>
						<?php
						$total_rating =  $this->crud_model->get_ratings('course', $course_details['id'], true)->row()->rating;
						$number_of_ratings = $this->crud_model->get_ratings('course', $course_details['id'])->num_rows();
						if ($number_of_ratings > 0) {
						  $average_ceil_rating = ceil($total_rating / $number_of_ratings);
						}else {
						  $average_ceil_rating = 0;
						}

						for($i = 1; $i < 6; $i++):?>
						<?php if ($i <= $average_ceil_rating): ?>
						  <i class="fas fa-star filled" style="color: #f5c85b;"></i>
						<?php else: ?>
						  <i class="fas fa-star"></i>
						<?php endif; ?>
						<?php endfor; ?>
						
						<span class="d-inline-block average-rating"><?php echo $average_ceil_rating; ?></span><span>(<?php echo $number_of_ratings.' '.get_phrase('ratings'); ?>)</span>
						
						<span> | </span>
						
						<span class="enrolled-num">
						<?php
						$number_of_enrolments = $this->crud_model->enrol_history($course_details['id'])->num_rows();
						echo $number_of_enrolments.' '.get_phrase('students_enrolled');
						?>
						</span>
						
						<span> | </span>
						
						<span>
						<?php if ($course_details['video_url'] != ""): ?>
							<a data-toggle="modal" data-target="#CoursePreviewModal">
								<span class="preview-text"><i class="fas fa-play-circle playb faa-pulse animated fa-1x"></i><?php echo get_phrase('preview_this_course'); ?></span>
							</a>
						<?php endif; ?>
						</span>
						
						<span> | </span>
						
						<span>
							Level:&nbsp;<?php echo ucfirst($course_details['level']); ?>
						</span>
					</div>
					
					<p class="subtitle" style="min-height: 55px;"><?php echo $course_details['short_description']; ?></p>
					
					<div class="row created-row borderstr ml-1 mr-1">
						<div class="col-lg-5 col-12 pt-1">
							<div class="media">
								<a href="<?php echo site_url('home/instructor_page/'.$instructor_details['id']) ?>">
									<img src="<?php echo $this->user_model->get_user_image_url($instructor_details['id']); ?>" alt="" class="img-fluid img-thumbnail authorimg">
								</a>
								<p class="media-body pb-3 mb-0">
									<strong class="d-block"><a href="<?php echo site_url('home/instructor_page/'.$course_details['user_id']); ?>"><?php echo $instructor_details['first_name'].' '.$instructor_details['last_name']; ?></a></strong>
									<?php echo get_phrase('course_author')?>
								</p>
							</div>
						
						</div>
						<div class="col-lg-3 col-6">
							<div class="media pt-1">
								<i class="fa fa-language fa-3x mr-2"></i>
								<p class="media-body pb-3 mb-0">
									<strong class="d-block"><a>Language</a></strong>
									<?php echo ucfirst($course_details['language']); ?>
								</p>
							</div>
						
						</div>
						<div class="col-lg-4 col-6 pr-0">
							<div class="media pt-1">
								<i class="fas fa-calendar-alt fa-3x mr-2"></i>
								 <?php if ($course_details['last_modified'] > 0): ?>
								<p class="media-body pb-3 mb-0">
									<strong class="d-block"><?php echo get_phrase('last_updated')?></strong>
										<?php echo date('d-M-Y', $course_details['last_modified']); ?>
									<?php else: ?>	
								</p>
								
								<p class="media-body pb-3 mb-0">
									<strong class="d-block"><?php echo get_phrase('last_updated')?></strong>
										<?php echo date('D, d-M-Y', $course_details['date_added']); ?>
									<?php endif; ?>
								</p>
							
							</div>
						</div>
					  
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="course-sidebar-text-box bg-white mt-2 rounded10">
					<div class="price text-center">
					<?php if ($course_details['is_free_course'] == 1): ?>
					  <span class = "current-price"><span class="current-price"><?php echo get_phrase('free'); ?></span></span>
					<?php else: ?>
					  <?php if ($course_details['discount_flag'] == 1): ?>
						<span class="discount cross prc2 text-muted pt-2"><?php echo currency($course_details['price']) ?></span>
						<span class = "current-price"><span class="current-price"><?php echo currency($course_details['discounted_price']); ?></span></span>
						
						<input type="hidden" id = "total_price_of_checking_out" value="<?php echo currency($course_details['discounted_price']); ?>">
					  <?php else: ?>
						<span class = "current-price"><span class="current-price"><?php echo currency($course_details['price']); ?></span></span>
						<input type="hidden" id = "total_price_of_checking_out" value="<?php echo currency($course_details['price']); ?>">
					  <?php endif; ?>
					<?php endif; ?>
					</div>

					<?php if(is_purchased($course_details['id'])) :?>
					<div class="already_purchased">
					  <a href="<?php echo site_url('home/my_courses'); ?>"><?php echo get_phrase('already_purchased'); ?></a>
					</div>
					<?php else: ?>
					<?php if ($course_details['is_free_course'] == 1): ?>
					<div class="buy-btns">
					<?php if ($this->session->userdata('user_login') != 1): ?>
					  <a href = "#" class="btn btn-buy-now" onclick="handleEnrolledButton()"><?php echo get_phrase('get_enrolled'); ?></a>
					<?php else: ?>
					  <a href = "<?php echo site_url('home/get_enrolled_to_free_course/'.$course_details['id']); ?>" class="btn btn-buy-now"><?php echo get_phrase('get_enrolled'); ?></a>
					<?php endif; ?>
					</div>
					<?php else: ?>
					<div class="buy-btns">
					
					<a href = "javascript::" class="btn btn-buy-now" id = "course_<?php echo $course_details['id']; ?>" onclick="handleBuyNow(this)"><?php echo get_phrase('buy_now'); ?></a>
					<?php if (in_array($course_details['id'], $this->session->userdata('cart_items'))): ?>
					  <button class="btn btn-add-cart addedToCart" type="button" id = "<?php echo $course_details['id']; ?>" onclick="handleCartItems(this)"><?php echo get_phrase('added_to_cart'); ?></button>
					<?php else: ?>
					  <button class="btn btn-add-cart" type="button" id = "<?php echo $course_details['id']; ?>" onclick="handleCartItems(this)"><?php echo get_phrase('add_to_cart'); ?></button>
					<?php endif; ?>
					</div>
					<?php endif; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="course-content-area">
  <div class="container-lg">
    <div class="row">
      <div class="col-lg-8">

        
        <div class="course-curriculum-box mt-4">
          <div class="course-curriculum-title clearfix">
            <div class="title float-left"><?php echo get_phrase('course_contents'); ?><small class="ml-2">(Sections/Lessons)</small></div>
            <div class="float-right">
              <span class="total-lectures">
                <?php echo $this->crud_model->get_lessons('course', $course_details['id'])->num_rows(); ?>&nbsp;Lectures
              </span>
              <span class="total-time">
                <?php
                echo $this->crud_model->get_total_duration_of_lesson_by_course_id($course_details['id']);
                ?>
              </span>
            </div>
          </div>
          <div class="course-curriculum-accordion">
            <?php
            $sections = $this->crud_model->get_section('course', $course_id)->result_array();
            $counter = 0;
            foreach ($sections as $section): ?>
            <div class="lecture-group-wrapper">
              <div class="lecture-group-title clearfix" data-toggle="collapse" data-target="#collapse<?php echo $section['id']; ?>" aria-expanded="<?php if($counter == 0) echo 'true'; else echo 'false' ; ?>">
                <div class="title float-left">
                  <?php  echo $section['title']; ?>
                </div>
                <div class="float-right">
                  <span class="total-lectures">
                    <?php echo $this->crud_model->get_lessons('section', $section['id'])->num_rows().' '.get_phrase('lessons'); ?>
                  </span>
                  <span class="total-time">
                    <?php /* echo $this->crud_model->get_total_duration_of_lesson_by_section_id($section['id']); */ ?>
					
					<?php $lessons = $this->crud_model->get_lessons('section', $section['id'])->result_array();
					  foreach ($lessons as $lesson):?>
						<?php if ($lesson['lesson_type'] != 'other'):?>
							<i class="mr-2 fas fa-video"></i><?php echo $this->crud_model->get_total_duration_of_lesson_by_section_id($section['id']); ?>
						<?php else: ?>
							<?php 
								$tmp = explode('.', $lesson['attachment']);
								$fileExtension = strtolower(end($tmp));
							?>

							<?php if ($fileExtension == 'jpg' || $fileExtension == 'jpeg' || $fileExtension == 'png' || $fileExtension == 'bmp' || $fileExtension == 'svg'): ?>
								<i class="fas fa-camera-retro"></i>  <?php echo get_phrase('attachment'); ?>
							<?php elseif($fileExtension == 'pdf'): ?>
								<i class="far fa-file-pdf"></i>  <?php echo get_phrase('pdf'); ?>
							<?php elseif($fileExtension == 'doc' || $fileExtension == 'docx'): ?>
								<i class="far fa-file-word"></i>  <?php echo get_phrase('document'); ?>
							<?php elseif($fileExtension == 'txt'): ?>
								<i class="far fa-file-alt"></i>  <?php echo get_phrase('text'); ?>
							<?php else: ?>
								<i class="fa fa-file"></i>  <?php echo get_phrase('attachment'); ?>
							<?php endif; ?>
							
						<?php endif; ?>
					<?php endforeach; ?>
					
                  </span>
                </div>
              </div>

              <div id="collapse<?php echo $section['id']; ?>" class="lecture-list collapse <?php if($counter == 0) echo 'show'; ?>">
                <ul>
                  <?php $lessons = $this->crud_model->get_lessons('section', $section['id'])->result_array();
                  foreach ($lessons as $lesson):?>
                  <li class="lecture has-preview">
                    <span class="lecture-title"><?php echo $lesson['title']; ?></span>
                    <span class="lecture-time float-right">
						<?php if ($lesson['lesson_type'] != 'other'):?>
							<i class="mr-2 fas fa-video"></i><?php echo $lesson['duration']; ?>
						<?php else: ?>
							<i class="mr-2 fas fa-file-pdf"></i>
						<?php endif; ?>
					</span>
                    <!-- <span class="lecture-preview float-right" data-toggle="modal" data-target="#CoursePreviewModal">Preview</span> -->
					
                  </li>
                <?php endforeach; ?>
				
				
              </ul>
            </div>
          </div>
          <?php
          $counter++;
        endforeach; ?>
      </div>
    </div>
	
	<div class="what-you-get-box learn rounded10 bg-darkgrey">
          <div class="what-you-get-title"><?php echo get_phrase('what_you_will_learn'); ?></div>
          <ul class="what-you-get__items">
            <?php foreach (json_decode($course_details['outcomes']) as $outcome): ?>
              <?php if ($outcome != ""): ?>
                <li><?php echo $outcome; ?></li>
              <?php endif; ?>
            <?php endforeach; ?>
          </ul>
        </div>   
	
	
    <div class="description-box view-more-parent">
      <div class="view-more" onclick="viewMore(this,'hide')">+ <?php echo get_phrase('view_more'); ?></div>
      <div class="description-title"><?php echo get_phrase('course_description'); ?></div>
      <div class="description-content-wrap">
        <div class="description-content">
          <?php echo $course_details['description']; ?>
        </div>
      </div>
    </div>


    <div class="compare-box view-more-parent">
      <div class="view-more" onclick="viewMore(this)">+ <?php echo get_phrase('view_more'); ?></div>
      <div class="compare-title"><?php echo get_phrase('related_courses'); ?></div>
      <div class="compare-courses-wrap">
        <?php
        $other_realted_courses = $this->crud_model->get_courses($course_details['category_id'], $course_details['sub_category_id'])->result_array();
        foreach ($other_realted_courses as $other_realted_course):
          if($other_realted_course['id'] != $course_details['id'] && $other_realted_course['status'] == 'active'): ?>
		  
		  
          <div class="course-comparism-item-container this-course">
            <div class="course-comparism-item clearfix">
               <?php /*
			  <div class="item-image float-left">
                <a href="<?php echo site_url('home/course/'.slugify($other_realted_course['title']).'/'.$other_realted_course['id']); ?>"><img src="<?php $this->crud_model->get_course_thumbnail_url($other_realted_course['id']); ?>" alt="" class="img-fluid"></a>
                <div class="item-duration"><b><?php echo $this->crud_model->get_total_duration_of_lesson_by_course_id($other_realted_course['id']); ?></b></div>
              </div>
			  */ ?>
			  
              <div class="item-title first float-left pl-0">
                <div class="title"><a href="<?php echo site_url('home/course/'.slugify($other_realted_course['title']).'/'.$other_realted_course['id']); ?>"><?php echo $other_realted_course['title']; ?></a></div>
                <?php if ($other_realted_course['last_modified'] > 0): ?>
                <div class="updated-time">
					<?php echo get_phrase('updated').' '.date('D, d-M-Y', $other_realted_course['last_modified']); ?>
					<span> | </span>
					<i class="fab fa-youtube mr-1 ml-1"></i><?php echo $this->crud_model->get_total_duration_of_lesson_by_course_id($other_realted_course['id']); ?>
				</div>
                <?php else: ?>
                <div class="updated-time">
					<?php echo get_phrase('updated').' '.date('D, d-M-Y', $other_realted_course['date_added']); ?>
					<span> | </span>
					<i class="fab fa-youtube mr-1 ml-1"></i><?php echo $this->crud_model->get_total_duration_of_lesson_by_course_id($other_realted_course['id']); ?>
				</div>
                <?php endif; ?>
              </div>
              <div class="item-details second float-left">
                <span class="item-rating">
                  <i class="fas fa-star"></i>
                  <?php
                  $total_rating =  $this->crud_model->get_ratings('course', $other_realted_course['id'], true)->row()->rating;
                  $number_of_ratings = $this->crud_model->get_ratings('course', $other_realted_course['id'])->num_rows();
                  if ($number_of_ratings > 0) {
                    $average_ceil_rating = ceil($total_rating / $number_of_ratings);
                  }else {
                    $average_ceil_rating = 0;
                  }
                  ?>
                  <span class="d-inline-block average-rating"><?php echo $average_ceil_rating; ?></span>
                </span>
                <span class="enrolled-student">
                  <i class="far fa-user"></i>
                  <?php echo $this->crud_model->enrol_history($other_realted_course['id'])->num_rows(); ?>
                </span>
                <?php if ($other_realted_course['is_free_course'] == 1): ?>
                  <span class="item-price">
                    <span class="current-price"><?php echo get_phrase('free'); ?></span>
                  </span>
                <?php else: ?>
                  <?php if ($other_realted_course['discount_flag'] == 1): ?>
                    <span class="item-price">
                      <span class="original-price"><?php echo currency($other_realted_course['price']); ?></span>
                      <span class="current-price"><?php echo currency($other_realted_course['discounted_price']); ?></span>
                    </span>
                  <?php else: ?>
                    <span class="item-price">
                      <span class="current-price"><?php echo currency($other_realted_course['price']); ?></span>
                    </span>
                  <?php endif; ?>
                <?php endif; ?>
              </div>
            </div>
          </div>
		  
        <?php endif; ?>
      <?php endforeach; ?>
    </div>
	 
	
  </div>

  <div class="about-instructor-box">
    <div class="about-instructor-title">
      <?php echo get_phrase('about_the_instructor'); ?>
    </div>
    <div class="row">
      <div class="col-lg-5">
		<div class="row">
			<div class="col-md-5 text-center mb-2">
				<div class="about-instructor-image2">
					<a href="<?php echo site_url('home/instructor_page/'.$instructor_details['id']) ?>">
						<img src="<?php echo $this->user_model->get_user_image_url($instructor_details['id']); ?>" alt="" class="img-fluid">
					</a>
				  
				</div>
			</div>
			<div class="col-md-7 mb-2">
				<div class="ratingbox2 text-center text-white">
				<div class="average-rating">
				  <div class="num" style="font-size: 51px;font-weight: 500;line-height: 1;">
					<?php
					$total_rating =  $this->crud_model->get_ratings('course', $course_details['id'], true)->row()->rating;
					$number_of_ratings = $this->crud_model->get_ratings('course', $course_details['id'])->num_rows();
					if ($number_of_ratings > 0) {
					  $average_ceil_rating = ceil($total_rating / $number_of_ratings);
					}else {
					  $average_ceil_rating = 0;
					}
					echo $average_ceil_rating;
					?>
				  </div>
				  <div class="rating" style="font-size: 20px;">
					<?php
					for($i = 1; $i < 6; $i++):?>
					<?php if ($i <= $average_ceil_rating): ?>
					  <i class="fas fa-star filled" style="color: #f5c85b;"></i>
					<?php else: ?>
					  <i class="fas fa-star" style="color: #fff;"></i>
					<?php endif; ?>
				  <?php endfor; ?>
				</div>
				<div class="title"><?php echo get_phrase('average_rating'); ?></div>
			  </div>
			  </div>

			</div>
		
		</div>
		
		
		
		
      </div>
      <div class="col-lg-7 mb-2">
        <div class="about-instructor-details">
          <div class="instructor-name">
            <a href="<?php echo site_url('home/instructor_page/'.$course_details['user_id']); ?>"><?php echo $instructor_details['first_name'].' '.$instructor_details['last_name']; ?></a>
          </div>
          <div class="instructor-title row mt-4">
            <?php echo $instructor_details['title']; ?>
			
            <!-- <li><i class="fas fa-star"></i><b>4.4</b> Average Rating</li> -->
				<div class="col">
					<i class="fas fa-star-half-alt mr-2"></i>
					<b class="mr-2"><?php echo $this->crud_model->get_instructor_wise_course_ratings($instructor_details['id'], 'course')->num_rows(); ?></b>
					<?php echo get_phrase('reviews'); ?>
				</div>
				<div class="col">
					<i class="fas fa-users mr-2"></i><b class="mr-2">
						  <?php
						  $course_ids = $this->crud_model->get_instructor_wise_courses($instructor_details['id'], 'simple_array');
						  $this->db->select('user_id');
						  $this->db->distinct();
						  $this->db->where_in('course_id', $course_ids);
						  echo $this->db->get('enrol')->num_rows();
						  ?>
						</b><?php echo get_phrase('students') ?>
				</div class="col">
				<div class="col">
					<i class="fas fa-chalkboard-teacher mr-2"></i><b class="mr-2">
					<?php echo $this->crud_model->get_instructor_wise_courses($instructor_details['id'])->num_rows(); ?></b><?php echo get_phrase('courses'); ?>
				</div>
				
			
          </div>
          <div class="instructor-bio mt-4">
            <a class="btn btn-block btn-sm" href="<?php echo site_url('home/instructor_page/'.$course_details['user_id']); ?>"><?php echo get_phrase('view_author_biography'); ?></a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="student-feedback-box">
    <div class="student-feedback-title">
      <?php echo get_phrase('student_reviews'); ?>
    </div>
	<?php /*
    <div class="row ratingbox">
      <div class="col-lg-3">
        <div class="average-rating">
          <div class="num">
            <?php
            $total_rating =  $this->crud_model->get_ratings('course', $course_details['id'], true)->row()->rating;
            $number_of_ratings = $this->crud_model->get_ratings('course', $course_details['id'])->num_rows();
            if ($number_of_ratings > 0) {
              $average_ceil_rating = ceil($total_rating / $number_of_ratings);
            }else {
              $average_ceil_rating = 0;
            }
            echo $average_ceil_rating;
            ?>
          </div>
          <div class="rating">
            <?php
            for($i = 1; $i < 6; $i++):?>
            <?php if ($i <= $average_ceil_rating): ?>
              <i class="fas fa-star filled" style="color: #f5c85b;"></i>
            <?php else: ?>
              <i class="fas fa-star" style="color: #abb0bb;"></i>
            <?php endif; ?>
          <?php endfor; ?>
        </div>
        <div class="title"><?php echo get_phrase('average_rating'); ?></div>
      </div>
    </div>
    <div class="col-lg-9">
      <div class="individual-rating">
        <ul>
          <?php for($i = 1; $i <= 5; $i++): ?>
            <li>
              <div class="progress">
                <div class="progress-bar" style="width: <?php echo $this->crud_model->get_percentage_of_specific_rating($i, 'course', $course_id); ?>%"></div>
              </div>
              <div>
                <span class="rating">
                  <?php for($j = 1; $j <= (5-$i); $j++): ?>
                    <i class="fas fa-star"></i>
                  <?php endfor; ?>
                  <?php for($j = 1; $j <= $i; $j++): ?>
                    <i class="fas fa-star filled"></i>
                  <?php endfor; ?>

                </span>
                <span><?php echo $this->crud_model->get_percentage_of_specific_rating($i, 'course', $course_id); ?>%</span>
              </div>
            </li>
          <?php endfor; ?>
        </ul>
      </div>
    </div>
  </div>
  */ ?>
  <div class="reviews">
    <?php /* <div class="reviews-title"><?php echo get_phrase('reviews'); ?></div> */?>
    <ul>
      <?php
      $ratings = $this->crud_model->get_ratings('course', $course_id)->result_array();
	  
	  foreach($ratings as $rating):
        ?>
        <li>
          <div class="row">
            <div class="col-lg-4">
              <div class="reviewer-details clearfix">
                <div class="reviewer-img float-left">
                  <img src="<?php echo $this->user_model->get_user_image_url($rating['user_id']); ?>" alt="">
                </div>
                <div class="review-time">
                  <div class="time">
                    <?php echo date('D, d-M-Y', $rating['date_added']); ?>
                  </div>
                  <div class="reviewer-name">
                    <?php
                    $user_details = $this->user_model->get_user($rating['user_id'])->row_array();
                    echo $user_details['first_name'].' '.$user_details['last_name'];
                    ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-8">
              <div class="review-details">
                <div class="rating">
                  <?php
                  for($i = 1; $i < 6; $i++):?>
                  <?php if ($i <= $rating['rating']): ?>
                    <i class="fas fa-star filled" style="color: #f5c85b;"></i>
                  <?php else: ?>
                    <i class="fas fa-star" style="color: #abb0bb;"></i>
                  <?php endif; ?>
                <?php endfor; ?>
              </div>
              <div class="review-text">
                <?php echo $rating['review']; ?>
              </div>
            </div>
          </div>
        </div>
      </li>
    <?php endforeach; ?>
  </ul>
</div>
</div>
</div>
<div class="col-lg-4">
<?php /*
  <div class="">
    
    <div class="course-sidebar-text-box">
      <div class="price text-center">
        <?php if ($course_details['is_free_course'] == 1): ?>
          <span class = "current-price"><span class="current-price"><?php echo get_phrase('free'); ?></span></span>
        <?php else: ?>
          <?php if ($course_details['discount_flag'] == 1): ?>
			<span class="discount cross prc2"><?php echo currency($course_details['price']) ?></span>
            <span class = "current-price"><span class="current-price"><?php echo currency($course_details['discounted_price']); ?></span></span>
            
            <input type="hidden" id = "total_price_of_checking_out" value="<?php echo currency($course_details['discounted_price']); ?>">
          <?php else: ?>
            <span class = "current-price"><span class="current-price"><?php echo currency($course_details['price']); ?></span></span>
            <input type="hidden" id = "total_price_of_checking_out" value="<?php echo currency($course_details['price']); ?>">
          <?php endif; ?>
        <?php endif; ?>
      </div>

      <?php if(is_purchased($course_details['id'])) :?>
        <div class="already_purchased">
          <a href="<?php echo site_url('home/my_courses'); ?>"><?php echo get_phrase('already_purchased'); ?></a>
        </div>
      <?php else: ?>
        <?php if ($course_details['is_free_course'] == 1): ?>
          <div class="buy-btns">
            <?php if ($this->session->userdata('user_login') != 1): ?>
              <a href = "#" class="btn btn-buy-now" onclick="handleEnrolledButton()"><?php echo get_phrase('get_enrolled'); ?></a>
            <?php else: ?>
              <a href = "<?php echo site_url('home/get_enrolled_to_free_course/'.$course_details['id']); ?>" class="btn btn-buy-now"><?php echo get_phrase('get_enrolled'); ?></a>
            <?php endif; ?>
          </div>
        <?php else: ?>
          <div class="buy-btns">
            <a href = "<?php echo site_url('home/shopping_cart'); ?>" class="btn btn-buy-now" id = "course_<?php echo $course_details['id']; ?>" onclick="handleBuyNow(this)"><?php echo get_phrase('buy_now'); ?></a>
            <?php if (in_array($course_details['id'], $this->session->userdata('cart_items'))): ?>
              <button class="btn btn-add-cart addedToCart" type="button" id = "<?php echo $course_details['id']; ?>" onclick="handleCartItems(this)"><?php echo get_phrase('added_to_cart'); ?></button>
            <?php else: ?>
              <button class="btn btn-add-cart" type="button" id = "<?php echo $course_details['id']; ?>" onclick="handleCartItems(this)"><?php echo get_phrase('add_to_cart'); ?></button>
            <?php endif; ?>
          </div>
        <?php endif; ?>
      <?php endif; ?>
    </div>
  </div>
  
*/ ?>
  
  <div class="what-you-get-box sidebar rounded10">
	<?php if ($course_details['video_url'] != ""): ?>
      <div class="preview-video-box">
        <a data-toggle="modal" data-target="#CoursePreviewModal">
          <img src="<?php echo $this->crud_model->get_course_thumbnail_url($course_details['id']); ?>" alt="" class="w-100 img-fluid">
          <span class="preview-text"><?php echo get_phrase('preview_this_course'); ?></span>
          <span class="play-btn"></span>
        </a>
      </div>
    <?php endif; ?>
	</div>
	
	<div class="course-sidebar-text-box side rounded10 mt-3">	
		<div class="includes">
			<div class="title"><b><?php echo get_phrase('course_includes'); ?>:</b></div>
			<ul>
				<li><i class="fab fa-youtube mr-1"></i>
					<?php
						echo $this->crud_model->get_total_duration_of_lesson_by_course_id($course_details['id']).' '.get_phrase('on_demand_videos');
					 ?>
				</li>
				<li><i class="fas fa-chalkboard-teacher mr-1"></i><?php echo $this->crud_model->get_lessons('course', $course_details['id'])->num_rows().' '.get_phrase('lessons'); ?></li>
				<li><i class="far fa-thumbs-up mr-1"></i><?php echo get_phrase('full_lifetime_access'); ?></li>
				<!--<li><i class="fas fa-mobile-alt mr-1"></i><?php /* echo get_phrase('access_on_mobile_and_tv'); */ ?></li>-->
			</ul>
		</div>
	</div>
	
	<div class="course-sidebar-text-box side rounded10 mt-3">
		<div class="includes">
			<div class="title"><b><?php echo get_phrase('requirements'); ?></b></div>
			  <ul class="requirements__list">
				  <?php foreach (json_decode($course_details['requirements']) as $requirement): ?>
					<?php if ($requirement != ""): ?>
					  <li><i class="iconbuttonsuffix mr-1"></i><?php echo $requirement; ?></li>
					<?php endif; ?>
				  <?php endforeach; ?>
				</ul>
	  </div>
    </div>
	
	<div class="course-sidebar-text-box side rounded10 mt-3 bg-darkgrey">	
		<div class="promo">
			<ul class="list-group list-group-flush">
				<li class="list-group-item bg-darkgrey">
					<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fas fa-desktop"></i></div>
					<div>
						<p class="txt-purple font-weight-bold mb-0">
						<?php 
							$status_wise_courses = $this->crud_model->get_status_wise_courses();
							$number_of_courses = $status_wise_courses['active']->num_rows();
							echo get_phrase('online_courses'); 
						?>
						</p>
						
						<p><?php echo $number_of_courses.' '.get_phrase('topics_to_learn_from'); ?></p>
					</div>
				</li>
				<li class="list-group-item bg-darkgrey">
					<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fa fa-key"></i></div>
					<div>
						<p class="txt-purple font-weight-bold mb-0"><?php echo get_phrase('lifetime_access'); ?></p>
						<p><?php echo get_phrase('learn_as_per_your_convenience'); ?></p>
					</div>
				</li>
				<li class="list-group-item bg-darkgrey">
					<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fas fa-chalkboard-teacher"></i></div>
					<div>
						<p class="txt-purple font-weight-bold mb-0"><?php echo get_phrase('inspire_students'); ?></p>
						<p><?php echo get_phrase('help_people_learn_new_skills'); ?></p>
					</div>
				</li>
				<li class="list-group-item bg-orange">
					<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fas fa-user-graduate"></i></div>
					<div>
						<p class="txt-purple font-weight-bold mb-0"><?php echo get_phrase('course_completion_acknowledgement'); ?></p>
						<p><?php echo get_phrase('get_proof_of_course_completion'); ?></p>
					</div>
				</li>
				
			</ul>
		</div>
		
	</div>
	
<?php /*
	<div class="course-sidebar-text-box side mt-3">
		<div class="includes">
			<div class="title"><b>Courses By Me</b></div>
				<ul class="requirements__list">
					<?php
						$author_courses = $this->crud_model->get_instructor_wise_courses($instructor_details['id'], 'course')->result_array();
						foreach ($author_courses as $author_courses):
						  if($author_courses['id'] != $course_details['id'] && $author_courses['status'] == 'active'): 
					?>
						<li><i class="iconbuttonsuffix mr-1"></i><a href="<?php echo site_url('home/course/'.slugify($author_courses['title']).'/'.$author_courses['id']); ?>"><?php echo $author_courses['title']; ?></a></li>
					<?php endif; ?>
				  <?php endforeach; ?>
				</ul>
		</div>
	</div>
*/?>
  
</div>
</div>
</div>
</section>

<?php if ($this->session->userdata('user_login') == "1") : ?>

<?php else : ?>
<section class="footer-top-widget-area testimon purple mt-3">
	<div class="container-xl">
		<div class="row">
			<div class="col-md-12  text-center">
				<h2 class="mb-4 instructor text-light">Learn from experts all over the world or inspire students and teach them new skills.</h2>
					<a class="btn btn-sign-in whitebck rounded25 px-5" style="color:#343a40!important;" href="<?php echo site_url('home/sign_up'); ?>">Sign Up Now !</a>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>

<!-- Modal -->
<?php if ($course_details['video_url'] != ""):
  $provider = "";
  $video_details = array();
  if ($course_details['course_overview_provider'] == "html5" || $course_details['course_overview_provider'] == "upload") {
    $provider = 'html5';
  }else {
    $video_details = $this->video_model->getVideoDetails($course_details['video_url']);
    $provider = $video_details['provider'];
  }
  ?>
  <div class="modal fade" id="CoursePreviewModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content course-preview-modal">
        <div class="modal-header">
          <h5 class="modal-title"><span><?php echo get_phrase('course_preview') ?>:</span><?php echo $course_details['title']; ?></h5>
          <button type="button" class="close" data-dismiss="modal" onclick="pausePreview()">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="course-preview-video-wrap">
            <div class="embed-responsive embed-responsive-16by9">
              <?php if (strtolower(strtolower($provider)) == 'youtube'): ?>
                <!------------- PLYR.IO ------------>
                <link rel="stylesheet" href="<?php echo base_url();?>assets/global/plyr/plyr.css">

                <div class="plyr__video-embed" id="player">
                  <iframe height="500" src="<?php echo $course_details['video_url'];?>?origin=https://plyr.io&amp;iv_load_policy=3&amp;modestbranding=1&amp;playsinline=1&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1" allowfullscreen allowtransparency allow="autoplay"></iframe>
                </div>

                <script src="<?php echo base_url();?>assets/global/plyr/plyr.js"></script>
                <script>const player = new Plyr('#player');</script>
                <!------------- PLYR.IO ------------>
              <?php elseif (strtolower($provider) == 'vimeo'): ?>
                <!------------- PLYR.IO ------------>
                <link rel="stylesheet" href="<?php echo base_url();?>assets/global/plyr/plyr.css">
                <div class="plyr__video-embed" id="player">
                  <iframe height="500" src="https://player.vimeo.com/video/<?php echo $video_details['video_id']; ?>?loop=false&amp;byline=false&amp;portrait=false&amp;title=false&amp;speed=true&amp;transparent=0&amp;gesture=media" allowfullscreen allowtransparency allow="autoplay"></iframe>
                </div>

                <script src="<?php echo base_url();?>assets/global/plyr/plyr.js"></script>
                <script>const player = new Plyr('#player');</script>
                <!------------- PLYR.IO ------------>
              <?php else :?>
                <!------------- PLYR.IO ------------>
                <link rel="stylesheet" href="<?php echo base_url();?>assets/global/plyr/plyr.css">
                <video poster="<?php echo $this->crud_model->get_course_thumbnail_url($course_details['id']);?>" id="player" playsinline controls>
                  <?php if (get_video_extension($course_details['video_url']) == 'mp4'): ?>
                    <source src="<?php echo $course_details['video_url']; ?>" type="video/mp4">
                    <?php elseif (get_video_extension($course_details['video_url']) == 'webm'): ?>
                      <source src="<?php echo $course_details['video_url']; ?>" type="video/webm">
                      <?php else: ?>
                        <h4><?php get_phrase('video_url_is_not_supported'); ?></h4>
                      <?php endif; ?>
                    </video>

                    <style media="screen">
                    .plyr__video-wrapper {
                      height: 450px;
                    }
                    </style>

                    <script src="<?php echo base_url();?>assets/global/plyr/plyr.js"></script>
                    <script>const player = new Plyr('#player');</script>
                    <!------------- PLYR.IO ------------>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php endif; ?>
    <!-- Modal -->

    <style media="screen">
    .embed-responsive-16by9::before {
      padding-top : 0px;
    }
    </style>
    <script type="text/javascript">
    function handleCartItems(elem) {
      url1 = '<?php echo site_url('home/handleCartItems');?>';
      url2 = '<?php echo site_url('home/refreshWishList');?>';
      $.ajax({
        url: url1,
        type : 'POST',
        data : {course_id : elem.id},
        success: function(response)
        {
          $('#cart_items').html(response);
          if ($(elem).hasClass('addedToCart')) {
            $(elem).removeClass('addedToCart')
            $(elem).text("<?php echo get_phrase('add_to_cart'); ?>");
          }else {
            $(elem).addClass('addedToCart')
            $(elem).text("<?php echo get_phrase('added_to_cart'); ?>");
          }
          $.ajax({
            url: url2,
            type : 'POST',
            success: function(response)
            {
              $('#wishlist_items').html(response);
            }
          });
        }
      });
    }

    function handleBuyNow(elem) {

      url1 = '<?php echo site_url('home/handleCartItemForBuyNowButton');?>';
      url2 = '<?php echo site_url('home/refreshWishList');?>';
	  urlToRedirect = '<?php echo site_url('home/shopping_cart'); ?>';
      var explodedArray = elem.id.split("_");
      var course_id = explodedArray[1];

      $.ajax({
        url: url1,
        type : 'POST',
        data : {course_id : course_id},
        success: function(response)
        {
          $('#cart_items').html(response);
          $.ajax({
            url: url2,
            type : 'POST',
            success: function(response)
            {
              $('#wishlist_items').html(response);
			  toastr.warning('<?php echo get_phrase('please_wait').'....'; ?>');
              setTimeout(
              function()
              {
                window.location.replace(urlToRedirect);
              }, 1500);
            }
          });
        }
      });
    }

    function handleEnrolledButton() {
      console.log('here');
      $.ajax({
        url: '<?php echo site_url('home/isLoggedIn');?>',
        success: function(response)
        {
          if (!response) {
            window.location.replace("<?php echo site_url('login'); ?>");
          }
        }
      });
    }

    function pausePreview() {
      player.pause();
    }
    </script>
