<section class="page-header-area my-course-area">
    <div class="container-lg">
        <div class="row">
            <div class="col">
                <h1 class="page-title"><i class="fas fa-hand-holding-heart mr-2"></i><?php echo get_phrase('my_wishlist'); ?></h1>
                <ul>
					<li class=""><a href="<?php echo site_url('home/my_courses'); ?>"><i class="fas fa-book-reader mr-1"></i><?php echo get_phrase('subscribed_courses'); ?></a></li>
					<li class="active"><a href="<?php echo site_url('home/my_wishlist'); ?>"><i class="fas fa-hand-holding-heart mr-1"></i><?php echo get_phrase('my_wishlists'); ?></a></li>
					<?php /* <li><a href="<?php echo site_url('home/my_messages'); ?>"><?php echo get_phrase('my_messages'); ?></a></li> */ ?>
					<li><a href="<?php echo site_url('home/purchase_history'); ?>"><i class="fas fa-cart-plus mr-1"></i><?php echo get_phrase('purchase_history'); ?></a></li>
					<?php /* <li><a href="<?php echo site_url('home/profile/user_profile'); ?>"><?php echo get_phrase('user_profile'); ?></a></li> */ ?>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user-edit mr-1"></i><?php echo get_phrase('my_profile'); ?></a>
						<div class="dropdown-menu profile">
							<a class="dropdown-item px-3 py-2" href="<?php echo site_url('home/profile/user_profile'); ?>">
								<i class="fas fa-user-edit mr-1"></i><?php echo get_phrase('edit_profile'); ?>
							</a>
							<a class="dropdown-item px-3 py-2" href="<?php echo site_url('home/profile/user_credentials'); ?>">
								<i class="fas fa-key mr-1"></i><?php echo get_phrase('change_password'); ?>
							</a>
							<a class="dropdown-item px-3 py-2" href="<?php echo site_url('home/profile/user_photo'); ?>">
								<i class="far fa-image mr-1"></i><?php echo get_phrase('edit_photo'); ?>
							</a>
						</div>
					</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="my-courses-area">
    <div class="container-lg">
        <div class="row">
            <div class="col">
                <div class="my-course-search-bar">
                    <form action="">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="<?php echo get_phrase('search_my_wishlist'); ?>" onkeyup="getMyWishListsBySearchString(this.value)">
                            <div class="input-group-append menusearch1" style="position: absolute;right: 6px;top: 50%;transform: translateY(-50%);z-index: 3;">
                                <button class="btn" type="button"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
		
		<div class="row">
			<div class="col-md-9">
				<div class="row no-gutters" id = "my_wishlists_area">
					<?php foreach ($my_courses as $my_course):
						  $instructor_details = $this->user_model->get_all_user($my_course['user_id'])->row_array();?>
						<div class="col-lg-3">
							<div class="course-box-wrap">
								<div class="course-box">
									<div class="course-badge position best-seller price instrctr">
										<p class="intructor-badge">
											<i class="fas fa-chalkboard-teacher mr-1"> :</i>
											
											<a href="<?php echo site_url('home/instructor_page/'.$instructor_details['id']); ?>">
												<img src="<?php echo $this->user_model->get_user_image_url($instructor_details['id']);?>" class="imgcirclesmall" alt="">
												<?php echo $instructor_details['first_name'].' '.$instructor_details['last_name']; ?>
											</a>
										</p>
									</div>
									
									<div class="course-image">
										<a href="<?php echo site_url('home/course/'.slugify($my_course['title']).'/'.$my_course['id']); ?>">
											<img src="<?php echo $this->crud_model->get_course_thumbnail_url($my_course['id']); ?>" alt="" class="img-fluid">
										</a>
										<div class="instructor-img-hover">
											
											
										</div>
										
									</div>
									<div class="course-details">
										<a href="<?php echo site_url('home/course/'.slugify($my_course['title']).'/'.$my_course['id']); ?>">
											<h5 class="title"><?php echo $my_course['title']; ?></h5>
										</a>
										<div>
										<?php if ($my_course['is_free_course'] == 1):
											if($this->session->userdata('user_login') != 1) {
												$url = "#";
											}else {
												$url = site_url('home/get_enrolled_to_free_course/'.$my_course['id']);
											}?>
											<a href="<?php echo $url; ?>" class="btn add-to-cart-btn btn-block big-cart-button" onclick="handleEnrolledButton()"><i class="fas fa-shopping-cart"></i><?php echo get_phrase('get_enrolled'); ?></a>
										<?php else: ?>
											<button type="button" class="btn add-to-cart-btn btn-block <?php if(in_array($my_course['id'], $cart_items)) echo 'addedToCart'; ?> big-cart-button-<?php echo $my_course['id'];?>" id = "<?php echo $my_course['id']; ?>" onclick="handleCartItems(this)">
												<i class="fas fa-shopping-cart"></i><?php
												if(in_array($my_course['id'], $cart_items))
												echo get_phrase('added_to_cart');
												else
												echo get_phrase('add_to_cart');
												?>
											</button>
										<?php endif; ?>
									</div>

										
									</div>
									<div class="ratenow mycourses">
										<div>
											<span class="lsnhrswish float-left">
												<span>
													<?php
														$lessons = $this->crud_model->get_lessons('course', $my_course['id'])->num_rows();
														echo $lessons.' '.get_phrase('chapters');
													?>
													(<?php echo $this->crud_model->get_total_duration_of_lesson_by_course_id($my_course['id']);?>)
												</span>
												
											</span>
											<span class="lsnhrswish big">
												<?php if ($my_course['is_free_course'] == 1): ?>
													<p class="price text-right"><?php echo get_phrase('free'); ?></p>
												<?php else: ?>
													<?php if ($my_course['discount_flag'] == 1): ?>
														<p class="price text-right">
															<small class="cross"><?php echo currency($my_course['price']); ?></small>
															<?php echo currency($my_course['discounted_price']); ?>
														</p>
													<?php else: ?>
														<p class="price text-right"><?php echo currency($my_course['price']); ?></p>
													<?php endif; ?>
												<?php endif; ?>
											</span>
										</div>
									</div>
									
									
									
									<div class="wishlist-add wishlisted bottom" style="background: #f7a823;padding: 5px;border: 1px solid #f7a823;border-radius: 3px;">
										<button type="button" data-toggle="tooltip" data-placement="left" title="" style="cursor : pointer;" onclick="handleWishList(this)" id = "<?php echo $my_course['id']; ?>">
											<i class="fas fa-trash-alt" style="-webkit-text-stroke: 0px #fff;text-stroke: 0px #fff;"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="col-md-3">
				<div class="course-sidebar-text-box side mt-4 bg-orange">	
					<div class="promo">
						<ul class="list-group list-group-flush">
							<li class="list-group-item bg-orange">
								<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fas fa-desktop"></i></div>
								<div>
									<p class="txt-purple font-weight-bold mb-0">
									<?php 
										$status_wise_courses = $this->crud_model->get_status_wise_courses();
										$number_of_courses = $status_wise_courses['active']->num_rows();
										echo get_phrase('online_courses'); 
									?>
									</p>
									
									<p><?php echo $number_of_courses.' '.get_phrase('topics_to_learn_from'); ?></p>
								</div>
							</li>
							<li class="list-group-item bg-orange">
								<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fa fa-key"></i></div>
								<div>
									<p class="txt-purple font-weight-bold mb-0"><?php echo get_phrase('lifetime_access'); ?></p>
									<p><?php echo get_phrase('learn_as_per_your_convenience'); ?></p>
								</div>
							</li>
							<li class="list-group-item bg-orange">
								<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fas fa-chalkboard-teacher"></i></div>
								<div>
									<p class="txt-purple font-weight-bold mb-0"><?php echo get_phrase('inspire_students'); ?></p>
									<p><?php echo get_phrase('help_people_learn_new_skills'); ?></p>
								</div>
							</li>
							<li class="list-group-item bg-orange">
								<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fas fa-user-graduate"></i></div>
								<div>
									<p class="txt-purple font-weight-bold mb-0"><?php echo get_phrase('course_completion_acknowledgement'); ?></p>
									<p><?php echo get_phrase('get_proof_of_course_completion'); ?></p>
								</div>
							</li>
							
						</ul>
					</div>
						
				</div>
			</div>
		</div>
        
    </div>
</section>

<script type="text/javascript">
    function getMyWishListsBySearchString(search_string) {
        $.ajax({
            type : 'POST',
            url : '<?php echo site_url('home/get_my_wishlists_by_search_string'); ?>',
            data : {search_string : search_string},
            success : function (response) {
                $('#my_wishlists_area').html(response);
            }
        });
    }

    function handleWishList(elem) {

        $.ajax({
            url: '<?php echo site_url('home/handleWishList');?>',
            type : 'POST',
            data : {course_id : elem.id},
            success: function(response)
            {
              if ($(elem).hasClass('active')) {
                  $(elem).removeClass('active')
              }else {
                  $(elem).addClass('active')
              }
              $('#wishlist_items').html(response);
              $.ajax({
                  url: '<?php echo site_url('home/reload_my_wishlists');?>',
                  type : 'POST',
                  success: function(response)
                  {
                      $('#my_wishlists_area').html(response);
                  }
              });
            }
        });
    }
	
	function handleCartItems(elem) {
    url1 = '<?php echo site_url('home/handleCartItems');?>';
    url2 = '<?php echo site_url('home/refreshWishList');?>';
    $.ajax({
        url: url1,
        type : 'POST',
        data : {course_id : elem.id},
        success: function(response)
        {
            $('#cart_items').html(response);
            if ($(elem).hasClass('addedToCart')) {
                $('.big-cart-button-'+elem.id).removeClass('addedToCart')
                $('.big-cart-button-'+elem.id).text("<?php echo get_phrase('add_to_cart'); ?>");
            }else {
                $('.big-cart-button-'+elem.id).addClass('addedToCart')
                $('.big-cart-button-'+elem.id).text("<?php echo get_phrase('added_to_cart'); ?>");
            }
            $.ajax({
                url: url2,
                type : 'POST',
                success: function(response)
                {
                    $('#wishlist_items').html(response);
                }
            });
        }
    });
}
</script>
