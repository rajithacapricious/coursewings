<div class="col-lg-8">

    <div class="in-cart-box">
        <div class="title"><?php echo sizeof($this->session->userdata('cart_items')).' '.get_phrase('courses_in_cart'); ?></div>
        <div class="">
            <ul class="cart-course-list">
                <?php
                    $actual_price = 0;
                    $total_price  = 0;
                    foreach ($this->session->userdata('cart_items') as $cart_item):
                    $course_details = $this->crud_model->get_course_by_id($cart_item)->row_array();
                    $instructor_details = $this->user_model->get_all_user($course_details['user_id'])->row_array();
                    ?>
                    <li>
						<div class="cart-course-wrapper">
							<div class="image">
								<a href="<?php echo site_url('home/course/'.slugify($course_details['title']).'/'.$course_details['id']); ?>">
									<img src="<?php echo $this->crud_model->get_course_thumbnail_url($cart_item);?>" alt="" class="img-fluid img-thumbnail">
								</a>
							</div>
							<div class="details">
								<a href="<?php echo site_url('home/course/'.slugify($course_details['title']).'/'.$course_details['id']); ?>">
									<div class="name"><?php echo $course_details['title']; ?></div>
								</a>
								<a href="<?php echo site_url('home/instructor_page/'.$instructor_details['id']); ?>">
									<div class="media pt-2">
										<img src="<?php echo $this->user_model->get_user_image_url($instructor_details['id']); ?>" alt="" class="img-fluid img-thumbnail authorimg">
										<p class="media-body pb-3 mb-0">
											<strong class="d-block"><?php echo $instructor_details['first_name'].' '.$instructor_details['last_name']; ?></strong><?php echo get_phrase('course_author'); ?>
										</p>
									</div>
								</a>
							</div>
							<div class="move-remove">
								<div class="btn btn-sm btn-danger rmv mt-2" style="color: #fff !important;" id = "<?php echo $course_details['id']; ?>" onclick="removeFromCartList(this)"><i class="fas fa-trash-alt mr-1"></i><?php echo get_phrase('remove'); ?></div>
							</div>
							<div class="price">
								<a href="">
									<?php if ($course_details['discount_flag'] == 1): ?>
										<span class="cross">
											<?php
											$actual_price += $course_details['price'];
											echo currency($course_details['price']);
											?>
										</span>
										<span class="current-price">
											<?php
											$total_price += $course_details['discounted_price'];
											echo currency($course_details['discounted_price']);
											?>
										</span>
										
									<?php else: ?>
										<span class="current-price">
											<?php
											$actual_price += $course_details['price'];
											$total_price  += $course_details['price'];
											echo currency($course_details['price']);
											?>
										</span>
									<?php endif; ?>
									<span class="coupon-tag">
										<i class="fas fa-shopping-basket"></i>
									</span>
								</a>
							</div>
						</div>
					</li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>

</div>
<div class="col-lg-4">
    <div class="cart-sidebar">
		<div class="total bg-yellow px-2 py-2 mb-2"><?php echo get_phrase('total_cart'); ?>:</div>
        <span id = "total_price_of_checking_out" hidden><?php echo $total_price; ?></span>
         <div class="total-price text-center">
			 <?php if (($actual_price - $total_price) == 0): ?>
				
			 <?php else: ?>
				<span class="cross mr-2">
					<?php echo currency($actual_price); ?>
				</span>
			 <?php endif; ?>
			
				<?php echo currency($total_price); ?>
		</div>
					
		<?php if (($actual_price - $total_price) == 0): ?>
				
		<?php else: ?>
			<div class="total-original-price text-center">
				<!-- <span class="discount-rate">95% off</span> -->
				<span class="badge badge-pill bg-purple text-white discount-rate ml-2">
					<b class="mr-2"><?php echo get_phrase('you_have_saved'); ?>:</b><?php echo currency(($actual_price - $total_price)); ?>
				</span>
			</div>
		<?php endif; ?>
		<button type="button" class="btn btn-primary btn-block checkout-btn" onclick="handleCheckOut()"><?php echo get_phrase('checkout'); ?></button>
    </div>
	<div class="course-sidebar-text-box side mt-3 bg-orange">	
		<div class="promo">
			<ul class="list-group list-group-flush">
				<li class="list-group-item bg-orange">
					<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fa fa-key"></i></div>
					<div>
						<p class="txt-purple font-weight-bold mb-0"><?php echo get_phrase('lifetime_access'); ?></p>
						<p><?php echo get_phrase('learn_as_per_your_convenience'); ?></p>
					</div>
				</li>
				<li class="list-group-item bg-orange">
					<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fas fa-chalkboard-teacher"></i></div>
					<div>
						<p class="txt-purple font-weight-bold mb-0"><?php echo get_phrase('inspire_students'); ?></p>
						<p><?php echo get_phrase('help_people_learn_new_skills'); ?></p>
					</div>
				</li>
				
			</ul>
		</div>
		
	</div>
</div>
<script type="text/javascript">
function handleCheckOut() {
    $.ajax({
        url: '<?php echo site_url('home/isLoggedIn');?>',
        success: function(response)
        {
            if (!response) {
                window.location.replace("<?php echo site_url('login'); ?>");
            }else {
                $('#paymentModal').modal('show');
                $('.total_price_of_checking_out').val($('#total_price_of_checking_out').text());
            }
        }
    });
}
</script>
