<?php
$user_details = $this->user_model->get_user($this->session->userdata('user_id'))->row_array();
?>
<section class="menu-area sticky">
    <div class="container-xl">
        <div class="row">
            <div class="col px-2">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">

                    <ul class="mobile-header-buttons">
                        <li><a class="mobile-nav-trigger" href="#mobile-primary-nav">Menu<span></span></a></li>
                        <li><a class="mobile-search-trigger" href="#mobile-search">Search<span></span></a></li>
                    </ul>

                    <a href="<?php echo site_url(''); ?>" class="navbar-brand" href="#">
                        <img src="<?php echo base_url().'uploads/system/logo-dark.png'; ?>" alt="" height="30">
                    </a>
					<?php /* <a href="<?php echo site_url(''); ?>" class="navbar-brand" href="#"><i class="fas fa-home pl-2 txt-purple"></i></a> */ ?>
                    <?php include 'menu.php'; ?>

					
                    <form class="inline-form" action="<?php echo site_url('home/search'); ?>" method="get" style="width: 100%;" autocomplete="off">
                        <div class="input-group search-box mobile-search">
                            <input id="autosearchbar" type="text" name = 'query' class="form-control" placeholder="Search for the course you want to learn!">
                            <div class="input-group-append menusearch1" style="position: absolute;right: 6px;top: 50%;transform: translateY(-50%);z-index: 3;">
                                <button class="btn" type="submit"><i class="fas fa-search" style="margin-left: -5px !important;"></i></button>
                            </div>
                        </div>
                    </form>

                    <?php if (get_settings('allow_instructor') == 1): ?>
                        <div class="instructor-box menu-icon-box">
                            <div class="icons">
                                <a class="badge badge-pill" href="<?php echo site_url('user'); ?>" style="min-width: 100px;color: #fff;font-size: 15px !important;padding: 11px 10px 7px 12px !important;height: 40px;font-weight: unset;background: #6f2d87;"><?php echo get_phrase('instructors_panel'); ?></a>
                            </div>
                        </div>
                    <?php endif; ?>

                    <div class="instructor-box menu-icon-box">
                        <div class="icons">
                            <a class="badge badge-pill" href="<?php echo site_url('home/my_courses'); ?>" style="min-width: 100px;color: #fff;font-size: 15px !important;padding: 11px 10px 7px 12px !important;height: 40px;font-weight: unset;background: #6f2d87;"><?php echo get_phrase('my_courses'); ?></a>
                        </div>
                    </div>

                    <div class="wishlist-box menu-icon-box" id = "wishlist_items">
                        <?php include 'wishlist_items.php'; ?>
                    </div>

                    <div class="cart-box menu-icon-box" id = "cart_items">
                        <?php include 'cart_items.php'; ?>
                    </div>

                    <?php //include 'notifications.php'; ?>


                    <div class="user-box menu-icon-box">
                        <div class="icon">
                            <a href="javascript::">
                                <?php
                                if (file_exists('uploads/user_image/'.$user_details['id'].'.jpg')): ?>
                                <img src="<?php echo base_url().'uploads/user_image/'.$user_details['id'].'.jpg';?>" alt="" class="img-fluid">
                            <?php else: ?>
                                <img src="<?php echo base_url().'uploads/user_image/placeholder.png';?>" alt="" class="img-fluid">
                            <?php endif; ?>
                        </a>
                    </div>
                    <div class="dropdown user-dropdown corner-triangle top-right">
                        <ul class="user-dropdown-menu">

                            <li class="dropdown-user-info">
                                <a href="">
                                    <div class="clearfix">
                                        <div class="user-image float-left">
                                            <?php if (file_exists('uploads/user_image/'.$user_details['id'].'.jpg')): ?>
                                                <img src="<?php echo base_url().'uploads/user_image/'.$user_details['id'].'.jpg';?>" alt="" class="img-fluid">
                                            <?php else: ?>
                                                <img src="<?php echo base_url().'uploads/user_image/placeholder.png';?>" alt="" class="img-fluid">
                                            <?php endif; ?>
                                        </div>
                                        <div class="user-details">
                                            <div class="user-name">
                                                <span class=""><?php echo get_phrase('hi'); ?>,</span>
                                                <?php echo $user_details['first_name'].' '.$user_details['last_name']; ?><br><small><?php echo get_phrase("welcome_back"); ?>!</small>
                                            </div>
                                            <div class="user-email">
                                                <span class="email"><?php echo $user_details['email']; ?></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>

                            <li class="user-dropdown-menu-item"><a href="<?php echo site_url('home/my_courses'); ?>"><i class="fas fa-book-reader"></i><?php echo get_phrase('my_courses'); ?></a></li>
                            <li class="user-dropdown-menu-item"><a href="<?php echo site_url('home/my_wishlist'); ?>"><i class="far fa-heart"></i><?php echo get_phrase('my_wishlist'); ?></a></li>
							<?php /*
                            <li class="user-dropdown-menu-item"><a href="<?php echo site_url('home/my_messages'); ?>"><i class="fas fa-mail-bulk"></i><?php echo get_phrase('my_messages'); ?></a></li>
							*/ ?>
                            <li class="user-dropdown-menu-item"><a href="<?php echo site_url('home/purchase_history'); ?>"><i class="fas fa-shopping-basket"></i><?php echo get_phrase('purchase_history'); ?></a></li>
                            <li class="user-dropdown-menu-item"><a href="<?php echo site_url('home/profile/user_profile'); ?>"><i class="fas fa-user-shield"></i><?php echo get_phrase('my_profile'); ?></a></li>
                            <li class="dropdown-user-logout user-dropdown-menu-item text-center"><a href="<?php echo site_url('login/logout/user'); ?>"><i class="fas fa-power-off mr-1 text-white"></i><?php echo get_phrase('log_out'); ?></a></li>
                        </ul>
                    </div>
                </div>



                <span class="signin-box-move-desktop-helper"></span>
                <div class="sign-in-box btn-group d-none">

                    <button type="button" class="btn btn-sign-in" data-toggle="modal" data-target="#signInModal">Log In</button>

                    <button type="button" class="btn btn-sign-up" data-toggle="modal" data-target="#signUpModal">Sign Up</button>

                </div> <!--  sign-in-box end -->


            </nav>
        </div>
    </div>
</div>
</section>
