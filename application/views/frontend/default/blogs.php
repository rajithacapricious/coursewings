<section class="category-header-area bloghead">
    <div class="container-lg">
        <div class="row">
            <div class="col">
                <h1 class="category-name">
                    <i class="fas fa-pen-square mr-2"></i><?php echo $page_title; ?>
                </h1>
            </div>
        </div>
    </div>
</section>

<section class="category-course-list-area blogs">
	<div class="container-lg">
		<div class="row">
			<div class="col-md-9">
				<?php if (!empty($result)) : ?>
					<div class="blog_left_section">
						<div class="blog_section">
					<?php foreach ($result as $value) : ?>
						
							<div class="cw_blog_image">
								<?php if ($value->image) : ?>
									<img src="<?php echo base_url('uploads/blog_images/' . $value->image) ?>" alt="<?php echo $value->title; ?>" class="img-fluid">
									<span class="cw_blog_date"><?php echo date('M d, Y', strtotime($value->created_date)) ?></span>
								<?php else : ?>
									<?php $blog_media_placeholders = themeConfiguration(get_frontend_settings('theme'), 'blog_media_placeholders'); ?>
									<img src="<?php echo base_url() . $blog_media_placeholders['blog_image_placeholder']; ?>" alt="<?php echo $value->title; ?>" class="img-fluid">
									<span class="cw_blog_date"><?php echo date('M d, Y', strtotime($value->created_date)) ?></span>
								<?php endif; ?>
							</div>
							
							<div class="cw_blog_content">
								<h3><a href="<?php echo base_url('home/blog/' . $value->slug); ?>"><?php echo $value->title ?></a></h3>
								<div class="blog_user">
									<div class="user_name">
										<?php if ($value->created_by) :
										$user_details = $this->db->select(array('first_name', 'last_name'))->where('id', $value->created_by)->get('users')->row_array();
										?>
										<img src="<?php echo $this->user_model->get_user_image_url($value->created_by); ?>" style="width:30px; height:30px;" alt="image">
										<a href="javascript:;"><span><?php echo $user_details['first_name'] . ' ' . $user_details['last_name']; ?></span></a>
										<?php endif ?>
									</div>
									<div class="comment_block">
										<span><i class="far fa-eye" aria-hidden="true"></i></span>
										<a><?php echo $value->hits.' Views' ?></a>
									</div>
								</div>
								<p><?php echo $value->short_description; ?></p>
								<a class="btn btn-primary rounded25" href="<?php echo base_url('home/blog/' . $value->slug); ?>">read more <span><i class="fas fa-arrow-circle-right" aria-hidden="true"></i></span></a>
							</div>
							<hr class="bloghr">
					<?php endforeach; ?>
						</div>
					</div>
					<!--
					<nav>
						<ul class="pagination justify-content-center">
							<li class="page-item active disabled"> <span class="page-link">1</span></li>
							<li class="page-item"><a href="#" data-ci-pagination-page="2">2</a></li>
							<li class="page-item"><a href="#" data-ci-pagination-page="2" rel="next"><i class="fas fa-chevron-right"></i></a></li>
						</ul>
					</nav>
					-->
					<?php if (!empty($pagination)) : ?>
						<div class="row">
							<div class="col text-center">
								<nav aria-label="Page navigation example">
									<ul class="pagination">
										<?php echo $pagination; ?>
									</ul>
								</nav>
							</div>
						</div>
					<?php endif ?>
				<?php endif ?>
			</div>
			<div class="col-md-3">
				<div class="blog_sidebar">
					<div class="sidebar_block">
                        <div class="widget widget-author">
                            <!--<div class="sidebar_heading">
                                <h3>about me</h3>
                                <img src="../assets/images/footer_underline.png" alt="image">
                            </div>-->
                            <div class="author-card">
                                <div class="author-card-cover" style="background-image: url(<?php echo site_url('uploads/system/bloggerheader.jpg'); ?>);"></div>
                                    <div class="author-card-profile">
									<?php 
											$user_details = $this->db->select(array('first_name', 'last_name'))->where('id', 1)->get('users')->row_array();
											?>
                                        <div class="author-card-avatar">
											
											<img src="<?php echo $this->user_model->get_user_image_url(1); ?>" alt="image">
											
                                        </div>
                                        <div class="author-card-details">
                                        <h5 class="author-card-name"><?php echo $user_details['first_name'] . ' ' . $user_details['last_name']; ?></h5><span class="author-card-position">Admin</span>
                                        </div>
                                    </div>
                                <p class="author-card-info">Blogs on latest trends & insights concerning Industries, Business, E-Learning & User experience.</p>
                                <div class="author-card-social-bar-wrap">
                                    <div class="author-card-social-bar">
                                        <a class="social-btn sb-style-1 sb-facebook" href="https://www.facebook.com/CourseWings/" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                        <a class="social-btn sb-style-1 sb-twitter" href="https://twitter.com/CourseWings" target="_blank"><i class="fab fa-twitter"></i></a>
                                        <a class="social-btn sb-style-1 sb-instagram" href="https://www.instagram.com/coursewings_videos/" target="_blank"><i class="fab fa-instagram"></i></a>
                                        <a class="social-btn sb-style-1 sb-linkedin" href="https://www.linkedin.com/company/course-wings/" target="_blank"><i class="fab fa-linkedin"></i></a>
                                        <a class="social-btn sb-style-1 sb-youtube" href="https://www.youtube.com/channel/UCG1ovYGI3fWScL2SF3Gg1XQ?view_as=subscriber" target="_blank"><i class="fab fa-youtube"></i></a>
                                    </div>
                                </div>
                            </div>
                      </div>
                    </div>
				
				
					<?php
					$result = $this->db->where('hits != ', 0)->where('status', 1)->order_by('hits', 'DESC')->limit(5)->get('blogs')->result_array();
					if (!empty($result)) : ?>
                    <div class="sidebar_block">
                        <div class="widget widget-author">
							<div class="author-card blogside">
								<div class="sidebar_heading">
									<h3>popular posts</h3>
								</div>
								<div class="sidebar_post">
									<ul>
									<?php foreach ($result as $key => $value) : ?>
										<li>
											<div class="post_content">
												<p><?php echo date('M d, Y', strtotime($value['created_date'])) ?></p>
												<a href="<?php echo base_url('home/blog/' . $value['slug']); ?>"><?php echo $value['title'] ?></a>
											</div>
										</li>
									<?php endforeach; ?>
									</ul>
								</div>
							</div>
                        </div>
                    </div>
					<?php endif; ?>
                    
					<?php
					$result = $this->db->where('status', 1)->order_by('created_date', 'DESC')->limit(5)->get('blogs')->result_array();
					if (!empty($result)) : ?>
                    <div class="sidebar_block">
                        <div class="widget widget-author">
							<div class="author-card blogside">
								<div class="sidebar_heading">
									<h3>recent posts</h3>
								</div>
								<div class="sidebar_post">
									<ul>
									<?php foreach ($result as $key => $value) : ?>
										<li>
											<div class="post_content">
												<p><?php echo date('M d, Y', strtotime($value['created_date'])) ?></p>
												<a href="<?php echo base_url('home/blog/' . $value['slug']); ?>"><?php echo $value['title'] ?></a>
											</div>
										</li>
									<?php endforeach; ?>
									</ul>
									
								</div>
							</div>
                        </div>
                    </div>
					<?php endif; ?>
                    <!-- TAGS ECHO
                    <div class="sidebar_block">
                        <div class="widget widget-author">
                            <div class="author-card blogside">
                                <div class="sidebar_heading">
                                    <h3>tags</h3>
                                </div>
                                <div class="sidebar_tags">
                                    <ul>
                                        <li><a href="javascript:;">Agriculture</a></li>
                                        <li><a href="javascript:;">farm</a></li>
                                        <li><a href="javascript:;">Organic</a></li>
                                        <li><a href="javascript:;">egg</a></li>
                                        <li><a href="javascript:;">Permaculture</a></li>
                                        <li><a href="javascript:;">Green</a></li>
                                        <li><a href="javascript:;">garden</a></li>
                                        <li><a href="javascript:;">milk</a></li>
                                        <li><a href="javascript:;">dairy farm</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    -->
                    
                </div>
			</div>
		</div>
	</div>
</section>