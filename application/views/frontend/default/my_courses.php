<?php

$my_courses = $this->user_model->my_courses()->result_array();

$categories = array();
foreach ($my_courses as $my_course) {
    $course_details = $this->crud_model->get_course_by_id($my_course['course_id'])->row_array();
    if (!in_array($course_details['category_id'], $categories)) {
        array_push($categories, $course_details['category_id']);
    }
}
?>
<?php /*
<section class="page-header-area my-course-area">
    <div class="container-lg">
        <div class="row">
            <div class="col">
                <h1 class="page-title"><?php echo get_phrase('my_courses'); ?></h1>
                <ul>
                  <li class="active"><a href="<?php echo site_url('home/my_courses'); ?>"><?php echo get_phrase('all_courses'); ?></a></li>
                  <li><a href="<?php echo site_url('home/my_wishlist'); ?>"><?php echo get_phrase('wishlists'); ?></a></li>
                  <li><a href="<?php echo site_url('home/my_messages'); ?>"><?php echo get_phrase('my_messages'); ?></a></li>
                  <li><a href="<?php echo site_url('home/purchase_history'); ?>"><?php echo get_phrase('purchase_history'); ?></a></li>
                  <li><a href="<?php echo site_url('home/profile/user_profile'); ?>"><?php echo get_phrase('user_profile'); ?></a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
*/ ?>
<section class="page-header-area my-course-area">
    <div class="container-lg">
        <div class="row">
            <div class="col">
                <h1 class="page-title"><i class="fas fa-book-reader mr-2"></i><?php echo get_phrase('my_courses'); ?></h1>
                <ul>
					<li class="active"><a href="<?php echo site_url('home/my_courses'); ?>"><i class="fas fa-book-reader mr-1"></i><?php echo get_phrase('subscribed_courses'); ?></a></li>
					<li><a href="<?php echo site_url('home/my_wishlist'); ?>"><i class="fas fa-hand-holding-heart mr-1"></i><?php echo get_phrase('my_wishlists'); ?></a></li>
					<?php /* <li><a href="<?php echo site_url('home/my_messages'); ?>"><?php echo get_phrase('my_messages'); ?></a></li> */ ?>
					<li><a href="<?php echo site_url('home/purchase_history'); ?>"><i class="fas fa-cart-plus mr-1"></i><?php echo get_phrase('purchase_history'); ?></a></li>
					<?php /* <li><a href="<?php echo site_url('home/profile/user_profile'); ?>"><?php echo get_phrase('user_profile'); ?></a></li> */ ?>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user-edit mr-1"></i><?php echo get_phrase('my_profile'); ?></a>
						<div class="dropdown-menu profile">
							<a class="dropdown-item px-3 py-2" href="<?php echo site_url('home/profile/user_profile'); ?>">
								<i class="fas fa-user-edit mr-1"></i><?php echo get_phrase('edit_profile'); ?>
							</a>
							<a class="dropdown-item px-3 py-2" href="<?php echo site_url('home/profile/user_credentials'); ?>">
								<i class="fas fa-key mr-1"></i><?php echo get_phrase('change_password'); ?>
							</a>
							<a class="dropdown-item px-3 py-2" href="<?php echo site_url('home/profile/user_photo'); ?>">
								<i class="far fa-image mr-1"></i><?php echo get_phrase('edit_photo'); ?>
							</a>
						</div>
					</li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="my-courses-area">
    <div class="container-lg">
        <div class="row align-items-baseline">
			<div class="col-lg-9">
                <div class="my-course-search-bar">
                    <form action="">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="<?php echo get_phrase('search_my_courses'); ?>" onkeyup="getCoursesBySearchString(this.value)">
                            <div class="input-group-append menusearch1" style="position: absolute;right: 6px;top: 50%;transform: translateY(-50%);z-index: 3;">
                                <button class="btn" type="submit"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </form>
					
					
					
					
					
					
					
                </div>
            </div>
            <div class="col-lg-3">
                <div class="my-course-filter-bar filter-box">
                    <div class="btn-group">
						<span class="filter-by my-auto"><?php echo get_phrase('filter_by'); ?></span>
                        <a class="btn btn-outline-secondary dropdown-toggle all-btn" href="#"data-toggle="dropdown">
                            <?php echo get_phrase('categories'); ?>
                        </a>

                        <div class="dropdown-menu">
                            <?php foreach ($categories as $category):
                                $category_details = $this->crud_model->get_categories($category)->row_array();
                                ?>
                                <a class="dropdown-item" href="#" id = "<?php echo $category; ?>" onclick="getCoursesByCategoryId(this.id)"><?php echo $category_details['name']; ?></a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <?php /* <div class="btn-group">
                        <a class="btn btn-outline-secondary dropdown-toggle" href="#"data-toggle="dropdown">
                            <?php echo get_phrase('instructors'); ?>
                        </a>

                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#"><?php echo $instructor_details['first_name'].' '.$instructor_details['last_name']; ?></a>

                        </div>
                    </div>*/ ?>
                    <div class="btn-group">
                        <a href="<?php echo site_url('home/my_courses'); ?>" class="btn reset-btn" disabled><i class="fas fa-retweet mr-1"></i><?php echo get_phrase('reset'); ?></a>
                    </div>
                </div>
            </div>
            
        </div>
		<hr>
		<div class="row">
			<div class="col-md-9">
				<div class="row no-gutters" id = "my_courses_area">
					<?php foreach ($my_courses as $my_course):
						$course_details = $this->crud_model->get_course_by_id($my_course['course_id'])->row_array();
						$instructor_details = $this->user_model->get_all_user($course_details['user_id'])->row_array();?>

						<div class="col-lg-3">
							<div class="course-box-wrap">
									<div class="course-box">
										<div class="course-badge position best-seller price instrctr">
											<p class="intructor-badge">
												<a href="<?php echo site_url('home/instructor_page/'.$instructor_details['id']); ?>">
													<i class="fas fa-chalkboard-teacher mr-1">:</i>
													<img src="<?php echo $this->user_model->get_user_image_url($instructor_details['id']);?>" alt="" class="imgcirclesmall">
													<?php echo $instructor_details['first_name'].' '.$instructor_details['last_name']; ?>
												</a>
											</p>
										</div>
										<a href="<?php echo site_url('home/lesson/'.slugify($course_details['title']).'/'.$my_course['course_id']); ?>">
											<div class="course-image">
												<img src="<?php echo $this->crud_model->get_course_thumbnail_url($my_course['course_id']); ?>" alt="" class="img-fluid">
												<span class="play-btn"></span>
											</div>
										</a>

										<div class="" id = "course_info_view_<?php echo $my_course['course_id'];  ?>">
										  <div class="course-details buttn">
											<a href="<?php echo site_url('home/course/'.slugify($course_details['title']).'/'.$my_course['course_id']); ?>">
												<h5 class="title"><?php echo ellipsis($course_details['title']); ?></h5>
											</a>
											
											<a href="<?php echo site_url('home/course/'.slugify($course_details['title']).'/'.$my_course['course_id']); ?>" class="btn btn-block coursedetails">
												<?php echo get_phrase('course_detail'); ?>
											</a>
											
											<a href="<?php echo site_url('home/lesson/'.slugify($course_details['title']).'/'.$my_course['course_id']); ?>" class="btn btn-block coursedetails">
												<?php echo get_phrase('start_lesson'); ?>
											</a>
											
											<div class="text-center">
												<span class="lsnhrs">
													<?php
														$lessons = $this->crud_model->get_lessons('course', $my_course['course_id'])->num_rows();
														echo $lessons.' '.get_phrase('chapters');
													?>
													( <?php
														echo $this->crud_model->get_total_duration_of_lesson_by_course_id($my_course['course_id']);
													?> )
												</span>
												
											</div>
											  
											 
											  <div class="progress mt-1" style="height: 10px;">
												<div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: <?php echo course_progress($my_course['course_id']); ?>%" aria-valuenow="<?php echo course_progress($my_course['course_id']); ?>" aria-valuemin="0" aria-valuemax="100"></div>
											  </div>
											  <div class="text-center"><small class="text-center"><?php echo ceil(course_progress($my_course['course_id'])); ?>% <?php echo get_phrase('completed'); ?></small></div>
											  
											  
										  </div>
										  
										  <div class="rating your-rating-box ratenow" style="position: unset;">

											  <?php
											   $get_my_rating = $this->crud_model->get_user_specific_rating('course', $my_course['course_id']);
											   for($i = 1; $i < 6; $i++):?>
											   <?php if ($i <= $get_my_rating['rating']): ?>
												  <i class="fas fa-star filled"></i>
											  <?php else: ?>
												  <i class="fas fa-star"></i>
											   <?php endif; ?>
											  <?php endfor; ?>
											  <!-- <p class="your-rating-text" id = "<?php echo $my_course['course_id']; ?>" onclick="getCourseDetailsForRatingModal(this.id)">
												  <span class="your"><?php echo get_phrase('your'); ?></span>
												  <span class="edit"><?php echo get_phrase('edit'); ?></span>
												  <?php echo get_phrase('rating'); ?>
											  </p> -->
											  <p class="your-rating-text">
												<a href="javascript::" id = "edit_rating_btn_<?php echo $course_details['id']; ?>" onclick="toggleRatingView('<?php echo $course_details['id']; ?>')" style="color: #2a303b; font-weight:bold">EDIT RATING</a>
													<a href="javascript::" class="hidden" id = "cancel_rating_btn_<?php echo $course_details['id']; ?>" onclick="toggleRatingView('<?php echo $course_details['id']; ?>')" style="color: #2a303b"><?php echo get_phrase('cancel_rating'); ?></a>
											  </p>
										  </div>
										</div>

										<div class="course-details" style="display: none; padding-bottom: 10px;" id = "course_rating_view_<?php echo $course_details['id']; ?>">
										  <a href="<?php echo site_url('home/course/'.slugify($course_details['title']).'/'.$my_course['course_id']); ?>"><h5 class="title"><?php echo ellipsis($course_details['title']); ?></h5></a>
										  <?php
															$user_specific_rating = $this->crud_model->get_user_specific_rating('course', $course_details['id']);
														?>
														<form class="" action="" method="post">
															<div class="form-group select">
																<div class="styled-select">
																	<select class="form-control" name="star_rating" id="star_rating_of_course_<?php echo $course_details['id']; ?>">
																		<option value="1" <?php if ($user_specific_rating['rating'] == 1): ?>selected=""<?php endif; ?>>1 <?php echo get_phrase('out_of'); ?> 5</option>
																		<option value="2" <?php if ($user_specific_rating['rating'] == 2): ?>selected=""<?php endif; ?>>2 <?php echo get_phrase('out_of'); ?> 5</option>
																		<option value="3" <?php if ($user_specific_rating['rating'] == 3): ?>selected=""<?php endif; ?>>3 <?php echo get_phrase('out_of'); ?> 5</option>
																		<option value="4" <?php if ($user_specific_rating['rating'] == 4): ?>selected=""<?php endif; ?>>4 <?php echo get_phrase('out_of'); ?> 5</option>
																		<option value="5" <?php if ($user_specific_rating['rating'] == 5): ?>selected=""<?php endif; ?>>5 <?php echo get_phrase('out_of'); ?> 5</option>
																	</select>
																</div>
															</div>
															<div class="form-group add_top_30">
																<textarea name="review" id ="review_of_a_course_<?php echo $course_details['id']; ?>" class="form-control" style="height:120px;" placeholder="<?php echo get_phrase('write_your_review_here'); ?>"><?php echo $user_specific_rating['review']; ?></textarea>
															</div>
															<button type="" class="btn btn-block coursedetails success" onclick="publishRating('<?php echo $course_details['id']; ?>')" name="button"><?php echo get_phrase('publish_rating'); ?></button>
															<a href="javascript::" class="btn btn-block coursedetails danger" onclick="toggleRatingView('<?php echo $course_details['id']; ?>')" name="button"><?php echo get_phrase('cancel_rating'); ?></a>
														</form>
										</div>
									</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="col-md-3">
				<div class="course-sidebar-text-box side mt-4 bg-orange">	
					<div class="promo">
						<ul class="list-group list-group-flush">
							<li class="list-group-item bg-orange">
								<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fas fa-desktop"></i></div>
								<div>
									<p class="txt-purple font-weight-bold mb-0">
									<?php 
										$status_wise_courses = $this->crud_model->get_status_wise_courses();
										$number_of_courses = $status_wise_courses['active']->num_rows();
										echo get_phrase('online_courses'); 
									?>
									</p>
									
									<p><?php echo $number_of_courses.' '.get_phrase('topics_to_learn_from'); ?></p>
								</div>
							</li>
							<li class="list-group-item bg-orange">
								<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fa fa-key"></i></div>
								<div>
									<p class="txt-purple font-weight-bold mb-0"><?php echo get_phrase('lifetime_access'); ?></p>
									<p><?php echo get_phrase('learn_as_per_your_convenience'); ?></p>
								</div>
							</li>
							<li class="list-group-item bg-orange">
								<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fas fa-chalkboard-teacher"></i></div>
								<div>
									<p class="txt-purple font-weight-bold mb-0"><?php echo get_phrase('inspire_students'); ?></p>
									<p><?php echo get_phrase('help_people_learn_new_skills'); ?></p>
								</div>
							</li>
							<li class="list-group-item bg-orange">
								<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fas fa-user-graduate"></i></div>
								<div>
									<p class="txt-purple font-weight-bold mb-0"><?php echo get_phrase('course_completion_acknowledgement'); ?></p>
									<p><?php echo get_phrase('get_proof_of_course_completion'); ?></p>
								</div>
							</li>
							
						</ul>
					</div>
						
				</div>
			</div>
		</div>
    </div>
</section>


<script type="text/javascript">
function getCoursesByCategoryId(category_id) {
    $.ajax({
        type : 'POST',
        url : '<?php echo site_url('home/my_courses_by_category'); ?>',
        data : {category_id : category_id},
        success : function(response){
            $('#my_courses_area').html(response);
        }
    });
}

function getCoursesBySearchString(search_string) {
    $.ajax({
        type : 'POST',
        url : '<?php echo site_url('home/my_courses_by_search_string'); ?>',
        data : {search_string : search_string},
        success : function(response){
            $('#my_courses_area').html(response);
        }
    });
}

function getCourseDetailsForRatingModal(course_id) {
    $.ajax({
        type : 'POST',
        url : '<?php echo site_url('home/get_course_details'); ?>',
        data : {course_id : course_id},
        success : function(response){
            $('#course_title_1').append(response);
            $('#course_title_2').append(response);
            $('#course_thumbnail_1').attr('src', "<?php echo base_url().'uploads/thumbnails/course_thumbnails/';?>"+course_id+".jpg");
            $('#course_thumbnail_2').attr('src', "<?php echo base_url().'uploads/thumbnails/course_thumbnails/';?>"+course_id+".jpg");
            $('#course_id_for_rating').val(course_id);
            // $('#instructor_details').text(course_id);
            console.log(response);
        }
    });
}
</script>
