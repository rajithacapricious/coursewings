<section class="home-banner-area">
    <div class="container-lg">
        <div class="row">
            <div class="col">
                <div class="home-banner-wrap d-none d-sm-block">
                    <h2><?php echo get_frontend_settings('banner_title'); ?></h2>
                     <p><?php echo get_frontend_settings('banner_sub_title'); ?></p>
					 <?php /*
                    <form class="pt-2" action="<?php echo site_url('home/search'); ?>" method="get">
                        
						<div class="input-group bannersearch">
                            <input type="text" class="form-control" name = "query" placeholder="<?php echo get_phrase('what_courses_are_you_interested_in'); ?>?" style="z-index: 0;"  autocomplete="off">
                            <div class="input-group-append menusearch1" style="position: absolute;top: 50%;transform: translateY(-50%);">
                                <button class="btn" type="submit"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
						
						<div class="p-1 bg-light rounded rounded-pill shadow-sm" style="background: #fff;border-radius: 50rem;padding: 5px;">
							<div class="input-group ">
							  <input type="text" placeholder="<?php echo get_phrase('what_courses_are_you_interested_in'); ?>?" aria-describedby="button-addon1" class="form-control border-0 bg-light"  style="border: 0px;border-radius: 50rem;padding: 0px 15px 0px 15px;">
							  <div class="input-group-append">
								<button id="button-addon1" type="submit" class="btn btn-link text-primary"><i class="fas fa-search"></i></button>
							  </div>
							</div>
						  </div>
						  
						  <form class="card card-sm">
                                <div class="card-body row no-gutters align-items-center" style="background: #fff;border-radius: 50rem;padding: 5px;">
                                    <div class="col">
                                        <input class="form-control form-control-lg form-control-borderless" type="search" placeholder="<?php echo get_phrase('what_courses_are_you_interested_in'); ?>?" style="border: 0px;border-radius: 50rem;padding: 0px 15px 0px 15px;">
                                    </div>
                                    <!--end of col-->
                                    <div class="col-auto">
                                        <button class="btn btn-lg btn-success" type="submit"><i class="fas fa-search"></i></button>
                                    </div>
                                    <!--end of col-->
                                </div>
                            </form>
						
                    </form>
					*/ ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="home-fact-area">
    <div class="container-lg">
        <div class="row">
            <?php $courses = $this->crud_model->get_courses(); ?>
            <div class="col-md-3 col-xs-12">
				<div class="home-fact-box mr-md-auto ml-auto mr-auto">
					<i class="fas fa-desktop float-left"></i>
					<div class="text-box">
						<h4>
						<?php
							$status_wise_courses = $this->crud_model->get_status_wise_courses();
							$number_of_courses = $status_wise_courses['active']->num_rows();
							echo get_phrase('learn_anything_online'); ?>
						</h4>
						<p><?php echo $number_of_courses.' '.get_phrase('topics_to_learn_from'); ?></p>
					</div>
				</div>
				</div>

            <div class="col-md-3 col-xs-12">
				<div class="home-fact-box">
					<i class="fas fa-key float-left"> </i>
					<div class="text-box">
						<h4><?php echo get_phrase('lifetime_access'); ?></h4>
						<p><?php echo get_phrase('learn_at_your_convenience'); ?></p>
					</div>
				</div>
			  </div>

			<div class="col-md-3 col-xs-12">
				<div class="home-fact-box mr-md-auto ml-auto mr-auto">
				<i class="fas fa-globe-americas float-left"></i>
					<div class="text-box">
						<h4><?php echo get_phrase('worldwide_experts'); ?></h4>
						<p><?php echo get_phrase('find_the_right_course'); ?></p>
					</div>
				</div>
			</div>
			
			<div class="col-md-3 col-xs-12">
				<div class="home-fact-box mr-md-auto ml-auto mr-auto">
				<i class="fas fa-chalkboard-teacher  float-left">
				</i>
					<div class="text-box">
						<h4><?php echo get_phrase('become_an_instructor'); ?></h4>
						<p><?php echo get_phrase('reach_millions_of_students'); ?></p>
					</div>
				</div>
			</div>
        </div>
    </div>
</section>

<section class="course-carousel-area">
    <div class="container-lg">
        <div class="row">
            <div class="col">
                <h2 class="course-carousel-title">
				<?php /* <img class="preicon" src="<?php echo base_url().'assets/frontend/logoicon.png'; ?>" height="35" width="35"/> */ ?>
				<?php echo get_phrase('featured_courses'); ?>
				</h2>
                <div class="course-carousel">
                    <?php $top_courses = $this->crud_model->get_top_courses()->result_array();
                    $cart_items = $this->session->userdata('cart_items');
                    foreach ($top_courses as $top_course):?>
                    <div class="course-box-wrap">
                        <a href="<?php echo site_url('home/course/'.slugify($top_course['title']).'/'.$top_course['id']); ?>" class="has-popover">
                            <div class="course-box">
                                <?php /* <div class="course-badge position best-seller">Best seller</div> */ ?>
								
								<div class="course-badge position best-seller price">
									<?php if ($top_course['is_free_course'] == 1): ?>
                                    <p class="price net text-right"><?php echo get_phrase('free'); ?></p>
									<?php else: ?>
										<?php if ($top_course['discount_flag'] == 1): ?>
											<p class="price net text-right">
												<small class="discount cross">
													<?php echo currency($top_course['price']); ?>
												</small>
												<?php echo currency($top_course['discounted_price']); ?></p>
										<?php else: ?>
											<p class="price net text-right"><?php echo currency($top_course['price']); ?></p>
										<?php endif; ?>
									<?php endif; ?>
								
								</div>
								
                                <div class="course-image">
                                    <img src="<?php echo $this->crud_model->get_course_thumbnail_url($top_course['id']); ?>" alt="" class="img-fluid" style="width: 260px;height: 260px;">
                                </div>
								<table width="100%"  class="course-details">
								<td style="width: 80%;border-bottom-left-radius: 15px;padding: 10px;">
									<span class="title"><?php echo $top_course['title']; ?></span>
                                    <?php /* <p class="instructors"><?php echo $top_course['short_description']; ?></p> */ ?>
									<p class="instructors">
										<i class="fas fa-chalkboard-teacher"> :</i> 
											<?php
												$instructor_details = $this->user_model->get_all_user($top_course['user_id'])->row_array();
												echo $instructor_details['first_name'].' '.$instructor_details['last_name']; 
											?>
									</p>
								</td>
								<td style="width: 15%;background: #6f2d87;border-bottom-right-radius: 15px;padding: 10px;color:#fff;text-align: center;">
								<i class="fas fa-caret-right"></i>
								</td>
								</table>
                                
								<?php /*
								<div class="course-details rating ratingsdesc">
									<?php
									$total_rating =  $this->crud_model->get_ratings('course', $top_course['id'], true)->row()->rating;
									$number_of_ratings = $this->crud_model->get_ratings('course', $top_course['id'])->num_rows();
									if ($number_of_ratings > 0) {
										$average_ceil_rating = ceil($total_rating / $number_of_ratings);
									}else {
										$average_ceil_rating = 0;
									}

									for($i = 1; $i < 6; $i++):?>
									<?php if ($i <= $average_ceil_rating): ?>
										<i class="fas fa-star filled"></i>
									<?php else: ?>
										<i class="fas fa-star"></i>
									<?php endif; ?>
								<?php endfor; ?>
									<span class="d-inline-block average-rating"><?php echo $average_ceil_rating; ?></span>
								</div>
								*/ ?>
                        </div>
                    </a>

                    <div class="webui-popover-content">
                        <div class="course-popover-content">
                            

                            <div class="course-title">
                                <a href="<?php echo site_url('home/course/'.slugify($top_course['title']).'/'.$top_course['id']); ?>"><?php echo $top_course['title']; ?></a>
                            </div>
                            <div class="course-meta">
                                <span class=""><i class="fas fa-play-circle"></i>
                                    <?php echo $this->crud_model->get_lessons('course', $top_course['id'])->num_rows().' '.get_phrase('chapters'); ?>
                                </span>
                                <span class=""><i class="far fa-clock"></i>
                                    <?php
                                    $total_duration = 0;
                                    $lessons = $this->crud_model->get_lessons('course', $top_course['id'])->result_array();
                                    foreach ($lessons as $lesson) {
                                        if ($lesson['lesson_type'] != "other") {
                                            $time_array = explode(':', $lesson['duration']);
                                            $hour_to_seconds = $time_array[0] * 60 * 60;
                                            $minute_to_seconds = $time_array[1] * 60;
                                            $seconds = $time_array[2];
                                            $total_duration += $hour_to_seconds + $minute_to_seconds + $seconds;
                                        }
                                    }
                                    echo gmdate("H:i:s", $total_duration).' '.get_phrase('hrs');
                                    ?>
                                </span>
                                <span class="">
									<i class="far fa-calendar-alt"></i>
										<?php /* echo ucfirst($top_course['language']); */ ?>
										<?php if ($top_course['last_modified'] == ""): ?>
											<?php echo date('dM, Y', $top_course['date_added']); ?>
										<?php else: ?>
											<?php echo date('dM, Y', $top_course['last_modified']); ?>
										<?php endif; ?>
								</span>
                            </div>
                            <div class="course-subtitle"><?php echo $top_course['short_description']; ?></div>
                            <?php /*
							<div class="what-will-learn">
                                <ul>
                                    <?php
                                    $outcomes = json_decode($top_course['outcomes']);
                                    foreach ($outcomes as $outcome):?>
                                    <li><?php echo $outcome; ?></li>
                                <?php endforeach; ?>
								</ul>
							</div>
							*/ ?>
                        <div class="popover-btns">
                            <?php if (is_purchased($top_course['id'])): ?>
                                <div class="purchased">
                                    <a href="<?php echo site_url('home/my_courses'); ?>"><?php echo get_phrase('already_purchased'); ?></a>
                                </div>
                            <?php else: ?>
                                <?php if ($top_course['is_free_course'] == 1):
                                    if($this->session->userdata('user_login') != 1) {
                                        $url = "#";
                                    }else {
                                        $url = site_url('home/get_enrolled_to_free_course/'.$top_course['id']);
                                    }?>
                                    <a href="<?php echo $url; ?>" class="btn add-to-cart-btn big-cart-button" onclick="handleEnrolledButton()"><?php echo get_phrase('get_enrolled'); ?></a>
                                <?php else: ?>
                                    <button type="button" class="btn add-to-cart-btn <?php if(in_array($top_course['id'], $cart_items)) echo 'addedToCart'; ?> big-cart-button-<?php echo $top_course['id'];?>" id = "<?php echo $top_course['id']; ?>" onclick="handleCartItems(this)">
                                        <?php
                                        if(in_array($top_course['id'], $cart_items))
                                        echo get_phrase('added_to_cart');
                                        else
                                        echo get_phrase('add_to_cart');
                                        ?>
                                    </button>
                                    <button type="button" class="wishlist-btn <?php if($this->crud_model->is_added_to_wishlist($top_course['id'])) echo 'active'; ?>" title="Add to wishlist" onclick="handleWishList(this)" id = "<?php echo $top_course['id']; ?>"><i class="fas fa-heart"></i></button>
                                <?php endif; ?>
                            <?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
</div>
</div>
</section>

<section class="course-carousel-area">
    <div class="container-lg">
        <div class="row">
			<div class="col">
				<h2 class="course-carousel-title">
				<?php /* <img class="preicon" src="<?php echo base_url().'assets/frontend/logoicon.png'; ?>" height="35" width="35"/> */ ?>
				<?php echo get_phrase('newly_added_courses'); ?>
				</h2>
                <div class="course-carousel">
                    <?php
                    $latest_courses = $this->crud_model->get_latest_10_course();
                    foreach ($latest_courses as $latest_course):?>
                    <div class="course-box-wrap">
                        <a href="<?php echo site_url('home/course/'.slugify($latest_course['title']).'/'.$latest_course['id']); ?>" class="has-popover">
                            <div class="course-box">
								<div class="course-badge position best-seller price">
									<?php if ($latest_course['is_free_course'] == 1): ?>
                                    <p class="price net text-right"><?php echo get_phrase('free'); ?></p>
									<?php else: ?>
										<?php if ($latest_course['discount_flag'] == 1): ?>
											<p class="price net text-right">
												<small class="discount cross">
													<?php echo currency($latest_course['price']); ?>
												</small>
												<?php echo currency($latest_course['discounted_price']); ?></p>
										<?php else: ?>
											<p class="price net text-right"><?php echo currency($latest_course['price']); ?></p>
										<?php endif; ?>
									<?php endif; ?>
								
								</div>
								<div class="course-image">
                                    <img src="<?php echo $this->crud_model->get_course_thumbnail_url($latest_course['id']); ?>" alt="" class="img-fluid" style="width: 260px;height: 260px;">
                                </div>
								
								
								<table width="100%"  class="course-details">
								<td style="width: 80%;border-bottom-left-radius: 15px;padding: 10px;">
									<span class="title"><?php echo $latest_course['title']; ?></span>
                                    <?php /* <p class="instructors"><?php echo $latest_course['short_description']; ?></p> */ ?>
									<p class="instructors">
										<i class="fas fa-chalkboard-teacher"> :</i> 
											<?php
												$instructor_details = $this->user_model->get_all_user($latest_course['user_id'])->row_array();
												echo $instructor_details['first_name'].' '.$instructor_details['last_name']; 
											?>
									</p>
								</td>
								<td style="width: 15%;background: #6f2d87;border-bottom-right-radius: 15px;padding: 10px;color:#fff;text-align: center;">
								<i class="fas fa-caret-right"></i>
								</td>
								</table>
								
								
                                
                        </div>
                    </a>
					
					<div class="webui-popover-content">
                        <div class="course-popover-content">
                            

                            <div class="course-title">
                                <a href="<?php echo site_url('home/course/'.slugify($latest_course['title']).'/'.$latest_course['id']); ?>"><?php echo $latest_course['title']; ?></a>
                            </div>
                            <div class="course-meta">
                                <span class=""><i class="fas fa-play-circle"></i>
                                    <?php echo $this->crud_model->get_lessons('course', $latest_course['id'])->num_rows().' '.get_phrase('chapters'); ?>
                                </span>
                                <span class=""><i class="far fa-clock"></i>
                                    <?php
                                    $total_duration = 0;
                                    $lessons = $this->crud_model->get_lessons('course', $latest_course['id'])->result_array();
                                    foreach ($lessons as $lesson) {
                                        if ($lesson['lesson_type'] != "other") {
                                            $time_array = explode(':', $lesson['duration']);
                                            $hour_to_seconds = $time_array[0] * 60 * 60;
                                            $minute_to_seconds = $time_array[1] * 60;
                                            $seconds = $time_array[2];
                                            $total_duration += $hour_to_seconds + $minute_to_seconds + $seconds;
                                        }
                                    }
                                    echo gmdate("H:i:s", $total_duration).' '.get_phrase('hrs');
                                    ?>
                                </span>
                                <span class="">
									<i class="far fa-calendar-alt"></i>
										<?php /* echo ucfirst($latest_course['language']); */ ?>
										<?php if ($latest_course['last_modified'] == ""): ?>
											<?php echo date('dM, Y', $latest_course['date_added']); ?>
										<?php else: ?>
											<?php echo date('dM, Y', $latest_course['last_modified']); ?>
										<?php endif; ?>
								</span>
                            </div>
                            <div class="course-subtitle"><?php echo $latest_course['short_description']; ?></div>
                            <?php /*
							<div class="what-will-learn">
                                <ul>
                                    <?php
                                    $outcomes = json_decode($latest_course['outcomes']);
                                    foreach ($outcomes as $outcome):?>
                                    <li><?php echo $outcome; ?></li>
                                <?php endforeach; ?>
								</ul>
							</div>
							*/ ?>
                        <div class="popover-btns">
                            <?php if (is_purchased($latest_course['id'])): ?>
                                <div class="purchased">
                                    <a href="<?php echo site_url('home/my_courses'); ?>"><?php echo get_phrase('already_purchased'); ?></a>
                                </div>
                            <?php else: ?>
                                <?php if ($latest_course['is_free_course'] == 1):
                                    if($this->session->userdata('user_login') != 1) {
                                        $url = "#";
                                    }else {
                                        $url = site_url('home/get_enrolled_to_free_course/'.$latest_course['id']);
                                    }?>
                                    <a href="<?php echo $url; ?>" class="btn add-to-cart-btn big-cart-button" onclick="handleEnrolledButton()"><?php echo get_phrase('get_enrolled'); ?></a>
                                <?php else: ?>
                                    <button type="button" class="btn add-to-cart-btn <?php if(in_array($latest_course['id'], $cart_items)) echo 'addedToCart'; ?> big-cart-button-<?php echo $latest_course['id'];?>" id = "<?php echo $latest_course['id']; ?>" onclick="handleCartItems(this)">
                                        <?php
                                        if(in_array($latest_course['id'], $cart_items))
                                        echo get_phrase('added_to_cart');
                                        else
                                        echo get_phrase('add_to_cart');
                                        ?>
                                    </button>
                                    <button type="button" class="wishlist-btn <?php if($this->crud_model->is_added_to_wishlist($latest_course['id'])) echo 'active'; ?>" title="Add to wishlist" onclick="handleWishList(this)" id = "<?php echo $latest_course['id']; ?>"><i class="fas fa-heart"></i></button>
                                <?php endif; ?>
                            <?php endif; ?>

                        </div>
                    </div>
                </div>
					
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
</div>
</section>

<!-- #DK [ TRENDING COURSES start]-->
<?php if (!empty($trending_course)) : ?>
  <section class="course-carousel-area">
    <div class="container-lg">
      <div class="row">
        <div class="col">
          <h2 class="course-carousel-title">
            <?php /* <img class="preicon" src="<?php echo base_url().'assets/frontend/logoicon.png'; ?>" height="35" width="35"/> */ ?>
            <?php echo get_phrase('trending_courses'); ?>
          </h2>
          <div class="course-carousel">
            <?php
            $cart_items = $this->session->userdata('cart_items');
            foreach ($trending_course as $tc) : ?>
              <div class="course-box-wrap">
                <a href="<?php echo site_url('home/course/' . slugify($tc['title']) . '/' . $tc['id']); ?>" class="has-popover">
                  <div class="course-box">
                    <?php /* <div class="course-badge position best-seller">Best seller</div> */ ?>

                    <div class="course-badge position best-seller price">
                      <?php if ($tc['is_free_course'] == 1) : ?>
                        <p class="price net text-right"><?php echo get_phrase('free'); ?></p>
                      <?php else : ?>
                        <?php if ($tc['discount_flag'] == 1) : ?>
                          <p class="price net text-right">
                            <small class="discount cross">
                              <?php echo currency($tc['price']); ?>
                            </small>
                            <?php echo currency($tc['discounted_price']); ?></p>
                        <?php else : ?>
                          <p class="price net text-right"><?php echo currency($tc['price']); ?></p>
                        <?php endif; ?>
                      <?php endif; ?>

                    </div>

                    <div class="course-image">
                      <img src="<?php echo $this->crud_model->get_course_thumbnail_url($tc['id']); ?>" alt="" class="img-fluid" style="width: 260px;height: 260px;">
                    </div>
					<table width="100%"  class="course-details">
								<td style="width: 80%;border-bottom-left-radius: 15px;padding: 10px;">
									<span class="title"><?php echo $tc['title']; ?></span>
                                    <?php /* <p class="instructors"><?php echo $tc['short_description']; ?></p> */ ?>
									<p class="instructors">
										<i class="fas fa-chalkboard-teacher"> :</i> 
											<?php
												$instructor_details = $this->user_model->get_all_user($tc['user_id'])->row_array();
												echo $instructor_details['first_name'].' '.$instructor_details['last_name']; 
											?>
									</p>
								</td>
								<td style="width: 15%;background: #6f2d87;border-bottom-right-radius: 15px;padding: 10px;color:#fff;text-align: center;">
								<i class="fas fa-caret-right"></i>
								</td>
								</table>
                  </div>
                </a>
				<div class="webui-popover-content">
                        <div class="course-popover-content">
                            

                            <div class="course-title">
                                <a href="<?php echo site_url('home/course/'.slugify($tc['title']).'/'.$tc['id']); ?>"><?php echo $tc['title']; ?></a>
                            </div>
                            <div class="course-meta">
                                <span class=""><i class="fas fa-play-circle"></i>
                                    <?php echo $this->crud_model->get_lessons('course', $tc['id'])->num_rows().' '.get_phrase('chapters'); ?>
                                </span>
                                <span class=""><i class="far fa-clock"></i>
                                    <?php
                                    $total_duration = 0;
                                    $lessons = $this->crud_model->get_lessons('course', $tc['id'])->result_array();
                                    foreach ($lessons as $lesson) {
                                        if ($lesson['lesson_type'] != "other") {
                                            $time_array = explode(':', $lesson['duration']);
                                            $hour_to_seconds = $time_array[0] * 60 * 60;
                                            $minute_to_seconds = $time_array[1] * 60;
                                            $seconds = $time_array[2];
                                            $total_duration += $hour_to_seconds + $minute_to_seconds + $seconds;
                                        }
                                    }
                                    echo gmdate("H:i:s", $total_duration).' '.get_phrase('hrs');
                                    ?>
                                </span>
                                <span class="">
									<i class="far fa-calendar-alt"></i>
										<?php /* echo ucfirst($tc['language']); */ ?>
										<?php if ($tc['last_modified'] == ""): ?>
											<?php echo date('dM, Y', $tc['date_added']); ?>
										<?php else: ?>
											<?php echo date('dM, Y', $tc['last_modified']); ?>
										<?php endif; ?>
								</span>
                            </div>
                            <div class="course-subtitle"><?php echo $tc['short_description']; ?></div>
                            <?php /*
							<div class="what-will-learn">
                                <ul>
                                    <?php
                                    $outcomes = json_decode($tc['outcomes']);
                                    foreach ($outcomes as $outcome):?>
                                    <li><?php echo $outcome; ?></li>
                                <?php endforeach; ?>
								</ul>
							</div>
							*/ ?>
                        <div class="popover-btns">
                            <?php if (is_purchased($tc['id'])): ?>
                                <div class="purchased">
                                    <a href="<?php echo site_url('home/my_courses'); ?>"><?php echo get_phrase('already_purchased'); ?></a>
                                </div>
                            <?php else: ?>
                                <?php if ($tc['is_free_course'] == 1):
                                    if($this->session->userdata('user_login') != 1) {
                                        $url = "#";
                                    }else {
                                        $url = site_url('home/get_enrolled_to_free_course/'.$tc['id']);
                                    }?>
                                    <a href="<?php echo $url; ?>" class="btn add-to-cart-btn big-cart-button" onclick="handleEnrolledButton()"><?php echo get_phrase('get_enrolled'); ?></a>
                                <?php else: ?>
                                    <button type="button" class="btn add-to-cart-btn <?php if(in_array($tc['id'], $cart_items)) echo 'addedToCart'; ?> big-cart-button-<?php echo $tc['id'];?>" id = "<?php echo $tc['id']; ?>" onclick="handleCartItems(this)">
                                        <?php
                                        if(in_array($tc['id'], $cart_items))
                                        echo get_phrase('added_to_cart');
                                        else
                                        echo get_phrase('add_to_cart');
                                        ?>
                                    </button>
                                    <button type="button" class="wishlist-btn <?php if($this->crud_model->is_added_to_wishlist($tc['id'])) echo 'active'; ?>" title="Add to wishlist" onclick="handleWishList(this)" id = "<?php echo $tc['id']; ?>"><i class="fas fa-heart"></i></button>
                                <?php endif; ?>
                            <?php endif; ?>

                        </div>
                    </div>
                </div>
                
              </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>
<!-- #DK [ TRENDING COURSES end]-->

<!-- #DK [ TRENDING CATEGORY]-->
<?php if (!empty($trending_category)) : ?>
  <section class="footer-top-widget-area trending-topics">
	
    <div class="container-lg">
		
		
		
		<div class="row">
			<div class="col-md-3" style="padding-bottom: 10px;">
				<div class="p-3 bg-yellow rounded10 overflow-hidden position-relative text-white backgrndimg2">
					<div class="">
						<h5 class="display-5 d-block l-h-n mx-0 px-3 fw-500" style="font-size: 2.5rem;margin-top: 1.5rem;margin-bottom: 1.5rem;background: rgba(3, 3, 3, 0.5);">TRENDING CATEGORIES</h5>
					</div>
				</div>
            </div>
			
			<div class="col-md-9">
				<div class="row">
					<?php foreach ($trending_category as $key => $value) : ?>
					<div class="col-md-4" style="padding-bottom: 10px;">
					<?php
					$categoryurl1 = preg_replace('/\s+/', '-', $value['name']);
					 $categoryurl = str_replace('&amp;', 'amp', $categoryurl1);
					 
					?>
					<a href="home/courses?category=<?php echo $categoryurl; ?>&&price=all&&level=all&&language=all&&rating=all">
						<div class="p-3 rounded10 overflow-hidden position-relative text-white backgrndimg">
							<div class="">
								<h6 class="display-6 d-block l-h-n my-cust mx-0 fw-500"><?php echo $value['name']; ?></h6>
							</div>
							<i class="<?php echo $value['font_awesome_class']; ?> position-absolute pos-right pos-bottom opacity-35 mb-n1 mr-n1" style="font-size:5rem"></i>
						</div>
					</a>
					</div>
					<?php endforeach; ?>
					
				</div>
			
			</div>
			
			
			
			
			<?php /*
			<div class="col-md-9">
				<div class="row">
					<?php foreach ($trending_category as $key => $value) : ?>
					<div class="col-md-4" style="padding-bottom: 10px;">
						<div class="card cta-box bg-primary text-white">
							<div class="card-body">
								<div class="media align-items-center">
									<div class="media-body">
									<h2 class="mt-0"><i class="<?php echo $value['font_awesome_class']; ?> float-left"></i></h2>
									<h3 class="m-0 font-weight-normal cta-box-title"><?php echo $value['name']; ?></h3>
									</div>
								
								</div>
							</div>
						</div>
					</div>
					<?php endforeach; ?>
				</div>
			</div>
			*/ ?>
		
		</div>
		  
	
	
     
	  
	  
	  
    </div>
  </section>
<?php endif; ?>

<!-- #DK [ TRENDING CATEGORY end]-->

<section class="home-fact-area greybck become-ins">
  <div class="container-xl">
    <div class="row">
      <div class="col">
        <h2 class="mb-4 instructor text-center text-dark">
          <?php /* <img class="preicon" src="<?php echo base_url().'assets/frontend/logoicon.png'; ?>" height="35" width="35"/> */ ?>
          <?php echo get_phrase('become_an_instructor'); ?>
        </h2>
      </div>
    </div>
	
	<div class="row pb-3">
		<div class="col-sm-4 text-content text-center ">
			<div class="home-fact-box mr-md-auto ml-auto mr-auto">
				<div><i class="fas fa-chalkboard-teacher"></i></div>
				<div class="mt-2">
					<h4><?php echo get_phrase('teach_what_you_love'); ?></h4>
					<p>Course wings gives you the tools to create an online course and reach millions of potential students</p>
				</div>
			</div>
		</div>
		<div class="col-sm-4 text-content text-center ">
			<div class="home-fact-box mr-md-auto ml-auto mr-auto">
				<div><i class="far fa-lightbulb"></i></div>
				<div class="mt-2">
					<h4><?php echo get_phrase('inspire_students'); ?></h4>
					<p>Help people learn new skills, advance their careers and explore their hobbies by sharing your knowledge</p>
				</div>
			</div>
		</div>
		<div class="col-sm-4 text-content text-center ">
			<div class="home-fact-box mr-md-auto ml-auto mr-auto">
				<div><i class="fas fa-hand-holding-usd"></i></div>
				<div class="mt-2">
					<h4><?php echo get_phrase('earn_money'); ?></h4>
					<p>Earn money every time a student purchases your course. Get paid monthly through PayPal</p>
				</div>
			</div>
		</div>
	</div>
	
    
    <div class="row mt-4 mb-3">
		<div class="col-md-12">	  
			<?php if ($this->session->userdata('user_login') != 1): ?>
				<button type="button" class="btn btn-sign-in bg-yellow btn-block rounded10" data-toggle="modal" data-target="#becomeinstructormodal">How To Become An Instructor</button>
			<?php endif; ?>
			
			
			<?php if ($this->session->userdata('user_login') == 1): ?>
				<a class="btn btn-sign-in bg-yellow btn-block rounded25" href="<?php echo site_url('user'); ?>">Start Teaching !</a>
			<?php endif; ?>
		</div>
    </div>
  </div>
</section>

<!-- #DK [Trending Authors]-->
<?php if (!empty($trending_author)) : ?>

<section class="footer-top-widget-area whitebck testimon">
  <div class="container-xl">
	<div class="row">
      <div class="col">
        <h2 class="mb-4 instructor text-center text-dark">
          <?php /* <img class="preicon" src="<?php echo base_url().'assets/frontend/logoicon.png'; ?>" height="35" width="35"/> */ ?>
          <?php echo get_phrase('trending_instructors'); ?>
        </h2>
      </div>
    </div>
    <div class="row">
      
<?php foreach ($trending_author as $key => $value) : ?>
  <div class="col-md-3 col-sm-6">
	<a href="<?php echo site_url('home/instructor_page/'.$value['id']); ?>">
	<div class="card feat-tutors rounded10">
	
		<div class="card-body text-center rounded10 trendingauthorcard">
			<p>
			<img class="imgcircle img-fluid" src="<?php echo $this->user_model->get_user_image_url($value['id']); ?>" alt="card image">
			</p>
			<h5 class="card-title text-dark"><?php echo $value['first_name'] . ' ' . $value['last_name']; ?></h5>
			<p class="card-text text-muted">
				<?php if (!empty($value['biography'])) : ?>
				<?php
				
				
					$string = strip_tags($value['biography']);
					// if (strlen($string) > 120) {

					// truncate string
					$stringCut = substr($string, 0, 70);
					$endPoint = strrpos($stringCut, ' ');
					

					//if the string doesn't contain any space then it will cut without word basis.
					$string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
					// $string .= '... <a href="' . site_url('home/instructor_page/'.$value['id']); . '">Read More</a>';
					
					$string .= '...';
					
					echo $string;
				?>
				<?php endif; ?>
				
				<?php if (empty($value['biography'])) : ?>
					<?php
						$string = 'Author has not entered biography<br>Click to view Author Page';
						echo $string;
					?>
				<?php endif; ?>
			</p>
		</div>
	
	</div>
	</a>
  </div>
<?php endforeach; ?>
      
      
      
    </div>
  </div>
</section>
<?php endif; ?>

<!-- #DK [Trending Authors END]-->

<!-- #DK [Testimonials]-->
<?php if(count($testimonials->result_array()) > 0): ?>
<section class="footer-top-widget-area testimon greybck">
  <div class="container-xl">
	
	<div class="row">
	<div class="col-md-6">
		<div class="preview-video-box what-you-get-box2 sidebar rounded10">
        <a data-toggle="modal" data-target="#aboutusmodal">
          <img src="uploads/system/aboutus.jpg" alt="" class="w-100 img-fluid">
          <span class="preview-text"><?php/* echo get_phrase('about_us'); */?></span>
          <span class="play-btn"></span>
        </a>
      </div>
	</div>
	<div class="col-md-6">
		 <h2 class="mb-5 course-carousel-title">
          <?php echo get_phrase('student_testimonials'); ?>
        </h2>
		<div class="bs-example">
			<div id="testimonials_cw" class="carousel slide" data-ride="carousel">
				<!-- Carousel indicators -->
				<?php /*
				<ol class="carousel-indicators">
					<li data-target="#testimonials_cw" data-slide-to="0" class="active"></li>
					<li data-target="#testimonials_cw" data-slide-to="1"></li>
					<li data-target="#testimonials_cw" data-slide-to="2"></li>
					<li data-target="#testimonials_cw" data-slide-to="3"></li>
					<li data-target="#testimonials_cw" data-slide-to="4"></li>
				</ol>
				*/ ?>
				<!-- Wrapper for carousel items -->
				
				<div class="carousel-inner">
					<?php $count = 0;  
                    foreach ($testimonials->result_array() as $enrol) : 
                        $count++;
						$user_data = $this->db->get_where('users', array('id' => $enrol['user_id']))->row_array();
					?>
					<div class="carousel-item <?php if($count == 1){echo ' active';} ?>">
						<div class="row">
							<div class="col-md-3">
								<img class="img-circle2 img-fluid" style="height:130px;width:130px;" src="<?php echo $this->user_model->get_user_image_url($enrol['user_id']); ?>" alt="card image">
							</div>
							<div class="col-md-9 text-left">
								<h5 class="card-title testititle"><?php echo $user_data['first_name'] . ' ' . $user_data['last_name']; ?></h5>
								<p class="card-text text-muted"><?php echo nl2br(urldecode(strip_slashes($enrol['text']))); ?></p>
							</div>
						</div>	
						
					</div>
					<?php endforeach; ?>
					
					
					
					
				</div>
				<div class="float-right navi">
				<a class="" href="#testimonials_cw" role="button" data-slide="prev">
				  <span class="carousel-control-prev-icon ico" aria-hidden="true"></span>
				  <span class="sr-only">Previous</span>
				</a>
				<a class="" href="#testimonials_cw" role="button" data-slide="next">
				  <span class="carousel-control-next-icon ico" aria-hidden="true"></span>
				  <span class="sr-only">Next</span>
				</a>
				</div>
				
			</div>
		</div>
		
	</div>
	
	</div>
	
	
  </div>
</section>
<?php endif; ?>
<!-- #DK [Testimonials END]-->

<?php if ($this->session->userdata('user_login') == "1") : ?>

<?php else : ?>
<section class="footer-top-widget-area testimon purple mt-3">
	<div class="container-xl">
		<div class="row">
			<div class="col-md-12  text-center">
				<h2 class="mb-4 instructor text-light">Learn from experts all over the world or inspire students and teach them new skills</h2>
	
					<a class="btn btn-sign-in whitebck rounded25 px-5" style="color:#343a40!important;" href="<?php echo site_url('home/sign_up'); ?>">Sign Up Now !</a>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>

<div class="modal fade" id="aboutusmodal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content course-preview-modal">
			<div class="modal-header">
				<h5 class="modal-title"><span><?php echo get_phrase('about_us') ?></h5>
				<button type="button" class="close" data-dismiss="modal" onclick="pausePreview()">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="course-preview-video-wrap">
					<div class="embed-responsive embed-responsive-16by9">
					<link rel="stylesheet" href="<?php echo base_url();?>assets/global/plyr/plyr.css">
					<video poster="<?php echo base_url();?>uploads/system/aboutus.jpg" id="cplayer" playsinline controls allow="autoplay">
						<source src="<?php echo base_url();?>uploads/system/CW-ad-final.mp4" type="video/mp4">
					</video>
					
					<?php /*
					<video id="plyr-video" poster="uploads/system/aboutus.jpg" playsinline controls>
					  <source src="uploads/system/CW-ad.mp4" type="video/mp4">
					  <source src="/path/to/video.webm" type="video/webm">
					</video>
					*/ ?>

					<style media="screen">
					.plyr__video-wrapper {
					  height: 450px;
					}
					</style>

					<script src="<?php echo base_url();?>assets/global/plyr/plyr.js"></script>
					<script>
						const cplayer = new Plyr('#cplayer');
					</script>
					<!------------- PLYR.IO ------------>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="becomeinstructormodal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content course-preview-modal">
			<div class="modal-header">
				<h5 class="modal-title"><span><?php echo get_phrase('how_to_become_an_instructor') ?></h5>
				<button type="button" class="close" data-dismiss="modal" onclick="pausePreview()">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="course-preview-video-wrap">
					<div class="embed-responsive embed-responsive-16by9">
					<link rel="stylesheet" href="<?php echo base_url();?>assets/global/plyr/plyr.css">
					<video poster="<?php echo base_url();?>uploads/system/instructor.jpg" id="player" playsinline controls allow="autoplay">
						<source src="<?php echo base_url();?>uploads/system/CWinstructor.mp4" type="video/mp4">
					</video>
					
					<?php /*
					<video id="plyr-video" poster="uploads/system/aboutus.jpg" playsinline controls>
					  <source src="uploads/system/CW-ad.mp4" type="video/mp4">
					  <source src="/path/to/video.webm" type="video/webm">
					</video>
					*/ ?>

					<style media="screen">
					.plyr__video-wrapper {
					  height: 450px;
					}
					</style>

					<script src="<?php echo base_url();?>assets/global/plyr/plyr.js"></script>
					<script>
						const player = new Plyr('#player');
					</script>
					<!------------- PLYR.IO ------------>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php /*
<div class="modal fade" id="becomeinstructormodal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content course-preview-modal">
			<div class="modal-header">
				<h5 class="modal-title"><span><?php echo get_phrase('how_to_become_an_instructor') ?></h5>
				<button type="button" class="close" data-dismiss="modal" onclick="pausePreview()">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="course-preview-video-wrap">
					<link rel="stylesheet" href="<?php echo base_url();?>assets/global/plyr/plyr.css">
                <div class="plyr__video-embed" id="player2">
                  <iframe height="500" src="https://player.vimeo.com/video/378469843?loop=false&amp;byline=false&amp;portrait=false&amp;title=false&amp;speed=true&amp;transparent=0&amp;gesture=media" allowfullscreen allowtransparency allow="autoplay"></iframe>
                </div>

                <style media="screen">
					.plyr__video-wrapper {
					  height: 450px;
					}
					</style>

					<script src="<?php echo base_url();?>assets/global/plyr/plyr.js"></script>
					<script>
						const player = new Plyr('#player2');
					</script>
					<!------------- PLYR.IO ------------>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
*/ ?>
<style media="screen">
    .embed-responsive-16by9::before {
      padding-top : 0px;
    }
    </style>
<script type="text/javascript">
function handleWishList(elem) {

    $.ajax({
        url: '<?php echo site_url('home/handleWishList');?>',
        type : 'POST',
        data : {course_id : elem.id},
        success: function(response)
        {
            if (!response) {
                window.location.replace("<?php echo site_url('login'); ?>");
            }else {
                if ($(elem).hasClass('active')) {
                    $(elem).removeClass('active')
                }else {
                    $(elem).addClass('active')
                }
                $('#wishlist_items').html(response);
            }
        }
    });
}

function handleCartItems(elem) {
    url1 = '<?php echo site_url('home/handleCartItems');?>';
    url2 = '<?php echo site_url('home/refreshWishList');?>';
    $.ajax({
        url: url1,
        type : 'POST',
        data : {course_id : elem.id},
        success: function(response)
        {
            $('#cart_items').html(response);
            if ($(elem).hasClass('addedToCart')) {
                $('.big-cart-button-'+elem.id).removeClass('addedToCart')
                $('.big-cart-button-'+elem.id).text("<?php echo get_phrase('add_to_cart'); ?>");
            }else {
                $('.big-cart-button-'+elem.id).addClass('addedToCart')
                $('.big-cart-button-'+elem.id).text("<?php echo get_phrase('added_to_cart'); ?>");
            }
            $.ajax({
                url: url2,
                type : 'POST',
                success: function(response)
                {
                    $('#wishlist_items').html(response);
                }
            });
        }
    });
}

function handleEnrolledButton() {
    $.ajax({
        url: '<?php echo site_url('home/isLoggedIn');?>',
        success: function(response)
        {
            if (!response) {
                window.location.replace("<?php echo site_url('login'); ?>");
            }
        }
    });
}

function pausePreview() {
      player.pause();
    }
</script>

<script>
	$(document).ready(function() {
		var myColors = [
			'#007bff', '#fa5c7c', '#727cf5', '#0acf97', '#39afd1', '#6c757d'
		];
		var i = 0;
		$('div.backgrndimg').each(function() {
			$(this).css('background-color', myColors[i]);
			i = (i + 1) % myColors.length;
		});
	});
</script>
