<?php require_once(APPPATH . 'third_party/crypto.php'); ?>
<?php $ccavenue = json_decode(get_settings('ccavenue'), true); ?>

<?php

error_reporting(0);

$workingKey = $ccavenue[0]['working_key'];		//Working Key should be provided here.
$encResponse = $_POST["encResp"];			//This is the response sent by the CCAvenue Server
$rcvdString = decrypt($encResponse, $workingKey);		//Crypto Decryption used as per the specified working key.
$order_status = "";
$decryptValues = explode('&', $rcvdString);
$dataSize = sizeof($decryptValues);

// #DK		
// Parses the string into variables
parse_str($rcvdString, $output);
// dd($output);
$order_status = $output['order_status']; 
$method = 'ccAvenue';
$user_id = $output['merchant_param2'];
$amount_paid = $output['amount'];

if ($order_status === "Success") {
	echo "<br>Thank you for shopping with us. Your credit card has been charged and your transaction is successful. We will be shipping your order to you soon.";
	echo $user_id;
	print_r($_SESSION);
	redirect(base_url() . 'home/payment_success/' . $method . '/' . $user_id . '/' . $amount_paid);
} else if ($order_status === "Aborted") {
	$msg = "Thank you for shopping with us. However,the transaction has been aborted.";
	redirect(base_url() . 'home/ccavenue_cancelResponse?error=' . $msg);
} else if ($order_status === "Failure") {
	$msg = "Thank you for shopping with us. However,the transaction has been declined.";
	redirect(base_url() . 'home/ccavenue_cancelResponse?error=' . $msg);
} else {
	$msg = "Security Error. Illegal access detected";
	redirect(base_url() . 'home/ccavenue_cancelResponse?error=' . $msg);
}
/*
	echo "<center>";

	for($i = 0; $i < $dataSize; $i++) 
	{
		$information=explode('=',$decryptValues[$i]);
		if($i==3)	$order_status=$information[1];
	}

	if($order_status==="Success")
	{
		echo "<br>Thank you for shopping with us. Your credit card has been charged and your transaction is successful. We will be shipping your order to you soon.";
		
	}
	else if($order_status==="Aborted")
	{
		echo "<br>Thank you for shopping with us.We will keep you posted regarding the status of your order through e-mail";
	
	}
	else if($order_status==="Failure")
	{
		echo "<br>Thank you for shopping with us.However,the transaction has been declined.";
	}
	else
	{
		echo "<br>Security Error. Illegal access detected";
	
	}

	echo "<br><br>";

	echo "<table cellspacing=4 cellpadding=4>";
	for($i = 0; $i < $dataSize; $i++) 
	{
		$information=explode('=',$decryptValues[$i]);
	    	echo '<tr><td>'.$information[0].'</td><td>'.$information[1].'</td></tr>';
	}

	echo "</table><br>";
	echo "</center>";
	*/
?>
