<section class="category-header-area">
    <div class="container-lg">
        <div class="row">
            <div class="col">
                <h1 class="category-name">
                    <i class="fas fa-file-signature mr-2"></i><?php echo $page_title; ?>
                </h1>
            </div>
        </div>
    </div>
</section>

<section class="category-course-list-area">
    <div class="container-lg">
        <div class="row">
            <div class="col-md-9" style="padding: 35px 35px 35px 15px;">
                <?php echo get_frontend_settings('terms_and_condition'); ?>
            </div>
			<div class="col-md-3">
				<div class="course-sidebar-text-box side mt-4 bg-orange">	
					<div class="promo">
						<ul class="list-group list-group-flush">
							<li class="list-group-item bg-orange">
								<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fas fa-desktop"></i></div>
								<div>
									<p class="txt-purple font-weight-bold mb-0">
									<?php 
										$status_wise_courses = $this->crud_model->get_status_wise_courses();
										$number_of_courses = $status_wise_courses['active']->num_rows();
										echo get_phrase('online_courses'); 
									?>
									</p>
									
									<p><?php echo $number_of_courses.' '.get_phrase('topics_to_learn_from'); ?></p>
								</div>
							</li>
							<li class="list-group-item bg-orange">
								<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fa fa-key"></i></div>
								<div>
									<p class="txt-purple font-weight-bold mb-0"><?php echo get_phrase('lifetime_access'); ?></p>
									<p><?php echo get_phrase('learn_as_per_your_convenience'); ?></p>
								</div>
							</li>
							<li class="list-group-item bg-orange">
								<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fas fa-chalkboard-teacher"></i></div>
								<div>
									<p class="txt-purple font-weight-bold mb-0"><?php echo get_phrase('inspire_students'); ?></p>
									<p><?php echo get_phrase('help_people_learn_new_skills'); ?></p>
								</div>
							</li>
							<li class="list-group-item bg-orange">
								<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fas fa-user-graduate"></i></div>
								<div>
									<p class="txt-purple font-weight-bold mb-0"><?php echo get_phrase('course_completion_acknowledgement'); ?></p>
									<p><?php echo get_phrase('get_proof_of_course_completion'); ?></p>
								</div>
							</li>
							
						</ul>
					</div>
						
				</div>
			</div>
        </div>
    </div>
</section>
