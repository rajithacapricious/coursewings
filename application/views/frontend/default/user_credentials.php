<section class="page-header-area my-course-area">
    <div class="container-lg">
        <div class="row">
            <div class="col">
                <h1 class="page-title"><i class="fas fa-key mr-2"></i><?php echo get_phrase('my_credentials'); ?></h1>
                <ul>
					<li class=""><a href="<?php echo site_url('home/my_courses'); ?>"><i class="fas fa-book-reader mr-1"></i><?php echo get_phrase('subscribed_courses'); ?></a></li>
					<li><a href="<?php echo site_url('home/my_wishlist'); ?>"><i class="fas fa-hand-holding-heart mr-1"></i><?php echo get_phrase('my_wishlists'); ?></a></li>
					<?php /* <li><a href="<?php echo site_url('home/my_messages'); ?>"><?php echo get_phrase('my_messages'); ?></a></li> */ ?>
					<li><a href="<?php echo site_url('home/purchase_history'); ?>"><i class="fas fa-cart-plus mr-1"></i><?php echo get_phrase('purchase_history'); ?></a></li>
					<?php /* <li><a href="<?php echo site_url('home/profile/user_profile'); ?>"><?php echo get_phrase('user_profile'); ?></a></li> */ ?>
					<li class="nav-item dropdown active">
						<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user-edit mr-1"></i><?php echo get_phrase('my_profile'); ?></a>
						<div class="dropdown-menu profile">
							<a class="dropdown-item px-3 py-2" href="<?php echo site_url('home/profile/user_profile'); ?>">
								<i class="fas fa-user-edit mr-1"></i><?php echo get_phrase('edit_profile'); ?>
							</a>
							<a class="dropdown-item px-3 py-2 active" href="<?php echo site_url('home/profile/user_credentials'); ?>">
								<i class="fas fa-key mr-1"></i><?php echo get_phrase('change_password'); ?>
							</a>
							<a class="dropdown-item px-3 py-2" href="<?php echo site_url('home/profile/user_photo'); ?>">
								<i class="far fa-image mr-1"></i><?php echo get_phrase('edit_photo'); ?>
							</a>
						</div>
					</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="user-dashboard-area">
    <div class="container-lg">
        <div class="row">
			<div class="col-lg-3">
				<div class="cart-sidebar mb-3" style="margin-top:40px;">
                    <div class="user-box text-center">
						<img src="<?php echo base_url().'uploads/user_image/'.$this->session->userdata('user_id').'.jpg';?>" alt="" class="img-fluid profile">
						<div class="name text-center mt-3">
							<div class="name"><?php echo $user_details['first_name'].' '.$user_details['last_name']; ?></div>
						</div>
					</div>
					
					<a class="btn btn-info btn-block checkout-btn mt-3" href="<?php echo site_url('home/profile/user_profile'); ?>">
						<?php echo get_phrase('profile'); ?>
					</a>
					<a class="btn btn-info btn-block checkout-btn mt-3 active" href="<?php echo site_url('home/profile/user_credentials'); ?>">
						<?php echo get_phrase('change_password'); ?>
					</a>
					<a class="btn btn-info btn-block checkout-btn mt-3" href="<?php echo site_url('home/profile/user_photo'); ?>">
						<?php echo get_phrase('photo'); ?>
					</a>
                </div>
			</div>
			
			<div class="col-lg-9">
                <div class="user-dashboard-box mb-5" style="margin-top:40px;">
                    
                    <div class="user-dashboard-content" style="width:100%;">
                        <div class="p-4 bg-yellow">
							<div class="h4 font-weight-bold"><?php echo get_phrase('account_settings'); ?></div>
                            <div class="subtitle"><?php echo get_phrase('edit_your_account_settings'); ?>.</div>
						</div>
						
						<form action="<?php echo site_url('home/update_profile/update_credentials'); ?>" method="post">
							<div class="content-box">
                                <div class="p-4">
									<div class="row">
										<div class="email-group col-md-12" hidden>
											<div class="form-group">
												<label for="email"><?php echo get_phrase('email'); ?>:</label>
												<input type="email" class="form-control" name = "email" id="email" placeholder="<?php echo get_phrase('email'); ?>" value="<?php echo $user_details['email']; ?>">
											</div>
										</div>
										<div class="form-group">
											<label>Your Account Email ID is: <i><?php echo $user_details['email']; ?></i></label>
										</div>
									</div>
									
									<div class="row">
										<div class="password-group col-md-12">
											<div class="form-group">
												<label for="password"><?php echo get_phrase('enter_current_password'); ?>:</label>
												<input type="password" class="form-control" id="current_password" name = "current_password" placeholder="<?php echo get_phrase('enter_current_password'); ?>" required>
											</div>
											<div class="form-group">
												<label for="new_password"><?php echo get_phrase('new_password'); ?>:</label>
												<input type="password" class="form-control" name = "new_password" placeholder="<?php echo get_phrase('enter_new_password'); ?>" required>
											</div>
											<div class="form-group">
												<input type="password" class="form-control" name = "confirm_password" placeholder="<?php echo get_phrase('re-type_your_new_password'); ?>" required>
											</div>
										</div>
									</div>
									
								</div>
							</div>
							<div class="p-4">
                                <button type="submit" class="btn btn-block"><i class="fas fa-save mr-2"></i><?php echo get_phrase('save'); ?></button>
                            </div>
						</form>
						
					</div>
				</div>
			</div>
		
            
        </div>
    </div>
</section>
