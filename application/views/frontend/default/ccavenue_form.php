<html>

<head>
	<title>ccAvenue CHECKOUT</title>
	<script>
		window.onload = function() {
			var d = new Date().getTime();
			document.getElementById("tid").value = document.getElementById("order_id").value = d;
			document.forms['customerData'].submit();
		};
	</script>
</head>

<body>
	<div class="container">
		<div class="row">
			<p>Please do not refresh or close your browser, redirecting to ccAvenue...</p>
			<form method="post" name="customerData" action="<?php echo base_url() . "home/ccavenue_sendRequest"; ?>" accept-charset="ISO-8859-1">
				<!-- Required Parameters -->
				<input type="hidden" name="tid" id="tid" readonly />
				<input type="hidden" name="merchant_id" value="<?php echo $ccavenue[0]['merchant_id']; ?>" readonly />
				<input type="hidden" name="order_id" id="order_id" />
				<input type="hidden" name="currency" value="<?php echo $ccavenue[0]['currency']; ?>" />
				<input type="hidden" name="amount" value="<?php echo $amount_to_pay; ?>" />
				<input type="hidden" name="redirect_url" value="<?php echo base_url() . "home/ccavenue_getResponse"; ?>" />
				<input type="hidden" name="cancel_url" value="<?php echo base_url() . "home/ccavenue_cancelResponse"; ?>" />
				<input type="hidden" name="language" value="EN" />

				<input type="hidden" name="billing_name" value="<?php echo $user_details['first_name'] . ' ' . $user_details['last_name']; ?>" />
				<input type="hidden" name="billing_email" value="<?php echo $user_details['email']; ?>" />
				<input type="hidden" name="merchant_param2" id="customer_id" value="<?php echo $user_details['id']; ?>" />
			</form>

		</div>
	</div>
</body>

</html>