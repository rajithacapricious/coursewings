<?php require_once(APPPATH . 'third_party/crypto.php'); ?>

<html>

<head>
	<title>ccAvenue CHECKOUT</title>
</head>

<body>
	<p>Please do not refresh or close your browser, redirecting to ccAvenue...</p>
	<center>
		<?php
		$ccavenue = json_decode(get_settings('ccavenue'), true);
		$ccavenue_url = 'https://secure.ccavenue.ae/transaction/transaction.do?command=initiateTransaction';//'http://localhost/testpayment.php';//'https://secure.ccavenue.ae/transaction/transaction.do?command=initiateTransaction';
		error_reporting(0);

		$merchant_data = '';
		$working_key = $ccavenue[0]['working_key']; //Shared by CCAVENUES
		$access_code = $ccavenue[0]['access_code']; //Shared by CCAVENUES

		foreach ($_POST as $key => $value) {
			$merchant_data .= $key . '=' . $value . '&';
		}

		$encrypted_data = encrypt($merchant_data, $working_key); // Method for encrypting the data.

		?>
		<form method="post" name="redirect" action="<?php echo $ccavenue_url; ?>">
			<?php
			echo "<input type=hidden name=encRequest value=$encrypted_data>";
			echo "<input type=hidden name=access_code value=$access_code>";
			?>
		</form>
	</center>
	<script language='javascript'>
		document.redirect.submit();
	</script>
</body>

</html>