<section class="page-header-area my-course-area">
    <div class="container-lg">
        <div class="row">
            <div class="col">
                <h1 class="page-title"><i class="far fa-image mr-2"></i><?php echo get_phrase('my_profile_picture'); ?></h1>
                <ul>
					<li class=""><a href="<?php echo site_url('home/my_courses'); ?>"><i class="fas fa-book-reader mr-1"></i><?php echo get_phrase('subscribed_courses'); ?></a></li>
					<li><a href="<?php echo site_url('home/my_wishlist'); ?>"><i class="fas fa-hand-holding-heart mr-1"></i><?php echo get_phrase('my_wishlists'); ?></a></li>
					<?php /* <li><a href="<?php echo site_url('home/my_messages'); ?>"><?php echo get_phrase('my_messages'); ?></a></li> */ ?>
					<li><a href="<?php echo site_url('home/purchase_history'); ?>"><i class="fas fa-cart-plus mr-1"></i><?php echo get_phrase('purchase_history'); ?></a></li>
					<?php /* <li><a href="<?php echo site_url('home/profile/user_profile'); ?>"><?php echo get_phrase('user_profile'); ?></a></li> */ ?>
					<li class="nav-item dropdown active">
						<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user-edit mr-1"></i><?php echo get_phrase('my_profile'); ?></a>
						<div class="dropdown-menu profile">
							<a class="dropdown-item px-3 py-2" href="<?php echo site_url('home/profile/user_profile'); ?>">
								<i class="fas fa-user-edit mr-1"></i><?php echo get_phrase('edit_profile'); ?>
							</a>
							<a class="dropdown-item px-3 py-2" href="<?php echo site_url('home/profile/user_credentials'); ?>">
								<i class="fas fa-key mr-1"></i><?php echo get_phrase('change_password'); ?>
							</a>
							<a class="dropdown-item px-3 py-2 active" href="<?php echo site_url('home/profile/user_photo'); ?>">
								<i class="far fa-image mr-1"></i><?php echo get_phrase('edit_photo'); ?>
							</a>
						</div>
					</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="user-dashboard-area">
    <div class="container-lg">
        <div class="row">
			<div class="col-lg-3">
				<div class="cart-sidebar mb-3" style="margin-top:40px;">
                    <div class="user-box text-center">
						<img src="<?php echo base_url().'uploads/user_image/'.$this->session->userdata('user_id').'.jpg';?>" alt="" class="img-fluid profile">
						<div class="name text-center mt-3">
							<div class="name"><?php echo $user_details['first_name'].' '.$user_details['last_name']; ?></div>
						</div>
					</div>
					
					<a class="btn btn-info btn-block checkout-btn mt-3" href="<?php echo site_url('home/profile/user_profile'); ?>">
						<?php echo get_phrase('profile'); ?>
					</a>
					<a class="btn btn-info btn-block checkout-btn mt-3 active" href="<?php echo site_url('home/profile/user_credentials'); ?>">
						<?php echo get_phrase('change_password'); ?>
					</a>
					<a class="btn btn-info btn-block checkout-btn mt-3" href="<?php echo site_url('home/profile/user_photo'); ?>">
						<?php echo get_phrase('photo'); ?>
					</a>
                </div>
			</div>
			
			<div class="col-lg-9">
                <div class="user-dashboard-box mb-5" style="margin-top:40px;">
                    
                    <div class="user-dashboard-content" style="width:100%;">
                        <div class="p-4 bg-yellow">
							<div class="h4 font-weight-bold"><?php echo get_phrase('update_your_photo'); ?></div>
                            <div class="subtitle"></div>
						</div>
						
						<form action="<?php echo site_url('home/update_profile/update_photo'); ?>" enctype="multipart/form-data" method="post">
							<div class="content-box">
                                <div class="p-4">
									<div class="row">
										<div class="col-md-12 mb-2">
											<label class="mr-2" for=""><?php echo get_phrase('current_image'); ?>:</label>
											<img class = "rounded-circle img-thumbnail" src="<?php echo base_url().'uploads/user_image/'.$this->session->userdata('user_id').'.jpg';?>" alt="" style="height: 50px; width: 50px;">
										</div>
										<div class="form-group col-md-12">
											<label for="user_image"><?php echo get_phrase('upload_new_image_(_size:_600x600px_)_'); ?>: </label>
											<input type="file" class="form-control" name = "user_image" id="user_image">
										</div>
										
									</div>
								</div>
							</div>
							<div class="p-4">
                                <button type="submit" class="btn btn-block"><i class="fas fa-save mr-2"></i><?php echo get_phrase('save'); ?></button>
                            </div>
						</form>
						
					</div>
				</div>
			</div>
			
			
            
        </div>
    </div>
</section>
