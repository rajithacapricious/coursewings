<?php
isset($layout) ? "": $layout = "list";
isset($selected_category_id) ? "": $selected_category_id = "all";
isset($selected_level) ? "": $selected_level = "all";
isset($selected_language) ? "": $selected_language = "all";
isset($selected_rating) ? "": $selected_rating = "all";
isset($selected_price) ? "": $selected_price = "all";
// echo $selected_category_id.'-'.$selected_level.'-'.$selected_language.'-'.$selected_rating.'-'.$selected_price;
$number_of_visible_categories = 0;
if (isset($sub_category_id)) {
    $sub_category_details = $this->crud_model->get_category_details_by_id($sub_category_id)->row_array();
    $category_details     = $this->crud_model->get_categories($sub_category_details['parent'])->row_array();
    $category_name        = $category_details['name'];
    $sub_category_name    = $sub_category_details['name'];
}
?>

<section class="category-header-area">
    <div class="container-lg">
        <div class="row">
            <div class="col">
				<h1 class="category-name">
                    <i class="fas fa-book-reader mr-2"></i><?php echo get_phrase('courses'); ?>
                </h1>
				<?php
                                if ($selected_category_id == "all") {
                                    echo get_phrase('category_:_');
									echo get_phrase('_all');
                                }else {
                                    $category_details = $this->crud_model->get_category_details_by_id($selected_category_id)->row_array();
                                    echo get_phrase('category:');
									echo $category_details['name'];
                                }
                             ?>
							 <span class="ml-4"><i><?php echo get_phrase('showing_on_this_page'); ?> : <?php echo count($courses); ?></span><span class="ml-2">Courses</i></span>
                
            </div>
        </div>
    </div>
</section>


<section class="category-course-list-area mt-4">
    <div class="container-lg">
		<?php /*
        <div class="category-filter-box filter-box clearfix">
            
			<span><?php echo get_phrase('showing_on_this_page'); ?> : <?php echo count($courses); ?></span>
            <a href="javascript::" onclick="toggleLayout('grid')" style="float: right; font-size: 19px; margin-left: 5px;"><i class="fas fa-th"></i></a>
            <a href="javascript::" onclick="toggleLayout('list')" style="float: right; font-size: 19px;"><i class="fas fa-th-list"></i></a>
            <a href="<?php echo site_url('home/courses'); ?>" style="float: right; font-size: 19px; margin-right: 5px;"><i class="fas fa-sync-alt"></i></a>
			
			<a class="btn btn-xs" href="<?php echo site_url('home/courses'); ?>" style="float: right; font-size: 19px; margin-right: 5px;"><i class="fas fa-retweet mr-1"></i><?php echo get_phrase('reset'); ?></a>
			
        </div>
		*/ ?>
        <div class="row">
            <div class="col-lg-2 filter-area">
                <div class="card">
                    <a href="javascript::"  style="color: unset;">
                        <div class="card-header filter-card-header" id="headingOne" data-toggle="collapse" data-target="#collapseFilter" aria-expanded="true" aria-controls="collapseFilter">
                            <h6 class="mb-0">
                                <?php echo get_phrase('filter'); ?>
                                <i class="fas fa-sliders-h" style="float: right;"></i>
                            </h6>
                        </div>
                    </a>
                    <div id="collapseFilter" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body pt-0 px-3">
							<a class="btn btn-sm btn-block mt-2" href="<?php echo site_url('home/courses'); ?>" style=""><i class="fas fa-retweet mr-1"></i><?php echo get_phrase('reset_filters'); ?></a>
                            <div class="filter_type">
                                <h6><?php echo get_phrase('categories'); ?></h6>
                                <ul>
                                   
									<li class="">
                                        <div class="">
                                            <input type="radio" id="category_all" name="sub_category" class="categories custom-radio" value="all" onclick="filter(this)" <?php if($selected_category_id == 'all') echo 'checked'; ?>>
                                            <label for="category_all"><?php echo get_phrase('all_categories'); ?></label>
                                        </div>
                                    </li>
									
                                    <?php
                                    $counter = 1;
                                    $total_number_of_categories = $this->db->get('category')->num_rows();
                                    $categories = $this->crud_model->get_categories()->result_array();
                                    foreach ($categories as $category): ?>
                                        <li class="">
                                            <div class="<?php if ($counter > $number_of_visible_categories): ?> hidden-categories hidden <?php endif; ?>">
                                                <input type="radio" id="category-<?php echo $category['id'];?>" name="sub_category" class="categories custom-radio" value="<?php echo $category['slug']; ?>" onclick="filter(this)" <?php if($selected_category_id == $category['id']) echo 'checked'; ?>>
                                                <label for="category-<?php echo $category['id'];?>"><?php echo $category['name']; ?></label>
                                            </div>
                                        </li>
                                        <?php foreach ($this->crud_model->get_sub_categories($category['id']) as $sub_category):
                                            $counter++; ?>
											 <?php /*
                                            <li class="ml-2">
                                                <div class="<?php if ($counter > $number_of_visible_categories): ?> hidden-categories hidden <?php endif; ?>">
                                                    <input type="radio" id="sub_category-<?php echo $sub_category['id'];?>" name="sub_category" class="categories custom-radio" value="<?php echo $sub_category['slug']; ?>" onclick="filter(this)" <?php if($selected_category_id == $sub_category['id']) echo 'checked'; ?>>
                                                    <label for="sub_category-<?php echo $sub_category['id'];?>"><?php echo $sub_category['name']; ?></label>
                                                </div>
                                            </li>
											*/ ?>
                                        <?php endforeach; ?>
                                    <?php endforeach; ?>
                                </ul>
                                <a class="btn btn-sm" href="javascript::" id = "city-toggle-btn" onclick="showToggle(this, 'hidden-categories')" style="font-size: 12px;"><?php echo $total_number_of_categories > $number_of_visible_categories ? get_phrase('show_more') : ""; ?></a>
                            </div>
                            <hr>
                            <div class="filter_type">
                                <div class="form-group">
                                    <h6><?php echo get_phrase('price'); ?></h6>
                                    <ul>
                                        <li>
                                            <div class="">
                                                <input type="radio" id="price_all" name="price" class="prices custom-radio" value="all" onclick="filter(this)" <?php if($selected_price == 'all') echo 'checked'; ?>>
                                                <label for="price_all"><?php echo get_phrase('all'); ?></label>
                                            </div>
                                            <div class="">
                                                <input type="radio" id="price_free" name="price" class="prices custom-radio" value="free" onclick="filter(this)" <?php if($selected_price == 'free') echo 'checked'; ?>>
                                                <label for="price_free"><?php echo get_phrase('free'); ?></label>
                                            </div>
                                            <div class="">
                                                <input type="radio" id="price_paid" name="price" class="prices custom-radio" value="paid" onclick="filter(this)" <?php if($selected_price == 'paid') echo 'checked'; ?>>
                                                <label for="price_paid"><?php echo get_phrase('paid'); ?></label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <hr>
                            <div class="filter_type">
                                <h6><?php echo get_phrase('level'); ?></h6>
                                <ul>
                                    <li>
                                        <div class="">
                                            <input type="radio" id="all" name="level" class="level custom-radio" value="all" onclick="filter(this)" <?php if($selected_level == 'all') echo 'checked'; ?>>
                                            <label for="all"><?php echo get_phrase('all'); ?></label>
                                        </div>
                                        <div class="">
                                            <input type="radio" id="beginner" name="level" class="level custom-radio" value="beginner" onclick="filter(this)" <?php if($selected_level == 'beginner') echo 'checked'; ?>>
                                            <label for="beginner"><?php echo get_phrase('beginner'); ?></label>
                                        </div>
                                        <div class="">
                                            <input type="radio" id="advanced" name="level" class="level custom-radio" value="advanced" onclick="filter(this)" <?php if($selected_level == 'advanced') echo 'checked'; ?>>
                                            <label for="advanced"><?php echo get_phrase('advanced'); ?></label>
                                        </div>
                                        <div class="">
                                            <input type="radio" id="intermediate" name="level" class="level custom-radio" value="intermediate" onclick="filter(this)" <?php if($selected_level == 'intermediate') echo 'checked'; ?>>
                                            <label for="intermediate"><?php echo get_phrase('intermediate'); ?></label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            
                            <div class="filter_type" hidden>
                                <h6><?php echo get_phrase('language'); ?></h6>
                                <ul>
                                    <li>
                                        <div class="">
                                            <input type="radio" id="all_language" name="language" class="languages custom-radio" value="<?php echo 'all'; ?>" onclick="filter(this)" <?php if($selected_language == "all") echo 'checked'; ?>>
                                            <label for="<?php echo 'all_language'; ?>"><?php echo get_phrase('all'); ?></label>
                                        </div>
                                    </li>
                                    <?php
                                    $languages = $this->crud_model->get_all_languages();
                                    foreach ($languages as $language): ?>
                                        <li>
                                            <div class="">
                                                <input type="radio" id="language_<?php echo $language; ?>" name="language" class="languages custom-radio" value="<?php echo $language; ?>" onclick="filter(this)" <?php if($selected_language == $language) echo 'checked'; ?>>
                                                <label for="language_<?php echo $language; ?>"><?php echo ucfirst($language); ?></label>
                                            </div>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                            <hr>
                            <div class="filter_type">
                                <h6><?php echo get_phrase('ratings'); ?></h6>
                                <ul>
                                    <li>
                                        <div class="">
                                            <input type="radio" id="all_rating" name="rating" class="ratings custom-radio" value="<?php echo 'all'; ?>" onclick="filter(this)" <?php if($selected_rating == "all") echo 'checked'; ?>>
                                            <label for="all_rating"><?php echo get_phrase('all'); ?></label>
                                        </div>
                                    </li>
                                    <?php for($i = 1; $i <= 5; $i++): ?>
                                        <li>
                                            <div class="">
                                                <input type="radio" id="rating_<?php echo $i; ?>" name="rating" class="ratings custom-radio" value="<?php echo $i; ?>" onclick="filter(this)" <?php if($selected_rating == $i) echo 'checked'; ?>>
                                                <label for="rating_<?php echo $i; ?>">
                                                    <?php for($j = 1; $j <= $i; $j++): ?>
                                                        <i class="fas fa-star" style="color: #f4c150;"></i>
                                                    <?php endfor; ?>
                                                    <?php for($j = $i; $j < 5; $j++): ?>
                                                        <i class="far fa-star" style="color: #dedfe0;"></i>
                                                    <?php endfor; ?>
                                                </label>
                                            </div>
                                        </li>
                                    <?php endfor; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-10">
				<div class="row">
					<div class="col-md-9">
						<div class="category-course-list">
							<?php include 'category_wise_course_'.$layout.'_layout.php'; ?>
							<?php if (count($courses) == 0): ?>
								<div class="img-fluid w-100 text-center">
								  <img style="opacity: 1; width: 100px;" src="<?php echo base_url('assets/backend/images/notfound.svg'); ?>"><br>
								  <?php echo get_phrase('no_data_found'); ?>
								  
								</div>
								<div class="w-100 text-center mt-2">
									<a class="btn btn-primary btn-xs" href="<?php echo site_url('home/courses'); ?>"><i class="fas fa-caret-square-left mr-1"></i>Back to All Courses</a>
								</div>
							<?php endif; ?>
						</div>
						<nav>
							<?php if ($selected_category_id == "all" && $selected_price == 0 && $selected_level == 'all' && $selected_language == 'all' && $selected_rating == 'all'){
								echo $this->pagination->create_links();
							}?>
						</nav>
					</div>
					<div class="col-md-3">
						<div class="course-sidebar-text-box side bg-orange">	
							<div class="promo">
								<ul class="list-group list-group-flush">
									<li class="list-group-item bg-orange">
										<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fas fa-desktop"></i></div>
										<div>
											<p class="txt-purple font-weight-bold mb-0">
											<?php 
												$status_wise_courses = $this->crud_model->get_status_wise_courses();
												$number_of_courses = $status_wise_courses['active']->num_rows();
												echo get_phrase('online_courses'); 
											?>
											</p>
											
											<p><?php echo $number_of_courses.' '.get_phrase('topics_to_learn_from'); ?></p>
										</div>
									</li>
									<li class="list-group-item bg-orange">
										<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fa fa-key"></i></div>
										<div>
											<p class="txt-purple font-weight-bold mb-0"><?php echo get_phrase('lifetime_access'); ?></p>
											<p><?php echo get_phrase('learn_as_per_your_convenience'); ?></p>
										</div>
									</li>
									<li class="list-group-item bg-orange">
										<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fas fa-chalkboard-teacher"></i></div>
										<div>
											<p class="txt-purple font-weight-bold mb-0"><?php echo get_phrase('inspire_students'); ?></p>
											<p><?php echo get_phrase('help_people_learn_new_skills'); ?></p>
										</div>
									</li>
									<li class="list-group-item bg-orange">
										<div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center sidebaricon"><i class="fas fa-user-graduate"></i></div>
										<div>
											<p class="txt-purple font-weight-bold mb-0"><?php echo get_phrase('course_completion_acknowledgement'); ?></p>
											<p><?php echo get_phrase('get_proof_of_course_completion'); ?></p>
										</div>
									</li>
									
								</ul>
							</div>
								
						</div>
					</div>
				</div>
                
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

function get_url() {
    var urlPrefix 	= '<?php echo site_url('home/courses?'); ?>'
    var urlSuffix = "";
    var slectedCategory = "";
    var selectedPrice = "";
    var selectedLevel = "";
    var selectedLanguage = "";
    var selectedRating = "";

    // Get selected category
    $('.categories:checked').each(function() {
        slectedCategory = $(this).attr('value');
    });

    // Get selected price
    $('.prices:checked').each(function() {
        selectedPrice = $(this).attr('value');
    });

    // Get selected difficulty Level
    $('.level:checked').each(function() {
        selectedLevel = $(this).attr('value');
    });

    // Get selected difficulty Level
    $('.languages:checked').each(function() {
        selectedLanguage = $(this).attr('value');
    });

    // Get selected rating
    $('.ratings:checked').each(function() {
        selectedRating = $(this).attr('value');
    });

    urlSuffix = "category="+slectedCategory+"&&price="+selectedPrice+"&&level="+selectedLevel+"&&language="+selectedLanguage+"&&rating="+selectedRating;
    var url = urlPrefix+urlSuffix;
    return url;
}
function filter() {
    var url = get_url();
    window.location.replace(url);
    //console.log(url);
}

function toggleLayout(layout) {
    $.ajax({
        type : 'POST',
        url : '<?php echo site_url('home/set_layout_to_session'); ?>',
        data : {layout : layout},
        success : function(response){
            location.reload();
        }
    });
}

function showToggle(elem, selector) {
    $('.'+selector).slideToggle(20);
    if($(elem).text() === "<?php echo get_phrase('show_more'); ?>")
    {
        $(elem).text('<?php echo get_phrase('show_less'); ?>');
    }
    else
    {
        $(elem).text('<?php echo get_phrase('show_more'); ?>');
    }
}
</script>
