<?php
if (sizeof($my_courses) > 0):
  foreach ($my_courses as $my_course):
          $instructor_details = $this->user_model->get_all_user($my_course['user_id'])->row_array();?>
      <div class="col-lg-3">
							<div class="course-box-wrap">
								<div class="course-box">
									<div class="course-badge position best-seller price instrctr">
										<p class="intructor-badge">
											<i class="fas fa-chalkboard-teacher mr-1"> :</i>
											
											<a href="<?php echo site_url('home/instructor_page/'.$instructor_details['id']); ?>">
												<img src="<?php echo $this->user_model->get_user_image_url($instructor_details['id']);?>" class="imgcirclesmall" alt="">
												<?php echo $instructor_details['first_name'].' '.$instructor_details['last_name']; ?>
											</a>
										</p>
									</div>
									
									<div class="course-image">
										<a href="<?php echo site_url('home/course/'.slugify($my_course['title']).'/'.$my_course['id']); ?>">
											<img src="<?php echo $this->crud_model->get_course_thumbnail_url($my_course['id']); ?>" alt="" class="img-fluid">
										</a>
										<div class="instructor-img-hover">
											
											
										</div>
										
									</div>
									<div class="course-details">
										<a href="<?php echo site_url('home/course/'.slugify($my_course['title']).'/'.$my_course['id']); ?>">
											<h5 class="title"><?php echo $my_course['title']; ?></h5>
										</a>
										

										
									</div>
									<div class="ratenow mycourses">
										<div>
											<span class="lsnhrswish float-left">
												<span>
													<?php
														$lessons = $this->crud_model->get_lessons('course', $my_course['id'])->num_rows();
														echo $lessons.' '.get_phrase('chapters');
													?>
													(<?php echo $this->crud_model->get_total_duration_of_lesson_by_course_id($my_course['id']);?>)
												</span>
												
											</span>
											<span class="lsnhrswish big">
												<?php if ($my_course['is_free_course'] == 1): ?>
													<p class="price text-right"><?php echo get_phrase('free'); ?></p>
												<?php else: ?>
													<?php if ($my_course['discount_flag'] == 1): ?>
														<p class="price text-right">
															<small class="cross"><?php echo currency($my_course['price']); ?></small>
															<?php echo currency($my_course['discounted_price']); ?>
														</p>
													<?php else: ?>
														<p class="price text-right"><?php echo currency($my_course['price']); ?></p>
													<?php endif; ?>
												<?php endif; ?>
											</span>
										</div>
									</div>
									<div>
										<?php if ($my_course['is_free_course'] == 1):
											if($this->session->userdata('user_login') != 1) {
												$url = "#";
											}else {
												$url = site_url('home/get_enrolled_to_free_course/'.$my_course['id']);
											}?>
											<a href="<?php echo $url; ?>" class="btn add-to-cart-btn btn-block big-cart-button" onclick="handleEnrolledButton()"><i class="fas fa-shopping-cart"></i><?php echo get_phrase('get_enrolled'); ?></a>
										<?php else: ?>
											<button type="button" class="btn add-to-cart-btn btn-block <?php if(in_array($my_course['id'], $cart_items)) echo 'addedToCart'; ?> big-cart-button-<?php echo $my_course['id'];?>" id = "<?php echo $my_course['id']; ?>" onclick="handleCartItems(this)">
												<i class="fas fa-shopping-cart"></i><?php
												if(in_array($my_course['id'], $cart_items))
												echo get_phrase('added_to_cart');
												else
												echo get_phrase('add_to_cart');
												?>
											</button>
										<?php endif; ?>
									</div>
									
									
									<div class="wishlist-add wishlisted bottom" style="background: #f7a823;padding: 5px;border: 1px solid #f7a823;border-radius: 3px;">
										<button type="button" data-toggle="tooltip" data-placement="left" title="" style="cursor : pointer;" onclick="handleWishList(this)" id = "<?php echo $my_course['id']; ?>">
											<i class="fas fa-trash-alt" style="-webkit-text-stroke: 0px #fff;text-stroke: 0px #fff;"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
  <?php endforeach; ?>
<?php endif; ?>
