<?php /*
  <footer class="footer-area d-print-none">
  <div class="container-xl">
  <div class="row">
  <div class="col-md-6">
  <p class="copyright-text">
  <a href=""><img src="<?php echo base_url().'uploads/system/logo-dark.png'; ?>" alt="" class="d-inline-block" width="110"></a>
  <a href="<?php echo get_settings('footer_link'); ?>" target="_blank"><?php echo get_settings('footer_text'); ?></a>
  </p>
  </div>
  <div class="col-md-6">
  <ul class="nav justify-content-md-end footer-menu">
  <li class="nav-item">
  <a class="nav-link" href="<?php echo site_url('home/about_us'); ?>"><?php echo get_phrase('about_us'); ?></a>
  </li>
  <li class="nav-item">
  <a class="nav-link" href="<?php echo site_url('home/privacy_policy'); ?>"><?php echo get_phrase('privacy_policy'); ?></a>
  </li>
  <li class="nav-item">
  <a class="nav-link" href="<?php echo site_url('home/terms_and_condition'); ?>"><?php echo get_phrase('terms_&_conditions'); ?></a>
  </li>

  <li class="nav-item">
  <a class="nav-link" href="<?php echo site_url('home/login'); ?>">
  <?php echo get_phrase('login'); ?>
  </a>
  </li>

  <li class="nav-item">
  <?php if ($this->session->userdata('user_login') == "1"): ?>
  <a type="button" class="nav-link" data-toggle="modal" data-target="#testimonial" style="color: #6f2d87;">
  <?php echo get_phrase('submit_testimonial'); ?>
  </a>
  <?php else: ?>
  <a type="button" class="nav-link" data-toggle="modal" data-target="#signInModal" style="color: #6f2d87;">
  <?php echo get_phrase('login'); ?>
  </a>
  <?php endif; ?>
  </li>
  </ul>
  </div>
  </div>
  </div>
  </footer>

 */ ?>

<section class="footer-area footers d-print-none">
    <div class="container-lg pt-2">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3 footers-one">



                <?php if ($this->session->userdata('user_login') == "1") : ?>
                    <div class="mb-2"><b><?php echo 'Hi,' . $user_details['first_name'] . ' ' . $user_details['last_name']; ?></b></div>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo site_url('home/profile/user_profile'); ?>"><?php echo get_phrase('my_profile'); ?></a></li>
                        <li><a href="<?php echo site_url('home/my_courses'); ?>"><?php echo get_phrase('my_courses'); ?></a></li>
                        <li><a href="<?php echo site_url('login/logout/user'); ?>"><?php echo get_phrase('log_out'); ?></a></li>
                    </ul>


                <?php else : ?>
                    <div class="mb-2 ml-2"><b><?php echo get_phrase('log_in'); ?></b></div>
                    <form action="<?php echo site_url('login/validate_login/user'); ?>" method="post">
                        <div class="input-group mb-2">
                            <input type="email" name="email" class="form-control rounded25" placeholder="<?php echo get_phrase('email'); ?>" value="" required>
                        </div>
                        <div class="input-group mb-2">
                            <input type="password" name="password" class="form-control rounded25" placeholder="<?php echo get_phrase('password'); ?>" value="" required>
                        </div>

                        <div class="forgot-pass">
                            <span><button type="submit" class="btn btn-primary btn-sm rounded25" style="padding: 5px 18px !important;margin: 0 5px 0 2px;"><?php echo get_phrase('log_in'); ?></button></span>
                            <small>
                                <span> or </span>
                                <span><a href="" data-toggle="modal" data-target="#forgotModal" data-dismiss="modal"><?php echo get_phrase('forgot_password'); ?></a></span>
                                <span> | </span>
                                <span><a href="<?php echo site_url('home/sign_up'); ?>"><?php echo get_phrase('sign_up'); ?></a></span>
                            </small>


                        </div>
                    </form>
                <?php endif; ?>


            </div>
            <div class="col-xs-12 col-sm-6 col-md-2 footers-one">
                <div class="mb-1"><b><?php echo get_phrase('essentials'); ?></b></div>
                <ul class="list-unstyled">
                    <li><a href="<?php echo site_url('home/about_us'); ?>"><?php echo get_phrase('about_us'); ?></a></li>
                    <li><a href="<?php echo site_url('home/privacy_policy'); ?>"><?php echo get_phrase('privacy_policy'); ?></a></li>
                    <li><a href="<?php echo site_url('home/terms_and_condition'); ?>">Terms & Conditions</a></li>
                    <!-- #DK -->

                    <li><a href="mailto:info@coursewings.com?subject=Contact%20Us%20Mail"><?php echo get_phrase('contact_us'); ?></a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-2 footers-two">
                <div><b><?php echo get_phrase('quick_links'); ?></b></div>
                <ul class="list-unstyled">
                    <li><a href="<?php echo site_url('home/courses'); ?>">Courses</a></li>
                    <li>
                        <?php if ($this->session->userdata('user_login') == "1") : ?>
                            <a type="button" data-toggle="modal" data-target="#testimonial" style="color: #6f2d87;-webkit-appearance: inherit;">
                                <?php echo get_phrase('submit_testimonial'); ?>
                            </a>


                        <?php else : ?>
                            <a type="button" data-toggle="modal" data-target="#signInModal" style="color: #6f2d87;-webkit-appearance: inherit;">
                                <?php echo get_phrase('login'); ?>
                            </a>
                        <?php endif; ?>
                    </li>
                    <li>
                        <?php if ($this->session->userdata('user_login') == "1") : ?>
                            <a href="<?php echo site_url('user'); ?>"><?php echo get_phrase('instructors_panel'); ?></a>
                        <?php else : ?>
                            <a href="<?php echo site_url('home/sign_up'); ?>"><?php echo get_phrase('register_now'); ?></a>
                        <?php endif; ?>
                    </li>
                    <li>
                        <a href="<?php echo site_url('home/faqs'); ?>">FAQs</a>
                    </li>
                    <!--li>
<a href="mailto:info@coursewings.com?subject=Resolution%20Required&body=Please%20mention%20your%20name%20and%20email%20id%20as%20in%20Course%20Wings%20login"><?php echo get_phrase('resolution_desk'); ?></a>
</li-->
                </ul>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-2 footers-three">
                <div class="mb-1"><b><?php echo get_phrase('address'); ?></b></div>
                <ul class="list-unstyled">
                    <li><?php echo get_phrase('course_wings_portal'); ?></li>
                    <li>JVC, Autumn 2 "B"</li>
                    <li>P.O Box 186729</li>
                    <li><?php echo get_phrase('dubai'); ?>, U.A.E</li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 footers-four">
                <div class="mb-1"><b><?php echo get_phrase('social_media'); ?></b></div>
                <div class="social-icons mt-2">
                    <div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center footericon mr-2"><a href="https://www.facebook.com/CourseWings/" target="_blank"><i class="fab fa-facebook-f"></i></a></div>
                    <div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center footericon mr-2"><a href="https://twitter.com/CourseWings" target="_blank"><i class="fab fa-twitter"></i></a></div>
                    <div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center footericon mr-2"><a href="https://www.instagram.com/coursewings_videos/" target="_blank"><i class="fab fa-instagram"></i></a></div>
                    <div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center footericon mr-2"><a href="https://www.linkedin.com/company/course-wings/" target="_blank"><i class="fab fa-linkedin-in"></i></a></div>
                    <div class="rounded-circle bg-purple d-inline-flex align-items-center justify-content-center footericon"><a href="https://www.youtube.com/channel/UCG1ovYGI3fWScL2SF3Gg1XQ?view_as=subscriber" target="_blank"><i class="fab fa-youtube"></i></a></div>
                </div>
                <div class="mt-3" style="text-align: center;">
                    <!--a href="<?php echo site_url('home/blogs'); ?>"><img border="0" alt="blogs" src="<?php echo site_url('uploads/system/blogsicon.png'); ?>" width="250" height="55"></a-->
                    <a class="btn btn-primary btn-sm rounded25 pull-left" style="float: left" href="<?php echo site_url('home/blogs'); ?>">Blogs</a>
                </div>
            </div>
        </div>



    </div>
</section>

<section class="footer-area greybck pb-1 d-print-none mt-0">
    <div class="container-lg">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 footers-one pt-2">
                Copyright &copy; 2020 Course Wings Portal. All rights reserved.
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 footers-one">
                <div class="footers-logo float-right">
                    <img src="<?php echo base_url() . 'uploads/system/logo-dark.png'; ?>" alt="Logo" style="width:200px;">
                </div>
            </div>
        </div>
    </div>
</section>

<!-- PAYMENT MODAL -->
<!-- Modal -->
<?php
$paypal_info = json_decode(get_settings('paypal'), true);
$stripe_info = json_decode(get_settings('stripe_keys'), true);
// DK
$ccavenue = json_decode(get_settings('ccavenue'), true);
// DK
if ($paypal_info[0]['active'] == 0) {
    $paypal_status = 'disabled';
} else {
    $paypal_status = '';
}
if ($stripe_info[0]['active'] == 0) {
    $stripe_status = 'disabled';
} else {
    $stripe_status = '';
}
// #DK
if ($ccavenue[0]['active'] == 0) {
    $ccavenue_status = 'disabled';
} else {
    $ccavenue_status = '';
}
// DK
?>
<div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content payment-in-modal">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo get_phrase('checkout'); ?>!</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <!-- #DK -->
                    <div class="col-md-6">
                        <form action="<?php echo site_url('home/ccavenue_checkout'); ?>" method="post">
                            <input type="hidden" id="total_price_of_checking_out" class="total_price_of_checking_out" name="total_price_of_checking_out" value="">
                            <button type="submit" class="btn btn-default paypal" <?php echo $ccavenue_status; ?>><?php echo get_phrase('secured_card_payment'); ?></button>
                        </form>
                    </div>
			<script>
				function checkoutWindow(totalPriceOfCheckingOut){
					window.open(`<?php echo base_url(); ?>home/ccavenue_checkout/${totalPriceOfCheckingOut}`,"CCavenue Checkout","menubar=1,resizable=1,width=350,height=250");
				}
			</script>
                    <div class="col-md-6">
                        <form action="<?php echo site_url('home/paypal_checkout'); ?>" method="post">
                            <input type="hidden" class = "total_price_of_checking_out" name="total_price_of_checking_out" value="">
                            <button type="submit" style="" class="btn btn-default paypal" <?php echo $paypal_status; ?>>PayPal</button>
                        </form>
                    </div>
                    <?php /*
                      <div class="col-md-12">
                      <form action="<?php echo site_url('#'); ?>" method="post">
                      <input type="hidden" class = "total_price_of_checking_out" name="CC Avenue" value="">
                      <button type="submit" class="btn btn-primary btn-block" >Payment Coming Soon</button>
                      </form>
                      </div>
                      <div class="col-md-6">
                      <form action="<?php echo site_url('home/paypal_checkout'); ?>" method="post">
                      <input type="hidden" class = "total_price_of_checking_out" name="total_price_of_checking_out" value="">
                      <button type="submit" class="btn btn-default paypal" <?php echo $paypal_status; ?>><?php echo get_phrase('paypal'); ?></button>
                      </form>
                      </div>

                      <div class="col-md-6">
                      <form action="<?php echo site_url('home/stripe_checkout'); ?>" method="post">
                      <input type="hidden" class = "total_price_of_checking_out" name="total_price_of_checking_out" value="">
                      <button type="submit" class="btn btn-primary stripe" <?php echo $stripe_status; ?>><?php echo get_phrase('stripe'); ?></button>
                      </form>
                      </div>
                     */ ?>
                </div>
            </div>
        </div>
    </div>
</div><!-- Modal -->

<!-- Modal -->
<div class="modal fade" id="EditRatingModal" tabindex="-1" role="dialog" aria-hidden="true" reset-on-close="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content edit-rating-modal">
            <div class="modal-header">
                <h5 class="modal-title step-1" data-step="1"><?php echo get_phrase('step') . ' 1'; ?></h5>
                <h5 class="modal-title step-2" data-step="2"></h5>
                <h5 class="m-progress-stats modal-title">
                    &nbsp;of&nbsp;<span class="m-progress-total"></span>
                </h5>

                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="m-progress-bar-wrapper">
                <div class="m-progress-bar">
                </div>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-8">
                            <h4 class="rating-title"><?php echo get_phrase('submit_your_rating_for_this_course'); ?>?</h4>
                            <div class="modal-rating-box">

                                <?php /*
                                  <div class="card-body">
                                  <h5 class="card-title" class = "course_title_for_rating" id = "course_title_1"></h5>
                                  <p class="card-text" id = "instructor_details">

                                  </p>
                                  </div>
                                 */ ?>

                                <fieldset class="your-rating">

                                    <input type="radio" id="star5" name="rating" value="5" />
                                    <label class="full" for="star5"></label>

                                    <?php /* <!-- <input type="radio" id="star4half" name="rating" value="4 and a half" />
                                      <label class="half" for="star4half"></label> --> */ ?>

                                    <input type="radio" id="star4" name="rating" value="4" />
                                    <label class="full" for="star4"></label>

                                    <?php /*  <!-- <input type="radio" id="star3half" name="rating" value="3 and a half" />
                                      <label class="half" for="star3half"></label> --> */ ?>

                                    <input type="radio" id="star3" name="rating" value="3" />
                                    <label class="full" for="star3"></label>

                                    <?php /* <!-- <input type="radio" id="star2half" name="rating" value="2 and a half" />
                                      <label class="half" for="star2half"></label> --> */ ?>

                                    <input type="radio" id="star2" name="rating" value="2" />
                                    <label class="full" for="star2"></label>

                                    <?php /* <!-- <input type="radio" id="star1half" name="rating" value="1 and a half" />
                                      <label class="half" for="star1half"></label> --> */ ?>

                                    <input type="radio" id="star1" name="rating" value="1" />
                                    <label class="full" for="star1"></label>

                                    <?php /* <!-- <input type="radio" id="starhalf" name="rating" value="half" />
                                      <label class="half" for="starhalf"></label> --> */ ?>

                                </fieldset>

                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="modal-course-preview-box text-center">
                                <div class="card">
                                    <img class="card-img-top img-fluid" id="course_thumbnail_1" alt="">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="modal-rating-comment-box">
                                <h4 class="rating-title"><?php echo get_phrase('write_a_public_review'); ?></h4>
                                <textarea id="review_of_a_course" name="review_of_a_course" placeholder="<?php echo get_phrase('write_a_review_based_on_ease_of_understanding_knowledge_gained_&_overall_experience'); ?>?" maxlength="750" class="form-control"></textarea>
                                <span><small>Max characters: 750 <i>(approx 150 words)</i></small></span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <input type="hidden" name="course_id" id="course_id_for_rating" value="">
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-block" onclick="publishRating($('#course_id_for_rating').val())" id="">
                    <i class="fas fa-pen-square mr-1"></i><?php echo get_phrase('submit_your_rating_&_review'); ?>
                </button>
            </div>
        </div>
    </div>
</div><!-- Modal -->

<!-- login modal -->

<div class="modal fade" id="signInModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content sign-in-modal">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo get_phrase('log_in_to_your_account'); ?>!</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?php echo site_url('login/validate_login/user'); ?>" method="post">
                    <div class="input-group">
                        <span class="input-field-icon"><i class="fas fa-envelope"></i></span>
                        <input type="email" name="email" class="form-control" placeholder="<?php echo get_phrase('email'); ?>" value="" required>
                    </div>
                    <div class="input-group">
                        <span class="input-field-icon"><i class="fas fa-lock"></i></span>
                        <input type="password" name="password" class="form-control" placeholder="<?php echo get_phrase('password'); ?>" value="" required>
                    </div>
                    <button type="submit" class="btn btn-primary"><?php echo get_phrase('log_in'); ?></button>
                    <div class="forgot-pass">
                        <span>or</span>
                        <a href="" data-toggle="modal" data-target="#forgotModal" data-dismiss="modal"><?php echo get_phrase('forgot_password'); ?></a>
                    </div>
                </form>








                <div class="agreement-text">
                    <?php echo get_phrase('by_signing_in_you_agree_to_our'); ?> <a href="<?php echo site_url('home/terms_and_condition'); ?>"><?php echo get_phrase('terms_of_use'); ?></a> <?php echo get_phrase('and'); ?> <a href="<?php echo site_url('home/privacy_policy'); ?>"><?php echo get_phrase('privacy_policy'); ?></a>.
                </div>
                <div class="account-have">
                    <?php echo get_phrase('do_not_have_an_account'); ?>? <a href="<?php echo site_url('home/sign_up'); ?>"><?php echo get_phrase('sign_up'); ?></a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Forgotpassword Modal -->

<div class="modal fade" id="forgotModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content sign-in-modal">
            <div class="modal-header">
                <h5 class="modal-title">Forgot Password</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?php echo site_url('login/forgot_password/frontend'); ?>" method="post">
                    <div class="input-group">
                        <input type="email" name="email" class="form-control forgot-email" placeholder="E-mail" value="" required>
                    </div>
                    <div>
                        <small class="form-text text-muted"><?php echo get_phrase('enter_your_email_id_to_get_password'); ?>.</small>
                    </div>
                    <div class="forgot-pass-btn">
                        <button type="submit" class="btn btn-primary d-inline-block"><?php echo get_phrase('reset_password'); ?></button>
                        <span>or</span>
                        <a href="" class="btn bg-yellow d-inline-block" data-toggle="modal" data-target="#signInModal" data-dismiss="modal"><?php echo get_phrase('back_to_login'); ?></a>
                    </div>
                </form>
                <div class="forgot-recaptcha">

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Testimonial Modal -->
<div class="modal fade testimonial_form_popup" id="testimonial" tabindex="-1" role="dialog" aria-hidden="true" reset-on-close="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content edit-rating-modal">
            <div class="modal-header">

                <h5 class="modal-title">Testimonial for Coursewings</h5>


                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!-- #DK -->
            <?php $attributes = array('class' => 'testimonial-form');
            echo form_open('', $attributes);
            ?>
            <div class="modal-body step step-2">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="modal-rating-comment-box">
                                <h4 class="rating-title">Submit your Testimonial</h4>
                                <h5 class="card-title" class="course_title_for_rating"></h5>
                                <textarea id="testimonial" name="testimonial_text" placeholder="Write your thoughts on what you feel about Coursewings platform, how it has helped you." maxlength="1000" class="form-control" required="required"></textarea>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="modal-course-preview-box text-center">
                                <a href="javascript::">
                                    <?php if (file_exists('uploads/user_image/' . $user_details['id'] . '.jpg')) : ?>
                                        <img src="<?php echo base_url() . 'uploads/user_image/' . $user_details['id'] . '.jpg'; ?>" alt="" class="img-fluid">
                                    <?php else : ?>
                                        <img src="<?php echo base_url() . 'uploads/user_image/placeholder.png'; ?>" alt="" class="img-fluid">
                                    <?php endif; ?>
                                </a>
                                <i class="fas fa-user"> :</i>
                                <?php echo $user_details['first_name'] . ' ' . $user_details['last_name']; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal-footer">
                <button id="testimonial_submit_btn" type="submit" class="btn btn-primary btn-block publish submitbtn"><i class="fas fa-quote-left mr-2"></i>..<i class="fas fa-quote-right ml-2 mr-2"></i>Submit</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>


<script type="text/javascript">
    function publishRating(course_id) {
        var review = $('#review_of_a_course').val();
        var starRating = 0;
        $('input:radio[name="rating"]:checked').each(function () {
            starRating = $('input:radio[name="rating"]:checked').val();
        });

        $.ajax({
            type: 'POST',
            url: '<?php echo site_url('home/rate_course'); ?>',
            data: {
                course_id: course_id,
                review: review,
                starRating: starRating
            },
            success: function (response) {
                console.log(response);
                $('#EditRatingModal').modal('hide');
                location.reload();
            }
        });
    }

    //submitting form
    $('form.testimonial-form').on('submit', function (e) {
        e.preventDefault();
        $('#testimonial_submit_btn').prop('disabled', true);
        $.ajax({
            type: 'post',
            url: "<?php echo base_url('user/testimonial_submit') ?>",
            data: $(this).serialize(),
            success: function (response) {
                //alert(response);
                $('.testimonial_form_popup .modal-body').html('');
                $('.testimonial_form_popup .modal-body').html('<h3>Thanks for your testimonial. Always.</h3><p>It will be published after verified by the admin.</p>');
                $('.testimonial_form_popup .submitbtn').remove();
                setTimeout(function () {
                    // $( ".testimonial_form .first_part" ).removeClass( 'visible' );
                    $('.testimonial_form_popup').modal('hide');
                }, 2000);
            }
        });
    });
</script>
