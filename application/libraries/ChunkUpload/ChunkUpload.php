<?php 
/**
 * Author: Dhaval Koradiya (https://www.linkedin.com/in/dhavalkoradiya)
 */
class ChunkUpload
{
    public $folder_path;
	protected $options;
	
	function __construct($folder_path = null, $options = null, $initialize = true, $error_messages = null)
	{
        $this->folder_path = $folder_path['upload_path'];
		// param
		$this->options = array(
			'script_url' => $this->get_full_url().'/'.$this->basename($this->get_server_var('SCRIPT_NAME')),
			'upload_dir' => empty($this->folder_path) ? dirname($this->get_server_var('SCRIPT_FILENAME')).'/files/' : dirname($this->get_server_var('SCRIPT_FILENAME')).$this->folder_path,
			'upload_url' => empty($this->folder_path) ? $this->get_full_url().'/files/' : $this->get_full_url().$this->folder_path,
            'accept_file_types' => '/\.(mp4|gif|jpe?g|png)$/i',
			'mkdir_mode' => 0755,
			'param_name' => 'upload_video',
			'input_stream' => 'php://input',
			'discard_aborted_uploads' => true,
			'user_dirs' => false,
			'download_via_php' => false,
			'replace_dots_in_filenames' => '-',
			'access_control_allow_origin' => '*',
			'access_control_allow_credentials' => false,
			'access_control_allow_methods' => array(
                'OPTIONS',
                'HEAD',
                'GET',
                'POST',
                'PUT',
                'PATCH',
                'DELETE'
            ),
            'access_control_allow_headers' => array(
                'Content-Type',
                'Content-Range',
                'Content-Disposition'
            ),
			// By default, allow redirects to the referer protocol+host:
            'redirect_allow_target' => '/^'.preg_quote(
                    parse_url($this->get_server_var('HTTP_REFERER'), PHP_URL_SCHEME)
                    .'://'
                    .parse_url($this->get_server_var('HTTP_REFERER'), PHP_URL_HOST)
                    .'/', // Trailing slash to not match subdomains by mistake
                    '/' // preg_quote delimiter param
                ).'/',
            'max_file_size' => null,
            'min_file_size' => 1,
            // The maximum number of files for the upload directory:
            'max_number_of_files' => null,
            'print_response' => true
		);
        if ($options) {
            $this->options = $options + $this->options;
        }
        if ($error_messages) {
            $this->error_messages = $error_messages + $this->error_messages;
        }
		if ($initialize) {
            $this->initialize();
        }
	}

	protected function initialize() {
        switch ($this->get_server_var('REQUEST_METHOD')) {
            case 'POST':
                $this->post($this->options['print_response']);
                break;
            default:
                $this->header('HTTP/1.1 405 Method Not Allowed');
        }
    }

	// 1st
	public function post($print_response = true) {

		$upload = $this->get_upload_data($this->options['param_name']);
        // Parse the Content-Disposition header, if available:
		$content_disposition_header = $this->get_server_var('HTTP_CONTENT_DISPOSITION');
		$file_name = $content_disposition_header ?
		rawurldecode(preg_replace(
			'/(^[^"]+")|("$)/',
			'',
			$content_disposition_header
		)) : null;
        // Parse the Content-Range header, which has the following form:
        // Content-Range: bytes 0-524287/2000000
		$content_range_header = $this->get_server_var('HTTP_CONTENT_RANGE');
		$content_range = $content_range_header ?
		preg_split('/[^0-9]+/', $content_range_header) : null;
		$size =  $content_range ? $content_range[3] : null;
		$files = array();
		if ($upload) {
			if (is_array($upload['tmp_name'])) {
                // param_name is an array identifier like "files[]",
                // $upload is a multi-dimensional array:
				foreach ($upload['tmp_name'] as $index => $value) {
					$files[] = $this->handle_file_upload(
						$upload['tmp_name'][$index],
						$file_name ? $file_name : $upload['name'][$index],
						$size ? $size : $upload['size'][$index],
						$upload['type'][$index],
						$upload['error'][$index],
						$index,
						$content_range
					);
				}
			} else {
                // param_name is a single object identifier like "file",
                // $upload is a one-dimensional array:
				$files[] = $this->handle_file_upload(
					isset($upload['tmp_name']) ? $upload['tmp_name'] : null,
					$file_name ? $file_name : (isset($upload['name']) ?
						$upload['name'] : null),
					$size ? $size : (isset($upload['size']) ?
						$upload['size'] : $this->get_server_var('CONTENT_LENGTH')),
					isset($upload['type']) ?
					$upload['type'] : $this->get_server_var('CONTENT_TYPE'),
					isset($upload['error']) ? $upload['error'] : null,
					null,
					$content_range
				);
			}
		}
		$response = array($this->options['param_name'] => $files);
		return $this->generate_response($response, $print_response);
	}

	// 2nd
	protected function handle_file_upload($uploaded_file, $name, $size, $type, $error,
		$index = null, $content_range = null) {
		$file = new \stdClass();
		$file->name = $this->get_file_name($uploaded_file, $name, $size, $type, $error,
			$index, $content_range);
		$file->size = $this->fix_integer_overflow((int)$size);
		$file->type = $type;
		if ($this->validate($uploaded_file, $file, $error, $index)) {
			// $this->handle_form_data($file, $index);
			$upload_dir = $this->get_upload_path();
			if (!is_dir($upload_dir)) {
				mkdir($upload_dir, $this->options['mkdir_mode'], true);
			}
			$file_path = $this->get_upload_path($file->name);
			$append_file = $content_range && is_file($file_path) &&
			$file->size > $this->get_file_size($file_path);
			if ($uploaded_file && is_uploaded_file($uploaded_file)) {
                // multipart/formdata uploads (POST method uploads)
				if ($append_file) {
					file_put_contents(
						$file_path,
						fopen($uploaded_file, 'r'),
						FILE_APPEND
					);
				} else {
					move_uploaded_file($uploaded_file, $file_path);
				}
			} else {
                // Non-multipart uploads (PUT method support)
				file_put_contents(
					$file_path,
					fopen($this->options['input_stream'], 'r'),
					$append_file ? FILE_APPEND : 0
				);
			}
			$file_size = $this->get_file_size($file_path, $append_file);
			if ($file_size === $file->size) {
				$file->url = $this->get_download_url($file->name);
				// if ($this->is_valid_image_file($file_path)) {
				// 	$this->handle_image_file($file_path, $file);
				// }
			} else {
				$file->size = $file_size;
				if (!$content_range && $this->options['discard_aborted_uploads']) {
					unlink($file_path);
					$file->error = $this->get_error_message('abort');
				}
			}
			// $this->set_additional_file_properties($file);
		}
		return $file;
	}

	// 3rd
	public function generate_response($content, $print_response = true) {
        $this->response = $content;
        if ($print_response) {
            $json = json_encode($content);
            $redirect = stripslashes($this->get_post_param('redirect'));
            if ($redirect && preg_match($this->options['redirect_allow_target'], $redirect)) {
                return header('Location: '.sprintf($redirect, rawurlencode($json)));
            }
            $this->head();
            if ($this->get_server_var('HTTP_CONTENT_RANGE')) {
                $files = isset($content[$this->options['param_name']]) ?
                    $content[$this->options['param_name']] : null;
                if ($files && is_array($files) && is_object($files[0]) && $files[0]->size) {
                    header('Range: 0-'.(
                        $this->fix_integer_overflow((int)$files[0]->size) - 1
                    ));
                }
            }
            $this->body($json);
        }
        return $content;
    }

	//internal
    protected function get_upload_data($id) {
        return @$_FILES[$id];
    }
	protected function body($str) {
        echo $str;
    }
	protected function get_post_param($id) {
        return @$_POST[$id];
    }
	protected function get_server_var($id) {
		return @$_SERVER[$id];
	}
	protected function header($str) {
        header($str);
    }
	public function head() {
        $this->header('Pragma: no-cache');
        $this->header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->header('Content-Disposition: inline; filename="files.json"');
        // Prevent Internet Explorer from MIME-sniffing the content-type:
        $this->header('X-Content-Type-Options: nosniff');
        if ($this->options['access_control_allow_origin']) {
            $this->header('Access-Control-Allow-Origin: '.$this->options['access_control_allow_origin']);
	        $this->header('Access-Control-Allow-Credentials: '
	            .($this->options['access_control_allow_credentials'] ? 'true' : 'false'));
	        $this->header('Access-Control-Allow-Methods: '
	            .implode(', ', $this->options['access_control_allow_methods']));
	        $this->header('Access-Control-Allow-Headers: '
	            .implode(', ', $this->options['access_control_allow_headers']));
        }
        $this->send_content_type_header();
    }
	protected function get_file_name($file_path, $name, $size, $type, $error,
        $index, $content_range) {
        $name = $this->trim_file_name($file_path, $name, $size, $type, $error,
            $index, $content_range);
        $name = $this->clearname($name);
        return $this->get_unique_filename(
            $file_path,
            $this->fix_file_extension($file_path, $name, $size, $type, $error,
                $index, $content_range),
            $size,
            $type,
            $error,
            $index,
            $content_range
        );
    }
    protected function trim_file_name($file_path, $name, $size, $type, $error,
        $index, $content_range) {
        // Remove path information and dots around the filename, to prevent uploading
        // into different directories or replacing hidden system files.
        // Also remove control characters and spaces (\x00..\x20) around the filename:
        $name = trim($this->basename(stripslashes($name)), ".\x00..\x20");
        // Replace dots in filenames to avoid security issues with servers
        // that interpret multiple file extensions, e.g. "example.php.png":
        $replacement = $this->options['replace_dots_in_filenames'];
        if (!empty($replacement)) {
            $parts = explode('.', $name);
            if (count($parts) > 2) {
                $ext = array_pop($parts);
                $name = implode($replacement, $parts).'.'.$ext;
            }
        }
        // Use a timestamp for empty filenames:
        if (!$name) {
            $name = str_replace('.', '-', microtime(true));
        }
        return $name;
    }
    protected function get_unique_filename($file_path, $name, $size, $type, $error,
        $index, $content_range) {
        while(is_dir($this->get_upload_path($name))) {
            $name = $this->upcount_name($name);
        }
        // Keep an existing filename if this is part of a chunked upload:
        $uploaded_bytes = $this->fix_integer_overflow((int)$content_range[1]);
        while (is_file($this->get_upload_path($name))) {
            if ($uploaded_bytes === $this->get_file_size(
                    $this->get_upload_path($name))) {
                break;
            }
            $name = $this->upcount_name($name);
        }
        return $name;
    }
    protected function upcount_name($name) {
        $name = $this->clearname($name);
        return $name;
        // return preg_replace_callback(
        //     '/(?:(?: \(([\d]+)\))?(\.[^.]+))?$/',
        //     array($this, 'upcount_name_callback'),
        //     $name,
        //     1
        // );
        
    }
    protected function fix_file_extension($file_path, $name, $size, $type, $error,
        $index, $content_range) {
        // Add missing file extension for known image types:
        if (strpos($name, '.') === false &&
            preg_match('/^image\/(mp4|gif|jpe?g|png)/', $type, $matches)) {
            $name .= '.'.$matches[1];
        }
        
        return $name;
    }

    // Fix for overflowing signed 32 bit integers,
    // works for sizes up to 2^32-1 bytes (4 GiB - 1):
    protected function fix_integer_overflow($size) {
        if ($size < 0) {
            $size += 2.0 * (PHP_INT_MAX + 1);
        }
        return $size;
    }

    protected function validate($uploaded_file, $file, $error, $index) {
        if ($error) {
            $file->error = $this->get_error_message($error);
            return false;
        }
        $content_length = $this->fix_integer_overflow(
            (int)$this->get_server_var('CONTENT_LENGTH')
        );
        $post_max_size = $this->get_config_bytes(ini_get('post_max_size'));
        if ($post_max_size && ($content_length > $post_max_size)) {
            $file->error = $this->get_error_message('post_max_size');
            return false;
        }
        if (!preg_match($this->options['accept_file_types'], $file->name)) {
            $file->error = $this->get_error_message('accept_file_types');
            return false;
        }
        if ($uploaded_file && is_uploaded_file($uploaded_file)) {
            $file_size = $this->get_file_size($uploaded_file);
        } else {
            $file_size = $content_length;
        }
        if ($this->options['max_file_size'] && (
                $file_size > $this->options['max_file_size'] ||
                $file->size > $this->options['max_file_size'])
        ) {
            $file->error = $this->get_error_message('max_file_size');
            return false;
        }
        if ($this->options['min_file_size'] &&
            $file_size < $this->options['min_file_size']) {
            $file->error = $this->get_error_message('min_file_size');
            return false;
        }
        if (is_int($this->options['max_number_of_files']) &&
            ($this->count_file_objects() >= $this->options['max_number_of_files']) &&
            // Ignore additional chunks of existing files:
            !is_file($this->get_upload_path($file->name))) {
            $file->error = $this->get_error_message('max_number_of_files');
            return false;
        }
        return true;
    }
    public function get_config_bytes($val) {
        $val = trim($val);
        $last = strtolower($val[strlen($val)-1]);
        if (is_numeric($val)) {
            $val = (int)$val;
        } else {
            $val = (int)substr($val, 0, -1);
        }
        switch ($last) {
            case 'g':
                $val *= 1024;
            case 'm':
                $val *= 1024;
            case 'k':
                $val *= 1024;
        }
        return $this->fix_integer_overflow($val);
    }
    protected function get_user_path() {
        if ($this->options['user_dirs']) {
            return $this->get_user_id().'/';
        }
        return '';
    }
    protected function get_user_id() {
        @session_start();
        return session_id();
    }

    protected function get_upload_path($file_name = null, $version = null) {
        $file_name = $file_name ? $file_name : '';
        if (empty($version)) {
            $version_path = '';
        } else {
            $version_path = $version.'/';
        }
        return $this->options['upload_dir'].$this->get_user_path()
            .$version_path.$file_name;
    }

    protected function get_file_size($file_path, $clear_stat_cache = false) {
        if ($clear_stat_cache) {
            if (version_compare(PHP_VERSION, '5.3.0') >= 0) {
                clearstatcache(true, $file_path);
            } else {
                clearstatcache();
            }
        }
        return $this->fix_integer_overflow(filesize($file_path));
    }
    protected function get_query_separator($url) {
        return strpos($url, '?') === false ? '?' : '&';
    }
    protected function get_singular_param_name() {
        return substr($this->options['param_name'], 0, -1);
    }
    protected function get_download_url($file_name, $version = null, $direct = false) {
        if (!$direct && $this->options['download_via_php']) {
            $url = $this->options['script_url']
                .$this->get_query_separator($this->options['script_url'])
                .$this->get_singular_param_name()
                .'='.rawurlencode($file_name);
            if ($version) {
                $url .= '&version='.rawurlencode($version);
            }
            return $url.'&download=1';
        }
        if (empty($version)) {
            $version_path = '';
        } else {
            $version_path = rawurlencode($version).'/';
        }
        return $this->options['upload_url'].$this->get_user_path()
            .$version_path.rawurlencode($file_name);
    }

    protected function get_error_message($error) {
        return isset($this->error_messages[$error]) ?
            $this->error_messages[$error] : $error;
    }

    protected function send_content_type_header() {
        $this->header('Vary: Accept');
        if (strpos($this->get_server_var('HTTP_ACCEPT'), 'application/json') !== false) {
            $this->header('Content-type: application/json');
        } else {
            $this->header('Content-type: text/plain');
        }
    }
    protected function get_full_url() {
        $https = !empty($_SERVER['HTTPS']) && strcasecmp($_SERVER['HTTPS'], 'on') === 0 ||
            !empty($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
            strcasecmp($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') === 0;
        return
            ($https ? 'https://' : 'http://').
            (!empty($_SERVER['REMOTE_USER']) ? $_SERVER['REMOTE_USER'].'@' : '').
            (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : ($_SERVER['SERVER_NAME'].
                ($https && $_SERVER['SERVER_PORT'] === 443 ||
                $_SERVER['SERVER_PORT'] === 80 ? '' : ':'.$_SERVER['SERVER_PORT']))).
            substr($_SERVER['SCRIPT_NAME'],0, strrpos($_SERVER['SCRIPT_NAME'], '/'));
    }
    protected function basename($filepath, $suffix = null) {
        $splited = preg_split('/\//', rtrim ($filepath, '/ '));
        return substr(basename('X'.$splited[count($splited)-1], $suffix), 1);
    }
    protected function upcount_name_callback($matches) {
        $index = isset($matches[1]) ? ((int)$matches[1]) + 1 : 1;
        $ext = isset($matches[2]) ? $matches[2] : '';
        return '('.$index.')'.$ext;
    }
    public function clearname($file=null,$long=false){
        $value = pathinfo($file, PATHINFO_FILENAME);
        $ext   = pathinfo($file, PATHINFO_EXTENSION);
        $count = empty($long) ? 32 : $long;
        $value = strtolower($value);
        $value = str_replace(" ", '', $value);
        $value = md5(trim($value));
        $value = substr(preg_replace ('/[^0-9a-z]/', '_', $value), 0, $count);
        $value = ($value).'.'.$ext;
        return $value;
    }
}

?>