<?php
$config['use_page_numbers'] = TRUE;
$config['page_query_string'] = FALSE;
$config['enable_query_strings'] = FALSE;
$config['num_links'] = 2;

$config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
$config['first_tag_open'] = '<li class="page-item">';
$config['first_tag_close'] = '</li>';

$config['prev_link'] = '<i class="fa fa-angle-left"></i>';
$config['prev_tag_open'] = '<li class="page-item">';
$config['prev_tag_close'] = '</li>';

$config['next_link'] = '<i class="fa fa-angle-right"></i>';
$config['next_tag_open'] = '<li class="page-item">';
$config['next_tag_close'] = '</li>';

$config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
$config['last_tag_open'] = '<li class="page-item">';
$config['last_tag_close'] = '</li>';

$config['cur_tag_open'] = '<li class="page-item active"><a href="javascript:void(0)" class="page-link">';
$config['cur_tag_close'] = '</a></li>';

$config['num_tag_open'] = '<li class="page-item">';
$config['num_tag_close'] = '</li>';
$config['attributes'] = array('class' => 'page-link');