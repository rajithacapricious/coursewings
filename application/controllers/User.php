<?php
defined('BASEPATH') OR exit('No direct script access allowed');
#DK
require 'assets/vimeo/vendor/autoload.php';
use Vimeo\Vimeo;


class User extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->library('session');
        
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');

        if (get_settings('allow_instructor') != 1){
            redirect(site_url('home'), 'refresh');
        }
    }

    public function index() {
        if ($this->session->userdata('user_login') == true) {
            $this->courses();
        }else {
            redirect(site_url('login'), 'refresh');
        }
    }






    public function courses() {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }
        $page_data['selected_category_id']   = isset($_GET['category_id']) ? $_GET['category_id'] : "all";
        $page_data['selected_instructor_id'] = $this->session->userdata('user_id');
        $page_data['selected_price']         = isset($_GET['price']) ? $_GET['price'] : "all";
        $page_data['selected_status']        = isset($_GET['status']) ? $_GET['status'] : "all";
        $page_data['courses']                = $this->crud_model->filter_course_for_backend($page_data['selected_category_id'], $page_data['selected_instructor_id'], $page_data['selected_price'], $page_data['selected_status']);
        $page_data['page_name']              = 'courses-server-side';
        $page_data['categories']             = $this->crud_model->get_categories();
        $page_data['page_title']             = get_phrase('my_courses');
        $this->load->view('backend/index', $page_data);
    }

    // This function is responsible for loading the course data from server side for datatable SILENTLY
    public function get_courses() {
      if ($this->session->userdata('user_login') != true) {
        redirect(site_url('login'), 'refresh');
      }
      $courses = array();
      // Filter portion
      $filter_data['selected_category_id']   = $this->input->post('selected_category_id');
      $filter_data['selected_instructor_id'] = $this->input->post('selected_instructor_id');
      $filter_data['selected_price']         = $this->input->post('selected_price');
      $filter_data['selected_status']        = $this->input->post('selected_status');

      // Server side processing portion
      $columns = array(
        0 => '#',
        1 => 'title',
        2 => 'category',
        3 => 'lesson_and_section',
        4 => 'enrolled_student',
        5 => 'status',
        6 => 'price',
        7 => 'actions',
        8 => 'course_id'
      );
		
      // Coming from databale itself. Limit is the visible number of data
      $limit = html_escape($this->input->post('length'));
      $start = html_escape($this->input->post('start'));
      $order = "";
      $dir   = $this->input->post('order')[0]['dir'];

      $totalData = $this->lazyload->count_all_courses($filter_data);
      $totalFiltered = $totalData;

      // This block of code is handling the search event of datatable
      if(empty($this->input->post('search')['value'])) {
        $courses = $this->lazyload->courses($limit, $start, $order, $dir, $filter_data);
      }
      else {
        $search = $this->input->post('search')['value'];
        $courses =  $this->lazyload->course_search($limit, $start, $search, $order, $dir, $filter_data);
        $totalFiltered = $this->lazyload->course_search_count($search);
      }

      // Fetch the data and make it as JSON format and return it.
      $data = array();
      if(!empty($courses)) {
        foreach ($courses as $key => $row) {
          $instructor_details = $this->user_model->get_all_user($row->user_id)->row_array();
          $category_details = $this->crud_model->get_category_details_by_id($row->sub_category_id)->row_array();
          $sections = $this->crud_model->get_section('course', $row->id);
          $lessons = $this->crud_model->get_lessons('course', $row->id);
          $enroll_history = $this->crud_model->enrol_history($row->id);

          $status_badge = "bg-success text-light";
          if ($row->status == 'pending') {
              $status_badge = "bg-danger text-light";
          }elseif ($row->status == 'draft') {
              $status_badge = "badge-dark-lighten";
          }

          $price_badge = "badge-primary";
          $price = 0;
          if ($row->is_free_course == null){
            if ($row->discount_flag == 1) {
              $price = currency($row->discounted_price);
            }else{
              $price = currency($row->price);
            }
          }elseif ($row->is_free_course == 1){
            $price_badge = "bg-pink text-light";
            $price = get_phrase('free');
          }

          $view_course_on_frontend_url = site_url('home/course/'.slugify($row->title).'/'.$row->id);
          $edit_this_course_url = site_url('user/course_form/course_edit/'.$row->id);
          $section_and_lesson_url = site_url('user/course_form/course_edit/'.$row->id);

          if ($row->status == 'active' || $row->status == 'pending') {
            $course_status_changing_action = "confirm_modal('".site_url('user/course_actions/draft/'.$row->id)."')";
            $course_status_changing_message = get_phrase('mark_as_draft');
			$publishbadge = 'btn-success text-dark';
			$publishicon = 'fas fa-eye-slash';
          }else{
            $course_status_changing_action = "confirm_modal('".site_url('user/course_actions/publish/'.$row->id)."')";
            $course_status_changing_message = get_phrase('publish_course');
			$publishbadge = 'btn-warning';
			$publishicon = 'fas fa-eye';
          }

          $delete_course_url = "confirm_modal('".site_url('user/course_actions/delete/'.$row->id)."')";
		  
		  $viewfrontend = '<a class="btn btn-xs2 btn-primary pull-right" href="' . $view_course_on_frontend_url . '" target="_blank"><i class="mdi mdi-airplay mr-1"></i>' . get_phrase("view_on_front") . '</a>';
				
		  $changemycoursestatus = '<a class="btn btn-xs2 ' .$publishbadge . '" href="javascript::" onclick="' . $course_status_changing_action . '"><i class="' .$publishicon .' mr-1"></i>' . $course_status_changing_message . '</a>';
		
		  $editmycourse = '<a class="btn btn-xs2 bg-blue text-light" href="' . $edit_this_course_url . '"><i class="mdi mdi-pencil mr-1"></i>' . get_phrase("edit_this_course") . '</a>';
		
		  $deletemycourse = '<a class="btn btn-xs2 btn-danger text-light" href="javascript::" onclick="' . $delete_course_url . '"><i class="mdi mdi-delete mr-1"></i>' . get_phrase("delete") . '</a>';

          $action = '
          <div class="dropright dropright">
            <button type="button" class="btn btn-sm btn-outline-primary btn-rounded btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="mdi mdi-dots-vertical"></i>
            </button>
            <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="'.$view_course_on_frontend_url.'" target="_blank">'.get_phrase("view_course_on_frontend").'</a></li>
                <li><a class="dropdown-item" href="'.$edit_this_course_url.'">'.get_phrase("edit_this_course").'</a></li>
                <li><a class="dropdown-item" href="'.$section_and_lesson_url.'">'.get_phrase("topic_and_chapter").'</a></li>
                <li><a class="dropdown-item" href="javascript::" onclick="'.$course_status_changing_action.'">'.$course_status_changing_message.'</a></li>
                <li><a class="dropdown-item" href="javascript::" onclick="'.$delete_course_url.'">'.get_phrase("delete").'</a></li>
            </ul>
        </div>
        ';
		  $number = $key+1;
		
          $nestedData['#'] = '<b>' . $number . '.</b> <strong><a href="'.site_url('user/course_form/course_edit/'.$row->id).'">'.ellipsis($row->title).'</a></strong><br>
          <span class="badge bg-blue text-light">Cat: '.$category_details['name'].'</span>';

          $nestedData['title'] = '<b>'.$instructor_details['first_name'].' '.$instructor_details['last_name'].'</b><br><small class="text-muted"><b>'.get_phrase('topics').'</b>: '.$sections->num_rows().'</small> | 
            <small class="text-muted"><b>'.get_phrase('chapters').'</b>: '.$lessons->num_rows().'</small>';

          $nestedData['category'] = '<span style="font-size:14px;" class="badge '.$price_badge.'">'.get_phrase($price).'</span><br>';

          $nestedData['lesson_and_section'] = '<span class="text-center"><b>'.$enroll_history->num_rows().'</b></span>';

          $nestedData['enrolled_student'] = '<span style="font-size:14px;" class="badge '.$status_badge.'">'.get_phrase($row->status).'</span>';

          $nestedData['status'] = $editmycourse;

          $nestedData['price'] = $changemycoursestatus;

          $nestedData['actions'] = $viewfrontend;

          $nestedData['course_id'] = $row->id;

          $data[] = $nestedData;
        }
      }

      $json_data = array(
        "draw"            => intval($this->input->post('draw')),
        "recordsTotal"    => intval($totalData),
        "recordsFiltered" => intval($totalFiltered),
        "data"            => $data
      );

      echo json_encode($json_data);
    }

    public function course_actions($param1 = "", $param2 = "") {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }

        if ($param1 == "add") {
            $course_id = $this->crud_model->add_course();
            redirect(site_url('user/course_form/course_edit/'.$course_id), 'refresh');

        }
        elseif ($param1 == "edit") {
            $this->is_the_course_belongs_to_current_instructor($param2);
            $this->crud_model->update_course($param2);
            redirect(site_url('user/courses'), 'refresh');

        }
        elseif ($param1 == 'delete') {
            $this->is_the_course_belongs_to_current_instructor($param2);
            $this->crud_model->delete_course($param2);
            redirect(site_url('user/courses'), 'refresh');
        }
        elseif ($param1 == 'draft') {
            $this->is_the_course_belongs_to_current_instructor($param2);
            $this->crud_model->change_course_status('draft', $param2);
            redirect(site_url('user/courses'), 'refresh');
        }
        elseif ($param1 == 'publish') {
            $this->is_the_course_belongs_to_current_instructor($param2);
            $this->crud_model->change_course_status('pending', $param2);
            redirect(site_url('user/courses'), 'refresh');
        }
    }

    public function course_form($param1 = "", $param2 = "") {

        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }

        if ($param1 == 'add_course') {
            $page_data['languages']	= $this->crud_model->get_all_languages();
            $page_data['categories'] = $this->crud_model->get_categories();
            $page_data['page_name'] = 'course_add';
            $page_data['page_title'] = get_phrase('add_course');
            $this->load->view('backend/index', $page_data);

        }elseif ($param1 == 'course_edit') {
            $this->is_the_course_belongs_to_current_instructor($param2);
            $page_data['page_name'] = 'course_edit';
            $page_data['course_id'] =  $param2;
            $page_data['page_title'] = get_phrase('edit_course');
            $page_data['languages']	= $this->crud_model->get_all_languages();
            $page_data['categories'] = $this->crud_model->get_categories();
            $this->load->view('backend/index', $page_data);
        }
    }

    public function payment_settings($param1 = "") {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }

        if ($param1 == 'paypal_settings') {
            $this->user_model->update_instructor_paypal_settings($this->session->userdata('user_id'));
            redirect(site_url('user/payment_settings'), 'refresh');
        }
        if ($param1 == 'stripe_settings') {
            $this->user_model->update_instructor_stripe_settings($this->session->userdata('user_id'));
            redirect(site_url('user/payment_settings'), 'refresh');
        }

        $page_data['page_name'] = 'payment_settings';
        $page_data['page_title'] = get_phrase('payment_settings');
        $this->load->view('backend/index', $page_data);
    }

    public function instructor_revenue($param1 = "") {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }

        $page_data['payment_history'] = $this->crud_model->get_instructor_revenue();
        $page_data['page_name'] = 'instructor_revenue';
        $page_data['page_title'] = get_phrase('instructor_revenue');
        $this->load->view('backend/index', $page_data);
    }
    
    /* START HOW TO BECOME INSTRUCTOR */ 
    public function howtobecomeinstructor($param1 = "") {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }

        $page_data['page_name'] = 'howtobecomeinstructor';
        $page_data['page_title'] = get_phrase('become_instructor');
        $this->load->view('backend/index', $page_data);
    }   
    /* END HOW TO BECOME INSTRUCTOR */

    public function preview($course_id = '') {
        if ($this->session->userdata('user_login') != 1)
        redirect(site_url('login'), 'refresh');

        $this->is_the_course_belongs_to_current_instructor($course_id);
        if ($course_id > 0) {
            $courses = $this->crud_model->get_course_by_id($course_id);
            if ($courses->num_rows() > 0) {
                $course_details = $courses->row_array();
                redirect(site_url('home/lesson/'.slugify($course_details['title']).'/'.$course_details['id']), 'refresh');
            }
        }
        redirect(site_url('user/courses'), 'refresh');
    }

    public function sections($param1 = "", $param2 = "", $param3 = "") {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }

        if ($param2 == 'add') {
          $this->is_the_course_belongs_to_current_instructor($param1);
            $this->crud_model->add_section($param1);
            $this->session->set_flashdata('flash_message', get_phrase('section_has_been_added_successfully'));
        }
        elseif ($param2 == 'edit') {
            $this->is_the_course_belongs_to_current_instructor($param1, $param3, 'section');
            $this->crud_model->edit_section($param3);
            $this->session->set_flashdata('flash_message', get_phrase('section_has_been_updated_successfully'));
        }
        elseif ($param2 == 'delete') {
            $this->is_the_course_belongs_to_current_instructor($param1, $param3, 'section');
            $this->crud_model->delete_section($param1, $param3);
            $this->session->set_flashdata('flash_message', get_phrase('section_has_been_deleted_successfully'));
        }
        redirect(site_url('user/course_form/course_edit/'.$param1));
    }

    public function lessons($course_id = "", $param1 = "", $param2 = "") {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }
        if ($param1 == 'add') {
            $this->is_the_course_belongs_to_current_instructor($course_id);
            $this->crud_model->add_lesson();
            $this->session->set_flashdata('flash_message', get_phrase('lesson_has_been_added_successfully'));
            redirect('user/course_form/course_edit/'.$course_id);
        }
        elseif ($param1 == 'edit') {
            $this->is_the_course_belongs_to_current_instructor($course_id, $param2, 'lesson');
            $this->crud_model->edit_lesson($param2);
            $this->session->set_flashdata('flash_message', get_phrase('lesson_has_been_updated_successfully'));
            redirect('user/course_form/course_edit/'.$course_id);
        }
        elseif ($param1 == 'delete') {
            $this->is_the_course_belongs_to_current_instructor($course_id, $param2, 'lesson');
            $this->crud_model->delete_lesson($param2);
            $this->session->set_flashdata('flash_message', get_phrase('lesson_has_been_deleted_successfully'));
            redirect('user/course_form/course_edit/'.$course_id);
        }
        elseif ($param1 == 'filter') {
            redirect('user/lessons/'.$this->input->post('course_id'));
        }
        $page_data['page_name'] = 'lessons';
        $page_data['lessons'] = $this->crud_model->get_lessons('course', $course_id);
        $page_data['course_id'] = $course_id;
        $page_data['page_title'] = get_phrase('chapters');
        $this->load->view('backend/index', $page_data);
    }

//  /*****This function checks if this course belongs to current logged in instructor **/ Redone with corrections below
//   function is_the_course_belongs_to_current_instructor($course_id) {
//        $course_details = $this->crud_model->get_course_by_id($course_id)->row_array();
//        if ($course_details['user_id'] != $this->session->userdata('user_id')) {
//            $this->session->set_flashdata('error_message', get_phrase('you_do_not_have_right_to_access_this_course'));
//            redirect(site_url('user/courses'), 'refresh');
//        }
//    }

    // Manage Quizes
    public function quizes($course_id = "", $action = "", $quiz_id = "") {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }

        if ($action == 'add') {
            $this->is_the_course_belongs_to_current_instructor($course_id);
            $this->crud_model->add_quiz($course_id);
            $this->session->set_flashdata('flash_message', get_phrase('quiz_has_been_added_successfully'));
        }
        elseif ($action == 'edit') {
            $this->is_the_course_belongs_to_current_instructor($course_id, $quiz_id, 'quize');
            $this->crud_model->edit_quiz($quiz_id);
            $this->session->set_flashdata('flash_message', get_phrase('quiz_has_been_updated_successfully'));
        }
        elseif ($action == 'delete') {
            $this->is_the_course_belongs_to_current_instructor($course_id, $quiz_id, 'quize');
            $this->crud_model->delete_lesson($quiz_id);
            $this->session->set_flashdata('flash_message', get_phrase('quiz_has_been_deleted_successfully'));
        }
        redirect(site_url('user/course_form/course_edit/'.$course_id));
    }

    // Manage Quize Questions
    public function quiz_questions($quiz_id = "", $action = "", $question_id = "") {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }
        $quiz_details = $this->crud_model->get_lessons('lesson', $quiz_id)->row_array();

        if ($action == 'add') {
            $this->is_the_course_belongs_to_current_instructor($quiz_details['course_id'], $quiz_id, 'quize');
            $response = $this->crud_model->add_quiz_questions($quiz_id);
            echo $response;
        }

        elseif ($action == 'edit') {
            if($this->db->get_where('question', array('id' => $question_id, 'quiz_id' => $quiz_id))->num_rows() <= 0){
              $this->session->set_flashdata('error_message', get_phrase('you_do_not_have_right_to_access_this_quiz_question'));
              redirect(site_url('user/courses'), 'refresh');
            }

            $response = $this->crud_model->update_quiz_questions($question_id);
            echo $response;
        }

        elseif ($action == 'delete') {
            if($this->db->get_where('question', array('id' => $question_id, 'quiz_id' => $quiz_id))->num_rows() <= 0){
              $this->session->set_flashdata('error_message', get_phrase('you_do_not_have_right_to_access_this_quiz_question'));
              redirect(site_url('user/courses'), 'refresh');
            }

            $response = $this->crud_model->delete_quiz_question($question_id);
            $this->session->set_flashdata('flash_message', get_phrase('question_has_been_deleted'));
            redirect(site_url('user/course_form/course_edit/'.$quiz_details['course_id']));
        }
    }

    function manage_profile() {
        redirect(site_url('home/profile/user_profile'), 'refresh');
    }

    function invoice($payment_id = "") {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }
        $page_data['page_name'] = 'invoice';
        $page_data['payment_details'] = $this->crud_model->get_payment_details_by_id($payment_id);
        $page_data['page_title'] = get_phrase('invoice');
        $this->load->view('backend/index', $page_data);
    }
    // Ajax Portion
    public function ajax_get_video_details() {
        $video_details = $this->video_model->getVideoDetails($_POST['video_url']);
        echo $video_details['duration'];
    }

    // this function is responsible for managing multiple choice question
    function manage_multiple_choices_options() {
        $page_data['number_of_options'] = $this->input->post('number_of_options');
        $this->load->view('backend/user/manage_multiple_choices_options', $page_data);
    }
    
    // This function checks if this course belongs to current logged in instructor
    function is_the_course_belongs_to_current_instructor($course_id, $id = null, $type = null) {
        $course_details = $this->crud_model->get_course_by_id($course_id)->row_array();
        if ($course_details['user_id'] != $this->session->userdata('user_id')) {
            $this->session->set_flashdata('error_message', get_phrase('you_do_not_have_rights_to_access_this_course'));
            redirect(site_url('user/courses'), 'refresh');
        }

        if($type == 'section' && $this->db->get_where('section', array('id' => $id, 'course_id' => $course_id))->num_rows() <= 0){
          $this->session->set_flashdata('error_message', get_phrase('you_do_not_have_rights_to_access_this_section'));
          redirect(site_url('user/courses'), 'refresh');
        }
        if($type == 'lesson' && $this->db->get_where('lesson', array('id' => $id, 'course_id' => $course_id))->num_rows() <= 0){
          $this->session->set_flashdata('error_message', get_phrase('you_do_not_have_rights_to_access_this_lesson'));
          redirect(site_url('user/courses'), 'refresh');
        }
        if($type == 'quize' && $this->db->get_where('lesson', array('id' => $id, 'course_id' => $course_id))->num_rows() <= 0){
          $this->session->set_flashdata('error_message', get_phrase('you_do_not_have_rights_to_access_this_quize'));
          redirect(site_url('user/courses'), 'refresh');
        }
        
    }

    public function ajax_sort_section() {
        $section_json = $this->input->post('itemJSON');
        $this->crud_model->sort_section($section_json);
    }
    public function ajax_sort_lesson() {
        $lesson_json = $this->input->post('itemJSON');
        $this->crud_model->sort_lesson($lesson_json);
    }
    public function ajax_sort_question() {
        $question_json = $this->input->post('itemJSON');
        $this->crud_model->sort_question($question_json);
    }
	
	// #DK
    public function testimonial_submit()
    {
    	$data = array(
        	'user_id' 		=> $this->session->userdata('user_id'),
        	'text'			=> urlencode(strip_slashes(strip_tags($this->input->post('testimonial_text')))),
        );
        $this->db->insert('testimonials', $data);
        return true;
    }

    // Mark this lesson as completed codes
    function save_course_progress() {
        $response = $this->crud_model->save_course_progress();
        echo $response;
    }
	//#DK 
	public function lession_video_upload($direct_upload=false)
    {
        if(empty($direct_upload)) {
            $upload_handler = $this->load->library('ChunkUpload',array('upload_path' => '/uploads/lesson_videos/'));
        } else{
            $config = array(
                'upload_path'   => './uploads/lesson_videos/',
                'allowed_types' => 'mp4',
                'overwrite'     => 1,
                'encrypt_name'  => true,
            );
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('upload_video')) {
                echo json_encode(array('files' => $this->upload->data()));
                exit;
            } else{
                echo '{"status":"error"}';   
                exit;
            }
            echo '{"status":"error"}';
        }
        exit;
    }
	public function course_video_upload($direct_upload=false)
    {
        if(empty($direct_upload)) {
            $upload_handler = $this->load->library('ChunkUpload',array('upload_path' => '/uploads/course_videos/'));
        } else{
            $config = array(
                'upload_path'   => './uploads/course_videos/',
                'allowed_types' => 'mp4',
                'overwrite'     => 1,
                'encrypt_name'  => true,
            );
            $this->load->library('upload', $config);
    
            if ($this->upload->do_upload('upload_video')) {
                echo json_encode(array('files' => $this->upload->data()));
                exit;
            } else{
                echo '{"status":"error"}';   
                exit;
            }
            echo '{"status":"error"}';
        }
        exit;
    }
    public function vimeo_video_upload()
    {
        $vimeo_key = $this->crud_model->get_vimeo_key('vimeo');
        $data['response'] = 'Uploading under process';
        $data['status'] = 400;
        $client = new Vimeo($vimeo_key[0]->vimeo_client_id, $vimeo_key[0]->vimeo_client_secret, $vimeo_key[0]->vimeo_access_token);
        $vimeo_api_status = $client->request('/tutorial', array(), 'GET');
        //check vimeo api status
        if ($vimeo_api_status['status'] == 200) {
            //start upload
            // $file_name = "./uploads/sample.mp4";
            $file_name = $_FILES['vimeo_upload_video']['tmp_name'];
            // $file_size = $_FILES['vimeo_upload_video']['size'];
            $title = pathinfo(basename($_FILES['vimeo_upload_video']['name']), PATHINFO_FILENAME);
            // $beta_uri = $client->upload($file_name, array( "name" => $title, )); //UPLOAD            
            $beta_uri = $client->upload($file_name, [
                'name' => $title,
                'privacy' => [
                    'download' => false,
                    'view' => 'disable',
                    'add' => false,
                    'comments' => 'nobody',
                    'embed' => 'whitelist'
                ]
            ]);
            // SET DOMAIN PRIVACY
            $client->request($beta_uri . '/privacy/domains/coursewings.com', array(),'PUT');
            //CHECKING TRANSCODING
            while (true) {
                sleep(10);
                $response = $client->request($beta_uri . '?fields=transcode.status');
                //wait until complete upload
                if ($response['body']['transcode']['status'] === 'complete') {
                    $final_response = $client->request($beta_uri . '?fields=link');
                    $data['response'] = base64_encode($final_response['body']['link']);
                    $data['status'] = 200;
                    break;
                }
            }
        } else {
            $data['response'] = $vimeo_api_status['body']['message'];
            $data['status'] = 400;
        }
        echo json_encode($data);
        exit;
    }
}
