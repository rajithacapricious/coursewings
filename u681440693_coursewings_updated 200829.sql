-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 29, 2020 at 07:36 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u681440693_coursewings`
--

-- --------------------------------------------------------

--
-- Table structure for table `bkup_akeeba_common`
--

CREATE TABLE `bkup_akeeba_common` (
  `key` varchar(190) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bkup_akeeba_common`
--

INSERT INTO `bkup_akeeba_common` (`key`, `value`) VALUES
('stats_lastrun', '1582959460'),
('stats_siteid', 'a2febdce554c80d3c86add49defe3f0d7283f9e9'),
('stats_siteurl', 'bc61f93510f59c4814aca0706fc0d7af');

-- --------------------------------------------------------

--
-- Table structure for table `bkup_ak_params`
--

CREATE TABLE `bkup_ak_params` (
  `tag` varchar(255) NOT NULL,
  `data` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bkup_ak_params`
--

INSERT INTO `bkup_ak_params` (`tag`, `data`) VALUES
('update_version', '3.6.1');

-- --------------------------------------------------------

--
-- Table structure for table `bkup_ak_profiles`
--

CREATE TABLE `bkup_ak_profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) NOT NULL,
  `configuration` longtext DEFAULT NULL,
  `filters` longtext DEFAULT NULL,
  `quickicon` tinyint(3) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bkup_ak_profiles`
--

INSERT INTO `bkup_ak_profiles` (`id`, `description`, `configuration`, `filters`, `quickicon`) VALUES
(1, 'Only Database Backup', '###AES128###T6j/Ik8MGLjyArKPQfIrarXC2t7ZxuSfz/tRGUyAxYEuxnJLfgQUK6DfHa2ihESb1uJtOgaVxq+YOPGaTrIzvmsPbaYb2tHNAuX7h4Jx0aEzwX/kxdFzycPVKfVHT8cN2hyOmQAqe6m3QYTCXeHpLTNS0zL0zOvnf4wIWbpjW93P1BxSuHJIPQqGCP5N0rXRCUrB5j4JjJdKvdcgSjAKYm9E8RG81+d8gQfZWi6z7lvcAJQk2AW95qLOygGTieXxKvlludPNvHjXvkfcWkQq9CxXKnR+Pzm7f9bvsGMa4xRZB7T7IH+HC9w051dDImzZyiLRll2JBRO825fzvmad1xlwbQBM84LTI6lhFkXVOZlusf2Qy6QGP8DfgonVS1iAnKuI4u+rffzGwPnxvTAjP1l/LDptMdBayMV+qjL9/m3CVSof2r7sISVxQFKULmxoYXUEhl/rkOs32MZY1lNr3bjH3XZFJf00PuVZQZNoyYK4KNjzTDSbZGEu4t9vDZt1z9sQruu92mAjkGNVR8iMLUN+YxaNfyse3UeDM1UG6bNQgrUkz+U6D0/ZjGnePKkQLrSXN4Yy1QktJKrYCIFhRG9uUdFUkcqzEoV6SR/dW65HMIMgV5dJdB5zYEpEkfiLd+8L2PZZ6t40fxKQAolxhBQ6N8OBYgRdnhuCt4fcxVk5OcsKtgQxuYzB+gsEEaUfmJD7CeW9U2Rw0CI8x59zcsoXCe6GpkpWdCQLm+mksJlbZ5KRgJfCd/ZKRpZgvEzwkXUkSFLJPrfNDkYJrLxwuPO5K+PV4kKaD/jpPqRXltOzIUnAg1ytmB9xI5/p1CL/7iIWwc5B7uUtRW9VGRqtMGWHRuIWHRIsqxsRv2v0cU9NRrH8zFKE0OEiMKHePQ9UbgQon1Q1s3gdlvUk9qL5c3LZfmVWMmdCthwDRue8MHBVXyS/q1KWmwe6Qj2ltmokNA47dYqRJ4Z0nzsBnx+7FchQo8eAbR+ha9KzpcOFuGDSfYGCoZ47QelEeVwFD52EL5muSXk+1bJbeS3SaCgxPsHZOpbeY383vDV0p4Oz8vK+NDzOzRtqbNu1GbgA5d+ha2DhpqDjyG+a7FvN1sZlFIUjyr/85BBuQJD1W0ZsSBYKo1Px20CrFaV46h4W74SvwFXy+Bm2ypH22u1Kkygn8v3RriHAk2VGctpDaclN7ALf2wLXwKoB2uyIZCIAmni7dHix7nvrN3urxNjrs585eCPvP72CS5gKLiXcgJpRWc0gFuKTQsVgGyS2zHb52hOjiuRa55C2qgpd7HimNiJ8dozaWLV77RlCGvq0JXHA83RICGvoaeNONs7PfTm3iYnL+Ygnnp89UFsEBllnND0nukywlCu5VDGNag6C+EMSlMeDInEQQy1sZoM0uJMxnQLUULhLe5rFJBJ7BqSvJTZ41FIzRSumEwmEohEYuC9jKmrTQyxlo8zCYbu7mFo3eM7VUkzIbcm9tQkNgw1+Bvm3849+KcH3XAoBRSLvoAd40xK2nQ1UBj22Dr4TlQdnTyKktIym8rF42xhjInASfBzuDY47YBsfGskjEYMnL7o3WJSEMYytiYkJXSnoWvWj0XR1iOSbkMw0jwhclkd5+7Anyg/DP3yjOsJUeoCeMbd6aD4mirAKP1nztuBKMl6OEEShnKfklv71Xfcmei9EKjDr1u2j4qljk4tJm4tpk1Ao4azvi6GXjqq984b4fQeF4cY3CL0JCfwiZz5/Xre50HgiANXvWF/f5z2Lb41Jt+aCxon3WsFEhSCSIXuDKUkhlPhmlY0rxXN36ZIfa30iSpvYzYqqaNXvo/Ak8k1NtOiJMH/SB0FrvxoJ662xgdSJN861fU9LGINJ/QmhQSDQNt2QkFGUfxxPPcrW4tfc368oKnSQaA6Mc2S2yz4KYBcvAzI3HJRTgzQXmgM+cND5TR8/cdZLYX582pYq3LPQHG7kKmavUwR6cr0qUb49BxGz/zqhZcgFhDlKQX43/SIt4AO9WB+8snT9YYDFyEqvktHQzlbwva1aVRqTwrEUCE9TVXbr+7SHOqoW7iLgRB37tri3PQ9sU/NWTuWVo7K/ZdgxM3DIu8et8Y1vchgRN9zYLcrLSF1wXxkVv3xLFaLAJjhqYiVVWaHGeCScKXLciD0zgdYGB1SFCmFrtPMELx8jfBp/xw6Nr1CoKMwsCJLA0HUd57QbISU+Z/gjOZUoDIT6DMVGxmhu2rK+YCrDUaUdr1NYg+EIMhpUrNABQnxKuQIdDC99+Ixa+5Gugjtwdcbc0VNMej+AhAlfUKsvHnLRAfU1QNsXPUxqL7DE4tQrTQFYOxnTq9pnCj5PaYXNIiAy+1CDUvWj2I6QI5QOzeUGb14eTtgEjOHSdnqmTeMk0GbypD+P/QhRyPK/9Zh7gqF0AZLm7LaAsU02t2ufu0thr2hDz67YyHoI6w/+ncL7MXHWAzRtbVOthc/o3GS7yjQNLAn62o6R4QSpt3e8sQzx9DhQv9kaIUvFzUz/GtwVjDRZ7zUaNe0ovCafRUz+RLlJSg/8K8gouAXLUgeul/O2at3ZY0ohCgNeFs446d2FD0+IMXIUU9ybd33Ar6hCwwpUvenG6jZreWeYB9elvK+cr20ECihokO/fL2wUVAWf/c//fLvbAIEBDKEiDEdvAvshznEOqs1hbw/kfNyZKYMp4nCTUc/qyDi821363UMDbPid2G4Tr5T04fYjBHWDPSkLwdBYjm+vJYW5CvHoxBo6liKyiY4pRndrtG+5wRwzJQIHIeldt/ar1vnxgQzhgbx2GsqSr0xTHyKsP33aNPH0274Sv5h2qGejHWA9MhwrUfRNh+dFbw7TpDEMW76D367IrRz3z4v8U4vvO6ktA+tfamBLsxVvjGlaox0imKYv8CTyowoiBvYKCxNU9lDqBMgeKXTA32YxAGcPfJotfhzDnCpDRcnExfpG9jUmPVQNTY+ITSXffpUW/i5OaCWSmMtB0EhEgjpeNKFxHv5v0aoSSQbMNmbETiC13xqQj11VKMdiI9SZU823AyG2R0C6Egc+ExKEDMb1/q5rl8IpFIQGlD407B2Wpr3bOMC8drri+d9nXxRp0PIuzQTy2d817xAls/2Zg/u01vRjyQB6te7jsAYEGlFm2vcWvgVF85wU/cD0fb2HnN3LGtnngXRwNzv1QxSEyqOl6jt+CA1JGGiwl/qmeBK2hGIwfDwB+SmSfwULsXQuV6KeHUEmWRiVUK7BErXlFrnlKlyGcVvSaDYspHrqV40flvgCzYJHRAqRj5cbr6SGV7Q3FElXlLx6M5uMcoJ9oOb5+jJsoWUjNa5L8X6O4d8j/wCV7N26Qw1MepFTOcLLlMxa1/GcnmYRXXrTFmVjed4Sv/lnKXCwaMZqLEG/lq/VosOmHnktyYTKTbbOStQ6fatsijj9o1/dZbXgBEQUliBFrkAD4N1g5hs1QBq5pw2zgIiBeQ2hnIt+KUuiHUmX4BcOCgA+Bw/8PG2SWaSWwOwHUnzTfLCEvZXMXIj1V+fOd5kEDBb3YPXFXxmpmt1saGt8UZa0quBORY86bUGxN+litUnqELfbjXoqa/sQ6UyXJjaKaaAsTXDxN2Ps7hlG28GG4e5cqh9d/BNraDXA3ltfeeAKrY6IqMarssaRCCjQKrbBDgruFDzIa2aR/CWeZKfZgPlLQklzkzWckPch9YF5bKkT+wz1IraB0wVYA+x8GSuY6Cf2deJf8+E/EwMlKmk14elWxdVidGjH4QMiWO/+DUfd6pc90CyWU/ZR9qPcvOW9f3UgT2xZbXnoH2H9VY3dF1TWFqXC2mdXUOldK92vbnIcDb0w2H0jb+CsCoX8z0wu/2tooX0e5p9DvpBMkD47VnwxvumwbTcmfL7HpuCBPqC9bQ9ZHFpITqc1oTon/lB/vBSW7oXt2tjpbcgL86M2CqRaA2d1Atff4Y+wH+msdusZYpM+pN54ihkap3ib2sTqhLMMWXSJ1nyPxErfEVjLXRkN5YbO9vADYXt53483WjFgM+83xXTr4elZ8EDhN8oybsco7oYe0X6ufXdj3VS6E7jDKHFeM33Rt+AUEF0EwkYJz26CkzyNhsGN3tYqMXeJIYCwLUzzMSp3uwD8MBECu8qQk8Rrij9f73pdhjtYhHKiuCruIFamXKGgNFsYmbue+82DdQpbhoZKcIOvHruo5gCdjz1IbM3tZ3xwdbVbsKk/uYATwvE1/quAqxRMbAvlVEyl8qBodTyb39ovZ4FZpQBK/KC5VWvM3CTSFFK+ul1kQmk4VuLasiSiqi93EYHmMmuzVdYuw4ktNIyQmRsfsS34Af61MwBLS4MJRyLnFwJTFT/jr/Q85bpeKHbeWuYyXt56YKVkI3fR+9HbDVFai9+soziPvGR14U8uRDUfXnvF/7a2ym2vN1T9kl4W/B/HuFpLnh1Nx2oEq4MChbJT4kqV/MeKY/STv49BZArFrJFmP1itC3qkR19qvGw96eIG+tSnqRsVh1SI63TOxY3jH0JxOUxw5UXfDromkc2cWrpkej76/k0UzFTTv0O+H/tOvKx2v3UKRcXUptrVdiZ7zxkiYcnk1o1cZHjm6S0KBQ8yu6OUjkfSn8VltH8XUrlL3rop/oW860C3auOnR0t2o/DTuGWnc6PNk5wKoDtxKtBZUS5a1yRzLwgaBbr2qAVGxsNtzJqw1CdggI2ccF29vVTQgg0QmyPxq+iXqmsujviCOmxADK1xQcccZuliAvr3d+36oLOKYhq1TrjKUxw/Dx/ANQyKKfkFENTBoczrGQ5QBLO0Ew5mRY0q6V0mRIS/pVEE1tjkTo/TJt/VZpkUEJxqEJ8TXagXUP5S4c4apzFWIPhxSRy6CaCdzynWjj6lM6lIoUjlwXhkty1hCJbm9FadT5Xj7f5yIepFNFpgCRF8jiCPbSjdzBTY+0c6VvoLb0p/atm6tS78xDLoaxC4+a3J5OrGrJBi2IaT12z0X/kRyWrJwYQAr3ZEvXP7UbT6TwLx/hyQO9qK7kaxUsDJ4QrK2QiFCedYhlKArt9ydvwuSW0o10clkaEWJrheEGZt7LYwoUYOEBdWHOB5bJEko5F/K19X4HzLktWJCCu8egPEE9vqYIBBStTUvVLsqoyeSfecUW/D1IXu8QOHnu6XOgxSgHehhATgUpnhLslhTIIwjnN80JBtwS0c+YGIVoBLkuTlCSmkvjHOqBdCv3Hnp3oBFT2obJwWaYq4Rne8EP6VKYITB6IMwDV4mCFUT7IE8NVwznKKje3rVkZLMU9LVs4M5gpbA9M+zypKUFNUixpa48sJkii8CiGMcEeCLpfq6BOBfUlx2ApqGJCtPPAj4Ah2zpGO76jfI6tsrkRl6PbhtpzJHyyXV7eupLKLFkpQSVbV/V5yVha7GRzp0A1iAyj0eQ8AAA==', '', 1),
(3, 'Only Uploads Folder Backup', '###AES128###OT2f7piWxzmJHZYLiq9VOkUDn9oJG/q1ysMjJ/XdFXzg95V1i045k/ArYQ217fe8XTeFFnEFB85GcAWHduGhkiOxHK7rRIumUhnf4QPtU0JQPTc7kH7bJc/xiJzZfEEiXCYcv/IauJnDGta7SZFbAsRYlFqOzNuRTdIcGmgvIBsp0L9rbP+6H2/6VWksr6RQARJAHXh4/NH+KIFd+AIj8OU/bD9nc4a5yyzaWe/b+7SRjc5n6FBBzmb6OuJwapsKKV/ern2lIVhU0uY338UUVrcCZojuto+qRlw5qFPejNO52yvpEmLpT9tnykVQJfghQ8/HQCoWsohXia/bcrFhStPSr7QpPDdbFEJfKYRtEU5xc2sxPlRmDGphJMweK3hOwr1tJclyOhouVT8CKD0v69omYAGkVWTkHegmW2lkrDr2s9ZhO2kxFzKIZi7ovvtya7VRSm7fRo+wc4pPnAciT0YDS1n1fzuGVhJ1PVIHTkOjkQihPJoTHyA80GiTDmjs2qZ2A7d5sMqaMOUtewGWsU0h1y/JwEuGVAm00kate7NL2IHXpzEHZ85YI5MDP34hiqE8okeZ4SaWFmHZTJVZn1gydKzjtkGzirJzAUFnLga9E0KUqIc+c4ngBlUMUWjREAOoA4oWtgYF7Q5b5Nt4wWlXAUgaF81/KOKQzZsuLwihWoJIAaTqtf/6BhAAugLvHlhzmEnWT9FfJMFp+vJf5dm1cGYngRiVyaq/AGGyXLgadSzgYuRfG4sNvpFc9GScxjVBIKr+hq3velGhVUN84/0vrr7xJ1A6jyjCAAtFfAHwo9hb3iowTBLBKlQ4HRTBIS0aw/SpiMN2USDBC2PDqnV5YKDHyXkSH8HL0EXrgedSAmOUGM0ojnmPeXH85q7B41287qOK28COU2VYfSlQZzA/AuOw+0BIgycL/Rs5m5sq/MNLmbizgBJ/1blpBRWVTgmADB9Xq3+Kc038IBe58Iw8ngxAbWude1JlHR7lofpFHAdAx2uTxgA8C2ryQ3xPsrq4RTDTN7LVhWdAWlVnb5Bg4AEQrhT5NDNZcXOkUtc6C9zJoRxcyZ024VMOwqa1CtwPHfEQzrX/ireBxraOA2xh5uGcm9pQi8BuvUCzhKLjJo3jlNHn+Fd7PCl6PEdzz0AYC5WimmOQmOPg6RehmHAOgOkUl6zY9crd6MsBCtQeTG0rNB7PmxY4R+BZqb5BnRsuKv+lizIb1dyKBSAtPba1HCq0pUw07SdH3HhAXhj4BTSViceL8IPQJOoOl3xoXX7v9jRvaP6xsCn6/hgqSUKC73J7FxVgsjHney3dVPpUS8/5wLY2n3hrTXhMNnskKx1bcrBPomGxHBX1WezDdiUyc7CQF46RI3ZDs9uedNmDpPDXg6a+OdeiIBcWPWJrTJ3nRvXqRjdLmNNv93j+YvMPnEhd8JT10Xt4D4/yrUHQarewvDB6RsFJbVP2ghR87T+lBxKY928UPsp6PKra3MQSRKgejSzZaYSIYBImxfso6EIKWly/PPspRB0l+KAwoKCh5t/OyFSIe+pvTfxZlPdVdHEvR07zgzwi4HhwFwZpbLecz49YlGXB7kLOftdg4X5rwH1Pq5wHHlhIT6FqMFr3lEweNcl1Q98yEzwKrSa81C4PVoedaeY/7PZOL33/9klEHKxY7mQN7hfHmgmq5O1ftQ7qzP6jyV2ShVXKqHmy9DtomNFhpVSOpUsP7oSl6AtrHeAps7Dueud8lIz5FE17m5n6/Gpfq1iwcnv/HQZ/wBnyCy1oP6x7H0XSFGhw9a5y7JnFtDhv+7E8N/Cm5D/MnPq6PVlR82voKaiC1wqB3lTkXNrsBaZX8OOuE+MDqnsFkZfXD/df+rjR0ImmiVZMq1ZTuwgbsWHTWqHpzBKKDtiPP8VW8w4iGhj+8tXZzDaSOSM2jmIst8KppUFdFvsnRKsL2ZjxDB249Lwo9I7hy2KS9Ipo8Wx6EIBhsS5IHZbzlQ6xTRWMH5hUVRgmpzEJZtP9T2bJj1MI0nOiNnfcIAeGYCc9WzF5udSd6WPTB2XKZ7XC2Ir+nGEenYIdJyslXFueCxxP2d1OH0tOrDn9fL9x5xhMtA1s45wYK8pFSFkWbk3Uo1nQ+NPLqj7WEvAZ4oRVlbf7APqw1T8jpOP1QLlDq+HcAjUlOTjZghQP4SVfjy4Ff5wkyWTu2Wx74OidzVhPfL/u6Hll8+5qq7iErvUS4i+KfVDdq+o5AyPwEHPu/MzP4Sc4Be76uy7GeY6DfUE5PU9sh2a7tU1i9XNg1AmWJtiKgWGiE+zDrjJuW9t9pxiGFsmebpRG/C/yCSg5yO2xIrtp/GCaMxSDMTAeSnzYMzHqP+XZ2mUwg5qQPvNIaPeRkgZJmTZ7x3eF7F3372X209hZt62tgrkyzRSxoIlOTGqnIYeG4NIk0ednEyXgzJuY8yLj5DRqZUFCfa5qPMcvo7d44AHFN3nciv45raqdUq9EjG6Cde27CSeQPsNg+HPGNa21PCVpGXvdQuvAliUoTdL9llqoPwbf/O4yDqzEBw6rR7m27ccfgwKVSvOB8YZrIpTCFUw8qtPaH2aNnpp0HvVjNbGGt5ah3+J7N6eQ13WT0XEYfLmveToL6OIdP8rfcSr0xyoqU+5TfSdsLuWvsZtU4kA/Qe0vmuzZOxaj+bD1cLUvK1rqzu06jH2XF6GvHxcm3SFhc+3rQXT2LNuTZMNN0cO8MK1gywug1D74ksnPy+90KBkhH4Qm+5JqYSuBIqUcZ9D6kzKmdbdRJCSObeEnNVIoWzwsJ599g20bwEvRSGNAFZlgSzmcXjJM+Dzn8Ihi3nfmHLFj+ZEkmDVGIEHp5ga9a+aVpwMmyKPHBEzYcg1Td/DztWO72oyy9M1IXrAqkewaU2xEDqP/zgv10giGzFdVGPC+4OWnG+xzkSqEk015YUkZQtjUL9p2+IQS/zTsSkhoMeM/uxknPwJsSQtxDckQdtooW8Cszu4RdpN41sI7d7jFcvlbwL7GSr9ZhrBnq63u2NmAGLnyk3uOq3W1YZlDGvfTrIlKahlqbOtkdic/WFifrnsTaknyzt1tfdzLQZm4g+g6tuI3YVTaIqnLdIvLjKYXPIPj4JM37X/gGgr2+IMVnBChhlaLE3Rvw3Dv3720QegVhzWRkwsm49rnUlcHPXbAPA0nNnR/c6ajg1xkzP3P+n31JwAsoT5vjZOjqv0sKuOC0/3TjY7WpBwuikB3JjtL/8uS5CI1hwOouc8Jm5seoGiRFwEqlwn8FA1bc1V23h5QG37xPPQ0dih3RoWryTvMOw8vKuYAWPB73TDirE80mEhtWneCLUxO5zss1rvPucz6umLxo6lf3g4fJB8c2aKrRhwGpyMkme35EZodhpSWXxhZwR2d0fCVmW19+UN/0Ba7w0e4pf6nhKDzwnqmjCKn486ME+HUS3QaeE4+aZ9Mo4/MJRePoWWZFgX/Y19xXLk9w6PlW8TNIhPmRRHbEfxJfhdPPvRsrAh6B72woCG3oN7z1LRvREC9LcIG4uaae5i3uoMhoObByp/REBoODZ5HBEWZkLAydLS0PmNJeUJYPxIIVjWmzmQjX/7PAp60r8Fbi/k60x5vA4bknxewPgvP0m6IgjsnRv3Ewn3FjK3vlIgY+F1a4prcomt9byJA4xtCtrbYk0Lg/x2CoUjR4kAm5+0O5On2RhS6/BAlJQY8WrK+HBIgKHq7AocvVsp3fuHQJNi1ZRa4U/NG4JDyIe+Uul0gVVP1+dKgyIwi7c9qA30nQAEwF9AERzlGCHGgemLqaxsiN13RzlTM/pxGpY+jJGV/Fjb7yjn6c7dYPB+eFiQfhKt3FtAo0Pg6pL64TSJudaDBUzHgmXtVLh4fCT492VXzWpUxRuxOq2lgPaGvvOcyP+E7HZKGCImvVVUoDel9LwSXw0j5OzSF1sSLRAHsOWq7qU+mPyQJL1/ZW+9nRDW5Xw06EndctsGH8I7Rvezdere8++D2206TNN2eFNWfeXzISPE4xVwoxGMfJDqMfhBCoF6FwRpZ4mT/JwKXA38rkfuKxemu+J388aspNDvlGz84YIZ3igYyb/2CzXwCyaKRoxPzzEHQ083ZVooa7SwmSeH8E3Ttts+0IFV6NMEG1qw0d8aQ4khoZotZrBLZnDwb8zI8axcPmDMHtN82qteF0Z6pTrN5P1+qF+SnbmyMOzCham1TRh0YNiOTuWx/j21cH/j4YICOXWISTkJN8VeE71VwEdQz/sbJT8+CoHiu+tUXK8tjK3q6xQ1iz0NVIn/qX03UfZmm2YcTWkAaAQwpAqGnv++3RnS0HFgq36aoE9OYOryilaOwqPo+9JQn6y/OdGZx5zzgCqC3uoGDjx1sMl8Ku+4Cmp8BAsABsYPtVSafTfUHcWdPQ7wUGXCbMRwyoD1YmmvljlAiKNaSQY/tm0u9xOg9alt2C7pdIboBeo3hUpudxVmEv65m3SAp0u8nFajiywbCjx9vuAWy/FwC6j1jiX3KOVIGXL9wGsZIVZlBpvnkNSg0K5gidGZYFGyQCQMUuS3QyfJHSiTaLBzhipN3OVrNKjvFktpjLvsFpviWHU0pN26dzmkVpwOf0iG3BrmQt1tezhCcwFoxBEEOpLAareunZqu2J4bkQcaJPgWwQhFX3zhCPpeKVsDKBHaLanchKvsrgWSsPfNM6QMCbbkX36IBPUCmu8hYt2CAlW0hgzG5Qud5m9h0UZcme2BAiLzsN/LzCcunKUFA+vz/AliXZBq01raSKlj2Bk3TJJrZd3uo5SrMl3IpdyV7xJTIimFcgMGzC/vNXFmIpULxvjMO/liwojZN8OmA5Wq/gjTn3h4xrDyxaiNtkfmFL2jY6YlmMgwMQKCSGHDqD1XjsYoacEdxuFyN9zaE+BH+c8BtdKHqS6+19t5fukDtdC8IeKJGKT9WVo3bwhonZ9OCiRWZf+z1U9aMm2kC1ZqrEyNsatu+mWWdoBPyxHQ/6WiI2OOmWRkjR6V42Bv+EpmHTzqHjeH+6J3XIqwVFUEb6uSmZ3dcQSvD5fArqHHVrQ3MRMWdIBAezSDJCiFdjjGDOBgoEMq4uQpOJkBVOPBtroWtoy/xQfIonAdrlh4KtMNv1m6Id3HeiFt1KgnX1YRDATERscZxdAT0l/FbZ7fCS3eVyQDokTZ/EqtuAhh3GgyV4P8mYmflUedoVzIu5PeNNdqFEAJCkZmsMxTsaFlXU7rqDejTtKowXJ4peJ8lUoZOsR/+bd/Ll4Fq8yZUblYyml4SpB8DynxlhDrZ2grJLX1KUFNUobIRQdHiyjVQu36qdjxl7XZqqSN0miexJx+iQUSFSVkTUG45F7pCNShVpWPZVwtHaZCqOsRN5dtjFAdU2/qBcEpQSVYDyBrw2UwJhEU6TOJdyoVwdw8AAA==', '{\n    \"skipdirs\": {\n        \"\\/home\\/runcloud\\/webapps\\/coursewings\": {\n            \"0\": \".vscode\",\n            \"1\": \"application\",\n            \"2\": \"assets\",\n            \"3\": \"ci_sessions\",\n            \"4\": \"system\",\n            \"5\": \"update_pack\",\n            \"6\": \"themes\"\n        }\n    },\n    \"skipfiles\": {\n        \"\\/home\\/runcloud\\/webapps\\/coursewings\": {\n            \"0\": \".vscode\",\n            \"1\": \"application\",\n            \"2\": \"assets\",\n            \"3\": \"ci_sessions\",\n            \"4\": \"system\",\n            \"5\": \"themes\",\n            \"6\": \"update_pack\"\n        }\n    },\n    \"files\": {\n        \"\\/home\\/runcloud\\/webapps\\/coursewings\": {\n            \"0\": \".htaccess\",\n            \"1\": \"README.txt\",\n            \"2\": \"composer.json\",\n            \"3\": \"google567db692c83c684f.html\",\n            \"4\": \"index-offline.html\",\n            \"5\": \"index.php\",\n            \"6\": \"info.php\",\n            \"7\": \"unzipper-22.php\"\n        }\n    },\n    \"tables\": {\n        \"[SITEDB]\": {\n            \"0\": \"#__users\",\n            \"1\": \"#__vimeo\",\n            \"2\": \"#__testimonials\",\n            \"3\": \"#__tag\",\n            \"4\": \"#__settings\",\n            \"5\": \"#__section\",\n            \"6\": \"#__role\",\n            \"7\": \"#__rating\",\n            \"8\": \"#__question\",\n            \"9\": \"#__payment\",\n            \"10\": \"#__message_thread\",\n            \"11\": \"#__message\",\n            \"12\": \"#__lesson\",\n            \"13\": \"#__language\",\n            \"14\": \"#__hits\",\n            \"15\": \"#__frontend_settings\",\n            \"16\": \"#__enrol\",\n            \"17\": \"#__currency\",\n            \"18\": \"#__course\",\n            \"19\": \"#__comment\",\n            \"20\": \"#__ci_sessions\",\n            \"21\": \"#__ccavenue\",\n            \"22\": \"#__category\",\n            \"23\": \"#__blogs\",\n            \"24\": \"#__bkup_akeeba_common\",\n            \"25\": \"#__bkup_ak_users\",\n            \"26\": \"#__bkup_ak_storage\",\n            \"27\": \"#__bkup_ak_stats\",\n            \"28\": \"#__bkup_ak_profiles\",\n            \"29\": \"#__bkup_ak_params\"\n        }\n    },\n    \"tabledata\": {\n        \"[SITEDB]\": {\n            \"0\": \"#__users\",\n            \"1\": \"#__vimeo\",\n            \"2\": \"#__testimonials\",\n            \"3\": \"#__tag\",\n            \"4\": \"#__settings\",\n            \"5\": \"#__section\",\n            \"6\": \"#__role\",\n            \"7\": \"#__rating\",\n            \"8\": \"#__question\",\n            \"9\": \"#__payment\",\n            \"10\": \"#__message_thread\",\n            \"11\": \"#__message\",\n            \"12\": \"#__lesson\",\n            \"13\": \"#__language\",\n            \"14\": \"#__hits\",\n            \"15\": \"#__frontend_settings\",\n            \"16\": \"#__enrol\",\n            \"17\": \"#__currency\",\n            \"18\": \"#__course\",\n            \"19\": \"#__comment\",\n            \"20\": \"#__ci_sessions\",\n            \"21\": \"#__ccavenue\",\n            \"22\": \"#__category\",\n            \"23\": \"#__blogs\",\n            \"24\": \"#__bkup_akeeba_common\",\n            \"25\": \"#__bkup_ak_users\",\n            \"26\": \"#__bkup_ak_storage\",\n            \"27\": \"#__bkup_ak_stats\",\n            \"28\": \"#__bkup_ak_profiles\",\n            \"29\": \"#__bkup_ak_params\"\n        }\n    }\n}', 1),
(4, 'Full Site Backup (DB + ALL FILES)', '###AES128###ySV0gvxWmHWA29zn7k1oGeMp+lUjSPd3iWrpnCS6vFKxWe3+U59r01OPXvEqj1bTUJh1SyryJHGZ2X4dXqyQFeDR/o2CI/Deux8mxi1wv/KGylo8IBAU87DtNYk4yRT/f/Zbnkn9vKDiblUyJ9G0IC8q5yZANIRMYu53U64Tk7CCVxh3XIdYlJZTGCJXl07kHN113WKJ2O8Zy9DbamvFYsUIOr1P9ou559Ouw/fdQolCWh5+cGN53B7dma264ln9P3REPavi5XFMyjo6iVhH643MNemrmKxdRuROUwkvwjEfIuheO/wFMD/74DZsmw80IJ5O7LNjGZo0MJFVd76N8G28j/4k5RIDps+Ts0DmIdEIKpCgRyUCzdbnX2aTudBjAqepJYdfd36YsZXgmbPpclAtpVDRjKF4OL4fmpgZBHc5uE+bpEoAQ3uD69G3R4f92vvBZf3RGTBm2bdWW6L7o1aQkMHQYhqNBVBlqrItJrk3VPaZLbxikEGg7KT+AAGMrZdOLt3jLOkVKoM1s/xgqRnh1fL3kMWumZmtt8lJU3jvYHwkyu5FF8QztsFLmDfgjJgUk3xMsKrmQshVz/XZRv7E1JAxz5XLS3HNfMUCEQTzBupTnUtA0TMRP4NffDC/NyPBQ3OLceHhj0KZYQpYbs9EEpIeD/lkrcbbEnGhfEaBfLQM6oY9/1H+F7ihyt2RwNZAWw22Cv4oOgxNga0YRpKSAqeHWeVSXyigk9r9bBbQyuNbVyetb+fG5mUokX3s5WzA2RST8PpKQFKvp0SwyhZYVPQ3gYdsecGvA1S8JNYJibm/NDQd1XwK8TJsVBYgrvZzVExNIgcfNqKYOvukt8foJQxXZ+TZlsLe59GK4ZgczGzCFBeRe/LqXazpGAc8HTBGM0JpID57MqHjEI+ETRIA+KfrHEA5axnEeF6A+6C99HkDsBzCDqs9aNLZFbPWlSUXCaTx5GC7OeWAtRcTlw1FQRFsvv/VCWOak0RQQXiJASkTa/xHEBb0F7aMaC+3IHESDuNfaNVNg4LJK17UoXD9oWqRXn+ZlI8jMaWEQY01xNQiXPYn4oTP0vz/QKaOQKnXS5zOkMa0S7LBg0Y0AYEFMrTrPl6ohfTraV8VSv7JV9qkBFZ1NBG/1aLn2RAmYWKJwNfjUkaQf91c0LPT/VbYXlPkOJq35+3+mnu1XReiFAkI6i0y3oCufV0QiAF7SQIVKTTQwviXFIO4q8Lnma9yUKH+DxVr3+DpYsG85SIUF5B+cogqsz8BZHAAnKe2/6odUCMIDnasKGjGpDgZmkZd1QyQvZwlMzcVH7Mj6NZmsGir3jvAKULnM/VgBBJwx4wOygneguRmmGmehAYYbWp9P6cNreAVMMXIH/+ZEHw80oKie8y3LOp5bfYm1EfukOcT4+/BTiIKj8fOOvL6srtE0XZdhM6QcGTXskb8O8KVDcJYAyV7AiQGAsblvneKe8vG7KQkYVeh9cIm8Lodz83eokMPjYNaAEgCqrj1i+e0TnVNCPomZg72U8pz7/qGCaxb4t7/vGO6m93Hk8lOOds1RZdxRGzf73/406vtsFXCeCTk8elpKc52ZlBbQu4ABZfxpmB7cwOjNPNEunox9w2isLck2ymRgmY3ImaiXH8BxOzN1rjjhgTh0BJQhLaXX0VyCGPM1/3FwKTdlCNj1VY4UE+6Ak2t4q3fuIlKewCJFcXRCJ546UKe/8UPha9rUdyG0O2Zqwbv0cqsz5FGILuCyJ8pNXQ3JpignBRE55G1zbWQhQKkpxFGuR2PIquR3t8USr+UBq4clUZGEzZTCUUkw+0ucI+bSLZLGFw07tPpVTi8f/5MXh951/S3C3wha9n4+c5eQ3R8R8fDf/nH0zTaoryJEHQDukCKQeBJ/YXDXqHi2amt8ZjdFEAFg1+hZDSZ/dmR9QJCFFjOnt92ghBHgoPJzuzFebzQqE6kA2Br+HCXLYv2UTmo96xjKtgKO+kGD4FQU5FfBbGdv1BEs30SKHr55lDb6NKm5tH7TOIa3A6YfzhLzO5spBRvgoFr8zUMANqOCy2xHYpxqyHu/uPacAK9c5El3K6w6tuAh74e2JbFxuxErwCapXEnVR6siOxYIfJyzCdEqkAYiMoaeDxv2yu+ZzCojCzcHfAnWGJXG7wJ12+ZPvndE6XNPQd9Jjv/B+fHaETpNofAVajVdH3LSP5r3NE/x7LvMzGl1oo4XTtzQu/atZdGJBfsdsQuXbjlKf8rN2LJygRH3HhB5vcj3A0GGXEYcuQlXkAHZgx442SBOJs3muj0Kuo2E1IjaOYDbBWq1/I8iWaCVjRWEJSr+SmYS2iJ1wJ0LDkY7Q0weZUmHU4ojHy3sZyTXFWfM1okLwkJgg/4isxPD/ri/t8kM7Z8l3kDEoEQGpbj+h4JHGiai7iUr9PeGqGb5/9c1cVYWY2HZRFP/PAncXYjkWdiLMybPqVpzlNB9TkypAcMy7FXFwYQ6Yf0NQ2aNIgP9OrqlvbsN2Vt5gK8wRUvT9dmHf7PXudG1mt8g4kg/n46uqbSXNOKjQ77oCfDevftHbCvgV0cerLDfU1RzN2fzPDfpDJyC6CutTflFuTCBLeXjSGYaFrutS71k4k6cAx34Wp4hDpFWw+Z9m87s32FGqQxroQF+TqDvCf5K+Q0T+4mbLmcbEccIIyjFy8iqzTRoZ7lPCncc2j464g3w6mxbNcjnpkHDeKHeAnOuMRou6pGHbTLijS5sYIxO7vuH1NfPQj9KoGphQSQaev6+ZCCkfd5Kll8W/NUyASi4CtNjyUgMZdYer4eEWHHgPKm6G22M419vuWwuYsIFPw7zCWpvwDzKiMK0C6ESy5Y80A9p01iiIIkfzelMq9oHpfiMyLn1oLMoSpJq84KacGVG5z29TgSF3MveOzP3MLg2JCZun+odjTONZrXsncyYq2D5Z4pRsPKhPxlGUWsCn1X9AXCuUJ486ROL2HpvsYSJaNZ0Pg+9lv5E8h/MQtTriiRKI4yxeEEi6RYu/nTBwi5M7r3esProzVsD4H8/MRWUr1wynWuGU0O9M4sPQZ2DdqmP5YweD697y+u5GPWhM4uSF3MRAeBEeK2K/g0n36/V9yVVUDnvzRo8hN1M8Rr8uPZSW6NfHu4GyJKkdk58Yxlakf7q/4m+V5rKEbGFEJfXxZOM606/pylm7VxL4wXOf7oDjTm30vwXdT+qecBfyx3xIda/9gq75dQ46B4f3Bf9cSoM0CFjVCgvLiofktLQdyzKcZKKAwZxgofyJmK6gw4pnC0XtgvI+kHTFVmfeyuBIIjVkgP/h1thwc/dbXFpscqCwJzcJ4nJQCmS5xP9W4o83p4sv2rLvMwT/3wtp+Itbm/pn8n6ztwh7tJpL6+LraAfCqsWkgeGk6IVwQX3TVQdfSV3OLOloZevOc9c1SaNkfv3iAmp5o/7ds6EHL8UN6IXn5gKS7qmA1CDsJnOIGNXQOVFoG2Twr4dJ63fSqRLhrxmlzx5b0qHNHukybeI5/ZjS2gpVuhy2q5tVrWQqLn/tW38BJVOCC5siLEPk7b50lOStlD183njXk8VpDHqC2PHa0pCkech/AWa5K8dNA6kVNRSG4ihoF+O/Clg652vjjAaxBSTzf+hy5x3Ef+b3ypSR90KmpU0Nm9jCU8QF5K57R+NZtiJ6S4Efal194Mm17EahSJ5xF5BuHlhcVSumwEM4Fos8qkq0uFdlbg/L78vk52APchP8Cx4IA4DSlNGt/gq4gAQWSu4L55GmMU53hjP8CKXE56iBTUWzjDEUo+n33xrAD2QqjjrEzx7RzSLX9Xy7rx3TuGQtcIytyi2A6w9XcY4wWk5VqM3GIq4pBjG03GXoEd3vCe/jC8AK/vte2HxdJuyCfPO4qWRpFRe0w16OG99KezegwxEACdFdPfqA7q2bFpdWChA4X836jPFrD3p8oPo27v8F+F1mLNNtwfKsBd9I+vTb2qPEWejMmi0Aof5ENUE9bsB8ILlMip8trbZ+AwRW8pXTx2fIzV7MjbhqHlyqS76eDlrq3KRLiETvObVGXKkF/SM//b8DP08w5uJB6l+asmln+Du0JllLW3pFnKIjmg+VWPni+0a7u9EyD3Dt5vud9YpR+k09rYXC21vBLFYVeMhc+Y76IEI6k7Tbt3/AFRQHqgTMrCwxcqEU2MJBOo/4M3+CKvOKCZfEPc2Y0kLGveHgbEr0JBWrepjlv2LJXM2smQa86nLBfuw/XeBuwNrXgApbOI6++IGpjS9jOBe8IN4ncFaVa1v2IMKdqY4eJwbPcymkXHE9U7a/hfc+YXMKy41lWX/L1EpNMN+L1n3cwaXD4C5FIb6HGpZHNA5OYkVwkj64cz3v39q3utDPAPievlDocCGa5qnDyQFsoBB0995b8fR4Vr94FeY31KDBOo/Cyd28L/6EWTlKUU626p6/rSBcOksVtFSfG7I2hFY8Y7uk85bU/u49VQ5n0oU4hn2GqWkj0KC1lKW5rcJt5UL+BUwPDDMVyeiIhNFnR6ufJbXy0DejcFRZaYLpGsAyUVXE0dFShpmEKJ14TwD00OtsoNtwYk1/IGplnLMvVZJyDS66ZCKr2YHYhBS4QHZRqGJlyvaXy2oyRB+1ej1y3FxAb/9DViiiuyW1ScHh9Zfoi0jNXMNrBKqthNS9LZW8zSvIyLXTcew9QoTqbx9JwGM2F6gdyETTUXIqkIIBaHi1tuMoVj27DX+fjHW+MJAujfP3ZY3SL5nMex6cms8+w8y24zQeXEoQUshvTDsWvimND0iv1jFh3diZto0jAOleRUtgZKaku87PV4NIjMwQcxNjz4SzxGBlVSjCBfXJUdN2asF4ecGOmPpG3wrAK8tV3VkFqp/GdUozQtoCw9b00uw+I0Y1aGcmPVEfx1Hexey1PBXn1ZDYZMfoIsO96diYLsuxyWLonSivZdvTtyjZn8cpdvEjPG0K+LbZ+Vi+CJggV48WaVcOfLqFQeogBeJEhRmP20put5X4vgWv7DQAx8glU0GXWcSz2a2CQkWDY6+vWWwtFGvkfvvjiDnb0oiXLCQsiBD5R3exzKNHgyRxzPx5r7nqYXSOiKjlVQnLx0FI9XtkxIjOzDel/gMMjHfOOzpRx5udaSmaUiOcFNwuIxgHj//SbpeCQsnJFYeZixVcmPHxMQUBurnfHRMel1vflBxvXF0AqcXHXAo3qXef8TyDbqP1qMskLcr5BmPKK2/P47U9FYNRgm9uJRWn1ZpQNBXdh4n1xAfhZ/fJkBmspHiFEMbCzacaTH95D/6C9KUFNULTkb0txcEocCK3Wyh9NYiE9APVzvo7hJsM6+Hm3l+Aj/6gyEirEHTSEB4aqcHMYERjUbMDsdl3EL+Qybg9loYkpQSVY9Q8wB+IX+gelTJoW9GYYwdw8AAA==', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `bkup_ak_stats`
--

CREATE TABLE `bkup_ak_stats` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(255) NOT NULL,
  `comment` longtext DEFAULT NULL,
  `backupstart` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `backupend` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` enum('run','fail','complete') NOT NULL DEFAULT 'run',
  `origin` varchar(30) NOT NULL DEFAULT 'backend',
  `type` varchar(30) NOT NULL DEFAULT 'full',
  `profile_id` bigint(20) NOT NULL DEFAULT 1,
  `archivename` longtext DEFAULT NULL,
  `absolute_path` longtext DEFAULT NULL,
  `multipart` int(11) NOT NULL DEFAULT 0,
  `tag` varchar(255) DEFAULT NULL,
  `backupid` varchar(255) DEFAULT NULL,
  `filesexist` tinyint(3) NOT NULL DEFAULT 1,
  `remote_filename` varchar(1000) DEFAULT NULL,
  `total_size` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bkup_ak_stats`
--

INSERT INTO `bkup_ak_stats` (`id`, `description`, `comment`, `backupstart`, `backupend`, `status`, `origin`, `type`, `profile_id`, `archivename`, `absolute_path`, `multipart`, `tag`, `backupid`, `filesexist`, `remote_filename`, `total_size`) VALUES
(8, 'Backup taken on Saturday, 29 February 2020 12:45', '', '2020-02-29 08:45:25', '2020-02-29 08:45:34', 'complete', 'backend', 'dbonly', 1, 'site-www.coursewings.com-20200229-124525+04.sql', '/home/runcloud/webapps/coursewings/backup-coursewings/backups/site-www.coursewings.com-20200229-124525+04.sql', 0, 'backend', 'id8', 1, NULL, 227756),
(9, 'Backup taken on Saturday, 29 February 2020 12:47', '', '2020-02-29 08:47:51', '2020-02-29 08:48:00', 'complete', 'backend', 'dbonly', 1, 'site-www.coursewings.com-20200229-124751+04.sql', '/home/runcloud/webapps/coursewings/backup-coursewings/backups/site-www.coursewings.com-20200229-124751+04.sql', 0, 'backend', 'id9', 1, NULL, 226819),
(10, 'Backup taken on Saturday, 29 February 2020 12:48', '', '2020-02-29 08:48:30', '2020-02-29 08:49:10', 'complete', 'backend', 'full', 4, 'site-www.coursewings.com-20200229-124830+04.zip', '/home/runcloud/webapps/coursewings/backup-coursewings/backups/site-www.coursewings.com-20200229-124830+04.zip', 1, 'backend', 'id10', 1, NULL, 174541809),
(11, 'Backup taken on Saturday, 29 February 2020 12:49', '', '2020-02-29 08:49:37', '2020-02-29 08:50:06', 'complete', 'backend', 'full', 3, 'site-www.coursewings.com-20200229-124937+04.zip', '/home/runcloud/webapps/coursewings/backup-coursewings/backups/site-www.coursewings.com-20200229-124937+04.zip', 1, 'backend', 'id11', 1, NULL, 119380129);

-- --------------------------------------------------------

--
-- Table structure for table `bkup_ak_storage`
--

CREATE TABLE `bkup_ak_storage` (
  `tag` varchar(255) NOT NULL,
  `lastupdate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `data` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bkup_ak_storage`
--

INSERT INTO `bkup_ak_storage` (`tag`, `lastupdate`, `data`) VALUES
('liveupdate', '2019-11-06 12:50:25', '{\"stuck\":0,\"software\":\"Akeeba Solo (Standalone)\",\"version\":\"3.6.1\",\"link\":\"http:\\/\\/cdn.akeebabackup.com\\/downloads\\/solo\\/3.6.1\\/akeeba_solo-core-3.6.1.zip\",\"date\":\"2019-10-21\",\"releasenotes\":\"<h3>What\'s new<\\/h3><p>    <strong>Database dump produces multiline queries, supports PROCEDUREs, FUNCTIONs and TRIGGERs even better<\\/strong>.    In the past, Akeeba Backup\'s database dumps generated single line queries for all data definition language and    insert queries. While this works great for all tables and views and most procedures, functions and triggers there    were some cases where the latter three entities were corrupt or impossible to restore e.g. when their definition    included comments or required preservation of whitespace. The backup and restoration engines have now been updated    to create and restore multiline queries, much like you\'d get with mysqldump\\/mysql or phpMyAdmin. As a result the    more complicated procedures, functions and triggers are now supported. IMPORTANT! Your database user must still be    able to retrieve the definition of these databases entities to run a backup and define or overwrite them to restore    the backup. These are limitations imposed by MySQL and controlled exclusively by MySQL privileges. Akeeba Backup    CAN NOT magically override your database access control features \\u2013 such a thing is <em>impossible<\\/em>.<\\/p><p>    <strong>Preliminary work for PHP future 7.4 compatibility<\\/strong>. This work is experimental since PHP 7.4 is still    in alpha.<\\/p><p>    <strong>Akeeba Backup for WordPress: Prevent site display and show link to the restoration script when the    installation folder is present and contains ANGIE<\\/strong>.    In the past we had a few users who were confused as to how the restoration of their database content is supposed to    work because, apparently, they didn\'t read the text on their screen when extracting the backup archive. Moreover, we    had users who left the installation folder on their site after restoration, despite a clear warning about the    security implications of that on their screen. We can\'t force people to read their screens unless we also prevent    them from doing anything else. This is exactly our solution now! The Akeeba Backup for WordPress plugin will disable    access to the site if the restoration script is present, telling people to either run the restoration script to    finalize their site\'s restoration or, if they already did that, to delete the installation folder.<\\/p><p>    <strong>Bug fixes<\\/strong>. We regularly fix smaller and bigger issues. Please consult the CHANGELOG below and the full change history available from the software\'s main page by clicking the CHANGELOG button.<\\/p><h3>PHP versions supported<\\/h3><p>    We only officially support using our software with PHP 5.6, 7.1, 7.2 or 7.3. We strongly advise you to run the latest available version of PHP on a branch currently maintained by the PHP project for security and performance reasons. Older versions of PHP have known major security issues which are being actively exploited to hack sites and they have stopped receiving security updates, leaving you exposed to these issues. Moreover, they are slower, therefore consuming more server resources to perform the same tasks.<\\/p><p>    Kindly note that our policy is to officially support only the PHP versions which are not yet End Of Life per the official PHP project with a voluntarily extension of support for 6 to 9 months after they become End of Life. After that time we stop providing any support for these obsolete versions of PHP without any further notice.<\\/p><h3>WordPress versions supported<\\/h3><p>    We officially support only the latest WordPress 4.9 and 5.x release.<\\/p><p>    We have received reports that Akeeba Backup for WordPress has been installed on WordPress sites as old as 3.8.    Backing up and restoring sites that old should be possible but it\'s not tested and not officially supported. It is a    bad idea running an unsupported version of WordPress due to security and performance concerns.<\\/p><h3>Changelog<\\/h3><h4>Bug fixes<\\/h4><ul>\\t<li>[HIGH] Obsolete backup record quotas would also remove Remote records<\\/li>\\t<li>[LOW] Fixed ALICE memory check when the server has no limit set<\\/li>\\t<li>[LOW] Obsolete backup record quotas would not run when there are no \\\"OK\\\" (files locally present) backup records in the backup profile<\\/li>\\t<li>[LOW] Transfer wizard, Yes\\/No switches rendered without styling<\\/li>\\t<li>[MEDIUM] WordPress restoration: blog name and tagline were not updated<\\/li><\\/ul><h4>New features<\\/h4><ul>\\t<li>Database dump produces multiline queries, supports PROCEDUREs, FUNCTIONs and TRIGGERs even better<\\/li><\\/ul><h4>Miscellaneous changes<\\/h4><ul>\\t<li>Preliminary work for PHP future 7.4 compatibility<\\/li><\\/ul>\",\"infourl\":\"https:\\/\\/www.akeebabackup.com\\/download\\/solo-standalone\\/3-6-1.html\",\"md5\":\"d664a9294f42b38e088d65adc037307b\",\"sha1\":\"6f2385a4f74ead32c1d1a93bac95e7abe5d3ea4d\",\"sha256\":\"55359ac7b074e0e8c9c6d759808b91ce4287a10e514c3ed5620ebf585207f103\",\"sha384\":\"b5832e74e928f3d05cb362528229b5ea7f7aa5b8c778ebb83e3b09de57018b19f66e9863b69c1e86160fd8b7ac021d7b\",\"sha512\":\"bf9ed5a342cb9090fdafa4b22c539d0ed0a59a21f67684486aa8d9e26a277ef1512cfa6b7358d9bca096e34537e952a68b73d14c382a84d89e3579a40a49c3de\",\"platforms\":\"php\\/5.6,php\\/7.0,php\\/7.1,php\\/7.2,php\\/7.3\",\"loadedUpdate\":1,\"stability\":\"stable\"}'),
('liveupdate_lastcheck', '2019-11-06 12:50:25', '1573044625');

-- --------------------------------------------------------

--
-- Table structure for table `bkup_ak_users`
--

CREATE TABLE `bkup_ak_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `parameters` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bkup_ak_users`
--

INSERT INTO `bkup_ak_users` (`id`, `username`, `name`, `email`, `password`, `parameters`) VALUES
(1, 'admin', 'Super Admin', 'info@etcnet.net', '$2y$10$lC7uQwHwgXrSV72QPjTeVOEq8Ie2saccvZcKQeTsbzDviarbNaKrm', '{\"acl\":{\"akeeba\":{\"backup\":true,\"configure\":true,\"download\":true}},\"tfa\":{\"method\":\"none\",\"otep\":[]}}'),
(2, 'admin2', 'Super Admin', 'info2@etcnet.net', 'MD5:27a7c51de96d731899dc84bb4b9bd521:emergency', '{\"acl\":{\"akeeba\":{\"backup\":true,\"configure\":true,\"download\":true}}}'),
(3, 'maddy', 'Maddy', 'ukmadaiah@gmail.com', '$2y$10$gZnWrkObUAl2UX0CbtEe9ONfFl.FqGTXP/uoJ3K3NiIdY03QKomyC', '{\"acl\":{\"akeeba\":{\"backup\":true,\"configure\":false,\"download\":true}},\"tfa\":{\"method\":\"none\",\"otep\":[]}}');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1:active 0:Inactive',
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `hits` double NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Latest news posts';

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `slug`, `short_description`, `description`, `image`, `meta_keywords`, `meta_description`, `status`, `created_by`, `created_date`, `modified_by`, `modified_date`, `hits`) VALUES
(1, 'Importance Of Online Educational Videos Courses', 'importance-of-online-educational-videos-courses', 'It is estimated that the global eLearning market will be worth $325 billion by 2025. Needless to say, with the advent of modern technology, almost everything has switched to embrace technology. It is also very evident that educational institutes are including video educational courses to ensure convenience to students.  Today students don&rsquo;t have to be in a class to learn, they have a choice to learn any topic, at any location and at any given time. ', '&lt;p class=&quot;MsoNormal&quot; xss=removed&gt;&lt;b&gt;&lt;span lang=&quot;EN-US&quot; xss=removed&gt;Importance Of Online Educational Videos Courses&lt;o&gt;&lt;/o&gt;&lt;/span&gt;&lt;/b&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=removed&gt;&lt;span lang=&quot;EN-US&quot; xss=removed&gt;It is estimated that the global eLearning market will be worth $325 billion by 2025. Needless to say, with the advent of modern technology, almost everything has switched to embrace technology. It is also very evident that educational institutes are rapidly including video educational courses to ensure convenience to students.&nbsp;&nbsp;Today students don&rsquo;t have to be in a class to learn, they have a choice to learn any topic, at any location and at any given time.&nbsp;&lt;o&gt;&lt;/o&gt;&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=removed&gt;&lt;b&gt;&lt;span lang=&quot;EN-US&quot; xss=removed&gt;Why are online educational videos important?&lt;o&gt;&lt;/o&gt;&lt;/span&gt;&lt;/b&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=removed&gt;&lt;span lang=&quot;EN-US&quot; xss=removed&gt;The benefits of using online videos are many.&lt;o&gt;&lt;/o&gt;&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=removed&gt;&lt;span lang=&quot;EN-US&quot; xss=removed&gt;Easier to understand: Learning by watching videos stimulates more sensory organs. It is proven that students can grasp more knowledge as compared to using print media or in a traditional classroom.&lt;o&gt;&lt;/o&gt;&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=removed&gt;&lt;span lang=&quot;EN-US&quot; xss=removed&gt;Remote learning experiences: Digital videos provide an environment where tutors can teach from anywhere in the world and reach students all over the globe.&lt;o&gt;&lt;/o&gt;&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=removed&gt;&lt;span lang=&quot;EN-US&quot; xss=removed&gt;Recall important topics: Students have the liberty to repeat topics if they want to recall what they learnt.&nbsp;&lt;o&gt;&lt;/o&gt;&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=removed&gt;&lt;b&gt;&lt;span lang=&quot;EN-US&quot; xss=removed&gt;Why should you choose online courses over traditional ones?&lt;o&gt;&lt;/o&gt;&lt;/span&gt;&lt;/b&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=removed&gt;&lt;span lang=&quot;EN-US&quot; xss=removed&gt;Online course are quickly replacing traditional courses due to the following advantages:&lt;o&gt;&lt;/o&gt;&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=removed&gt;&lt;b&gt;&lt;u&gt;&lt;span lang=&quot;EN-US&quot; xss=removed&gt;Flexible schedules&lt;o&gt;&lt;/o&gt;&lt;/span&gt;&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=removed&gt;&lt;span lang=&quot;EN-US&quot; xss=removed&gt;Taking an online course lets you choose the number of hours you would like to study and a location of your choice. This creates a better learning environment for students.&nbsp;&lt;o&gt;&lt;/o&gt;&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=removed&gt;&lt;b&gt;&lt;u&gt;&lt;span lang=&quot;EN-US&quot; xss=removed&gt;Self-discipline&lt;o&gt;&lt;/o&gt;&lt;/span&gt;&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=removed&gt;&lt;span lang=&quot;EN-US&quot; xss=removed&gt;Studying online encourages self-motivation and discipline. With on-line courses you are inclined to study alone without any deadlines. Self-motivation is a trait that will stick with you and let you stand out even at your place of work.&lt;o&gt;&lt;/o&gt;&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=removed&gt;&lt;b&gt;&lt;u&gt;&lt;span lang=&quot;EN-US&quot; xss=removed&gt;A wide spectrum if courses to select from&lt;o&gt;&lt;/o&gt;&lt;/span&gt;&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=removed&gt;&lt;span lang=&quot;EN-US&quot; xss=removed&gt;Most of the time, when choosing a traditional course, you will have to choose a school which might limit you on which courses to take. Not all schools that appeal to you will offer the program you wish to pursue. When choosing an online course, you have a variety of accredited courses on offer to choose from.&nbsp;&lt;o&gt;&lt;/o&gt;&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=removed&gt;&lt;b&gt;&lt;u&gt;&lt;span lang=&quot;EN-US&quot; xss=removed&gt;Growth of Online Education&lt;o&gt;&lt;/o&gt;&lt;/span&gt;&lt;/u&gt;&lt;/b&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=removed&gt;&lt;span lang=&quot;EN-US&quot; xss=removed&gt;Studies show that about 30% of students in college take online courses. Education experts project this number to grow annually by about 10% by 2023. Previously, institutions offered online courses to beat the competition. Nowadays, many of them offer online courses to create convenience for students rather than taking a competitive approach.&lt;o&gt;&lt;/o&gt;&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=removed&gt;&lt;b&gt;&lt;span lang=&quot;EN-US&quot; xss=removed&gt;Conclusion&lt;o&gt;&lt;/o&gt;&lt;/span&gt;&lt;/b&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=removed&gt;&lt;span lang=&quot;EN-US&quot; xss=removed&gt;With the advancement in technology today, students and teachers feel the need to incorporate educational videos and online courses in their programs to help ease the burden of traditional teaching. Online courses are slowly becoming an integral part of the education system. It is hard to fathom the probability of a world without online courses.&lt;o&gt;&lt;/o&gt;&lt;/span&gt;&lt;/p&gt;', '8480ee7459d876c74fad3a281e513a78.png', 'Online ,Marketing ,Digital ,e-commerce,online strategies ,online education ', 'Importance Of Online Educational Videos Courses\r\n\r\nIt is estimated that the global eLearning market will be worth $325 billion by 2025. Needless to say, with the advent of modern technology, almost everything has switched to embrace technology. It is also very evident that educational institutes are including video educational courses to ensure convenience to students.  Today students donâ€™t have to be in a class to learn, they have a choice to learn any topic, at any location and at any given time.', 1, 14, '2020-06-16 19:49:08', 14, '2020-06-16 20:28:04', 24),
(2, 'Why is the Digital Transformation crucial in todayâ€™s market?', 'why-is-the-digital-transformation-crucial-in-today-s-market', 'In the past decade or so, the best way to engage with customers is through digital media. The speed of transformation when it comes to digital media is phenomenal and companies should embrace the different digital platforms and use it as a tool to grow their business. ', '&lt;p class=&quot;MsoNormal&quot; xss=&quot;removed&quot;&gt;&lt;b xss=&quot;removed&quot;&gt;&lt;span lang=&quot;EN-US&quot; xss=&quot;removed&quot; xss=removed&gt;Why is the Digital Transformation crucial in today&rsquo;s market?&lt;/span&gt;&lt;/b&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=&quot;removed&quot;&gt;&lt;span xss=removed&gt;In the past decade or so, the best way to engage with customers is through digital media. The speed of transformation when it comes to digital media is phenomenal and companies should embrace the different digital platforms and use it as a tool to grow their business.&nbsp;&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=&quot;removed&quot;&gt;&lt;span xss=&quot;removed&quot; xss=removed&gt;As they say, digital strategies should be a part of a company&rsquo;s vision and companies should discover various online options to convenience their employees to embrace digital media as well. People are usually motivated by digital expertise and the innovation it brings with it. So much so that, a low incomed employee will ensure that he gets the best smartphone with all the latest features but will not bother getting himself a good health insurance plan. Digital media has a farfetched impact on the society as well, especially when it comes to interface, on job communication, buying goods and our everyday activities.&nbsp;&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=&quot;removed&quot;&gt;&lt;span xss=&quot;removed&quot; xss=removed&gt;&lt;span lang=&quot;EN-US&quot; xss=&quot;removed&quot;&gt;&nbsp;&lt;/span&gt;According to International Data Corporation (IDC), it is estimated that companies around the world will spend approximately $2 trillion by 2022 on digital transformation and continue spending considerably on digital strategies to boost growth. This proves that every company should invest generously on IT services and digital apps and most importantly they should be in a position to re-invent themselves.&nbsp;&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=&quot;removed&quot;&gt;&lt;span xss=&quot;removed&quot;&gt;&lt;b&gt;&lt;span lang=&quot;EN-US&quot; xss=&quot;removed&quot; xss=removed&gt;&ldquo;Immediate Interaction with clients, leading to immediate action&rdquo;&lt;/span&gt;&lt;/b&gt;&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=&quot;removed&quot;&gt;&lt;span xss=removed&gt;Tools like WhatsApp, Instagram, Facebook, LinkedIn messenger etc. are excellent platforms to interact with customer&rsquo;s, convey important messages and promote company products. Gone are the days where companies spend millions on traditional media for their campaigns or communicate company messages through traditional means. In recent times, you can geo-target customers and convey your message based on their location, their gender, occupation, hobbies, age etc. What is more attractive is the reaction from the target audience is almost immediate and transparent. In addition, on a daily basis, many apps are downloaded and as a company, you need to keep a constant track on what&rsquo;s is trending.&nbsp;&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=&quot;removed&quot;&gt;&lt;span lang=&quot;EN-US&quot; xss=&quot;removed&quot; xss=removed&gt;Picture this&hellip; every minute million of users posts messages on Facebook and WhatsApp and if companies are not a part of this social society, they might lose their popularity and loyalty towards their products and services. Brands that are in the Automobile and Retail sector especially use digital platforms to attract customers and they are slowly moving away from traditional media. Besides, digital media works best for them as they can personalize their message and also announce offers that get an immediate response.&nbsp;&lt;o&gt;&lt;/o&gt;&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=&quot;removed&quot;&gt;&lt;span xss=&quot;removed&quot;&gt;&lt;b&gt;&lt;span lang=&quot;EN-US&quot; xss=&quot;removed&quot; xss=removed&gt;&ldquo;Data is King and use it to entice Customers&rdquo;&lt;/span&gt;&lt;/b&gt;&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=&quot;removed&quot;&gt;&lt;span xss=&quot;removed&quot; xss=removed&gt;Companies spend a considerable amount of their marketing budget on collecting customer data or in most cases they hire professional companies to do the job. Usually, this data is used to convey the product message to new and repeat customers. Most companies also use tracking software&rsquo;s to understand customer reaction and convert them into valuable data, eventually leading to business leads. This also allows companies to personalize messages based on the target audience which will hopefully convince them to buy the product.&nbsp;&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=&quot;removed&quot;&gt;&lt;span xss=&quot;removed&quot;&gt;&lt;b&gt;&lt;span lang=&quot;EN-US&quot; xss=&quot;removed&quot; xss=removed&gt;&ldquo;The importance of Influencers is gaining momentum&rdquo;&nbsp;&lt;/span&gt;&lt;/b&gt;&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=&quot;removed&quot;&gt;&lt;span xss=&quot;removed&quot; xss=removed&gt;Based on the data collected, companies are also hiring influencers who have a huge fan following and usually they are very active on YouTube and Instagram. Influencers are usually very popular with unboxing newly introduced products. It works to a company&rsquo;s advantage to recruitment influencers as they give customers the first-hand experience of what they feel about the product. In addition, companies don&rsquo;t have to spend millions on asking celebrities to endorse their product. They also end up saving a lot of time signing lengthy contracts and abiding by complicated terms and conditions. The point to note here is that these influencers use various digital platforms to convey their message and companies should know exactly who they need to target.&nbsp;&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=&quot;removed&quot;&gt;&lt;span xss=&quot;removed&quot; xss=removed&gt;It&rsquo;s very evident, today if a company wants to be successful or even if they want to launch a marketing campaign, they have to invest in digital platforms and stay up to date with the latest software and apps. Companies have to recruit the best in the industry because they need the right team who can target the right audience. Data analytics is also of paramount importance and this is crucial to understand what a client wants or will be attracted to. And finally, and it&rsquo;s a proven fact that only companies that adapt to digital technology will survive today\'s fierce competition amongst brand.&nbsp;&lt;/span&gt;&lt;/p&gt;&lt;p class=&quot;MsoNormal&quot; xss=&quot;removed&quot;&gt;&lt;span xss=&quot;removed&quot;&gt;&lt;b&gt;&lt;span lang=&quot;EN-US&quot; xss=&quot;removed&quot;&gt;&lt;span xss=&quot;removed&quot; xss=removed&gt;&ldquo;Either to embrace digital transformation or be left behind&rdquo;&lt;/span&gt;&lt;/span&gt;&lt;/b&gt;&lt;/span&gt;&lt;/p&gt;', '831833ba78f00102cf5c5cd080fa1d5e.png', 'Digital,social media ,data,seo ,Facebook ,instagram ,youtube,strategy ', 'In the past decade or so, the best way to engage with customers is through digital media. The speed of transformation when it comes to digital media is phenomenal and companies should embrace the different digital platforms and use it as a tool to grow their business. ', 1, 14, '2020-06-16 20:08:46', 14, '2020-06-16 20:31:06', 23);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent` int(11) DEFAULT 0,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL,
  `font_awesome_class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `code`, `name`, `parent`, `slug`, `date_added`, `last_modified`, `font_awesome_class`, `thumbnail`) VALUES
(1, 'b863c40b88', 'Marketing', 0, 'marketing', 1560121200, NULL, 'fab fa-black-tie', 'category-thumbnail.png'),
(2, '8259184b26', 'Finance', 0, 'finance', 1560121200, NULL, 'fas fa-money-bill-alt', 'category-thumbnail.png'),
(3, 'f525d474f0', 'Designing', 0, 'designing', 1560121200, NULL, 'fas fa-paint-brush', 'category-thumbnail.png'),
(4, '99f042581e', 'Digital &amp; Social Media', 0, 'digital-amp-social-media', 1560121200, NULL, 'fas fa-globe', 'category-thumbnail.png'),
(5, 'aade8e9920', 'Human Resourses', 0, 'human-resourses', 1560121200, NULL, 'fas fa-users', 'category-thumbnail.png'),
(6, 'd3dc655ca3', 'Academic Learning', 0, 'academic-learning', 1560121200, NULL, 'fas fa-book', 'category-thumbnail.png'),
(7, '3b219f3b7e', 'Office Tools', 0, 'office-tools', 1560121200, NULL, 'fas fa-laptop', 'category-thumbnail.png'),
(8, '70f0d2eec2', 'Health &amp; Fitness', 0, 'health-amp-fitness', 1560121200, NULL, 'fas fa-universal-access', 'category-thumbnail.png'),
(9, '146c42717e', 'Management', 0, 'management', 1560121200, NULL, 'far fa-handshake', 'category-thumbnail.png'),
(10, '6d8e561eab', 'Personal Development', 0, 'personal-development', 1560121200, NULL, 'far fa-star', 'category-thumbnail.png'),
(11, '31e0c59f84', 'Fundamentals of Marketing', 1, 'fundamentals-of-marketing', 1560121200, NULL, NULL, NULL),
(12, 'f8385ee05b', 'Branding', 1, 'branding', 1560121200, NULL, NULL, NULL),
(13, '1d62baef3f', 'Sales Techniques', 1, 'sales-techniques', 1560121200, NULL, NULL, NULL),
(14, '7d17c4330c', 'Corporate Communication', 1, 'corporate-communication', 1560121200, NULL, NULL, NULL),
(15, '559089c93e', 'Advertising', 1, 'advertising', 1560121200, NULL, NULL, NULL),
(16, 'dbec5e8c5b', 'Media', 1, 'media', 1560121200, NULL, NULL, NULL),
(17, 'd2b2abc231', 'Marketing Analysis', 1, 'marketing-analysis', 1560121200, NULL, NULL, NULL),
(18, '32853c721e', 'Corporate Finance', 2, 'corporate-finance', 1560121200, NULL, NULL, NULL),
(19, 'ce4167fb96', 'Financial Analysis', 2, 'financial-analysis', 1560121200, 1560121200, NULL, NULL),
(20, 'c7c7457104', 'Financial Planning', 2, 'financial-planning', 1560121200, NULL, NULL, NULL),
(21, '232ce260d4', 'Financial Trading', 2, 'financial-trading', 1560121200, NULL, NULL, NULL),
(22, '0f3d4b0f52', 'Accounting', 2, 'accounting', 1560121200, NULL, NULL, NULL),
(23, 'f1da87e35b', 'Financial Tools', 2, 'financial-tools', 1560121200, NULL, NULL, NULL),
(24, 'f79921bbae', 'Graphic Designing', 3, 'graphic-designing', 1560121200, 1560121200, NULL, NULL),
(25, '016c3979cd', 'Animation', 3, 'animation', 1560121200, NULL, NULL, NULL),
(26, '043997a2c0', 'Illustration', 3, 'illustration', 1560121200, NULL, NULL, NULL),
(27, '5b748c7b15', 'Product Designing', 3, 'product-designing', 1560121200, NULL, NULL, NULL),
(28, 'e16e96000c', 'Web Designing', 3, 'web-designing', 1560121200, NULL, NULL, NULL),
(29, 'eaa878d6d8', 'Search Engine Optimization (SEO)', 4, 'search-engine-optimization-seo', 1560121200, NULL, NULL, NULL),
(30, '3a23410699', 'Search Engine Marketing (SEM)', 4, 'search-engine-marketing-sem', 1560121200, NULL, NULL, NULL),
(31, 'd89f4dfd08', 'Google Analytics', 4, 'google-analytics', 1560121200, NULL, NULL, NULL),
(32, 'aa8511698e', 'Social Media Marketing', 4, 'social-media-marketing', 1560121200, NULL, NULL, NULL),
(33, 'f644388a7e', 'HR Policies', 5, 'hr-policies', 1560121200, NULL, NULL, NULL),
(34, '6327e60127', 'HR Management', 5, 'hr-management', 1560121200, NULL, NULL, NULL),
(35, '55f3d6eef4', 'Training Tools', 5, 'training-tools', 1560121200, NULL, NULL, NULL),
(36, '28e6fb2656', 'Interview Techniques', 5, 'interview-techniques', 1560121200, NULL, NULL, NULL),
(37, '0ad17513c2', 'Sourcing &amp; Recruiting', 5, 'sourcing-amp-recruiting', 1560121200, NULL, NULL, NULL),
(38, '2d53cf28f5', 'Employee Management', 5, 'employee-management', 1560121200, NULL, NULL, NULL),
(39, 'f58e8784d1', 'Performance Appraisal', 5, 'performance-appraisal', 1560121200, NULL, NULL, NULL),
(40, '20bab539be', 'Languages', 6, 'languages', 1560121200, NULL, NULL, NULL),
(41, '9136716888', 'Science', 6, 'science', 1560121200, NULL, NULL, NULL),
(42, 'f10efcba1b', 'Mathematics', 6, 'mathematics', 1560121200, NULL, NULL, NULL),
(43, 'af8e2f27b3', 'Medicine', 6, 'medicine', 1560121200, NULL, NULL, NULL),
(44, 'ffec991bf0', 'Engineering', 6, 'engineering', 1560121200, NULL, NULL, NULL),
(45, 'e769149afd', 'ILS English Test', 6, 'ils-english-test', 1560121200, NULL, NULL, NULL),
(46, '1f8ded5a84', 'Exam Training', 6, 'exam-training', 1560121200, NULL, NULL, NULL),
(47, '22507a4bbe', 'Other Subjects', 6, 'other-subjects', 1560121200, NULL, NULL, NULL),
(48, '94aada62f9', 'Microsoft Excel', 7, 'microsoft-excel', 1560121200, NULL, NULL, NULL),
(49, 'd722d88f3f', 'Advanced Excel', 7, 'advanced-excel', 1560121200, NULL, NULL, NULL),
(50, 'daa860578c', 'Microsoft Power Point', 7, 'microsoft-power-point', 1560121200, NULL, NULL, NULL),
(51, 'da8c694cf6', 'Apple Applications', 7, 'apple-applications', 1560121200, NULL, NULL, NULL),
(52, '2b0c83e910', 'Google Products', 7, 'google-products', 1560121200, NULL, NULL, NULL),
(53, 'e593a2c30a', 'Modern Yoga', 8, 'modern-yoga', 1560121200, NULL, NULL, NULL),
(54, '4981b184d1', 'Sports', 8, 'sports', 1560121200, NULL, NULL, NULL),
(55, '9e52f5849f', 'Health &amp; Nutrition', 8, 'health-amp-nutrition', 1560121200, NULL, NULL, NULL),
(56, '7734c8370c', 'Dancing Classes', 8, 'dancing-classes', 1560121200, NULL, NULL, NULL),
(57, '9a3c056234', 'Cooking Classes', 8, 'cooking-classes', 1560121200, NULL, NULL, NULL),
(58, 'b78d58b925', 'Dietician', 8, 'dietician', 1560121200, NULL, NULL, NULL),
(59, '923d06dd8a', 'Sales Strategies', 9, 'sales-strategies', 1560121200, NULL, NULL, NULL),
(60, '840adcd2bf', 'Operations Management', 9, 'operations-management', 1560121200, NULL, NULL, NULL),
(61, '20034f0db6', 'Office Set-up', 9, 'office-set-up', 1560121200, NULL, NULL, NULL),
(62, 'a95bda9bbe', 'Budget Planning', 9, 'budget-planning', 1560121200, NULL, NULL, NULL),
(63, '68f250222c', 'Project Management', 9, 'project-management', 1560121200, NULL, NULL, NULL),
(64, 'efef1f0141', 'Corporate Presentations', 9, 'corporate-presentations', 1560121200, NULL, NULL, NULL),
(65, 'b77bfcc173', 'Personality Development', 10, 'personality-development', 1560121200, NULL, NULL, NULL),
(66, 'ea26d98950', 'Leadership Skills', 10, 'leadership-skills', 1560121200, NULL, NULL, NULL),
(67, 'fcb591a916', 'Public Speaking', 10, 'public-speaking', 1560121200, NULL, NULL, NULL),
(68, '146d3b4875', 'Networking Skills', 10, 'networking-skills', 1560121200, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ccavenue`
--

CREATE TABLE `ccavenue` (
  `id` int(11) NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ccavenue`
--

INSERT INTO `ccavenue` (`id`, `key`, `value`) VALUES
(1, 'ccavenue', '[{\"active\":\"1\",\"currency\":\"USD\",\"merchant_id\":\"45783\",\"access_code\":\"AVKT03GI11CI78TKIC\",\"working_key\":\"C1DA12D31321020C65AD2A283ED00676\"}]'),
(2, 'ccavenue_supported', 'AED,USD,EUR,GBP,SAR,BHD,OMR,KWR,QAR');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('1c5a5a110eea8236df45c839d6cf4a86f9aac815', '123.231.124.125', 1598679447, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539383637393434373b636172745f6974656d737c613a303a7b7d757365725f69647c733a323a223535223b726f6c655f69647c733a313a2232223b726f6c657c733a343a2255736572223b6e616d657c733a393a227468757368206b6161223b757365725f6c6f67696e7c733a313a2231223b),
('866f3946330db3ce002e60b145c67c43c0f3d4c2', '123.231.124.125', 1598679946, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539383637393934363b636172745f6974656d737c613a303a7b7d757365725f69647c733a323a223535223b726f6c655f69647c733a313a2232223b726f6c657c733a343a2255736572223b6e616d657c733a393a227468757368206b6161223b757365725f6c6f67696e7c733a313a2231223b),
('383e8e66f53f4cb3ba645471a75c4f1a85aed304', '123.231.124.125', 1598680296, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539383638303239363b636172745f6974656d737c613a303a7b7d757365725f69647c733a323a223535223b726f6c655f69647c733a313a2232223b726f6c657c733a343a2255736572223b6e616d657c733a393a227468757368206b6161223b757365725f6c6f67696e7c733a313a2231223b),
('24162027259e60c52bd64e039a8e4c65c2a76e32', '123.231.124.125', 1598680599, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539383638303539393b636172745f6974656d737c613a303a7b7d757365725f69647c733a323a223535223b726f6c655f69647c733a313a2232223b726f6c657c733a343a2255736572223b6e616d657c733a393a227468757368206b6161223b757365725f6c6f67696e7c733a313a2231223b),
('24876227c6fe04785ebc8f30b79b0f2edce18359', '123.231.124.125', 1598680940, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539383638303934303b636172745f6974656d737c613a303a7b7d757365725f69647c733a323a223535223b726f6c655f69647c733a313a2232223b726f6c657c733a343a2255736572223b6e616d657c733a393a227468757368206b6161223b757365725f6c6f67696e7c733a313a2231223b),
('f0c216024f2a09fe45201b9d2d919bafbd874e50', '123.231.124.125', 1598681300, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539383638313330303b636172745f6974656d737c613a303a7b7d757365725f69647c733a323a223535223b726f6c655f69647c733a313a2232223b726f6c657c733a343a2255736572223b6e616d657c733a393a227468757368206b6161223b757365725f6c6f67696e7c733a313a2231223b),
('188e566ee126b11c767929f6738745bc4d4eff1d', '123.231.124.125', 1598681601, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539383638313630313b636172745f6974656d737c613a303a7b7d),
('8415a3a0cb732fa9a53b4578389cd985bda6f985', '123.231.124.125', 1598682081, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539383638323038313b636172745f6974656d737c613a303a7b7d757365725f69647c733a323a223134223b726f6c655f69647c733a313a2231223b726f6c657c733a353a2241646d696e223b6e616d657c733a31333a22436f75727365202057696e6773223b757365725f6c6f67696e7c733a313a2231223b61646d696e5f6c6f67696e7c733a313a2231223b),
('8c310249abac9620b95588f9e738cea3cf2a9201', '123.231.124.125', 1598682440, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539383638323434303b636172745f6974656d737c613a303a7b7d757365725f69647c733a323a223134223b726f6c655f69647c733a313a2231223b726f6c657c733a353a2241646d696e223b6e616d657c733a31333a22436f75727365202057696e6773223b757365725f6c6f67696e7c733a313a2231223b61646d696e5f6c6f67696e7c733a313a2231223b),
('1c4dfa4682c5c20336eabe7802196fbbfd59c770', '123.231.124.125', 1598682765, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539383638323736353b636172745f6974656d737c613a303a7b7d757365725f69647c733a323a223134223b726f6c655f69647c733a313a2231223b726f6c657c733a353a2241646d696e223b6e616d657c733a31333a22436f75727365202057696e6773223b757365725f6c6f67696e7c733a313a2231223b61646d696e5f6c6f67696e7c733a313a2231223b),
('17aad1ea1dcf287f486b1754edb4b42879be9619', '123.231.124.125', 1598683097, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539383638333039373b636172745f6974656d737c613a303a7b7d757365725f69647c733a323a223134223b726f6c655f69647c733a313a2231223b726f6c657c733a353a2241646d696e223b6e616d657c733a31333a22436f75727365202057696e6773223b757365725f6c6f67696e7c733a313a2231223b61646d696e5f6c6f67696e7c733a313a2231223b),
('19fce0cd1d5f302783fc1d683b5d538eceee9837', '123.231.124.125', 1598683658, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539383638333635383b636172745f6974656d737c613a303a7b7d757365725f69647c733a323a223134223b726f6c655f69647c733a313a2231223b726f6c657c733a353a2241646d696e223b6e616d657c733a31333a22436f75727365202057696e6773223b757365725f6c6f67696e7c733a313a2231223b61646d696e5f6c6f67696e7c733a313a2231223b),
('b6de798e3dc75903b8c2b7f038e6b17bb636c2d1', '123.231.124.125', 1598684389, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539383638343338393b636172745f6974656d737c613a303a7b7d757365725f69647c733a323a223134223b726f6c655f69647c733a313a2231223b726f6c657c733a353a2241646d696e223b6e616d657c733a31333a22436f75727365202057696e6773223b757365725f6c6f67696e7c733a313a2231223b61646d696e5f6c6f67696e7c733a313a2231223b),
('5cb4ba6ba7af326be4a7a3b4e88b826b67631e11', '123.231.124.125', 1598684696, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539383638343639363b636172745f6974656d737c613a303a7b7d),
('2bec05458061c545ac0d46e434d9e419b28bd237', '123.231.124.125', 1598684708, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539383638343639363b636172745f6974656d737c613a303a7b7d757365725f69647c733a323a223134223b726f6c655f69647c733a313a2231223b726f6c657c733a353a2241646d696e223b6e616d657c733a31333a22436f75727365202057696e6773223b61646d696e5f6c6f67696e7c733a313a2231223b);

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) UNSIGNED NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `commentable_id` int(11) DEFAULT NULL,
  `commentable_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `short_description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `outcomes` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `section` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `requirements` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` double DEFAULT NULL,
  `discount_flag` int(11) DEFAULT 0,
  `discounted_price` int(11) DEFAULT NULL,
  `level` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL,
  `visibility` int(11) DEFAULT NULL,
  `is_top_course` int(11) DEFAULT 0,
  `is_admin` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `course_overview_provider` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_free_course` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `title`, `short_description`, `description`, `outcomes`, `language`, `category_id`, `sub_category_id`, `section`, `requirements`, `price`, `discount_flag`, `discounted_price`, `level`, `user_id`, `thumbnail`, `video_url`, `date_added`, `last_modified`, `visibility`, `is_top_course`, `is_admin`, `status`, `course_overview_provider`, `meta_keywords`, `meta_description`, `is_free_course`) VALUES
(44, 'Turn Opportunities into Business Deals', 'How to convert a Sales Lead or Business Referral into Business\r\n', '<p class=\"MsoNormal\" xss=\"removed\"><span xss=\"removed\" xss=removed><span lang=\"EN-US\" xss=removed><font face=\"arial, sans-serif\" xss=removed>In this short\r\nvideo, I will be talking about the importance of Business Reference and Sales\r\nLeads that we receive from our members, from our colleagues or even from our company.Â </font></span><span xss=\"removed\" xss=removed>One must have a\r\npositive outlook in life every time you receive a sales lead because opportunitiesÂ usuallyÂ are the starting point ofÂ greater things in life and you should explore it at any cost.Â </span></span></p><p class=\"MsoNormal\" xss=\"removed\"><span xss=\"removed\" xss=removed><span lang=\"EN-US\"><font face=\"arial, sans-serif\">I will also narrate a very interesting story of a person called Steve who got the opportunity of a lifetime and because of his positive outlook, today he is aÂ very successful person.Â Â </font></span><span xss=\"removed\">If you are\r\npursuing a career in sales or marketing you should always remember one thing,\r\nwhen an opportunity knocks at your door, whether it\'s small or big, you should\r\nnever ignore it, always explore the possibilities because you never know what\r\nyou gonna get.</span></span></p><p class=\"MsoNormal\" xss=\"removed\"><span xss=removed>This is a very powerful course and it will benefit you immensely. Not only will it help you in your personal life but in your professional career as well.Â </span><br></p>', '[\"How to convert an opportunity into Business. \",\"You will improve your networking skills.\",\"You will be take your sales leads more seriously. \",\"It will help you in your career especially if your in Marketing or Sales.\"]', 'english', 9, 59, '[1]', '[\"Marketing Professionals\",\"Sales Professionals\",\"Students \",\"Passionate for Sales & Marketing \",\"Business Owners \"]', 5, 1, 2, 'advanced', 44, NULL, 'https://vimeo.com/430626832', 1592092800, 1592697600, NULL, 1, 0, 'active', 'vimeo_upload', 'Sales,Marketing ,Networking ,Business ,Law of attraction,Communications,Strategies,Business School ,Operations,Organizations', 'Why you should take your business referrals or sales leads seriously. Why you should consider every opportunity as a chance to generate business.', NULL),
(47, 'Learn the Art of attracting what you Desire?', 'Understand the Law of Attraction & how to implement it in your business?', '<p class=\"MsoNormal\" xss=\"removed\"><span lang=\"EN-US\" xss=\"removed\">This video talks about the Law of Attraction and how to implement it in your business or your personal life. We will be highlighting certain key parameters that influence your brain when it comes to taking a decision.Â Â </span></p><p class=\"MsoNormal\" xss=\"removed\"><font face=\"Calibri, sans-serif\"><span xss=\"removed\">We will be talking about why you need to beÂ </span></font><span xss=\"removed\">specific, clear and consistent about your ask or desire and if you work towards it, you will receive what you want in abundance.</span></p><p class=\"MsoNormal\" xss=\"removed\"><span lang=\"EN-US\" xss=\"removed\">This video will also help you achieve your targets in your business and we will talk about what should be your approach toward opportunities that are presented to you. The principles in this video, if implemented effectively in your life, it will benefit you to a great extend. In addition, there is an interesting case study as well and that will give you a better understanding of how you can succeed in whatever you do.</span></p><p class=\"MsoNormal\" xss=\"removed\"><span lang=\"EN-US\" xss=\"removed\"><br></span></p><p class=\"MsoNormal\" xss=\"removed\"><br></p><p class=\"MsoNormal\" xss=\"removed\"><b><span lang=\"EN-US\" xss=\"removed\"><o></o></span></b></p>', '[\"If the principles in this video are practiced, then you will attract a lot of wealth in your life\",\"You will learn to have a positive outlook to all the opportunities that are presented to you\",\"How to convert an opportunity into Business. \"]', 'english', 10, 65, '[2]', '[\"Should have a desire to success \",\"All Executives & Professionals who want to excel in your career\",\"Some who want to attract wealth and success into their life\",\"Some one who believes in the Law of Attraction\"]', 5, 1, 2, 'advanced', 46, NULL, 'https://vimeo.com/431220560', 1592265600, 1595203200, NULL, 1, 0, 'active', 'vimeo_upload', 'Law of Attraction ,Success ,Wealth ,Happiness ,Business ,Marketing ,Sales ,Development ,Personality ', 'Specific is Terrific - The Law of Attraction and how to implement it in your business', NULL),
(48, 'How to be Unique in a Presentation & Standout amongst the others?', 'Learn how to promote your Products & Services by using the \"5\" Block Model in your Presentation', '<p class=\"MsoNormal\" xss=removed><span xss=removed>Your presentations skills </span><span lang=\"EN-US\" xss=removed>play a pivotal role in your career, especially if your in sales or marketing. This video will help you with the key elements which are also known as the â€œ5â€ Block model strategy that you need to incorporate in your presentation in order to make it impactful.<o></o></span></p><p class=\"MsoNormal\" xss=removed><span lang=\"EN-US\" xss=removed>Â </span></p><p class=\"MsoNormal\" xss=removed><span lang=\"EN-US\" xss=removed>When it comes to promoting your products or services you need to be unique in front of your potential clients and this video will guide you with the essential tips on how to be outstanding. The ultimate objective of a presentation should be targeted towards convincing your clients that you are the best option to partner in the market and if they don\'t partner with you, they will end up losing a great opportunity.<o></o></span></p><p class=\"MsoNormal\" xss=removed><span lang=\"EN-US\" xss=removed>Â </span></p><p class=\"MsoNormal\" xss=removed><span lang=\"EN-US\" xss=removed>This video will also help you with the essential guidelines of how you can improve your elevator pitch when you get an opportunity to prove to your audience why you\'re better than the competition.</span></p><p class=\"MsoNormal\" xss=removed><br></p><p class=\"MsoNormal\" xss=removed><span lang=\"EN-US\" xss=removed>In every presentation, you have to define your objectives. In other words, you need to decide w</span><span xss=removed>hat are the main points </span><span lang=\"EN-US\" xss=removed>you</span><span xss=removed> want </span><span lang=\"EN-US\" xss=removed>your</span><span xss=removed> audience to take away from </span><span lang=\"EN-US\" xss=removed>your</span><span xss=removed> presentation? Your presentation should provide</span><span xss=removed>Â focus </span><span lang=\"EN-US\" xss=removed>to your </span><span xss=removed>audience on what they will gain </span><span lang=\"EN-US\" xss=removed>while </span><span xss=removed>listening to your presentation.<o></o></span></p><p class=\"MsoNormal\" xss=removed><span xss=removed><o>Â </o></span></p><p class=\"MsoNormal\" xss=removed><span lang=\"EN-US\" xss=removed>This is a powerful video which will surely you help you prepare for your next presentation. Donâ€™t miss this opportunity.<o></o></span></p><p class=\"MsoNormal\" xss=removed><span xss=removed></span></p><p class=\"MsoNormal\" xss=removed><span lang=\"EN-US\" xss=removed><o></o></span></p>', '[\"Will be a better presenter or promoter\",\"Course will help individuals in their careers\",\"Course will help executives on how to promote in trade exhibitions \",\"Course will help you understand how to standout amongst your audience\"]', 'english', 1, 13, '[3]', '[\"Marketing & Sales Enthusiast \",\"Business professionals \",\"General understanding of Promoting products\",\"Basic Knowledge of Industries\"]', 5, 1, 2, 'advanced', 47, NULL, 'https://vimeo.com/430662373', 1592524800, 1592524800, NULL, 1, 0, 'active', 'vimeo_upload', 'sales,marketing ,promotion ,exhibitions,presentation skills,unique presentation ', 'Learn the art of how to standout in a presentation using the \"5\" Block Model Strategy. This video will help you understand how to promote your products or services through a presentation especially when your in front of a large audience.  ', NULL),
(49, 'How to deliver an Impactful Presentation', 'A masterclass on how to improve your presentation skills and impress your audience', '<p class=\"MsoNormal\" xss=removed>Stakes can be very high when it comes to delivering a presentation, especially if itâ€™s critical to your company. If you get the plot wrong, you can lose your clients, jeopardies your project and damage your reputation.<o></o></p><p class=\"MsoNormal\" xss=removed><o>Â </o></p><p class=\"MsoNormal\" xss=removed>This course will teach you how to improve your presentation skills, help you eliminate the fear of facing an audience and it will highlight the importance of using certain key elements in order to come up with an impactful presentation.<o></o></p><p class=\"MsoNormal\" xss=removed><o>Â </o></p><p class=\"MsoNormal\" xss=removed>In a presentation, the delivery has to be spot on. Like they say presentations donâ€™t fail because it was not pretty enough â€“ they fail because your message was not clear enough to the audience. When you\'re presenting you need to be specific about your ask and in this course, we will teach you how to be unique and put your message across effectively.<o></o></p><p class=\"MsoNormal\" xss=removed><o>Â </o></p><p class=\"MsoNormal\" xss=removed>This is a very powerful course and it touches upon all the vital points that you need to consider in order to increase your companyâ€™s or brandâ€™s presence amongst your target audience. We guarantee you after this course you will be a better presenter and it will surely help you in your career. Â <o></o></p><p><br></p>', '[\"Candidates will improve their presentation skills \",\"How to convert an opportunity into Business. \",\"Course will help you promote your product in an efficiently way\",\"Course whelp you achieve you sales targets \"]', 'english', 9, 64, '[4,5,6,7,8,9,10]', '[\"Marketing & Sales Professionals\",\"Basic knowledge of Sales and Marketing \",\"Someone who wants to improve their presentation skills\",\"Basic knowledge of the corporate world\"]', 10, 1, 4, 'advanced', 45, NULL, 'https://vimeo.com/432792491', 1593129600, 1593129600, NULL, 1, 0, 'active', 'vimeo_upload', 'marketing ,sales ,presentation skill,presentation ,promotion ,sales targets ', 'This course will teach you how to improve your presentation skills, help you eliminate the fear of facing an audience and it will highlight the importance of using certain key elements in order to come up with an impactful presentation. ', NULL),
(50, 'How to generate business through Networking', 'Tricks & best practices to get recognised & generate business through Networking', '<p class=\"MsoNormal\" xss=removed><span lang=\"EN-US\" xss=removed>P</span><span xss=removed>eople who are well connected â€¦ are the most successful people on this planet. Investing in people will always work to your benefit either in your personal life or in your professional career.Â </span></p><p class=\"MsoNormal\" xss=removed><span lang=\"EN-US\" xss=removed>In this course we will be talking about:<o></o></span></p><p class=\"MsoNormal\" xss=removed><span xss=removed>â€¢ How to network with a group of people,Â <o></o></span></p><p class=\"MsoNormal\" xss=removed><span xss=removed>â€¢ How to stand out in a networking group,Â <o></o></span></p><p class=\"MsoNormal\" xss=removed><span xss=removed>â€¢ How to ask for business thru networkingÂ <o></o></span></p><p class=\"MsoNormal\" xss=removed><span lang=\"EN-US\" xss=removed><o>Â </o></span></p><p class=\"MsoNormal\" xss=removed><span xss=removed>Whether you are in a junior or a senior position, networking will not only help you explore new opportunities</span><span lang=\"EN-US\" xss=removed> or </span><span xss=removed>generate more business, it will also help you associate with a wide range of people likeâ€¦ Influencers, Mentors, Celebrities and Consultants.</span></p><p class=\"MsoNormal\" xss=removed><span xss=removed><br></span></p><p class=\"MsoNormal\" xss=removed><span lang=\"EN-US\" xss=removed>This is a power course where you will get to learn the essentials of how you can be visible amongst your target audience and how you can convince them that you are the best option to partner with.Â </span><span xss=removed>This course also comes with case studies and examples of how to shine and excel in whatever your pursuing.</span></p><p class=\"MsoNormal\" xss=removed><span lang=\"EN-US\" xss=removed>Looking forward to seeing you at the other end.<o></o></span></p>', '[\"One will learn how to effectively Network \",\"One will learn how to improve their sales thru networking \",\"One will learn how to develop business relationship \",\"One will learn how to outshine their competition \"]', 'english', 10, 68, '[12,13,14,15,16,17,18,19]', '[\"Passion to connect with clients\",\"Passion for sales \",\"Basic understanding of Marketing and Sales\",\"Passion for networking to increase sales\"]', 15, 1, 5, 'advanced', 44, NULL, 'https://vimeo.com/435150045', 1593734400, 1593734400, NULL, 1, 0, 'active', 'vimeo_upload', 'networking ,sales ,marketing ,personal development ', 'Tricks & best practices to get recognised & generate business through Networking', NULL),
(51, 'Fundamentals of Business Finance ', 'All you need to know about Business Finance & how it can help you grow', '<p class=\"MsoNormal\" xss=removed><span lang=\"EN-GB\" xss=removed>This is a useful course if you want to learn everything about business finance and how to use it to your advantage and succeed in business. In addition, this course will highlight the <b><i><u>pulse of the business</u></i></b> through 5 major scopes of areas:</span></p><p class=\"MsoListParagraphCxSpFirst\" xss=removed><span lang=\"EN-GB\" xss=removed>1.<span xss=removed>Â Â Â Â Â </span></span><span lang=\"EN-GB\" xss=removed>Understanding of Business Reports</span><span lang=\"EN-GB\" xss=removed>,<o></o></span></p><p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"EN-GB\" xss=removed>2.<span xss=removed>Â Â Â Â Â </span></span><span lang=\"EN-GB\" xss=removed>Cash flow management</span><span lang=\"EN-GB\" xss=removed>,<o></o></span></p><p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"EN-GB\" xss=removed>3.<span xss=removed>Â Â Â Â Â </span></span><span lang=\"EN-GB\" xss=removed>Accounts Receivables management</span><span lang=\"EN-GB\" xss=removed>,<o></o></span></p><p class=\"MsoListParagraphCxSpMiddle\" xss=removed><span lang=\"EN-GB\" xss=removed>4.<span xss=removed>Â Â Â Â Â </span></span><span lang=\"EN-GB\" xss=removed>Inventory Control</span><span lang=\"EN-GB\" xss=removed>Â and<o></o></span></p><p class=\"MsoListParagraphCxSpLast\" xss=removed><span lang=\"EN-GB\" xss=removed>5.<span xss=removed>Â Â Â Â Â </span></span><span lang=\"EN-GB\" xss=removed>Cost Control & Cost Analysis</span></p><p class=\"MsoNormal\" xss=removed><span lang=\"EN-GB\" xss=removed>These areas are indispensable for any business and enable the decision-makers to make strategic decisions that support growth & profitability of every business</span></p><p class=\"MsoNormal\" xss=removed><span lang=\"EN-GB\" xss=removed></span></p><p class=\"MsoNormal\" xss=removed><span lang=\"EN-GB\" xss=removed>This course will help you understand how to improve your cash flows & cost-cutting, financial transparency, receivables controls, loss prevention and effective internal control system.</span></p><p class=\"MsoNormal\" xss=removed><span lang=\"EN-GB\" xss=removed>If you want to grow your business to the next level, take control of your business, master your stock, receivables, managing compliance and test/measure your business then this course is for you.<o></o></span></p><p class=\"MsoNormal\" xss=removed><span lang=\"EN-GB\" xss=removed></span></p><p class=\"MsoNormal\" xss=removed><span lang=\"EN-GB\" xss=removed>Looking forward to seeing you at the other end.Â <o></o></span></p><p class=\"MsoNormal\" xss=removed><span lang=\"EN-GB\" xss=removed><o><br></o></span></p><p class=\"MsoListParagraphCxSpFirst\" xss=removed><br></p><p class=\"MsoListParagraphCxSpLast\" xss=removed><br></p><p class=\"MsoListParagraphCxSpLast\" xss=removed><br></p><p class=\"MsoListParagraphCxSpLast\" xss=removed><br></p><p class=\"MsoListParagraphCxSpLast\" xss=removed><br></p>', '[\"Better understanding of Business Reports\",\"Better understanding of Cash Flow management\",\"One will learn how accounts receivables works\",\"Importance of Inventory Control and Fundamentals\",\"Better understanding of Cost Control & Cost Analysis  \",\"Better understanding of Business Financing \"]', 'english', 2, 18, '[20,21,22,23]', '[\"Passion to learn Financial Solutions\",\"Finance Professionals\",\"Basic understanding of finance\",\"Basic knowledge of how a company functions\"]', 10, 1, 4, 'advanced', 50, NULL, 'https://vimeo.com/435360648', 1593820800, 1593820800, NULL, 1, 0, 'active', 'vimeo_upload', 'business finance ,Cost Control,Cost Analysis ,Cash Flow, Business Reports,Inventory Control ', 'If you want to grow your business to the next level, take control of your business, master your stock, receivables, managing compliance and test/measure your business this course is for you. This is an excellent course if you want to learn the fundamentals of Business Finance. ', NULL),
(52, 'How to prepare for an Interview? ', 'Learn what you need to do to get hired and how you can impress your interviewer?', '<p class=\"MsoNormal\" xss=removed><span xss=removed>Every time you apply for a job, consider this, on average there are thousands of people applying for the same job. So how do you stand out amongst the thousands of CVâ€™s that have piled up on the HR managers table?Â <o></o></span></p><p class=\"MsoNormal\" xss=removed><span xss=removed>Â </span></p><p class=\"MsoNormal\" xss=removed><span lang=\"EN-US\" xss=removed>An</span><span xss=removed> employer is investing in you and he or she will have to decide if you\'re worth the investment in a short span of time.Â <o></o></span></p><p class=\"MsoNormal\" xss=removed><span xss=removed>At this point, all you will be thinking in your head is how to impress the person across the table and standout amongst the others and of course make a lasting impression.</span></p><p class=\"MsoNormal\" xss=removed><span xss=removed>This course will focus on the following elements that will prepare you for an interview:</span></p><p class=\"MsoNormal\" xss=removed><span xss=removed>Â </span></p><p class=\"MsoNormal\" xss=removed><b><span xss=removed>We will also talk about how toÂ <o></o></span></b></p><ul type=\"disc\" xss=removed><li class=\"MsoNormal\" xss=removed><span lang=\"EN-US\">How to write a covering letter?</span><o></o></li><li class=\"MsoNormal\" xss=removed><span lang=\"EN-US\">How to draft an impactful CV?</span><o></o></li><li class=\"MsoNormal\" xss=removed><span lang=\"EN-US\">How to </span>Face an Interviewer<span lang=\"EN-US\">?</span><o></o></li><li class=\"MsoNormal\" xss=removed>The various interview ethics and norms<o></o></li><li class=\"MsoNormal\" xss=removed>How to engage with the <span lang=\"EN-US\">interviewer</span> in the first 5 mins<span lang=\"EN-US\">?</span><o></o></li><li class=\"MsoNormal\" xss=removed>How to prove that you will be an asset to the company<span lang=\"EN-US\">?</span><o></o></li><li class=\"MsoNormal\" xss=removed>How to be prepared with any questions the panel might ask <span lang=\"EN-US\">?</span>... questions like tell me something about yourself or why do you want to leave your present job, how much salary do you expect etcâ€¦Â <o></o></li><li class=\"MsoNormal\" xss=removed>We will also talk about the questions you need to ask the interviewer<span lang=\"EN-US\">?</span><o></o></li><li class=\"MsoNormal\" xss=removed>How to do a background check on the company you intend to join<span lang=\"EN-US\">?</span><o></o></li><li class=\"MsoNormal\" xss=removed>Researching on the credentials of the interviewer beforehand andÂ <o></o></li><li class=\"MsoNormal\" xss=removed><span lang=\"EN-US\">M</span>ost importantly how to stand out amongst all the candidates<span lang=\"EN-US\">?</span><o></o></li></ul><p class=\"MsoNormal\" xss=removed><span xss=removed>Â </span></p><p class=\"MsoNormal\" xss=removed><span lang=\"EN-US\">This is a master course that\'ll definitely help you prepare for your next interview, so if you want to get hired and convince the interviewer that you are a worthwhile investment then this course is for you. We have also attached a copy of a CV template in word that will help you draft a CV and also a sample cover letter for your reference.<o></o></span></p><p class=\"MsoNormal\" xss=removed><span lang=\"EN-US\">We look forward to seeing you on the other end.<o></o></span></p>', '[\"You will learn how to prepare for an interview\",\"You will be prepared with all the answers that an interviewer may ask you\",\"You will learn what not to ask in an interview \",\"You will learn how to prepare a CV and write a Covering Letter\",\"You will learn how to connivence the interviewer that you are the best candidate to hire\"]', 'english', 5, 36, '[24,25,26,27,28,29,30]', '[\"Job seekers looking for a change\",\"Basic Education\",\"Basic understanding of the corporate sector\",\"Interns and fresh graduates\"]', 10, 1, 4, 'advanced', 44, NULL, 'https://vimeo.com/436373535', 1594166400, 1594166400, NULL, 1, 0, 'active', 'vimeo_upload', 'Interview,Job,cv ,resume,how to prepare for an interview,how to get recruited ,human resource ', 'Learn what you need to do to get hired and how you can impress your interviewer? This is a master course that\'ll definitely help you prepare for your next interview, so if you want to get hired and convince the interviewer that you are a worthwhile investment then this course is for you.', NULL),
(53, 'How to design an impressive LinkedIn profile?', 'Learn the key vectors that you need to consider when designing a LinkedIn profile', '<p class=\"MsoNormal\" xss=\"removed\"><span lang=\"EN-US\" xss=\"removed\">Your </span><span xss=\"removed\">LinkedIn profile page is the foundation for your personal branding. </span><span lang=\"EN-US\" xss=\"removed\">You need to regularly add features in your profile in order </span><span xss=\"removed\">to increase </span><span lang=\"EN-US\" xss=\"removed\">your chances of using it as a</span><span xss=\"removed\"> personal marketing platform. </span><span lang=\"EN-US\" xss=\"removed\">This course talks about certain key vectors that you need to consider in order to come up with an impressive LinkedIn profile.</span></p><p class=\"MsoNormal\" xss=\"removed\"><span xss=removed>If these vectors are considered, you will be in a position to not only attract more audience to your page, but you will also increase your connection and brand presence.</span></p><p class=\"MsoNormal\" xss=\"removed\"><span lang=\"EN-US\" xss=\"removed\" xss=removed>Consider this, </span><span xss=\"removed\" xss=removed>LinkedIn is </span><span lang=\"EN-US\" xss=\"removed\" xss=removed>one of the most influential </span><span xss=\"removed\" xss=removed>networking websiteÂ </span><span lang=\"EN-US\" xss=\"removed\" xss=removed>for </span><span xss=\"removed\" xss=removed>professionals</span><span lang=\"EN-US\" xss=\"removed\" xss=removed> and job seekers</span><span xss=\"removed\" xss=removed>. </span><span lang=\"EN-US\" xss=\"removed\" xss=removed>With more than </span><span xss=\"removed\" xss=removed>500 million members</span><span xss=\"removed\" xss=removed> </span><span xss=\"removed\" xss=removed>in over 200 countries,</span><span xss=\"removed\" xss=removed> <span lang=\"EN-US\">itâ€™s the perfect platform to generate business. This course might prove to be important for you as it talks about the key elements you need to incorporate in order to stand out. In addition, a</span></span><span xss=\"removed\" xss=removed> professionally </span><span lang=\"EN-US\" xss=\"removed\" xss=removed>crafted</span><span xss=\"removed\" xss=removed> LinkedIn profile create</span><span lang=\"EN-US\" xss=\"removed\" xss=removed>s</span><span xss=\"removed\" xss=removed> an online professional brand which can help open doors to opportunities</span><span lang=\"EN-US\" xss=\"removed\" xss=removed> that your seeking.Â </span><span lang=\"EN-US\" xss=\"removed\" xss=removed>If you want to </span><span xss=\"removed\" xss=removed>showcase your profile, expertise, recommendations and connections,</span><span xss=\"removed\" xss=removed> <span lang=\"EN-US\">as well as highlight your achievements, then this course is for you. The useful tips in this course will definitely help you use this platform more effectively and you will surely benefit from it.</span></span><br></p><p class=\"MsoNormal\" xss=\"removed\"><span lang=\"EN-US\" xss=\"removed\"><o>Â </o></span></p><p class=\"MsoNormal\" xss=\"removed\"><span xss=\"removed\"><o>Â </o></span></p>', '[\"How to prepare a LinkedIn Profile \",\"How to attract more clients to your page\",\"What are the key vectors you need to use to attract more traffic to your site\",\"How to seek a job on LinkedIn\"]', 'english', 4, 32, '[31,32,33]', '[\"Marketing Professionals\",\"Marketing & Sales Professionals\",\"Passion to connect with clients\",\"Job seekers\",\"Networkers\",\"HR Consultants \"]', 10, 1, 2, 'advanced', 52, NULL, 'https://vimeo.com/443725086', 1596240000, 1596240000, NULL, 1, 0, 'active', 'vimeo_upload', 'LinkedIn ,Sales ,Marketing ,Jobseekers,SME,business ,networking ,HR', 'Your LinkedIn profile page is the foundation for your personal branding. You need to regularly add features in your profile in order to increase your chances of using it as a personal marketing platform. This course talks about certain key vectors that you need to consider in order to come up with an impressive LinkedIn profile. ', NULL),
(55, 'My Signature Dishes ', 'Cranberry Cake, Fish Tawa Fry, Dynamite Shrimps, Magnolia Bakery Banana Pudding, Shrimp Pasta & Bread Toast', '<p>If you want to know about the secrete of making mouth-watering dishes, then this is the course for you. In this course, I will be talking about how to traditionally make dishes that are easy to prepare and I guarantee you it will melt in your mouth. </p><p>I have more than 10 years of experience in cooking dishes with a fusion twist and I have also helped people who want to pursue a career in cooking or become a chef.Â </p><p>In this course, I will be talking about how to cook a Cranberry Cake, Fish Tawa Fry, Dynamite Shrimps, Magnolia Bakery Banana Pudding,Â Shrimp Pasta & Bread toast</p><p>So If you\'re interested in knowing more about these dishes, then go ahead and buy my course today. Looking forward to seeing you on the other end.Â </p>', '[\"One will learn new dishes \",\"One will understand the importance of ingredients in a dish \",\"One will understand the importance presentation when you serve a dish\",\"One will learn how you can make a traditional dish with a Fusion twist\"]', 'english', 8, 57, '[35,36,37,38,39,40]', '[\"Basic knowledge of cooking \",\"Basic Knowledge of Ingredients \",\"Passion for Cooking\",\"Passion for Fusion Dishes \"]', 20, 1, 5, 'advanced', 53, NULL, 'https://vimeo.com/449008207', 1597708800, 1597795200, NULL, 1, 0, 'active', 'vimeo_upload', 'cooking ,online cooking class ,chef ,cooking classes ,Recipes,Cranberry Cake, Magnolia Bakery Banana Pudding,Bread toast', 'Learn how to make mouth-watering dishes with a fusion twist and a blend of traditional ingredients  ', NULL),
(56, 'My Signature Dishes 2', 'Cranberry Cake, Fish Tawa Fry, Dynamite Shrimps, Magnolia Bakery Banana Pudding, Shrimp Pasta & Bread Toast2', '<p>If you want to know about the secrete of making mouth-watering dishes, then this is the course for you. In this course, I will be talking about how to traditionally make dishes that are easy to prepare and I guarantee you it will melt in your mouth. </p><p>I have more than 10 years of experience in cooking dishes with a fusion twist and I have also helped people who want to pursue a career in cooking or become a chef.Â </p><p>In this course, I will be talking about how to cook a Cranberry Cake, Fish Tawa Fry, Dynamite Shrimps, Magnolia Bakery Banana Pudding,Â Shrimp Pasta & Bread toast</p><p>So If you\'re interested in knowing more about these dishes, then go ahead and buy my course today. Looking forward to seeing you on the other end.Â </p>', '[\"One will learn new dishes \",\"One will understand the importance of ingredients in a dish \",\"One will understand the importance presentation when you serve a dish\",\"One will learn how you can make a traditional dish with a Fusion twist\"]', 'english', 8, 57, '[35,36,37,38,39,40]', '[\"Basic knowledge of cooking \",\"Basic Knowledge of Ingredients \",\"Passion for Cooking\",\"Passion for Fusion Dishes \"]', 2000, 1, 5, 'advanced', 55, NULL, 'https://vimeo.com/449008207', 1597708800, 1597795200, NULL, 1, 0, 'active', 'vimeo_upload', 'cooking ,online cooking class ,chef ,cooking classes ,Recipes,Cranberry Cake, Magnolia Bakery Banana Pudding,Bread toast', 'Learn how to make mouth-watering dishes with a fusion twist and a blend of traditional ingredients  ', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `symbol` varchar(255) DEFAULT NULL,
  `paypal_supported` int(11) DEFAULT NULL,
  `stripe_supported` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `name`, `code`, `symbol`, `paypal_supported`, `stripe_supported`) VALUES
(1, 'Leke', 'ALL', 'Lek', 0, 1),
(2, 'Dollars', 'USD', '$', 1, 1),
(3, 'Afghanis', 'AFN', 'Ø‹', 0, 1),
(4, 'Pesos', 'ARS', '$', 0, 1),
(5, 'Guilders', 'AWG', 'Æ’', 0, 1),
(6, 'Dollars', 'AUD', '$', 1, 1),
(7, 'New Manats', 'AZN', 'Ð¼Ð°Ð½', 0, 1),
(8, 'Dollars', 'BSD', '$', 0, 1),
(9, 'Dollars', 'BBD', '$', 0, 1),
(10, 'Rubles', 'BYR', 'p.', 0, 0),
(11, 'Euro', 'EUR', 'â‚¬', 1, 1),
(12, 'Dollars', 'BZD', 'BZ$', 0, 1),
(13, 'Dollars', 'BMD', '$', 0, 1),
(14, 'Bolivianos', 'BOB', '$b', 0, 1),
(15, 'Convertible Marka', 'BAM', 'KM', 0, 1),
(16, 'Pula', 'BWP', 'P', 0, 1),
(17, 'Leva', 'BGN', 'Ð»Ð²', 0, 1),
(18, 'Reais', 'BRL', 'R$', 1, 1),
(19, 'Pounds', 'GBP', 'Â£', 1, 1),
(20, 'Dollars', 'BND', '$', 0, 1),
(21, 'Riels', 'KHR', 'áŸ›', 0, 1),
(22, 'Dollars', 'CAD', '$', 1, 1),
(23, 'Dollars', 'KYD', '$', 0, 1),
(24, 'Pesos', 'CLP', '$', 0, 1),
(25, 'Yuan Renminbi', 'CNY', 'Â¥', 0, 1),
(26, 'Pesos', 'COP', '$', 0, 1),
(27, 'ColÃ³n', 'CRC', 'â‚¡', 0, 1),
(28, 'Kuna', 'HRK', 'kn', 0, 1),
(29, 'Pesos', 'CUP', 'â‚±', 0, 0),
(30, 'Koruny', 'CZK', 'KÄ', 1, 1),
(31, 'Kroner', 'DKK', 'kr', 1, 1),
(32, 'Pesos', 'DOP ', 'RD$', 0, 1),
(33, 'Dollars', 'XCD', '$', 0, 1),
(34, 'Pounds', 'EGP', 'Â£', 0, 1),
(35, 'Colones', 'SVC', '$', 0, 0),
(36, 'Pounds', 'FKP', 'Â£', 0, 1),
(37, 'Dollars', 'FJD', '$', 0, 1),
(38, 'Cedis', 'GHC', 'Â¢', 0, 0),
(39, 'Pounds', 'GIP', 'Â£', 0, 1),
(40, 'Quetzales', 'GTQ', 'Q', 0, 1),
(41, 'Pounds', 'GGP', 'Â£', 0, 0),
(42, 'Dollars', 'GYD', '$', 0, 1),
(43, 'Lempiras', 'HNL', 'L', 0, 1),
(44, 'Dollars', 'HKD', '$', 1, 1),
(45, 'Forint', 'HUF', 'Ft', 1, 1),
(46, 'Kronur', 'ISK', 'kr', 0, 1),
(47, 'Rupees', 'INR', 'Rp', 1, 1),
(48, 'Rupiahs', 'IDR', 'Rp', 0, 1),
(49, 'Rials', 'IRR', 'ï·¼', 0, 0),
(50, 'Pounds', 'IMP', 'Â£', 0, 0),
(51, 'New Shekels', 'ILS', 'â‚ª', 1, 1),
(52, 'Dollars', 'JMD', 'J$', 0, 1),
(53, 'Yen', 'JPY', 'Â¥', 1, 1),
(54, 'Pounds', 'JEP', 'Â£', 0, 0),
(55, 'Tenge', 'KZT', 'Ð»Ð²', 0, 1),
(56, 'Won', 'KPW', 'â‚©', 0, 0),
(57, 'Won', 'KRW', 'â‚©', 0, 1),
(58, 'Soms', 'KGS', 'Ð»Ð²', 0, 1),
(59, 'Kips', 'LAK', 'â‚­', 0, 1),
(60, 'Lati', 'LVL', 'Ls', 0, 0),
(61, 'Pounds', 'LBP', 'Â£', 0, 1),
(62, 'Dollars', 'LRD', '$', 0, 1),
(63, 'Switzerland Francs', 'CHF', 'CHF', 1, 1),
(64, 'Litai', 'LTL', 'Lt', 0, 0),
(65, 'Denars', 'MKD', 'Ð´ÐµÐ½', 0, 1),
(66, 'Ringgits', 'MYR', 'RM', 1, 1),
(67, 'Rupees', 'MUR', 'â‚¨', 0, 1),
(68, 'Pesos', 'MXN', '$', 1, 1),
(69, 'Tugriks', 'MNT', 'â‚®', 0, 1),
(70, 'Meticais', 'MZN', 'MT', 0, 1),
(71, 'Dollars', 'NAD', '$', 0, 1),
(72, 'Rupees', 'NPR', 'â‚¨', 0, 1),
(73, 'Guilders', 'ANG', 'Æ’', 0, 1),
(74, 'Dollars', 'NZD', '$', 1, 1),
(75, 'Cordobas', 'NIO', 'C$', 0, 1),
(76, 'Nairas', 'NGN', 'â‚¦', 0, 1),
(77, 'Krone', 'NOK', 'kr', 1, 1),
(78, 'Rials', 'OMR', 'ï·¼', 0, 0),
(79, 'Rupees', 'PKR', 'â‚¨', 0, 1),
(80, 'Balboa', 'PAB', 'B/.', 0, 1),
(81, 'Guarani', 'PYG', 'Gs', 0, 1),
(82, 'Nuevos Soles', 'PEN', 'S/.', 0, 1),
(83, 'Pesos', 'PHP', 'Php', 1, 1),
(84, 'Zlotych', 'PLN', 'zÅ‚', 1, 1),
(85, 'Rials', 'QAR', 'ï·¼', 0, 1),
(86, 'New Lei', 'RON', 'lei', 0, 1),
(87, 'Rubles', 'RUB', 'Ñ€ÑƒÐ±', 1, 1),
(88, 'Pounds', 'SHP', 'Â£', 0, 1),
(89, 'Riyals', 'SAR', 'ï·¼', 0, 1),
(90, 'Dinars', 'RSD', 'Ð”Ð¸Ð½.', 0, 1),
(91, 'Rupees', 'SCR', 'â‚¨', 0, 1),
(92, 'Dollars', 'SGD', '$', 1, 1),
(93, 'Dollars', 'SBD', '$', 0, 1),
(94, 'Shillings', 'SOS', 'S', 0, 1),
(95, 'Rand', 'ZAR', 'R', 0, 1),
(96, 'Rupees', 'LKR', 'â‚¨', 0, 1),
(97, 'Kronor', 'SEK', 'kr', 1, 1),
(98, 'Dollars', 'SRD', '$', 0, 1),
(99, 'Pounds', 'SYP', 'Â£', 0, 0),
(100, 'New Dollars', 'TWD', 'NT$', 1, 1),
(101, 'Baht', 'THB', 'à¸¿', 1, 1),
(102, 'Dollars', 'TTD', 'TT$', 0, 1),
(103, 'Lira', 'TRY', 'TL', 0, 1),
(104, 'Liras', 'TRL', 'Â£', 0, 0),
(105, 'Dollars', 'TVD', '$', 0, 0),
(106, 'Hryvnia', 'UAH', 'â‚´', 0, 1),
(107, 'Pesos', 'UYU', '$U', 0, 1),
(108, 'Sums', 'UZS', 'Ð»Ð²', 0, 1),
(109, 'Bolivares Fuertes', 'VEF', 'Bs', 0, 0),
(110, 'Dong', 'VND', 'â‚«', 0, 1),
(111, 'Rials', 'YER', 'ï·¼', 0, 1),
(112, 'Zimbabwe Dollars', 'ZWD', 'Z$', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `enrol`
--

CREATE TABLE `enrol` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `enrol`
--

INSERT INTO `enrol` (`id`, `user_id`, `course_id`, `date_added`, `last_modified`) VALUES
(1, 46, 44, 1592265600, NULL),
(2, 44, 47, 1592265600, NULL),
(3, 47, 47, 1592265600, NULL),
(4, 44, 49, 1593129600, NULL),
(5, 46, 48, 1593129600, NULL),
(6, 44, 44, 1593129600, NULL),
(7, 45, 50, 1593734400, NULL),
(8, 47, 51, 1593907200, NULL),
(9, 47, 44, 1593907200, NULL),
(10, 51, 47, 1594252800, NULL),
(11, 46, 47, 1597795200, NULL),
(12, 51, 44, 1597795200, NULL),
(13, 51, 48, 1597881600, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `frontend_settings`
--

CREATE TABLE `frontend_settings` (
  `id` int(11) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `frontend_settings`
--

INSERT INTO `frontend_settings` (`id`, `key`, `value`) VALUES
(1, 'banner_title', 'Course Wings'),
(2, 'banner_sub_title', 'Learn new skills from industry Experts'),
(4, 'about_us', '<p>Course Wings website is owned by \"Course Wings Portal\", a company incorporated in Dubai, United Arab Emirates.<br><br>We strongly endorse the fact that Online Education is the way forward and Course Wings was established with the purpose of sharing knowledge with everyone who is interested in learning and anyone who is interested in sharing their knowledge with the world.  We are a platform where experts or instructors can upload pre-recorded educational video courses and students can view them and broaden their knowledge. <br><br>We always believe that â€œeasy access to learning is of paramount importance in one\'s lifeâ€. At Course Wings, we strive to share the knowledge that will not only benefit people in their life\'s but will also help them in their professional careers and broaden their horizon. We believe that, if you have a talent or if you specialize in a certain field, then come showcase your knowledge in the form of a video on our site and let the world benefit from it. We never know, through these course videos, we might transform someone\'s life for the better.<br><br>In general, Course Wings is a portal where you can easily access a wide spectrum of courses covering a large gamut of topics ranging from Intellectual, Educational, Business, Digital, Management to topics related to your Hobbies, Lifestyle, Fun and many more ... we have it all covered in one site. <br><br>All topics on Course Wings are uploaded by experienced instructors who have vast knowledge in their respective fields.  We have a stringent weeding process where we only select the best instructors. We believe if you want to move up the ladder or if you want to pursue something in order to get noticed, the first step is to enhance your knowledge and Course Wings is the right platform to start.<br><br><b>Welcome to Course Wings... Your platform to online learning!</b><br></p>'),
(10, 'terms_and_condition', '<p><b>1. Introduction: </b><br>Welcome to Course Wings Portal terms and conditions page. These terms and conditions apply to the online uploading and buying of videos provided by Course Wings website, a company owned by Course Wings Portal (henceforth to be addressed as Course Wings), based in the Dubai, United Arab Emirates. Course Wings (www.coursewings.com) was launched in order to help experts / instructors upload professional videos and give an opportunity to students / users to view these videos at a cost or in some instances free of cost. <br><br>The primary purpose of the terms and conditions are to outline the rules and regulations for the use of Course Wings Website. These terms and conditions are in addition to the website disclaimer and apply to the uploading or sale of any online videos.  Please read them carefully before uploading or buying any video.<br><br>By accessing our site www.coursewings.com, accepting or buying our videos through Course Wings website, it is understood that you accept our terms and conditions. We kindly request you not to continue using Course Wing website if you do not agree to accept all of the terms and conditions.<br><br>Please note, the following terminology/clauses apply to these Terms and Conditions page: \"Client/Student\", \"You\" and \"Your\" refers to you, the person log on this website and compliant to the Companyâ€™s terms and conditions. \"The Company\", \"Ourselves\", \"We\", \"Our\" and \"Us\", refers to our Company  Course Wings.  Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to same.<br><br>As instructors, if you upload any video on Course Wings, it is understood that you agree to the â€œInstructors Terms and Conditionsâ€ and agree to furnish your personal details. Simultaneously, as Students, if you agree to buy a course/video, you agree to the â€œStudents Terms and Conditionsâ€ and agree to furnish your personal details. <br><br>Please be advised that Course Wings always reserves the rights to change the terms and conditions (T&C), any time or as many times it wants. In most cases, whatever changes we make will be effective after 20 days of incorporation and if you are using our services it is understood that you accept our T&C. Therefore, we request you to constantly review our T&C page. Any kind of disagreement between Course Wings and Users (Students or Instructors) will be addressed based on the present T&C. <br><br>This Agreement shall begin on the date hereof a user agrees to engage in our services.<br><br><br><b>2. Cookies:</b><br>We employ the use of cookies. By accessing Course Wings, you agreed to use cookies in agreement with the Course Wings Privacy Policy.<br><br>Most interactive websites use cookies to let us retrieve the userâ€™s details for each visit. Cookies are used by our website to enable the functionality of certain areas to make it easier for people visiting our website. Some of our affiliate/advertising partners may also use cookies.<br><br><b><br>3. License, Trademark & Communication with the Users:</b><br>Unless otherwise specified, Course Wings Portal and/or its licensors own the intellectual property rights for all materials published on Course Wings Portal. All intellectual property rights are reserved. <br><br><i>You may not: </i><br>â€¢    Republish material/videos from Course Wings website <br>â€¢    Sell, rent or sub-license material from Course Wings website <br>â€¢    Reproduce, duplicate or copy material from Course Wings website<br>â€¢    Redistribute content from Course Wings website<br>â€¢    Tamper with Course Wing Site and its contents<br>â€¢    You canâ€™t use Course Wings Name, Logo and Domain Name to represent your business, as this is a registered name in the UAE.<br><br>All communication will be done electronically and if you sign-up with us it is understood that you accept this mode of communication. <br><br><i>Point of Agreement: </i><br>â€¢    As a student, when you open an account with Course Wings either to view a course or for any other reason/s, you are granted the permission to access/view our courses. <br>â€¢    As soon as you open an account (either as a student or lecturer) you agree to all the terms and conditions.<br>â€¢    As a student, in most cases, you will have lifetime access to most of our course. However, we have the right to cancel your account at any given point of time without notice. <br><br>The same is the case if you breach the terms and conditions, we reserve the right to shut down your account. As instructors, each time you upload a course, you give us the permission/license to market your contents/courses anywhere in the world, to any student in the world, either for free or for the said agreed fee. We also reserve the right to cancel your account anytime without notice. Please also note: as instructors, you can\'t give any license of your courses to students in order for them to resell your courses.<br><br>â€¢    As a student, you get an approval from Course Wings to view videos/courses only on Course Wings platform. However, you may not resell / or reproduce / share / illegally download/share to download material especially through pirated / illegal sites etc.<br>â€¢    Itâ€™s up to the instructorâ€™s discretion that he will no longer offer any assistance to students related to the course they have uploaded.<br><br> <br><b>4. Course-related comments and offensive comments: </b><br>Parts of this website offer an opportunity for experts to post videos and exchange opinions and information. Course Wings does not filter, edit, publish or review comments prior to their presence on the website. Comments (from anyone) do not reflect the views and opinions of Course Wings, its agents and or its affiliates. Comments reflect the views and opinions of the person who post the content. To the extent permitted by applicable laws, Course Wings shall not be liable for the Comments or for any liability, damages or expenses caused and/or suffered as a result of any use of and/or posting of and/or appearance of the Comments on this website. <br><br><i>You warrant and represent that:</i><br>â€¢    You are entitled to post the Comments on our website and have all necessary licenses and consents to do so;<br>â€¢    The Comments do not invade any intellectual property right, including without limitation copyright, patent or trademark of any third party;<br>â€¢    The Comments do not contain any defamatory, libellous, offensive, indecent or otherwise unlawful material which is an invasion of privacy<br>â€¢    Comments will not be used to solicit or promote business or custom or present commercial activities or unlawful activity/ies.<br><br>You hereby grant Course Wings owned by Course Wings Portal, a non-exclusive license to use, reproduce, edit and authorize others to use, reproduce and edit any of your Comments in any and all forms, formats or media.<br><br>Course Wings Portal reserves the right to monitor all Comments and to remove any Comments without prior notice which appear to be inappropriate, offensive, violent, or causes a breach of these Terms and Conditions. It is understood that you as a user (Student or Instructor) of the Course Wings Portal will not post anything concerning pornography; racist/sexist comments; hateful speech or intent toward an individual, group, company, politic party, insulting to anyone, offensive topic, society or community. <br><br><br><b>5. Reservation of rights and Termination of your account: </b><br>We reserve the right to request that you remove all videos uploaded by you, links or any particular link to our Website. You approve to immediately remove all videos/links to our Website upon request. We also reserve the right to amend these terms and conditions and itâ€™s policies at any time we think necessary. By continuously connecting/visiting our website, you agree to be bound to and follow these linking terms and conditions.<br><br>Course Wings Portal also reserves the right to terminate/closing your account within 24 hours if we observe that you have violated the said terms and conditions without any warning. In doing so, we also reserve the rights not to offer any products and services to you again in the future and you cannot hold us responsible for the action. Offering services to you will be solely at our discretion. Settling of outstanding bills will be discussed and settled if the resolution committee feels it is justifiable. <br><br><br><b>6. Site Traffic, Maintenance and Peripheral Links: </b><br>For promotion purposes or any other reason, Course Wings may contain external links in the site. The contents in these links have nothing to do with Course Wings and we are not responsible for the contents in it. If for some reason, the user clicks on these sites, they will be doing so at their own discretion/risk and we are not responsible in any way for the contents on these external links. <br><br>Our intention will always be to run the site for 24 hours a day, however, there might be times when the site is under maintenance and it might cause some delays. In addition, due to Internet connection/speed, there might be a scenario where a video/content might take time to commence or download. In this regard, the user can not hold Course Wings Portal responsible in any way.  <br><br><br><b>7. Payment Terms & Conditions, Taxes & Refund procedure: </b><br>If you sign-up with Course Wings and agree to pay the said amount set by the Instructor or our Marketing Team, then you authorize us to debit your credit/debit card or through any other mode. All payments need to be made in the name of Course Wings Portal and all information furnished towards the payment will be shared with our payment provider with the utmost confidentiality. Primary payment currency is US Dollars however; there will be an option of paying in the local country currency. We accept Visa or MasterCard or Paypal and the payment will be accepted only through an online payment. If the payment is declined for some reason, then you will have to pay the said fees within 30 days with a late charge fee. <br><br>For the Instructors, you will have to furnish us with all the details of your account and any personal information requested. Once your course or courses are purchased, the money will be remitted into your account after deduction of service charges within 30 days. <br><br>Taxes or Value Added Tax will be collected as per the country law and remitted to the concerned authorities every time a transaction is made. In case it\'s a currency other than the local country, the same will be converted based on the local currency and the Tax will be remitted to the concerned authority. <br><br>At Course Wings Portal only the best videos in the industry are uploaded and it is understood that if you purchase the video you are getting the best product that money can buy. Keeping our quality in mind we don\'t have a policy of returning the money to our users. However, due to certain unforeseen circumstances, if the video is corrupted for some reason and the content cannot be viewed, you will have to get in touch with us through the help centre and we will review the case and revert back with a feedback. If the concern is genuine, then we will refund your money within 30 days through online payment medium. Please note, we will only accept this request if the concern is registered with 3 days of the purchase. <br><br>Note: All course prices on Course Wings are based on what the instructor has decided and should be in the line with our payment policy. We will also run some promotions on a regular basis on a price agreed upon by the Instructor or Course Wings or Both. <br><br><br><b>7. Limitations of Liability:</b><br>We shall not be held responsible for any content that appears on our Website posted by anyone. You agree to protect and defend us against all claims. No link(s) should appear on any Website that may be interpreted as libellous, obscene or criminal, or which infringes, otherwise violates, or advocates the infringement or other violation of, any third party rights. You agree that you cannot hold Course Wings Portal, Partners, Agents, Suppliers or anyone associated with us, responsible for any damages (direct/indirect), injuries, health issues, loss, mental stress or anything that harms your health, loss of data, profits, cause of death, fraud, punitive, consequential damage, lead to financial loses (direct/indirect), accuse us of wrong advise, etc., while you are viewing our courses or videos (either promotional videos or purchased). This is applicable even if you have alerted us in advance of possible damage, health issue, loss or Death that might arise.  These courses/videos are posted in good faith and if you choose to view them or buy them you will be doing so at your own risk and you can\'t sue us or take us to court or claim for damages. <br><br><br><b>8. iFrames:</b><br>Without prior approval and written permission, you may not produce/create frames/layouts around our Webpages that alter in any way the visual presentation or appearance of our Website.<br><br><br><b>9. Force Majeure event:</b><br>In the case of an unforeseen force majeure event or circumstance, that is out of our control, such as war, strike, fire, hacking, internet related issues, riot, enforceable law, crime, or an event described by the legal term act of God (hurricane, flood, earthquake, volcanic eruption, etc.) or any other unforeseen circumstances, Course Wings Portal is not responsible for the services we provide or the quality related to it. <br><br> <br><b>10. Your Privacy:</b><br>Please read Privacy Policy for more details and if you have any reservations, you could write directly to us or choose not to view or buy our courses. <br><br><br><b>11. Students/Instructor login:  </b><br>To browse promo videos and contents on Course Wings, you don\'t need to feed in your credentials to open an account. However, if you want to take benefits of viewing courses and uploading courses, you will have to create an account with us. All personal information furnished will be kept confidential, except the content that is meant for public viewing. <br><br><br><b>12. Age Limit: </b><br>We don\'t allow minors to upload videos on our platform. Nor do we allow them to open accounts on Course Wings. In other words, both students and Expects/Instructors need to be 18+. You need to be above the permissible age limit for accessing online content and creating accounts based on your countries regulations. Course Wings Portal reserves the right to cancel your account if we find out you\'re below the age limit stipulated by us (+18 years). <br><br><br><b>13. Login Credentials, Security, Content, & Code of Ethics: </b><br>If you choose to use our site then you also agree to compile with all relevant laws, country-specific country regulations, copyrights regulations and or any other copyrights issues or personal identity. If you want full access to our site, you will have to first open an account using your credentials. You also agree to furniture your personal detail and we will keep it confidential to the best of our ability. It is your responsibility that all information provided is accurate and it\'s not falsely quoted. All information should not be doctored, offend anyone and anything to do with religion. You agree not to transfer virus or junk content into the site and or thing concerning representing a 3rd Party etc.<br><br>Please be careful of your username, email address and password. It is your property and you need to guard it and please don\'t share it with anyone. You are solely responsible for the activities on your page/profile Course Wings. We are not responsible to amicably settle any disagreements between students and instructors unless until either of the party has acted against the Terms & Conditions stated by Course Wings. Please be advised, if we suspect any unusual activity on your site, we hold the right to close it. In any event, where you have lost your password or username or both, we will help you retrieve it provided you furnish us with some answers to security question(s). <br><br>If unfortunately, the user passes away due to unforeseen circumstances, Course Wing is authorized to close down the account, provided enough evidence is presented. If you also suspect that some third party individual/individuals are using your account, please do get in touch with us immediately. Please be advised that you can buy/view our videos for only lawful reasons and not for unlawful reasons.  You may not misuse our platform for any cybercrime, transfer any kind of virus, promote your personal material, use it for network marketing, junk material that can infect the site, or use Course Wings for any kind of commercial gain. <br><br>Finally, it is your responsibility to obey the cyber law that is practised in the country you reside in.<br><br>You agree that we may record all or any part of any Course (including voice chat communications) for quality control and delivering, marketing, promoting, demonstrating or operating the Site and the Products. You hereby grant Course Wings permission to use your name, likeness, image or voice in connection with offering, delivering, marketing, promoting, demonstrating, and selling Products, Courses, Company Content and Submitted Content and waive any and all rights of privacy, publicity, or any other rights of a similar nature in connection therewith, to the extent permissible under applicable law.<br><br>All rights not expressly granted in these Terms are retained by the Content owners and these Terms do not grant any implied licenses.<br><br><br><b>14. Use our Platform at your own risk:</b><br>We are offering a platform that can be used by anyone and everyone. People are free to upload course but you can\'t hold Course Wings accountable is it is offensive to you in any way. If you agree to use our platform then you are doing so at your own risk. This implies to both Students and Instructors. In this view, Course Wings is a platform where there will be a lot of communication between an Instructor and a Student or Both with the Company Admin. In case someone gets offended, we cannot be held responsible for it.  In other words, you cannot hold us responsible for arguments, disagreements, dues, damages, losses, grievances, etc. during Instructor and Student interaction. <br><br><br><b>15. Validity of Terms & Conditions:</b><br>The terms and conditions will be updated regularly and we encourage users to constantly keep observing the terms. All parameter of the Terms and Condition are considered valid until otherwise stated.  <br><br><br><b>16. Promotion-related activities:</b><br>As a user of our portal, you grant us the rights to promote your content or reproduce your content and post it on third-party platforms like Websites, YouTube, Facebook, Instagram, LinkedIn, Twitter, SnapChat or any other Social Media Platform. The core initiative will be to only promote Course Wings Portal and your contents / video / course.<br><br><br><b>17. Account Opening:</b><br>If you need full access to our site and videos you need to open an account. In doing so, you can assign yourself a user name and password. When you agree to open an account then you agree to get into a joint venture between you and Course Wings and that you agree to all the terms and Condition. While you create your account please ensure you furnish accurate information as you will be the sole owner of the content displayed in your account and we are not responsible about the content in your page. Please try not to share your credentials with other people, as we will not be responsible if you end up in a dispute. <br><br><br><b>18. Payment Commitment: </b><br>You opt to access or use our products/services that involves a payment, then it is understood that you have agreed to pay us for the services offered. This will also mean that you have agreed to accept all the terms and conditions as well. In doing so you have now agreed to pay an agreed sum, and will be since responsible for payment including all taxes associated with the product/services. In the event of using a credit card, you confirm that the information pertaining to your credit card is authentic and not fabricated and that you have given Course Wings Portal all the rights to debit your account with the agreed amount set by Course Wings. Due to some reason if your credit card is maxed out or not functional then we reserve the right of collecting the money using other methods or collection through authorized methods like involving a collection company that specializes in this respect, involve a Law firm or any other method deemed necessary. Course Wings Portal also reserve the right of freezing any other unresolved matter/s or amount relating to you if we discover foul play. <br><br><br><b>19. Non-Disclosure of information about Students or Mentors:</b><br>As a Student, it is understood that you will not disclose any personal information to the Instructor that could include your contact details or any other matter that is not relevant to Course Wings. In addition, you will also not reveal any other information personal or professional to any third person. <br><br>As Mentors, you agree that you will not disclose any information of Students to any other person other than the Course Wings administration team. You will also not indulge in a conversation with a Student through any other platform like personal email, or telephone calls etc. The only mode of communication is through Course Wings Website. <br><br><br><b>20. Duplicating or Reproducing content for Private Advantage / Profit / Benefit / Gain: </b><br>If you sign up with Course Wings Portal you agree that you will not duplicate / reproducing content for private advantage / distribution / profit / benefit or gain. The content posted should be original (your thoughts, experience and ideas) and not plagiarized from other sources. <br><br><br><b>21. Law and Legal Affairs: </b><br>Course Wings website is a portal that is accessible to the public and disputes and claims, if any, will be impeccably settled between you and Course Wings. You agree not to take us to a court or file a case against us in the Court of Law, in any country. This is a public domain and we don\'t take responsibility for anything. If this is not acceptable, then please don\'t register with us or view our site. <br><br><br><b>22. Disclaimer:</b><br>To the maximum extent permitted by applicable law, we exclude all representations, warranties and conditions relating to our website and the use of this website. Nothing in this disclaimer will:<br><br>â€¢    Limit or exclude our or your liability for death or personal injury;<br>â€¢    Limit or exclude our or your liability for fraud or fraudulent misrepresentation;<br>â€¢    Limit any of our or your liabilities in any way that is not permitted under applicable law; or<br>â€¢    Exclude any of our or your liabilities that may not be excluded under applicable law.<br><br>The limitations and prohibitions of liability set in this section and elsewhere in this disclaimer: (a) are subject to the preceding paragraph; and (b) govern all liabilities arising under the disclaimer, including liabilities arising in contract, in tort and for breach of statutory duty.<br><br>The videos, contents, products and services offered on Course Wings Portal, has no warranty or guarantee under the countryâ€™s applicable law where youâ€™re residing. Course Wings Portal hereby disclaims all such things including content accuracy, typos, fitness, errors if any, commercially acceptable quality, merchantability etc and we will not be liable for any loss or damage of any nature.  As a user of our site,  you cannot claim for any damages/losses of any nature including personal or professional. <br><br><br><b>23. Indemnification:</b><br>We always reserve the right of taking legal action against you if you get us in legal trouble or don\'t abide by the term  & conditions or privacy policy (available in the privacy page). You need to agree that you will not harm Course Wings in any way including its employees, associated suppliers; their admin and owners or whoever is directly or indirectly involved with Course Wings Portal. <br><br><br><b>24. Dispute Department:</b><br>If there is any dispute that arises between parties involved, please send us a written complain through a letter or thru email and we will respond to the same at the earliest depending on the nature of the concern. <br><br><br><b>25. Obligatory/Binding Agreement</b><br>As soon as you register to use our products, contents and services, you are agreeing to an agreement that is legally binding with Course Wings and are obliged to abide by all the points mention in this Terms and Conditions. Please go thru these terms and conditions thoroughly and only after agreeing to it register otherwise you are requested not to register.  If there is any concern the matter has to be brought to the Companyâ€™s attention in writing immediately. <br><br><br><b>26. Terms and Conditions Updates: </b><br>On a regular basis, we will be updating these terms and conditions section and Course Wings reserves the right to make any changes. Hence, it is advisable for users to view the T&C regularly. Note: The Terms and Conditions will be in effect from the time it is posted.<br></p>'),
(11, 'privacy_policy', '<p>Welcome to Course Wings Privacy Policy. Your privacy is extremely critical and important to us. Course Wings is a company registered under the trade license name Course Wings Portal and the company is located in the Emirate of Dubai, United Arab Emirates.</p> <p>It is Course Wings policy to respect your privacy regarding any information we may collect while operating our website. This Privacy Policy applies to coursewings.com (hereinafter, \"us\", \"we\", or \"coursewings.com\" or Course Wings Portal). We respect your privacy and are committed to protecting personally identifiable information you may provide us through the Website www.coursewings.com. We have adopted this privacy policy (\"Privacy Policy\") to explain what information may be collected on our Website, how we use this information, and under what circumstances we may reveal the information to third parties. This Privacy Policy applies only to information we collect through the Website and does not apply to our collection of information from other sources.</p> <p>This Privacy Policy, together with the Terms and Conditions posted on our Website, set forth the general rules and policies governing your use of our Website. Depending on your activities / specialization when visiting our Website, you may be required to agree to additional terms and conditions, if necessary.</p> <p>Irrespective of which country your based or residing in, if you agree to the Privacy Police it is understood that you are getting into a legally binding contract with Course Wings Portal. In other words, this policy is applicable worldwide. If not acceptable, with the terms of use and with the privacy terms, then you are strongly recommended not to use our services. </p> <p><b>1. Website Visitors:</b></p> <p>Like most website operators, Course Wings collects non-personally-identifying information of the sort that web browsers and servers typically make available, such as the browser type, language preference, referring site, and the date and time of each visitor request. Course Wings purpose in collecting non-personally identifying information is to better understand how Course Wings visitors use the website. From time to time, Course Wings may release non-personally-identifying information in the aggregate, e.g., by publishing a report on trends in the usage of its website.</p> <p>Course Wings also collects potentially personally identifying information like Internet Protocol (IP) addresses for logged in users and for users leaving comments on www.coursewings.com blog posts or instructor page. Course Wings only discloses logged in user and commenter IP addresses under the same circumstances that it uses and discloses personally identifying information as described below.<br></p> <p><b>2. Security of Personal Information:</b></p> <p>The security of your Personal Information is important to us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Information, we cannot guarantee its absolute security.</p> <p>The following information will be collected, but not limited to: </p> <blockquote> <p>1. Information during registration with us (either directly or indirectly through third party links) as a Student or as an Instructor that may include â€“ your name, profession, gender, date of birth, country residing in, preferred mode of receiving payment, bank address, email address, telephone no., address, password, username, or any other information as required. </p> <p>2. Your profile will be public for anyone to view, hence you are recommended not to disclose false information and Course Wings doesn\'t take any responsibility regarding any information you furnish us with. </p> <p>3. Course Wings is a public platform and we allow users to post comments and share details. For our internal use, we keep these data for customer tracking or for contacting users. </p> <p>4. Since this a public platform, it is understood that you have given us permission (non-exclusive rights) to use the comments / contents and suggestions shared by users in order to promote Course Wings. </p> <p>5. Please be advised, Course Wings cannot guarantee any flow of content from either Students or Instructors and is since not liable for any suggestions / content or conclusion. </p> <p></p> </blockquote> <p><b>5. Gathering of Personally-Identifying Information:</b></p> <p>Certain visitors to Course Wings websites choose to interact or open an account with Course Wings in ways that require Course Wings to gather personally identifying information. The amount and type of information that Course Wings gathers depend on the nature of the interaction. For example, we ask Students to sign up for a course or ask Instructors to upload course at www.coursewings.com and in doing so Students / Instructors will have to provide their information to Course Wings.</p> <p> </p> <p><b>6. Gathering of Credit Card details: </b> </p> <p>As Course Wings, we operate with a recognized payment gate and we are in line with the all the online payment norms and guidelines and Payment Card Industry Data Security Standard. All information collected from users for payment purposes will be kept strictly confidential by the payment gateway company and we, as Course Wings, don\'t store any information.<span xss=\"removed\"> </span></p> <p> </p> <p><b>7. Advertisements:</b></p> <p>Ads on our website may be delivered to users by advertising partners, who may set cookies. These cookies allow the ad server to recognize your computer each time they send you an online advertisement to compile information about you or others who use your computer. This information allows ad networks to, among other things, deliver targeted advertisements that they believe will be of most interest to you. This Privacy Policy covers the use of cookies by Course Wings and does not cover the use of cookies by any advertisers.</p> <p> </p> <p><b>8. Age limit concerning usage of Course Wings</b></p> <p>Age limit to use our site is 18+ years. In other words, we consider people under the age of 18 as minors and do not accept their involvement with our site. People below the age of 18 will be violating the terms of our policy and the information collected or the page created by them will be terminated immediately. We are also not responsible for the loss of contents in this case.</p> <p> </p> <p><b>9. Links To External Sites</b></p> <p>Our Service may contain links to external sites that are not operated by us. If you click on a third party link, you will be directed to that third party\'s site. We strongly advise you to review the Privacy Policy and the Terms and Conditions of every site you visit.</p> <p>We have no control over and assume no responsibility for the content, Privacy Policies or practices of any third party sites, products or services. </p> <p>In case your providing us information through a third party site, then it is understood that you are allowing us to collate information from the third party site and use it for our use. </p> <p> </p> <p><b>10. Aggregated Statistics / Collection and Sharing of Customer Data:</b></p> <p>When you access our site, Course Wings may collect statistics / data about the behavior of visitors to its website through automated methods. Information might relate to your IP address, behaviour traits, demographics, location, your computer, wireless device, domain etc or in other words only aggregated information of the user. Course Wings may also deploy web beacons or similar technology to track e-mail movements, page visits, to run promotions etc. </p> <p> </p> <p>Course Wings may display this information publicly or provide it to others. However, Course Wings does not disclose your personally identifying information. This information also might be shared by third-party sources in case we need them to get involved with the marketing of the site or courses. In other words, we reserve the rights of sharing your data with Students, Instructors, external advertising / social media agencies, email campaigns, freelancers, partners, survey agencies, for push notification, for security & law reasons etc.<span xss=\"removed\"> We also reserve the right for communicating with you concerning your account / preference, sending you messages concerning your course or action or resolving disputes, newsletters, request feedback, data for survey, any attempt to improve the customer experience etc.</span></p> <p> </p> <p><b>11. Survey and opt-out option</b></p> <p>Course Wings also may conduct surveys to understand customer needs and preferences. The survey may demand that we collect relevant data to conduct the survey and this might include, but not limited to: Your Name, Phone No., E-mail address, Physical Address, Age, etc. We might display the content on our public platform or might just use it for internal purpose depending on the nature of the survey. The sole purpose of collecting the data is to promote our services better and to understand how we can better our services. </p> <p> </p> <p>It is your right to opt-out of the survey or any personal information we request. It is also your right to cancel your account anytime if you feel you don\'t want to disclose any information. You also have the option of not accepting our cookies policy and opting out of any promotion. </p> <p> </p> <p>All updates and access to your personal data is your right and it must be requested through a written request to info@coursewings.com. </p> <p> </p> <p><b>12. Cookies</b></p> <p>To enrich and perfect your online experience, Course Wings uses \"Cookies\", similar technologies and services provided by others to display personalized content, appropriate advertising and store your preferences on your computer.</p> <p> </p> <p>A cookie is a string of information that a website stores on a visitor\'s computer, and that the visitor\'s browser provides to the website each time the visitor returns. Course Wings uses cookies to help Course Wings identify and track visitors, their usage of http://coursewings.com, protect from fraud logins / activities enabling the process of your course purchasing process and their website access preferences. </p> <p> </p> <p>Course Wings visitors who do not wish to have cookies placed on their computers should set their browsers to refuse cookies before using Course Wings websites, with the drawback that certain features of Course Wings websites may not function properly without the aid of cookies.</p> <p>By continuing to navigate our website without changing your cookie settings, you hereby acknowledge and agree to Course Wings use of cookies.</p> <p> </p> <p><b>13. Privacy Policy Changes</b></p> <p>Although most changes are likely to be minor, Course Wings may change its Privacy Policy from time to time; it is at the sole discretion of Course Wings to do so. Course Wings encourages visitors to frequently check this page for any changes to its Privacy Policy. Your continued use of this site after any change in this Privacy Policy will constitute your acceptance of such change.</p> <p> </p> <p><b>14. Contact Information</b></p> <p>If you have any questions about this Privacy Policy, please contact us via email: info@coursewings.com.</p> <p> </p> <p><i>ThisPrivacy Policy was last updated on 01/02/2020</i></p>'),
(13, 'theme', 'default');

-- --------------------------------------------------------

--
-- Table structure for table `hits`
--

CREATE TABLE `hits` (
  `id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `course_category` int(11) DEFAULT NULL,
  `course_subcategory` int(11) DEFAULT NULL,
  `course_author` int(11) DEFAULT NULL,
  `hit_conter` double NOT NULL DEFAULT 1,
  `last_updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hits`
--

INSERT INTO `hits` (`id`, `course_id`, `course_category`, `course_subcategory`, `course_author`, `hit_conter`, `last_updated_at`) VALUES
(1, 44, 9, 59, 44, 168, '2020-08-27 15:38:50'),
(2, 47, 10, 65, 46, 129, '2020-08-25 11:02:20'),
(3, 48, 1, 13, 47, 97, '2020-08-21 17:23:12'),
(4, 49, 9, 64, 45, 67, '2020-08-21 12:22:43'),
(5, 50, 10, 68, 44, 70, '2020-08-21 16:08:57'),
(6, 51, 2, 18, 50, 82, '2020-08-21 21:12:17'),
(7, 52, 5, 36, 44, 53, '2020-08-21 12:22:34'),
(8, 53, 4, 32, 52, 72, '2020-08-21 12:22:33'),
(9, 55, 8, 57, 53, 41, '2020-08-21 21:45:31'),
(10, 56, 8, 57, 55, 1, '2020-08-29 06:08:20');

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `phrase_id` int(11) NOT NULL,
  `phrase` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `english` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `Bengali` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`phrase_id`, `phrase`, `english`, `Bengali`) VALUES
(1, 'manage_profile', NULL, NULL),
(3, 'dashboard', NULL, NULL),
(4, 'categories', NULL, NULL),
(5, 'courses', NULL, NULL),
(6, 'students', NULL, NULL),
(7, 'enroll_history', NULL, NULL),
(8, 'message', NULL, NULL),
(9, 'settings', NULL, NULL),
(10, 'system_settings', NULL, NULL),
(11, 'frontend_settings', NULL, NULL),
(12, 'payment_settings', NULL, NULL),
(13, 'manage_language', NULL, NULL),
(14, 'edit_profile', NULL, NULL),
(15, 'log_out', NULL, NULL),
(16, 'first_name', NULL, NULL),
(17, 'last_name', NULL, NULL),
(18, 'email', NULL, NULL),
(19, 'facebook_link', NULL, NULL),
(20, 'twitter_link', NULL, NULL),
(21, 'linkedin_link', NULL, NULL),
(22, 'a_short_title_about_yourself', NULL, NULL),
(23, 'biography', NULL, NULL),
(24, 'photo', NULL, NULL),
(25, 'select_image', NULL, NULL),
(26, 'change', NULL, NULL),
(27, 'remove', NULL, NULL),
(28, 'update_profile', NULL, NULL),
(29, 'change_password', NULL, NULL),
(30, 'current_password', NULL, NULL),
(31, 'new_password', NULL, NULL),
(32, 'confirm_new_password', NULL, NULL),
(33, 'delete', NULL, NULL),
(34, 'cancel', NULL, NULL),
(35, 'are_you_sure_to_update_this_information', NULL, NULL),
(36, 'yes', NULL, NULL),
(37, 'no', NULL, NULL),
(38, 'system settings', NULL, NULL),
(39, 'system_name', NULL, NULL),
(40, 'system_title', NULL, NULL),
(41, 'slogan', NULL, NULL),
(42, 'system_email', NULL, NULL),
(43, 'address', NULL, NULL),
(44, 'phone', NULL, NULL),
(45, 'youtube_api_key', NULL, NULL),
(46, 'get_youtube_api_key', NULL, NULL),
(47, 'vimeo_api_key', NULL, NULL),
(48, 'purchase_code', NULL, NULL),
(49, 'language', NULL, NULL),
(50, 'text-align', NULL, NULL),
(51, 'update_system_settings', NULL, NULL),
(52, 'update_product', NULL, NULL),
(53, 'file', NULL, NULL),
(54, 'install_update', NULL, NULL),
(55, 'system_logo', NULL, NULL),
(56, 'update_logo', NULL, NULL),
(57, 'get_vimeo_api_key', NULL, NULL),
(58, 'add_category', NULL, NULL),
(59, 'category_title', NULL, NULL),
(60, 'sub_categories', NULL, NULL),
(61, 'actions', NULL, NULL),
(62, 'action', NULL, NULL),
(63, 'manage_sub_categories', NULL, NULL),
(64, 'edit', NULL, NULL),
(65, 'add_course', NULL, NULL),
(66, 'select_category', NULL, NULL),
(67, 'title', NULL, NULL),
(68, 'category', NULL, NULL),
(69, '#_section', NULL, NULL),
(70, '#_lesson', NULL, NULL),
(71, '#_enrolled_user', NULL, NULL),
(72, 'view_course_details', NULL, NULL),
(73, 'manage_section', NULL, NULL),
(74, 'manage_lesson', NULL, NULL),
(75, 'student', NULL, NULL),
(76, 'add_student', NULL, NULL),
(77, 'name', NULL, NULL),
(78, 'date_added', NULL, NULL),
(79, 'enrolled_courses', NULL, NULL),
(80, 'view_profile', NULL, NULL),
(81, 'admin_dashboard', NULL, NULL),
(82, 'total_courses', NULL, NULL),
(83, 'number_of_courses', NULL, NULL),
(84, 'total_lessons', NULL, NULL),
(85, 'number_of_lessons', NULL, NULL),
(86, 'total_enrollment', NULL, NULL),
(87, 'number_of_enrollment', NULL, NULL),
(88, 'total_student', NULL, NULL),
(89, 'number_of_student', NULL, NULL),
(90, 'manage_sections', NULL, NULL),
(91, 'manage sections', NULL, NULL),
(92, 'course', NULL, NULL),
(93, 'add_section', NULL, NULL),
(94, 'lessons', NULL, NULL),
(95, 'serialize_sections', NULL, NULL),
(96, 'add_lesson', NULL, NULL),
(97, 'edit_section', NULL, NULL),
(98, 'delete_section', NULL, NULL),
(99, 'course_details', NULL, NULL),
(100, 'course details', NULL, NULL),
(101, 'details', NULL, NULL),
(102, 'instructor', NULL, NULL),
(103, 'sub_category', NULL, NULL),
(104, 'enrolled_user', NULL, NULL),
(105, 'last_modified', NULL, NULL),
(106, 'manage language', NULL, NULL),
(107, 'language_list', NULL, NULL),
(108, 'add_phrase', NULL, NULL),
(109, 'add_language', NULL, NULL),
(110, 'option', NULL, NULL),
(111, 'edit_phrase', NULL, NULL),
(112, 'delete_language', NULL, NULL),
(113, 'phrase', NULL, NULL),
(114, 'value_required', NULL, NULL),
(115, 'frontend settings', NULL, NULL),
(116, 'banner_title', NULL, NULL),
(117, 'banner_sub_title', NULL, NULL),
(118, 'about_us', NULL, NULL),
(119, 'blog', NULL, NULL),
(120, 'update_frontend_settings', NULL, NULL),
(121, 'update_banner', NULL, NULL),
(122, 'banner_image', NULL, NULL),
(123, 'update_banner_image', NULL, NULL),
(124, 'payment settings', NULL, NULL),
(125, 'paypal_settings', NULL, NULL),
(126, 'client_id', NULL, NULL),
(127, 'sandbox', NULL, NULL),
(128, 'production', NULL, NULL),
(129, 'active', NULL, NULL),
(130, 'mode', NULL, NULL),
(131, 'stripe_settings', NULL, NULL),
(132, 'testmode', NULL, NULL),
(133, 'on', NULL, NULL),
(134, 'off', NULL, NULL),
(135, 'test_secret_key', NULL, NULL),
(136, 'test_public_key', NULL, NULL),
(137, 'live_secret_key', NULL, NULL),
(138, 'live_public_key', NULL, NULL),
(139, 'save_changes', NULL, NULL),
(140, 'category_code', NULL, NULL),
(141, 'update_phrase', NULL, NULL),
(142, 'check', NULL, NULL),
(143, 'settings_updated', NULL, NULL),
(144, 'checking', NULL, NULL),
(145, 'phrase_added', NULL, NULL),
(146, 'language_added', NULL, NULL),
(147, 'language_deleted', NULL, NULL),
(148, 'add course', NULL, NULL),
(149, 'add_courses', NULL, NULL),
(150, 'add_course_form', NULL, NULL),
(151, 'basic_info', NULL, NULL),
(152, 'short_description', NULL, NULL),
(153, 'description', NULL, NULL),
(154, 'level', NULL, NULL),
(155, 'beginner', NULL, NULL),
(156, 'advanced', NULL, NULL),
(157, 'intermediate', NULL, NULL),
(158, 'language_made_in', NULL, NULL),
(159, 'is_top_course', NULL, NULL),
(160, 'outcomes', NULL, NULL),
(161, 'category_and_sub_category', NULL, NULL),
(162, 'select_a_category', NULL, NULL),
(163, 'select_a_category_first', NULL, NULL),
(164, 'requirements', NULL, NULL),
(165, 'price_and_discount', NULL, NULL),
(166, 'price', NULL, NULL),
(167, 'has_discount', NULL, NULL),
(168, 'discounted_price', NULL, NULL),
(169, 'course_thumbnail', NULL, NULL),
(170, 'note', NULL, NULL),
(171, 'thumbnail_size_should_be_600_x_600', NULL, NULL),
(172, 'course_overview_url', NULL, NULL),
(173, '0%', NULL, NULL),
(174, 'manage profile', NULL, NULL),
(175, 'edit_course', NULL, NULL),
(176, 'edit course', NULL, NULL),
(177, 'edit_courses', NULL, NULL),
(178, 'edit_course_form', NULL, NULL),
(179, 'update_course', NULL, NULL),
(180, 'course_updated', NULL, NULL),
(181, 'number_of_sections', NULL, NULL),
(182, 'number_of_enrolled_users', NULL, NULL),
(183, 'add section', NULL, NULL),
(184, 'section', NULL, NULL),
(185, 'add_section_form', NULL, NULL),
(186, 'update', NULL, NULL),
(187, 'serialize_section', NULL, NULL),
(188, 'serialize section', NULL, NULL),
(189, 'submit', NULL, NULL),
(190, 'sections_have_been_serialized', NULL, NULL),
(191, 'select_course', NULL, NULL),
(192, 'search', NULL, NULL),
(193, 'thumbnail', NULL, NULL),
(194, 'duration', NULL, NULL),
(195, 'provider', NULL, NULL),
(196, 'add lesson', NULL, NULL),
(197, 'add_lesson_form', NULL, NULL),
(198, 'video_type', NULL, NULL),
(199, 'select_a_course', NULL, NULL),
(200, 'select_a_course_first', NULL, NULL),
(201, 'video_url', NULL, NULL),
(202, 'invalid_url', NULL, NULL),
(203, 'your_video_source_has_to_be_either_youtube_or_vimeo', NULL, NULL),
(204, 'for', NULL, NULL),
(205, 'of', NULL, NULL),
(206, 'edit_lesson', NULL, NULL),
(207, 'edit lesson', NULL, NULL),
(208, 'edit_lesson_form', NULL, NULL),
(209, 'login', NULL, NULL),
(210, 'password', NULL, NULL),
(211, 'forgot_password', NULL, NULL),
(212, 'back_to_website', NULL, NULL),
(213, 'send_mail', NULL, NULL),
(214, 'back_to_login', NULL, NULL),
(215, 'student_add', NULL, NULL),
(216, 'student add', NULL, NULL),
(217, 'add_students', NULL, NULL),
(218, 'student_add_form', NULL, NULL),
(219, 'login_credentials', NULL, NULL),
(220, 'social_information', NULL, NULL),
(221, 'facebook', NULL, NULL),
(222, 'twitter', NULL, NULL),
(223, 'linkedin', NULL, NULL),
(224, 'user_image', NULL, NULL),
(225, 'add_user', NULL, NULL),
(226, 'user_update_successfully', NULL, NULL),
(227, 'user_added_successfully', NULL, NULL),
(228, 'student_edit', NULL, NULL),
(229, 'student edit', NULL, NULL),
(230, 'edit_students', NULL, NULL),
(231, 'student_edit_form', NULL, NULL),
(232, 'update_user', NULL, NULL),
(233, 'enroll history', NULL, NULL),
(234, 'filter', NULL, NULL),
(235, 'user_name', NULL, NULL),
(236, 'enrolled_course', NULL, NULL),
(237, 'enrollment_date', NULL, NULL),
(238, 'biography2', NULL, NULL),
(239, 'home', NULL, NULL),
(240, 'search_for_courses', NULL, NULL),
(241, 'total', NULL, NULL),
(242, 'go_to_cart', NULL, NULL),
(243, 'your_cart_is_empty', NULL, NULL),
(244, 'log_in', NULL, NULL),
(245, 'sign_up', NULL, NULL),
(246, 'what_do_you_want_to_learn', NULL, NULL),
(247, 'online_courses', NULL, NULL),
(248, 'explore_a_variety_of_fresh_topics', NULL, NULL),
(249, 'expert_instruction', NULL, NULL),
(250, 'find_the_right_course_for_you', NULL, NULL),
(251, 'lifetime_access', NULL, NULL),
(252, 'learn_on_your_schedule', NULL, NULL),
(253, 'top_courses', NULL, NULL),
(254, 'last_updater', NULL, NULL),
(255, 'hours', NULL, NULL),
(256, 'add_to_cart', NULL, NULL),
(257, 'top', NULL, NULL),
(258, 'latest_courses', NULL, NULL),
(259, 'added_to_cart', NULL, NULL),
(260, 'admin', NULL, NULL),
(261, 'log_in_to_your_udemy_account', NULL, NULL),
(262, 'by_signing_up_you_agree_to_our', NULL, NULL),
(263, 'terms_of_use', NULL, NULL),
(264, 'and', NULL, NULL),
(265, 'privacy_policy', NULL, NULL),
(266, 'do_not_have_an_account', NULL, NULL),
(267, 'sign_up_and_start_learning', NULL, NULL),
(268, 'check_here_for_exciting_deals_and_personalized_course_recommendations', NULL, NULL),
(269, 'already_have_an_account', NULL, NULL),
(270, 'checkout', NULL, NULL),
(271, 'paypal', NULL, NULL),
(272, 'stripe', NULL, NULL),
(273, 'step', NULL, NULL),
(274, 'how_would_you_rate_this_course_overall', NULL, NULL),
(275, 'write_a_public_review', NULL, NULL),
(276, 'describe_your_experience_what_you_got_out_of_the_course_and_other_helpful_highlights', NULL, NULL),
(277, 'what_did_the_instructor_do_well_and_what_could_use_some_improvement', NULL, NULL),
(278, 'next', NULL, NULL),
(279, 'previous', NULL, NULL),
(280, 'publish', NULL, NULL),
(281, 'search_results', NULL, NULL),
(282, 'ratings', NULL, NULL),
(283, 'search_results_for', NULL, NULL),
(284, 'category_page', NULL, NULL),
(285, 'all', NULL, NULL),
(286, 'category_list', NULL, NULL),
(287, 'by', NULL, NULL),
(288, 'go_to_wishlist', NULL, NULL),
(289, 'hi', NULL, NULL),
(290, 'my_courses', NULL, NULL),
(291, 'my_wishlist', NULL, NULL),
(292, 'my_messages', NULL, NULL),
(293, 'purchase_history', NULL, NULL),
(294, 'user_profile', NULL, NULL),
(295, 'already_purchased', NULL, NULL),
(296, 'all_courses', NULL, NULL),
(297, 'wishlists', NULL, NULL),
(298, 'search_my_courses', NULL, NULL),
(299, 'students_enrolled', NULL, NULL),
(300, 'created_by', NULL, NULL),
(301, 'last_updated', NULL, NULL),
(302, 'what_will_i_learn', NULL, NULL),
(303, 'view_more', NULL, NULL),
(304, 'other_related_courses', NULL, NULL),
(305, 'updated', NULL, NULL),
(306, 'curriculum_for_this_course', NULL, NULL),
(307, 'about_the_instructor', NULL, NULL),
(308, 'reviews', NULL, NULL),
(309, 'student_feedback', NULL, NULL),
(310, 'average_rating', NULL, NULL),
(311, 'preview_this_course', NULL, NULL),
(312, 'includes', NULL, NULL),
(313, 'on_demand_videos', NULL, NULL),
(314, 'full_lifetime_access', NULL, NULL),
(315, 'access_on_mobile_and_tv', NULL, NULL),
(316, 'course_preview', NULL, NULL),
(317, 'instructor_page', NULL, NULL),
(318, 'buy_now', NULL, NULL),
(319, 'shopping_cart', NULL, NULL),
(320, 'courses_in_cart', NULL, NULL),
(321, 'student_name', NULL, NULL),
(322, 'amount_to_pay', NULL, NULL),
(323, 'payment_successfully_done', NULL, NULL),
(324, 'filter_by', NULL, NULL),
(325, 'instructors', NULL, NULL),
(326, 'reset', NULL, NULL),
(327, 'your', NULL, NULL),
(328, 'rating', NULL, NULL),
(329, 'course_detail', NULL, NULL),
(330, 'start_lesson', NULL, NULL),
(331, 'show_full_biography', NULL, NULL),
(332, 'terms_and_condition', NULL, NULL),
(333, 'about', NULL, NULL),
(334, 'terms_&_condition', NULL, NULL),
(335, 'sub categories', NULL, NULL),
(336, 'add_sub_category', NULL, NULL),
(337, 'sub_category_title', NULL, NULL),
(338, 'add sub category', NULL, NULL),
(339, 'add_sub_category_form', NULL, NULL),
(340, 'sub_category_code', NULL, NULL),
(341, 'data_deleted', NULL, NULL),
(342, 'edit_category', NULL, NULL),
(343, 'edit category', NULL, NULL),
(344, 'edit_category_form', NULL, NULL),
(345, 'font', NULL, NULL),
(346, 'awesome class', NULL, NULL),
(347, 'update_category', NULL, NULL),
(348, 'data_updated_successfully', NULL, NULL),
(349, 'edit_sub_category', NULL, NULL),
(350, 'edit sub category', NULL, NULL),
(351, 'sub_category_edit', NULL, NULL),
(352, 'update_sub_category', NULL, NULL),
(353, 'course_added', NULL, NULL),
(354, 'user_deleted_successfully', NULL, NULL),
(355, 'private_messaging', NULL, NULL),
(356, 'private messaging', NULL, NULL),
(357, 'messages', NULL, NULL),
(358, 'select_message_to_read', NULL, NULL),
(359, 'new_message', NULL, NULL),
(360, 'email_duplication', NULL, NULL),
(361, 'your_registration_has_been_successfully_done', NULL, NULL),
(362, 'profile', NULL, NULL),
(363, 'account', NULL, NULL),
(364, 'add_information_about_yourself_to_share_on_your_profile', NULL, NULL),
(365, 'basics', NULL, NULL),
(366, 'add_your_twitter_link', NULL, NULL),
(367, 'add_your_facebook_link', NULL, NULL),
(368, 'add_your_linkedin_link', NULL, NULL),
(369, 'credentials', NULL, NULL),
(370, 'edit_your_account_settings', NULL, NULL),
(371, 'enter_current_password', NULL, NULL),
(372, 'enter_new_password', NULL, NULL),
(373, 're-type_your_password', NULL, NULL),
(374, 'save', NULL, NULL),
(375, 'update_user_photo', NULL, NULL),
(376, 'update_your_photo', NULL, NULL),
(377, 'upload_image', NULL, NULL),
(378, 'updated_successfully', NULL, NULL),
(379, 'invalid_login_credentials', NULL, NULL),
(380, 'blank_page', NULL, NULL),
(381, 'no_section_found', NULL, NULL),
(382, 'select_a_message_thread_to_read_it_here', NULL, NULL),
(383, 'send', NULL, NULL),
(384, 'type_your_message', NULL, NULL),
(385, 'date', NULL, NULL),
(386, 'total_price', NULL, NULL),
(387, 'payment_type', NULL, NULL),
(388, 'edit section', NULL, NULL),
(389, 'edit_section_form', NULL, NULL),
(390, 'reply_message', NULL, NULL),
(391, 'reply', NULL, NULL),
(392, 'log_in_to_your_account', NULL, NULL),
(393, 'no_result_found', NULL, NULL),
(394, 'enrollment', NULL, NULL),
(395, 'enroll_a_student', NULL, NULL),
(396, 'report', NULL, NULL),
(397, 'admin_revenue', NULL, NULL),
(398, 'instructor_revenue', NULL, NULL),
(399, 'instructor_settings', NULL, NULL),
(400, 'view_frontend', NULL, NULL),
(401, 'number_of_active_courses', NULL, NULL),
(402, 'number_of_pending_courses', NULL, NULL),
(403, 'all_instructor', NULL, NULL),
(404, 'active_courses', NULL, NULL),
(405, 'pending_courses', NULL, NULL),
(406, 'no_data_found', NULL, NULL),
(407, 'view_course_on_frontend', NULL, NULL),
(408, 'mark_as_pending', NULL, NULL),
(409, 'add category', NULL, NULL),
(410, 'add_categories', NULL, NULL),
(411, 'category_add_form', NULL, NULL),
(412, 'icon_picker', NULL, NULL),
(413, 'enroll a student', NULL, NULL),
(414, 'enrollment_form', NULL, NULL),
(415, 'admin revenue', NULL, NULL),
(416, 'total_amount', NULL, NULL),
(417, 'instructor revenue', NULL, NULL),
(418, 'status', NULL, NULL),
(419, 'instructor settings', NULL, NULL),
(420, 'allow_public_instructor', NULL, NULL),
(421, 'instructor_revenue_percentage', NULL, NULL),
(422, 'admin_revenue_percentage', NULL, NULL),
(423, 'update_instructor_settings', NULL, NULL),
(424, 'payment_info', NULL, NULL),
(425, 'required_for_instructors', NULL, NULL),
(426, 'paypal_client_id', NULL, NULL),
(427, 'stripe_public_key', NULL, NULL),
(428, 'stripe_secret_key', NULL, NULL),
(429, 'mark_as_active', NULL, NULL),
(430, 'mail_subject', NULL, NULL),
(431, 'mail_body', NULL, NULL),
(432, 'paid', NULL, NULL),
(433, 'pending', NULL, NULL),
(434, 'this_instructor_has_not_provided_valid_paypal_client_id', NULL, NULL),
(435, 'pay_with_paypal', NULL, NULL),
(436, 'this_instructor_has_not_provided_valid_public_key_or_secret_key', NULL, NULL),
(437, 'pay_with_stripe', NULL, NULL),
(438, 'create_course', NULL, NULL),
(439, 'payment_report', NULL, NULL),
(440, 'instructor_dashboard', NULL, NULL),
(441, 'draft', NULL, NULL),
(442, 'view_lessons', NULL, NULL),
(443, 'course_title', NULL, NULL),
(444, 'update_your_payment_settings', NULL, NULL),
(445, 'edit_course_detail', NULL, NULL),
(446, 'edit_basic_informations', NULL, NULL),
(447, 'publish_this_course', NULL, NULL),
(448, 'save_to_draft', NULL, NULL),
(449, 'update_section', NULL, NULL),
(450, 'analyzing_given_url', NULL, NULL),
(451, 'select_a_section', NULL, NULL),
(452, 'update_lesson', NULL, NULL),
(453, 'website_name', NULL, NULL),
(454, 'website_title', NULL, NULL),
(455, 'website_keywords', NULL, NULL),
(456, 'website_description', NULL, NULL),
(457, 'author', NULL, NULL),
(458, 'footer_text', NULL, NULL),
(459, 'footer_link', NULL, NULL),
(460, 'update_backend_logo', NULL, NULL),
(461, 'update_favicon', NULL, NULL),
(462, 'favicon', NULL, NULL),
(463, 'active courses', NULL, NULL),
(464, 'product_updated_successfully', NULL, NULL),
(465, 'course_overview_provider', NULL, NULL),
(466, 'youtube', NULL, NULL),
(467, 'vimeo', NULL, NULL),
(468, 'html5', NULL, NULL),
(469, 'meta_keywords', NULL, NULL),
(470, 'meta_description', NULL, NULL),
(471, 'lesson_type', NULL, NULL),
(472, 'video', NULL, NULL),
(473, 'select_type_of_lesson', NULL, NULL),
(474, 'text_file', NULL, NULL),
(475, 'pdf_file', NULL, NULL),
(476, 'document_file', NULL, NULL),
(477, 'image_file', NULL, NULL),
(478, 'lesson_provider', NULL, NULL),
(479, 'select_lesson_provider', NULL, NULL),
(480, 'analyzing_the_url', NULL, NULL),
(481, 'attachment', NULL, NULL),
(482, 'summary', NULL, NULL),
(483, 'download', NULL, NULL),
(484, 'system_settings_updated', NULL, NULL),
(485, 'course_updated_successfully', NULL, NULL),
(486, 'please_wait_untill_admin_approves_it', NULL, NULL),
(487, 'pending courses', NULL, NULL),
(488, 'course_status_updated', NULL, NULL),
(489, 'smtp_settings', NULL, NULL),
(490, 'free_course', NULL, NULL),
(491, 'free', NULL, NULL),
(492, 'get_enrolled', NULL, NULL),
(493, 'course_added_successfully', NULL, NULL),
(494, 'update_frontend_logo', NULL, NULL),
(495, 'system_currency_settings', NULL, NULL),
(496, 'select_system_currency', NULL, NULL),
(497, 'currency_position', NULL, NULL),
(498, 'left', NULL, NULL),
(499, 'right', NULL, NULL),
(500, 'left_with_a_space', NULL, NULL),
(501, 'right_with_a_space', NULL, NULL),
(502, 'paypal_currency', NULL, NULL),
(503, 'select_paypal_currency', NULL, NULL),
(504, 'stripe_currency', NULL, NULL),
(505, 'select_stripe_currency', NULL, NULL),
(506, 'heads_up', NULL, NULL),
(507, 'please_make_sure_that', NULL, NULL),
(508, 'system_currency', NULL, NULL),
(509, 'are_same', NULL, NULL),
(510, 'smtp settings', NULL, NULL),
(511, 'protocol', NULL, NULL),
(512, 'smtp_host', NULL, NULL),
(513, 'smtp_port', NULL, NULL),
(514, 'smtp_user', NULL, NULL),
(515, 'smtp_pass', NULL, NULL),
(516, 'update_smtp_settings', NULL, NULL),
(517, 'phrase_updated', NULL, NULL),
(518, 'registered_user', NULL, NULL),
(519, 'provide_your_valid_login_credentials', NULL, NULL),
(520, 'registration_form', NULL, NULL),
(521, 'provide_your_email_address_to_get_password', NULL, NULL),
(522, 'reset_password', NULL, NULL),
(523, 'want_to_go_back', NULL, NULL),
(524, 'message_sent!', NULL, NULL),
(525, 'selected_icon', NULL, NULL),
(526, 'pick_another_icon_picker', NULL, NULL),
(527, 'show_more', NULL, NULL),
(528, 'show_less', NULL, NULL),
(529, 'all_category', NULL, NULL),
(530, 'price_range', NULL, NULL),
(531, 'price_range_withing', NULL, NULL),
(532, 'all_categories', NULL, NULL),
(533, 'all_sub_category', NULL, NULL),
(534, 'number_of_results', NULL, NULL),
(535, 'showing_on_this_page', NULL, NULL),
(536, 'welcome', NULL, NULL),
(537, 'my_account', NULL, NULL),
(538, 'logout', NULL, NULL),
(539, 'visit_website', NULL, NULL),
(540, 'navigation', NULL, NULL),
(541, 'add_new_category', NULL, NULL),
(542, 'enrolment', NULL, NULL),
(543, 'enrol_history', NULL, NULL),
(544, 'enrol_a_student', NULL, NULL),
(545, 'language_settings', NULL, NULL),
(546, 'congratulations', NULL, NULL),
(547, 'oh_snap', NULL, NULL),
(548, 'close', NULL, NULL),
(549, 'parent', NULL, NULL),
(550, 'none', NULL, NULL),
(551, 'category_thumbnail', NULL, NULL),
(552, 'the_image_size_should_be', NULL, NULL),
(553, 'choose_thumbnail', NULL, NULL),
(554, 'data_added_successfully', NULL, NULL),
(555, '', NULL, NULL),
(556, 'update_category_form', NULL, NULL),
(557, 'student_list', NULL, NULL),
(558, 'choose_user_image', NULL, NULL),
(559, 'finish', NULL, NULL),
(560, 'thank_you', NULL, NULL),
(561, 'you_are_almost_there', NULL, NULL),
(562, 'you_are_just_one_click_away', NULL, NULL),
(563, 'country', NULL, NULL),
(564, 'website_settings', NULL, NULL),
(565, 'write_down_facebook_url', NULL, NULL),
(566, 'write_down_twitter_url', NULL, NULL),
(567, 'write_down_linkedin_url', NULL, NULL),
(568, 'google_link', NULL, NULL),
(569, 'write_down_google_url', NULL, NULL),
(570, 'instagram_link', NULL, NULL),
(571, 'write_down_instagram_url', NULL, NULL),
(572, 'pinterest_link', NULL, NULL),
(573, 'write_down_pinterest_url', NULL, NULL),
(574, 'update_settings', NULL, NULL),
(575, 'upload_banner_image', NULL, NULL),
(576, 'update_light_logo', NULL, NULL),
(577, 'upload_light_logo', NULL, NULL),
(578, 'update_dark_logo', NULL, NULL),
(579, 'upload_dark_logo', NULL, NULL),
(580, 'update_small_logo', NULL, NULL),
(581, 'upload_small_logo', NULL, NULL),
(582, 'upload_favicon', NULL, NULL),
(583, 'logo_updated', NULL, NULL),
(584, 'favicon_updated', NULL, NULL),
(585, 'banner_image_update', NULL, NULL),
(586, 'frontend_settings_updated', NULL, NULL),
(587, 'setup_payment_informations', NULL, NULL),
(588, 'update_system_currency', NULL, NULL),
(589, 'setup_paypal_settings', NULL, NULL),
(590, 'update_paypal_keys', NULL, NULL),
(591, 'setup_stripe_settings', NULL, NULL),
(592, 'test_mode', NULL, NULL),
(593, 'update_stripe_keys', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lesson`
--

CREATE TABLE `lesson` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `duration` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `video_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL,
  `lesson_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachment_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `summary` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `lesson`
--

INSERT INTO `lesson` (`id`, `title`, `duration`, `course_id`, `section_id`, `video_type`, `video_url`, `date_added`, `last_modified`, `lesson_type`, `attachment`, `attachment_type`, `summary`, `order`) VALUES
(1, 'Turn Opportunities into business deals', '00:00:00', 44, 1, 'vimeo_upload', 'https://vimeo.com/428992634', 1592092800, 1592697600, 'video', '', 'url', 'How to convert a Sales Lead or Business Referral into a Business deal. Why you should consider every Sales Lead as an opportunity and work towards converting it into business.', 0),
(2, 'Specific is Terrific - The Art of Attracting what you want', '00:05:59', 47, 2, 'vimeo_upload', 'https://vimeo.com/429653081', 1592265600, NULL, 'video', NULL, 'url', 'Specific is Terrific - The Law of Attraction and how to implement it in your business or life?', 0),
(3, 'How to use the &quot;5&quot; Block Strategy in your presentation and make an impact', '00:09:19', 48, 3, 'vimeo_upload', 'https://vimeo.com/430664600', 1592524800, NULL, 'video', NULL, 'url', 'This video will help you with the key elements which are also known as the â€œ5â€ Block strategy that you need to incorporate in your presentation in order to make it impactful.  These are essential learning on how to promote a product or service infront of a large audience.', 0),
(4, 'Introduction to the course', '00:01:46', 49, 4, 'vimeo_upload', 'https://vimeo.com/432811326', 1593129600, NULL, 'video', NULL, 'url', 'The course highlights how to  Abstract or Outline what you want to say. Thatâ€™s is one of the most vital elements in a presentation. The course will cover the following elements: \r\nâ€¢ Knowing your audience and their company \r\nâ€¢ How to prepare your presentation slides\r\nâ€¢ Use of visual text and data\r\nâ€¢ How to start your presentation\r\nâ€¢ Storytelling ability\r\nâ€¢ Engaging the audience\r\nâ€¢ Use of humour\r\nâ€¢ Problem solving ability \r\nâ€¢ Your Specific Ask?\r\n', 0),
(5, 'Know your Audience and the company they represent', '00:01:56', 49, 5, 'vimeo_upload', 'https://vimeo.com/432821790', 1593129600, NULL, 'video', NULL, 'url', 'This course highlights the fact that your spectators will have different proficiencies, knowledge and understanding of your business. \r\n\r\nWhat you need to address are: \r\nâ€¢ How much do they know about your product or service?\r\nâ€¢ What is that surprise element that will interest them? \r\nand \r\nâ€¢ What is the Unique Selling Point - USP that will benefit their company?', 0),
(6, 'How to prepare your Presentation', '00:07:01', 49, 6, 'vimeo_upload', 'https://vimeo.com/432823381', 1593129600, NULL, 'video', NULL, 'url', 'This course will cover the following topic:\r\nA. Your slide templatesÂ \r\nB. Highlighting key slides\r\nC. Amount of content on your slidesÂ \r\nD. Font Size and guidelines\r\nE. Animation and transitions \r\nF. Use of data graphs and diagramsÂ \r\nG. Use of Pictures\r\nH. Use of highlightersÂ \r\nI. User videos \r\n', 0),
(7, 'Presentation Guidelines', '00:02:35', 49, 7, 'vimeo_upload', 'https://vimeo.com/432828425', 1593129600, NULL, 'video', NULL, 'url', 'Grabbing your audience attention is the first and foremost step and that has to be done in the introduction part. This topic will cover three very important aspects you have to consider during a presentation.', 0),
(8, 'You personality traits during a presentation? ', '00:02:54', 49, 8, 'vimeo_upload', 'https://vimeo.com/432830558', 1593129600, NULL, 'video', NULL, 'url', 'This course will highlight the importance of your personality traits and why it is so important in a presentation if you want to make an impact. Close 11 key points will be discussed in this topic. ', 0),
(9, 'â€œ5â€ Block Model in your presentations to be unique and make an impact', '00:00:00', 49, 9, 'vimeo_upload', 'https://vimeo.com/432834476', 1593129600, 1593129600, 'video', '', 'url', 'You should always use the â€œ5â€ Block Model in your presentations\r\nThis model has been very successful especially when you want to introduce your products or services to a group of entrepreneurs, in a networking forum or in an exhibition. \r\nThis topic highlights the importance of the â€œ5â€ Block Model and how it can help you in your presentation. ', 0),
(10, 'In conclusion, that covers the key points that need to be remembered', '00:01:24', 49, 10, 'vimeo_upload', 'https://vimeo.com/432837910', 1593129600, NULL, 'video', NULL, 'url', 'The conclusion part will cover the key points of the course and what you need to remember in your presentation.', 0),
(11, 'Presentation Templates, Animation and Diagrams', NULL, 49, 10, NULL, NULL, 1593129600, NULL, 'other', 'e2889db5f0ef729327b63e3f9acdba96.pptx', 'doc', 'This section includes Presentation Templates, Animation and Diagrams for your future use in you would like to use certian elements to make an impact in your presentation. ', 0),
(12, 'What is your Networking Style or Approach?', '00:02:48', 50, 12, 'vimeo_upload', 'https://vimeo.com/435151252', 1593734400, NULL, 'video', NULL, 'url', 'First, we need to talk about what is your networking style or your approach to networking? You need to figure out what is the best style that fits your personality. \r\n\r\nJust because you\'re a shy person doesn\'t mean that you cannot network with people or on the other hand just because you\'re an outgoing person doesn\'t mean that people are going to run away from you. \r\n\r\nWhat you need to do is to figure out what is the best style that you identify yourself with and then work on that. ', 0),
(13, 'Make a calendar of your Networking events', '00:00:00', 50, 13, 'vimeo_upload', 'https://vimeo.com/435153450', 1593734400, 1593734400, 'video', '', 'url', 'Everyday day is an opportunity to network... but if you want to structure it then you have to make a list of all the events you would like to attend in a year and have a target of how many people you want to connect with. It is important that you should have a goal for each event.', 0),
(14, 'What is the best way to Start Networking?', '00:00:00', 50, 14, 'vimeo_upload', 'https://vimeo.com/435156927', 1593734400, 1593734400, 'video', '', 'url', 'There are two very important words â€œGivers Gainâ€ the more you give the more you get or what goes around comes around. You need to first see how you can help someone before connecting with him or her. In other words, you need to figure out what the other person may need first... rather than telling him or her what you need? ', 0),
(15, 'How to start a Conversation &amp; Connect with people', '00:00:57', 50, 15, 'vimeo_upload', 'https://vimeo.com/435158881', 1593734400, NULL, 'video', NULL, 'url', 'How to start a conversation especially when you don\'t know the person is very tricky. However, you need to always find something interesting in the person if you want to connect with them. For e.g. if they are wearing an interesting suit or saying a like-minded hobby that you can talk about and then try and get their attention. Itâ€™s about being interested in them rather than being interesting. ', 0),
(16, 'Your presentation to your target Audience', '00:00:57', 50, 16, 'vimeo_upload', 'https://vimeo.com/435160427', 1593734400, NULL, 'video', NULL, 'url', 'Always be ready to deliver your presentation before you go to a networking event. Remember you have approximately 1 minute, that is if youâ€™re lucky, to catch someoneâ€™s attention. So remember to be persuasive like your selling water to a drowning person. Consider thisâ€¦ the person on the other end would want to know your business only if it benefits him or her. Letâ€™s say you meet someone on a train, and you have only 3 mins to sell your product because he gets off in the next stop. ', 0),
(17, 'Specific is Terrific: - The best way to do business', '00:05:55', 50, 17, 'vimeo_upload', 'https://vimeo.com/435161264', 1593734400, NULL, 'video', NULL, 'url', 'This is a very common term that is used in networking which means you need to be specific about your ask and only then you will be terrific. This is a video that talks about networking events where you get a chance to talk about your business, your products or servicesâ€¦. You need to be specific about your ask only then people will be able to refer you. \r\n', 0),
(18, 'Always take your Leads or Referrals seriously', '00:06:12', 50, 18, 'vimeo_upload', 'https://vimeo.com/435167684', 1593734400, NULL, 'video', NULL, 'url', 'A lot of people get leads after the networking event but they don\'t do anything about it because they feel nothing is going to come out of it. Every lead or a referral you receive from your colleagues, your friends, or your networking members iare opportunities to do business and you should do your due diligence and make most of it.', 0),
(19, 'The Common mistakes you make during Networking: ', '00:01:53', 50, 19, 'vimeo_upload', 'https://vimeo.com/435171462', 1593734400, NULL, 'video', NULL, 'url', 'This video highlights some of the common mistakes that people make during networking events and this should be avoided. This video highlights these points and its is important that you need to take a note of it. ', 0),
(20, 'The importance of business reports and how it impacts the business', '00:09:20', 51, 20, 'vimeo_upload', 'https://vimeo.com/435365633', 1593820800, NULL, 'video', NULL, 'url', 'This course will give you a better understanding of the various Business Reports for decision making purpose. These Business Reports helps businesses to take the right decision at the right time. These reports are a reflection of your business health. The more you use the business report, you will be in a better position to analyse the key trends, know what is working, not working, strengths and weakness of the business etc. ', 0),
(22, 'The importance of Cash Flow &amp; Accounts Receivables in your business', '00:08:27', 51, 21, 'vimeo_upload', 'https://vimeo.com/435367562', 1593820800, NULL, 'video', NULL, 'url', 'In every business cash is King and profit is not equivalent to Cash. Always remember Cash Profit = Actual Profit â€“ Accounts Receivables. Your companyâ€™s revenue figures are great to exhibit, but they donâ€™t ultimately mean much if your cash flow is bad. \r\nIf you have full control over cash, receivables, stock, Business Reports & Compliances, then you will have full control of your business.\r\nHence, in this course we will be talking about the importance of Cash Flow and Accounts receivables and how it impacts your business. ', 0),
(23, 'The importance of Inventory Control ', '00:06:25', 51, 22, 'vimeo_upload', 'https://vimeo.com/435367911', 1593820800, NULL, 'video', NULL, 'url', 'Managing an adequate and right level of stock is key for managing inventory successfully. Overstocking will lead to cash blockage and increases the holding stock. While understocking lead to loss of sales and sometimes permanent loss of a customer. This course talks about some key factors to manage inventory successfully.\r\n', 0),
(24, 'Why Cost Control and Cost Analysis should be managed carefully in a company. ', '00:08:31', 51, 23, 'vimeo_upload', 'https://vimeo.com/435368300', 1593820800, NULL, 'video', NULL, 'url', 'Controlling and monitoring all the expenses is crucial to any business. There should be internal control in place where all the expenses must be approved by the authorised personnel. This is essential if you want your company to succeed and ensure that a correct decision is taken. ', 0),
(25, 'How to Draft a CV', '00:03:37', 52, 24, 'vimeo_upload', 'https://vimeo.com/436378188', 1594166400, NULL, 'video', NULL, 'url', 'Every time you apply for a job, consider this, on average there are thousands of people applying for the same job. So how do you stand out amongst the thousands of CVâ€™s that have piled up on the HR managers table? The first think you have to learn is how to draft CV? If you can draft the impactful CV then it will definitely open doors for you in the initial stages of an interview process. This course will teach you what you need to do and how you can draft your CV. ', 0),
(26, 'Sample CV (Word Format)', NULL, 52, 24, NULL, NULL, 1594166400, 1594166400, 'other', 'dfc92384773b366dc3e18af07b275336.docx', 'doc', 'Sample CV copy in Word Document', 0),
(27, 'Sample CV (PDF Format)', NULL, 52, 24, NULL, NULL, 1594166400, 1594166400, 'other', '38f11c6e6dd1ff4d15a50565431f95be.pdf', 'pdf', 'Sample CV in PDF format for your reference', 0),
(28, 'Essentials of writing an impactful covering letter ', '00:02:45', 52, 25, 'vimeo_upload', 'https://vimeo.com/436380290', 1594166400, NULL, 'video', NULL, 'url', 'A cover letter is usually used for a detailed description of your achievements. In other words, it is a summary of your CV in a few sentences. A cover letter is a platform to talk about why you\'re moving jobs and why you\'re looking forward to joining the company you\'re applying for. \r\n\r\nBasically, there are some principal points that you need to address in your covering letter and that is what we will be discussing.\r\n', 0),
(30, 'Sample Covering Letter (Word format) - Option 1', NULL, 52, 25, NULL, NULL, 1594166400, NULL, 'other', '688bbf01663161aa36412511d7cd38b6.docx', 'doc', 'Sample Covering Letter in Word format for your future reference - Option 1', 0),
(31, 'Sample Covering Letter (PDF Format) - Option 1', NULL, 52, 25, NULL, NULL, 1594166400, NULL, 'other', 'e9b1d5ddca7f84a657cfd84e3ffb27f9.pdf', 'pdf', 'Sample Covering Letter in PDF format for your future reference - Option 1', 0),
(32, 'Sample Covering Letter (Word format)  Option 2', NULL, 52, 25, NULL, NULL, 1594166400, NULL, 'other', 'be89f2f557ab95b6c752d08abc27cac7.docx', 'doc', 'Sample Covering Letter in Word format for your future reference - Option 2', 0),
(33, 'Sample Covering Letter (PDF format) - Option 2', NULL, 52, 25, NULL, NULL, 1594166400, NULL, 'other', 'e8a996386a8f76b3aeee6df072ca4603.pdf', 'pdf', 'Sample Covering Letter in PDF format for your reference  - Option 2', 0),
(34, 'Essential tips on how to prepare for an interview?', '00:05:21', 52, 26, 'vimeo_upload', 'https://vimeo.com/436383294', 1594166400, NULL, 'video', NULL, 'url', 'There is no hard and fast rule about how to prepare for an interview. It all depends on your attitude and confidence. This course highlights certain essential elements that you need to consider when it comes to preparing for an interview.  If the points in this cost are practised then you will definitely come out successful after an interview. ', 0),
(36, 'What you need to avoid saying in an interview?', '00:02:58', 52, 27, 'vimeo_upload', 'https://vimeo.com/436384833', 1594166400, NULL, 'video', NULL, 'url', 'There are some questions you need to avoid asking in an interview.  You need to be smart and avoid asking condescending or inappropriate questions. Always remember you don\'t know the person who\'s on the other side of the table and you\'re trying your best to impress him, so come up with the right questions and answers.  This course exactly talks about this aspect.', 0),
(37, 'What are the possible questions that an interviewer might ask?', '00:12:22', 52, 24, 'vimeo_upload', 'https://vimeo.com/436385922', 1594166400, NULL, 'video', NULL, 'url', 'There are certain questions that are very obvious in an interview. This course exactly discusses that.', 0),
(38, 'How to prepare for possible question that an interviewer might ask?', '00:12:22', 52, 28, 'vimeo_upload', 'https://vimeo.com/436387732', 1594166400, NULL, 'video', NULL, 'url', 'This course covers certain questions that the interviewer might  ask and you need to be prepared before you go for an interview.', 0),
(39, 'Tell me something about yourself?  A sample draft', NULL, 52, 28, NULL, NULL, 1594166400, NULL, 'other', '9c9de061ef2c6886c07af74a66526b28.pdf', 'pdf', 'A very common question is tell me something about yourself and here is a sample answer.', 0),
(40, 'Questions that you should ask the interviewer ', '00:01:59', 52, 29, 'vimeo_upload', 'https://vimeo.com/436391193', 1594166400, NULL, 'video', NULL, 'url', 'Questions that you should ask the interviewer in order to gain his or her confidence.', 0),
(41, 'In conclusion &amp; a summary of what you need to keep in mind ', '00:02:00', 52, 30, 'vimeo_upload', 'https://vimeo.com/436391846', 1594166400, NULL, 'video', NULL, 'url', 'In conclusion & a summary of what you need to keep in mind. Recap of all the points discussed in this course.', 0),
(42, 'Why certain vectors are important in your LinkedIn Profile', '00:01:48', 53, 31, 'vimeo_upload', 'https://vimeo.com/443726251', 1596240000, NULL, 'video', NULL, 'url', 'There are certain vectors that you need to consider before you prepare your LinkedIn Profile. This course discusses about the same and the role it plays in enhancing your profile. ', 0),
(43, 'How to make your Profile picture, Title, About &amp; Skills attractive to the target audience', '00:00:00', 53, 32, 'vimeo_upload', 'https://vimeo.com/443728678', 1596240000, 1596240000, 'video', '', 'url', 'What are the parameters you need to consider when you incorporate your profile picture, Title, About & Skills? This course exactly about the importance it has in enhancing your profile. ', 0),
(44, 'What you need to consider when your preparing your Experience, Education, Industry, Location and Connections', '00:04:54', 53, 33, 'vimeo_upload', 'https://vimeo.com/443730808', 1596240000, NULL, 'video', NULL, 'url', 'There are certain key elements you need to consider when you\'re drafting your Experience, the Industry you come from, your location, and the connection you have on your page. These are the vectors that will attract the target audience to your page and increase your brand awareness. This course exactly talks about that. ', 0),
(45, 'How to make a authentic Cranberry Cake ', '00:05:43', 55, 35, 'vimeo_upload', 'https://vimeo.com/449144991', 1597795200, NULL, 'video', NULL, 'url', 'Learn how to make an authentic Cranberry Cake dish.  This is how the delicacy is actually made and through this course, you will learn the various ingredients that is needed to make this dish. ', 0),
(46, 'Learn how to make a traditional Fish Tawa Fry', '00:07:50', 55, 36, 'vimeo_upload', 'https://vimeo.com/449149751', 1597795200, NULL, 'video', NULL, 'url', 'Learn how to make a delicious Fish Tawa Fry in the most traditional and authentic manner. After this course, you will learn all the essential ingredients that is needed in making this dish. ', 0),
(47, 'How to make one of the most delicious Dynamite Shrimps dish', '00:00:00', 55, 37, 'vimeo_upload', 'https://vimeo.com/449152350', 1597795200, 1597795200, 'video', '', 'url', 'Dynamite Shrimps is one of the most famous dishes in recent times and is often served at parties and as snacks. If you want to learn how to make this dish the authentic way, then this is the course for you. ', 0),
(48, 'Lean how to make a original Magnolia Bakery Banana Pudding', '00:00:00', 55, 38, 'vimeo_upload', 'https://vimeo.com/449154842', 1597795200, 1597795200, 'video', '', 'url', 'Want to know how the original Magnolia Bakery Banana Pudding is made? Then this is the course for you. In this course, we will teach you everything from what ingredients are used till the time how it is prepared and served. A must watch course if you\'re interested in serving a delicious dish on the table.', 0),
(49, 'Learn how to prepare a Shrimp Pasta dish especially if you love Italian cuisines', '00:00:00', 55, 39, 'vimeo_upload', 'https://vimeo.com/449156837', 1597795200, 1597795200, 'video', '', 'url', 'Have you ever wondered how an authentic Shrimp Pasta dish is made? Have you wondered what ingredients are used in this delicious dish? Well, this course will answer all your questions. If you love to prepare Italian dishes then this is the course for you.', 0),
(50, 'Learn the best and the easiest way to make a tasty Bread Toast ', '00:05:32', 55, 40, 'vimeo_upload', 'https://vimeo.com/449160347', 1597795200, NULL, 'video', NULL, 'url', 'You can do a lot of things with bread and the term \" Toast \" is used commonly all over the world. But what if we teach you the best and the easiest way to prepare a tasty Bread Toast that will melt in your mouth. View this course and by the time your done with it, you will learn how interesting a Bread Toast can be.  ', 0);

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `message_id` int(11) NOT NULL,
  `message_thread_code` longtext DEFAULT NULL,
  `message` longtext DEFAULT NULL,
  `sender` longtext DEFAULT NULL,
  `timestamp` longtext DEFAULT NULL,
  `read_status` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `message_thread`
--

CREATE TABLE `message_thread` (
  `message_thread_id` int(11) NOT NULL,
  `message_thread_code` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `sender` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `receiver` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `last_message_timestamp` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `payment_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL,
  `admin_revenue` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instructor_revenue` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instructor_payment_status` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `user_id`, `payment_type`, `course_id`, `amount`, `date_added`, `last_modified`, `admin_revenue`, `instructor_revenue`, `instructor_payment_status`) VALUES
(1, 46, 'ccAvenue', 44, 2, 1592265600, NULL, '0.8', '1.2', 0),
(2, 44, 'ccAvenue', 47, 2, 1592265600, NULL, '0.8', '1.2', 1),
(3, 47, 'ccAvenue', 47, 2, 1592265600, NULL, '0.8', '1.2', 0),
(4, 44, 'ccAvenue', 49, 4, 1593129600, NULL, '1.6', '2.4', 0),
(5, 45, 'ccAvenue', 50, 5, 1593734400, NULL, '2', '3', 0),
(6, 47, 'ccAvenue', 51, 4, 1593907200, NULL, '1.6', '2.4', 0),
(7, 47, 'ccAvenue', 44, 2, 1593907200, NULL, '0.8', '1.2', 0),
(8, 51, 'ccAvenue', 47, 2, 1594252800, NULL, '0.8', '1.2', 0),
(9, 46, 'ccAvenue', 47, 2, 1597795200, NULL, '0.8', '1.2', 0),
(10, 51, 'ccAvenue', 44, 2, 1597795200, NULL, '0.8', '1.2', 0),
(11, 51, 'ccAvenue', 48, 20, 1597881600, NULL, '1800', '5000', 0),
(12, 55, 'ccAvenue', 56, 2000, 1597881600, NULL, '800', '1200', 0);

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `id` int(11) UNSIGNED NOT NULL,
  `quiz_id` int(11) DEFAULT NULL,
  `title` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `number_of_options` int(11) DEFAULT NULL,
  `options` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `correct_answers` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE `rating` (
  `id` int(11) UNSIGNED NOT NULL,
  `rating` double DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ratable_id` int(11) DEFAULT NULL,
  `ratable_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL,
  `review` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `rating`
--

INSERT INTO `rating` (`id`, `rating`, `user_id`, `ratable_id`, `ratable_type`, `date_added`, `last_modified`, `review`) VALUES
(1, NULL, NULL, NULL, 'course', 1597795200, NULL, NULL),
(2, 5, 46, 44, 'course', 1592265600, NULL, 'Excellent video. Very inspirational. Thank you for sharing and I would highly recommend others to watch it.'),
(3, 5, 47, 47, 'course', 1592265600, NULL, 'Amazing video on Motivation. Highly recommend people see this video esp. people who need some kind of inspiration in life.'),
(4, 5, 44, 49, 'course', 1593129600, NULL, 'A impactful course lots to learn. Compakt Consultancy comes up with good content and I highly recommend this course to others. '),
(5, 5, 45, 50, 'course', 1593734400, NULL, ''),
(6, 5, 47, 51, 'course', 1593907200, NULL, 'Great course on Business Finance, esp. if you need some clarity on the subject. A must watch and you will gain a lot for the topics covered. '),
(7, 5, 44, 44, 'course', 1594252800, NULL, 'Great course and I would highly recommend it to others.  You will definitely benefit from it.  '),
(8, 5, 47, 44, 'course', 1597795200, NULL, 'A great course... a must see if you want to turn your opportunities into business'),
(9, 5, 51, 47, 'course', 1597881600, NULL, 'Excellent video... Strongly suggest you all see it. Highly recommended and good learnings. ');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`, `date_added`, `last_modified`) VALUES
(1, 'Admin', 1234567890, 1234567890),
(2, 'User', 1234567890, 1234567890);

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

CREATE TABLE `section` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `section`
--

INSERT INTO `section` (`id`, `title`, `course_id`, `order`) VALUES
(1, 'Turn your opportunities into business deals', 44, 1),
(2, 'Learn how to Attract what you Desire in your Life?', 47, 0),
(3, 'How to be unique in your Presentation &amp; Standout', 48, 0),
(4, 'Introduction: ', 49, 0),
(5, 'Your Audience ', 49, 0),
(6, 'Your Presentation', 49, 0),
(7, 'Your Presentation Guidelines ', 49, 0),
(8, 'You personality traits ', 49, 0),
(9, 'How to be unique - The â€œ5â€ Block Model ', 49, 0),
(10, 'Conclusion and template with Diagrams & Animation ', 49, 0),
(12, 'Your Networking Style', 50, 0),
(13, 'Calendar to Network', 50, 0),
(14, 'The best way to start Networking?', 50, 0),
(15, 'Starting a Conversation', 50, 0),
(16, 'How to present your products or services to your target audience?', 50, 0),
(17, 'Specific is Terrific: - Thats how you do business ', 50, 0),
(18, 'Leads or Business Referrals are very important in Networking', 50, 0),
(19, 'Common mistakes you need to avoid when it comes to networking', 50, 0),
(20, 'UNDERSTANDING OF BUSINESS REPORTS', 51, 0),
(21, 'CASH FLOW MANAGEMENT &amp; ACCOUNTS RECEIVABLES MANAGEMENT', 51, 0),
(22, 'INVENTORY CONTROL', 51, 0),
(23, 'COST CONTROL &amp; COST ANALYSIS', 51, 0),
(24, 'Introduction to CV writing and what you need to do?', 52, 0),
(25, 'How to write a strong Covering Letter?', 52, 0),
(26, 'How to prepare before an Interview?', 52, 0),
(27, 'What you should not say in an interview?', 52, 0),
(28, 'What are the possible questions an interviewer might ask?', 52, 0),
(29, 'Questions that you should ask the interviewer?', 52, 0),
(30, 'In conclusion', 52, 0),
(31, 'Importance of Key Vectors', 53, 0),
(32, 'Your Profile Picture, Title, About Yourself &amp; Skills', 53, 0),
(33, 'Your Experience, Education, Industry &amp; Location, and Connections', 53, 0),
(34, 'Know your Audience and the company they represent', 54, 0),
(35, 'Cranberry Cake ', 55, 0),
(36, 'Fish Tawa Fry ', 55, 0),
(37, 'Dynamite Shrimps', 55, 0),
(38, 'Magnolia Bakery Banana Pudding ', 55, 0),
(39, 'Shrimp Pasta', 55, 0),
(40, 'Bread toast ', 55, 0);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `value`) VALUES
(1, 'language', 'english'),
(2, 'system_name', 'Course Wings'),
(3, 'system_title', 'Course Wings'),
(4, 'system_email', 'coursewings@gmail.com'),
(5, 'address', 'Course Wings Portal\r\nJVC, Autumn 2 &quot;B&quot;, Dubai, United Arab Emirates\r\nP.O Box: 328772'),
(6, 'phone', '+123-45-6789'),
(7, 'purchase_code', '267af801-ddb2-4d89-9044-456679754aa2'),
(8, 'paypal', '[{\"active\":\"1\",\"mode\":\"production\",\"sandbox_client_id\":\"ASJYfMBfU8H6dr_h7uWt9-VuUngM2PozxfEnjzFGGDiNERcxl1PQuLkvgbHG64at-JkDNFrFH7A0NrUl\",\"production_client_id\":\"AWdudgCGGMavTgmt-EkJRki1bt7TutoypxD8SYoOBZKEQsurpudxaWP4DnqCxcGDp1jMlzNq8C68leB0\"}]'),
(9, 'stripe_keys', '[{\"active\":\"1\",\"testmode\":\"on\",\"public_key\":\"pk_test_c6VvBEbwHFdulFZ62q1IQrar\",\"secret_key\":\"sk_test_9IMkiM6Ykxr1LCe2dJ3PgaxS\",\"public_live_key\":\"pk_live_xxxxxxxxxxxxxxxxxxxxxxxx\",\"secret_live_key\":\"sk_live_xxxxxxxxxxxxxxxxxxxxxxxx\"}]'),
(10, 'youtube_api_key', 'AIzaSyAlxF2mMzknfjPexOGstZbElXKueM2X0Ec'),
(11, 'vimeo_api_key', '1ed8e5a601593f5116c97efabf41e4bbbbf010dd'),
(12, 'slogan', 'Learn From Industry Experts'),
(13, 'text_align', NULL),
(14, 'allow_instructor', '1'),
(15, 'instructor_revenue', '60'),
(16, 'system_currency', 'USD'),
(17, 'paypal_currency', 'USD'),
(18, 'stripe_currency', 'USD'),
(19, 'author', 'ETC'),
(20, 'currency_position', 'left'),
(21, 'website_description', 'Course Wings is an online learning and teaching marketplace connecting Teachers &amp; Students.'),
(22, 'website_keywords', 'e-learning,teach online,sell courses,learn online,online videos,online courses,online training, online education, online classes,online learning'),
(23, 'footer_text', NULL),
(24, 'footer_link', 'http://www.etcnet.net/'),
(25, 'protocol', 'smtp'),
(26, 'smtp_host', 'ssl://smtp.googlemail.com'),
(27, 'smtp_port', '465'),
(28, 'smtp_user', 'info@coursewings.com'),
(29, 'smtp_pass', 'Shiven2014'),
(30, 'version', '2.4'),
(31, 'student_email_verification', 'disable'),
(32, 'ccavenue', '[{\"active\":\"1\",\"currency\":\"USD\",\"merchant_id\":\"43560\",\"access_code\":\"AVNB03GH07BD82BNDB\",\"working_key\":\"65BF333B856A78FF1D8637194E969B74\"}]');

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE `tag` (
  `id` int(11) UNSIGNED NOT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tagable_id` int(11) DEFAULT NULL,
  `tagable_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `text` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `last_modified_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='store user testimonials';

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `user_id`, `text`, `status`, `last_modified_at`) VALUES
(1, 48, 'All+the+most+successful+people+I+know+are+constantly+sharpening+their+skills%2C+and+online+courses+are+a+great+way+to+do+that.%0D%0ACoursewings.com+offers+tons+of+online+courses%2C+both+for+fun+and+to+develop+real-life+skills+that+can+further+your+career.', 1, '2020-06-19 10:27:18'),
(2, 47, 'Great+site+and+amazing+videos.+I+would+highly+recommend+you+to+view+Course+Wing+videos%2C+it+will+definitely+help+you+improve+your+skills+and+it+will+be+benificial+in+your+corporate+career.+', 1, '2020-06-19 10:38:11'),
(3, 44, 'This+is+a+great+platform+if+you+want+to+sharpen+your+skills.+Some+of+the+videos+have+good+insights+into+the+current+market+scenario+and+it+will+surely+help+you+in+your+professional+career+or+whatever+you+are+trying+to+pursue.+I+encourage+instructors+to+come+on+board+and+share+their+knowledge+with+the+world.%C2%A0Well+done+Course+Wings+team+and+continue+a+good+job.%C2%A0', 1, '2020-06-19 10:53:50'),
(4, 46, 'Great+sites%2C+good+Insights%2C+important+learnings.+If+you+want+to+learn+something+that+you+are+passionate+about+this+is+the+site+for+you.+Highly+recommend+it+to+all+visitors+and+encourage+instructors+to+come+one+board+and+share+their+knowledge.++', 1, '2020-06-19 11:08:15'),
(5, 45, 'Amazing+site+and+impactful+videos+to+learn+from.+Highly+recommend+all+ages+to+view+these+courses%2C+as+they+can+change+the+way+you+look+at+the+world.+These+courses+can+also+help+you+in+your+career+and+assist+you+in+climbing+the+corporate+ladder.', 1, '2020-06-26 13:01:06');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `social_links` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `biography` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `date_added` int(11) DEFAULT NULL,
  `last_modified` int(11) DEFAULT NULL,
  `watch_history` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `wishlist` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `paypal_email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paypal_keys` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `stripe_keys` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_details` text COLLATE utf8_unicode_ci NOT NULL,
  `verification_code` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `social_links`, `biography`, `role_id`, `date_added`, `last_modified`, `watch_history`, `wishlist`, `title`, `paypal_email`, `paypal_keys`, `stripe_keys`, `bank_details`, `verification_code`, `status`) VALUES
(14, 'Course ', 'Wings', 'ukmadaiah@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', '{\"facebook\":\"https:\\/\\/www.facebook.com\\/CourseWings\\/\",\"twitter\":\"https:\\/\\/twitter.com\\/CourseWings\",\"linkedin\":\"https:\\/\\/www.linkedin.com\\/company\\/course-wings\\/\"}', '<p>Online Video Courses based on many categories</p>', 1, 1563511502, 1579854684, '[]', '[]', 'Online Video Courses based on many categories', NULL, '[{\"production_client_id\":null}]', '[{\"public_live_key\":null,\"secret_live_key\":null}]', '', '9f690a0b649ed453f0d3fd23daba7bfd', 1),
(38, 'Info', 'Etc', 'info@etcnet.net', 'c164aea11c49fa8e1373ecc5953b1178e3f4b082', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', NULL, 1, 1582970625, NULL, '[]', '[]', NULL, NULL, '[{\"production_client_id\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', '', 'bd7387a7f73094e9ae7057b0ed929e75', 1),
(39, 'Brent', 'Dalley', 'brent.dalley4@gmail.com', '448ff2e88aad770e0cfe08181fdf5e21868d5fd8', '{\"facebook\":\"https:\\/\\/www.facebook.com\\/profile.php?id=100009150031418\",\"twitter\":\"\",\"linkedin\":\"\"}', '<p>For the past 14 years Brent has run his own business.Â  He works from home, writing and producingÂ training courses in those disciplines.Â  He consults and coaches winning teams and team leaders.</p>\r\n<p>With forty years\' experience in management, he has gained a lifetimeâ€™s knowledge and experience.Â </p>\r\n<p>Throughout his career Brent has received awards in education, sales performance, cost control and asset management, and employee/management relationships.</p>\r\n<p>Brent also as counseled couples and individuals and worked with them to make their relationships stronger with each other and also with themselves.</p>\r\n<p>He is married to the same young lady he met 60 years ago and together they have two daughters and one son and 9 grandchildren.</p>\r\n<p>His current interests are mountain biking, hiking and spoiling grandchildren.</p>', 2, 1584539179, 1584541519, '[]', '[]', NULL, NULL, '[{\"production_client_id\":null}]', '[{\"public_live_key\":null,\"secret_live_key\":null}]', '', 'c1f92a05f7e38e188e2d7ddbd53c284d', 1),
(44, 'Madaiah ', 'Udiyanda ', 'madaiah@hotmail.com', '487044a74d9fae1b3a88d106123ca2b1068dc522', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"https:\\/\\/www.linkedin.com\\/in\\/madaiah-udiyanda\\/\"}', '<p><span lang=\"EN-US\">Instructor in Marketing & Sales - In my 15 years of professional career, I have worked for leading private holding and multi-disciplinary conglomerates that governs several prominent subsidiaries involved in a wide spectrum of business activities. Through the various integrated marketing, digital & communication tools, I have actively promoted group portfolios to potential investors, stakeholders and clients who demonstrate an interest in either business partnerships or joint ventures.</span></p>\r\n<p><span lang=\"EN-US\">Â </span></p>\r\n<p><span lang=\"EN-US\">I have a proven track record of implementing several noteworthy marketing, communication and digital campaigns that demand substantial research and an in-depth understanding of the target audience. I have a strong understanding of the marketing trends and I actively liaise with external agencies to assist group companies with their PR, digital, advertising, trade exhibitions and media buying requirements.Â Â </span></p>\r\n<p><span lang=\"EN-US\">Â </span></p>', 2, 1591958296, 1592164505, '[]', '[]', NULL, NULL, '[{\"production_client_id\":\"ASCjSBbk6ntIpAG0UzHe833DwNCW2mLol4RAxR7-o6RueuaH2OMMsqQZg4p4oILPeh6Kp0_uyoh9u85h\"}]', '[{\"public_live_key\":null,\"secret_live_key\":null}]', '', '0ac3e19cc6deecfc8f9f075d03c70950', 1),
(45, 'Compakt ', 'Mgmt. Consultancy', 'madaiah@firstcomm-me.com', '90a3241b912e0199611eec082914c5d9981c9fc5', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '<p class=\"MsoNormal\"><span lang=\"EN-US\">Compakt & Co Consultancy supports clients all over the world and helps them with a wide spectrum management-related issue. We as a group are consultants for business management processes, business development, corporate finance,Â </span>digital and traditionalÂ <span lang=\"EN-US\">marketing strategies</span>, web<span lang=\"EN-US\">Â development</span>,Â promotional events,Â <span lang=\"EN-US\">social media campaigns</span>, PR and communication-related<span lang=\"EN-US\">Â activities and many more matters affecting today\'s business.Â Â </span></p>\r\n<p class=\"MsoNormal\">Â </p>\r\n<p class=\"MsoNormal\">WeÂ <span lang=\"EN-US\">haveÂ </span>the knowledge<span lang=\"EN-US\">Â andÂ </span>experience to assist our client\'s launch products and servicesÂ <span lang=\"EN-US\">while targeting their potential clients. We also work in collaboration withÂ </span>our partners to define their needs, build a strategic plan, execute projects, and analyze results.Â Â <span lang=\"EN-US\">We also conduct training and workshops for corporates and help them improve their management skills.</span></p>\r\n<p class=\"MsoNormal\">Â </p>', 2, 1592149318, 1593190749, '[]', '[]', NULL, NULL, '[{\"production_client_id\":null}]', '[{\"public_live_key\":null,\"secret_live_key\":null}]', '', '7d75cb9a254ff3ec824b756005f28d1a', 1),
(46, 'Firstcomm ', 'Consultancy', 'madaiah1976@gmail.com', 'bd44956c951d31cea82978b8ae41bd0b85c72d91', '{\"facebook\":\"https:\\/\\/www.facebook.com\\/FirstcomMarketing\\/\",\"twitter\":\"\",\"linkedin\":\"https:\\/\\/www.linkedin.com\\/company\\/firstcomm-marketing-consult\"}', '<p class=\"MsoNormal\">First<span lang=\"EN-US\">c</span>omm <span lang=\"EN-US\">Marketing Consultants </span>specializes in a wide spectrum of integrated Advertising, Branding, Design, WebÂ &Â Communications solutions. <span lang=\"EN-US\">We are an agency with a </span>unique blend of creativity<span lang=\"EN-US\"> and experience</span> that is capable of catapulting the image of any â€˜Brand â€˜or â€˜Serviceâ€™.</p>\r\n<p class=\"MsoNormal\">Â </p>\r\n<p class=\"MsoNormal\"><span lang=\"EN-US\">We also conduct many training programs in Personal Development, Marketing, Sales, Digital Media and Corporate Communications.Â </span>Due to our insightful understanding of the market and the latest trends, we take pride in offering a comprehensive and superior roster of specialized services to our clients.</p>\r\n<p class=\"MsoNormal\">Â </p>\r\n<p class=\"MsoNormal\"><span lang=\"EN-US\">In addition, our</span>Â guaranteed<span lang=\"EN-US\">Â </span>services and expertise increase<span lang=\"EN-US\">s</span> <span lang=\"EN-US\">our clientâ€™s </span>brand efficiency and help<span lang=\"EN-US\">s then</span> achieve <span lang=\"EN-US\">their</span> desired outcomes. In addition, we have a strong understanding of both traditional and emerging <span lang=\"EN-US\">marketing</span> tools as well as their implications on B2B/B2C marketing strategies.</p>', 2, 1592313901, 1592320233, '[]', '[]', NULL, NULL, '[{\"production_client_id\":\"AWToFwERLZ3QN2C9Fy3OlRqtZXLNEZ6gdhDF0BpKJzjFU-xOdUHJ8ko8DIDpewhq32N5ewVvhazn_jJx\"}]', '[{\"public_live_key\":null,\"secret_live_key\":null}]', '', 'adef66ab2876fb4bbc563096163ed0f9', 1),
(47, 'Curate ', 'Mgmt. Consultancy ', 'firstcommconsultancy@gmail.com', '2a8809a27eb9ab6f29427f50361b8453ecae55c9', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '<p>A management consulting company specialising in educational video in the field of Management, Corporate Affairs, Marketing, Sales and Personal Development.Â </p>', 2, 1592327895, 1592328076, '[]', '[]', NULL, NULL, '[{\"production_client_id\":null}]', '[{\"public_live_key\":null,\"secret_live_key\":null}]', '', '40b588c4733ebeb6e4c6cadca349a8a3', 1),
(48, 'Saurabh', 'Gautam', 'saurabh@etcnet.net', '7c4a8d09ca3762af61e59520943dc26494f8941b', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', NULL, 2, 1592562256, NULL, '[]', '[]', NULL, NULL, '[{\"production_client_id\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', '', '3635fe53188664a3f45252e85385e7a3', 1),
(49, 'Juliana', 'Khalil', 'juliana@julianakhalil.com', '0874e5ae5fbc59a1fefecd6bff53533b6ac69aac', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', NULL, 2, 1593269397, NULL, '[]', '[]', NULL, NULL, '[{\"production_client_id\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', '', 'e516eacced88ed0f8527f693b1f4f5cb', 1),
(50, 'Chirag', 'Agarwal ', 'chirag@earningouae.com', '8cb2237d0679ca88db6464eac60da96345513964', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"https:\\/\\/www.linkedin.com\\/in\\/ca-chirag-agarwal-6b077313b\\/\"}', '<p><span class=\"lt-line-clamp__raw-line\" xss=removed>An enterprising and number-crunching professional pursued Chartered Accountancy after completing commerce graduation. I have embarked this entrepreneurial journey of setting up and giving direction to Accounting and VAT advisory company â€“ Earningo Accounting & Tax Consultancy - after 7+ years of extensive work experience in India & Dubai. Under my dynamic leadership, Earningo has seen appreciable expansion in terms of clients and today it has 50+ highly satisfied clients.<br xss=removed><br xss=removed>This expansion has been possible only because of our clients who comprehends our expertise in Corporate Outsourced CFO services, Risk Advisory Services and VAT Consultancy and first-rate knowledge of tax matters and legal issues to inspire co-workers to design customer-centric and tailor-made solutions for the clients. The aim always is to devise solutions that can help in solving the clientsâ€™ most pressing tax and accounting problems.<br xss=removed><br xss=removed>Steadfast and focused on my goal of taking Earningo to greater heights, my aim is to accomplish this goal by creating a dedicated and well-qualified team of tax and accountancy experts who work with zeal and single-minded devotion to provide seamless service to the clients thereby adding value in terms of growth and profitability.<br xss=removed><br xss=removed>Originally from Delhi, India, I shifted my base to Dubai in 2016 and plans to develop a company culture that focuses on doing everything the right way in order to not only meet but exceed client expectations.</span><span xss=removed>Â </span></p>', 2, 1593890138, 1593890606, '[]', '[]', NULL, NULL, '[{\"production_client_id\":null}]', '[{\"public_live_key\":null,\"secret_live_key\":null}]', '', '89d95d28645e1c0a115190bd980cde37', 1),
(51, 'Kavya ', 'Nanjappa', 'knanjappa@gmail.com', '57f828ca3141850a7964801fa79c07763e494c0c', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', NULL, 2, 1594298980, NULL, '[]', '[]', NULL, NULL, '[{\"production_client_id\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', '', 'e0c0d7d6f051870c4ac95f25f9988ee9', 1),
(52, 'Sudeep', 'Victor', 'sudeepvictor@gmail.com', '2bfb1ad0bf4bf6e210f8a6c5cb57234bfa914458', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"https:\\/\\/www.linkedin.com\\/in\\/sudeep-victor\\/\"}', '<p><span xss=removed>Sudeep is Franklin Covey Certified Sales Professional, NetApp Accredited Sales Professional and a Nutanix Certified Sales Representative.</span><br xss=removed><span xss=removed>He has 5 years of rich experience in Sales (Data Storage and Data Management Solutions) - Commercial Accounts.</span><br xss=removed><span xss=removed>He is also skilled in Sales, Research, Territory and Channel Management, Account Management, Proposal Writing and Technical positioning.</span><br xss=removed><span xss=removed>He has strong information technology and sales professional experience with a Master Degree specialised in IT and Software Technology from VIT, Vellore.</span></p>', 2, 1596217534, 1596217741, '[]', '[]', NULL, NULL, '[{\"production_client_id\":null}]', '[{\"public_live_key\":null,\"secret_live_key\":null}]', '', '267995cdadf77df4e39526396a6c8ba1', 1),
(53, 'Bonnita', 'Nevidhitha', 'nevidhithabonnita@gmail.com', '0aa2978158c58b7d68acbab0824021423c0109c3', '{\"facebook\":\"\",\"twitter\":\"https:\\/\\/www.youtube.com\\/channel\\/UC9Me008eTjD-hbZd4CvhhAQ\",\"linkedin\":\"\"}', '<p class=\"gmail-ik\"><span lang=\"EN-US\">N</span>evidhitha<span lang=\"EN-US\"> B</span>onnita<span lang=\"EN-US\"> is a passionate cook that believes \"</span><span lang=\"EN-US\">n</span>ot everyone in this world <span lang=\"EN-US\">fancies</span> <span lang=\"EN-US\">cooking.</span> However, those who love cooking are the ones who are truly lucky, including their families\"<span lang=\"EN-US\">. Since a very young age,Â Nevidhitha has enjoyed every dish she has prepared and has many signature dishes against her name. In her opinion, when </span>others enjoy what you cook<span lang=\"EN-US\"> and complement you dishes â€¦. It is the best feeling in the world. She believes that </span>Cooking is a <span lang=\"EN-US\">joyful </span>art <span lang=\"EN-US\">that curtails </span>creativity,<span lang=\"EN-US\"> innovation, coupled with </span>your imagination <span lang=\"EN-US\">and when you put these ingredients together you have a dish that you can be proud of.</span></p>\r\n<p class=\"MsoNormal\"><span lang=\"EN-US\">Nevidhitha has a huge fan following and runs a successful YouTube channel called \"Tummy & Cheeks\" and in addition, she is also a specialist beautician that passionately talks about the latest beauty tips as well.Â Â </span></p>', 2, 1596452321, 1596823786, '[]', '[]', NULL, NULL, '[{\"production_client_id\":null}]', '[{\"public_live_key\":null,\"secret_live_key\":null}]', '', 'bf70d482a5524de5630ccda437373076', 1),
(54, 'Kuldeep', 'Singh', 'kuldeep11071978@gmail.com', '031f1c8716c7ea9b4732da6c9f79cee92df255cf', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', '<p xss=removed>K U L D E E P</p>\r\n<p xss=removed>S I N G H</p>\r\n<p xss=removed>G R A P H I C D E S I G N E R</p>\r\n<p xss=removed>+98182 75705</p>\r\n<p xss=removed>+971523672976</p>\r\n<p xss=removed>kuldeep11071978@gmail.com</p>\r\n<p xss=removed>kuldeepsinghdhn55@gmail.com</p>\r\n<p xss=removed>C O N T A C T</p>\r\n<p xss=removed>A creative & innovative</p>\r\n<p xss=removed>professional with total of 10.5</p>\r\n<p xss=removed>years of experience in Graphic</p>\r\n<p xss=removed>Designing and Web Designing,</p>\r\n<p xss=removed>Animation, Client Servicing</p>\r\n<p xss=removed>and Team Management.</p>\r\n<p xss=removed>P R O F I L E</p>\r\n<p xss=removed>K</p>\r\n<p xss=removed>P R O F E S S I O N A L S Y N O P S I S</p>\r\n<p xss=removed>Presently working with Amourion Training Institute, as</p>\r\n<p xss=removed>a Creative Designer, Sheikh Zayed Road, Dubai Advance Skills in</p>\r\n<p xss=removed>graphic user interface designing through CorelDraw, Illustrator,</p>\r\n<p xss=removed>Photoshop, flash, HTML, CSS, DreamWeaver, InDesing, After</p>\r\n<p xss=removed>Effects, Fireworks, 3ds Max Studio(Basic), Autocad(Basic), Photo</p>\r\n<p xss=removed>Editing etc.</p>\r\n<p xss=removed>Possess advanced knowledge and understanding of graphic design</p>\r\n<p xss=removed>& Web Design layout along with knowledge about the latest</p>\r\n<p xss=removed>cutting-edge technologies.</p>\r\n<p xss=removed>Expertise in tapping prospects, analyzing their requirements,</p>\r\n<p xss=removed>rendering guidance to the clients and negotiate by meeting them</p>\r\n<p xss=removed>personally (commercially) for the orders for Financial products.</p>\r\n<p xss=removed>Endowed with excellent Client Relationship Management and</p>\r\n<p xss=removed>Communication skills with ability to effectively comprehend</p>\r\n<p xss=removed>client and accordingly conceptualize web/ graphic designs.</p>\r\n<p xss=removed>C o r e l D r a w</p>\r\n<p xss=removed>I l l u s t r a t o r</p>\r\n<p xss=removed>I n D e s i g n</p>\r\n<p xss=removed>H T M L</p>\r\n<p xss=removed>D r e a mWe a v e r</p>\r\n<p xss=removed>P h o t o S h o p</p>\r\n<p xss=removed>P a g eMa k e r</p>\r\n<p xss=removed>G r a p h i c D e s i g n</p>\r\n<p xss=removed>C S S</p>\r\n<p xss=removed>A f t e r E f f e c t s B a s i c</p>\r\n<p xss=removed>F i r eWo r k s</p>\r\n<p xss=removed>L a y o u t D e s i g n</p>\r\n<p xss=removed>F l e x</p>\r\n<p xss=removed>3 D S M a x S t u d i o B a s i c</p>\r\n<p xss=removed>F l a s h D i g i t a l M a r k e t i n g D e s i g n s</p>\r\n<p xss=removed>C U R R I C U L U M - V I T A E</p>\r\n<p xss=removed>I T S K I L L S</p>\r\n<p xss=removed>F l a s h P r e s e n t a t i o n P r o m o t i o n a l B a n n e r s</p>\r\n<p xss=removed>F a c e b o o k B a n n e r s S t a t i o n e r y D e s i g n i n g</p>\r\n<p xss=removed>We b s i t e D e s i g n i n g L o g o D e s i g n i n g</p>\r\n<p xss=removed>I N D U S T R I A L C R E A T I V E E X P E R I E N C E</p>\r\n<p xss=removed>A C A D E M I C Q U A L I F I C A T I O N</p>\r\n<p xss=removed>U n i v e r s i t y o f M a h a t m a G a n d h i K a a s h i V i d y a p i t h , V a r a n a s i , U t t a r P r a d e s h | 1 9 9 7 - 1 9 9 9</p>\r\n<p xss=removed>B A C H E L O R O F A R T S</p>\r\n<p xss=removed>U n i v e r s i t y o f M a h a t m a G a n d h i U n i v e r s i t y , M e g h a l a y a , G u w a h a t i | 2 0 1 3 - 2 0 1 5</p>\r\n<p xss=removed>E X E C U T I V E M A S T E R I N B U S I N E S S A D M I N I S T R A T I O N ( E M B A )</p>\r\n<p xss=removed>1 9 9 9</p>\r\n<p xss=removed>2 0 1 5</p>\r\n<p xss=removed>1 y e a r c e r t i f i c a t e c o u r s e f r o m N S I C , O k h l a , N e w D e l h i</p>\r\n<p xss=removed>â€œ N E TWO R K I N G WI T H I N T E R N E T â€ <span xss=removed>2 0 0 0</span></p>\r\n<p xss=removed>D i p l o m a i n E n d U s e r C o m p u t i n g f r o m F i r s t C o m p u t e r s , N e w D e l h i</p>\r\n<p xss=removed>â€œ E N D U S E R C O M P U T I N G â€ ( L E A R N I N G C O M P U T E R S ) <span xss=removed>1 9 9 6</span></p>\r\n<p xss=removed>R a p i d A c t i o n M a n a g e m e n t C o n s u l t a n c y | J a n \' 1 6 â€“ P r e s e n t</p>\r\n<p xss=removed>2 0 1 6</p>\r\n<p xss=removed>F r e e l a n c e r C r e a t i v e D e s i g n e r</p>\r\n<p xss=removed>E x p e r t k n o w l e d g e o f i m p o r t a n t d e s i g n s o f t w a r e l i k e C o r a l D r a w , P h o t o s h o p , I l l u s t r a t o r , F l a s h ,</p>\r\n<p xss=removed>F i r e w o r k s , I n d e s i g n , F r e e H a n d , e t c .</p>\r\n<p xss=removed>Wr i t t e n a n d v e r b a l c o m m u n i c a t i o n s k i l l s t h a t w o u l d h e l p y o u i n c o m m u n i c a t i n g w i t h t h e c l i e n t s</p>\r\n<p xss=removed>a n d u n d e r s t a n d t h e i r r e q u i r e m e n t s w i t h o u t a n y m i s t a k e s</p>\r\n<p xss=removed>C r e a t i v e a b i l i t i e s t o c o m e u p w i t h e x c e p t i o n a l c o n c e p t a n d p r o m o t i o n a l i d e a s</p>\r\n<p xss=removed>D e m o n s t r a t i n g c o n c e p t s a n d d e s i g n s t o t h e c l i e n t s</p>\r\n<p xss=removed>N u t e c h J e t t i n g E q u i p m e n t s I n d i a P v t . L t d . | J a n \' 1 1 â€“ N o v \' 1 5</p>\r\n<p xss=removed>2 0 1 5</p>\r\n<p xss=removed>S e n i o r G r a p h i c D e s i g n e r</p>\r\n<p xss=removed>M a n a g e d a l l d e s i g n i n g w o r k s a n d c r e a t e d v a r i o u s d i g i t a l a d s , a d v e r t i s m e n t s , u s e d h i g h</p>\r\n<p xss=removed>r e s o l u t i o n a n d o p t i m i s e d i m a g e s , f o r t h e c o m p a n y . P r e p a r e d b o o k l e t s a n d c a t a l o u g e s ,</p>\r\n<p xss=removed>s t a t i o n e r y , v i s i t i n g c a r d s , f l y e r s , f l e x b a n n e r s f o r t h e e x h i b i t i o n h e l d i n N S I C o k h l a . Wo r k e d</p>\r\n<p xss=removed>r e a l l y h a r d t o m a k e i t s u c c e s s f u l a n d a c h i e v e d a p p r i c i a t i o n a w a r d b y t h e c o m p a n y .</p>\r\n<p xss=removed>S e n i o r G r a p h i c D e s i g n e r</p>\r\n<p xss=removed>M i n d e x t e n d T e c h n o l o g i e s P v t . L t d . | D e c \' 0 9 â€“ S e p \' 1 1</p>\r\n<p xss=removed>H a n d l e d d e s i g n i n g w o r k s a n d w e b s i t e d e s i g n s t o t h e l o c a l c l i e n t s . H a n d l e d c u s t o m e r s o n t h e p h o n e</p>\r\n<p xss=removed>a n d p h y s i c a l l y i n t h e o f f i c e r e g a r d i n g t h e i r d e s i g n i n g w o r k s f o r v a r i o u s u p d a t e s a n d c h a n g e s .</p>\r\n<p xss=removed>2 0 1 1</p>\r\n<p xss=removed>G r a p h i c D e s i g n e r</p>\r\n<p xss=removed><span xss=removed>G l o A d s ( A d A g e n c y ) | A u g \' 0 7 â€“ N o v \' 0 9 </span><span xss=removed>2 0 0 9 </span>D o n e d e s i g n i n g w o r k s a n d c r e a t e d v a r i o u s d i g i t a l a d s , a d v e r t i s m e n t s , u s e d h i g h r e s o l u t i o n a n d o p t i m i s e d</p>\r\n<p xss=removed>i m a g e s , d i g i t a l p r i n t s t o t h e c l i e n t s , f l e x b a n n e r s a n d v a r i o u s d e s i g n i n g s e r v i c e s</p>\r\n<p xss=removed>C o l o r i n g A r t i s t / G r a p h i c D e s i g n e r</p>\r\n<p xss=removed>I k I n t e r n a t i o n a l P u b l i s h i n g P v t . L t d . | J a n \' 0 6 â€“ J u l \' 0 7</p>\r\n<p xss=removed>M a n a g e d a l l d e s i g n i n g w o r k s a n d c r e a t e d v a r i o u s d i g i t a l a d s , c o l o r i n g m e d i c a l i m a g e s , u s e d h i g h</p>\r\n<p xss=removed>r e s o l u t i o n a n d o p t i m i s e d i m a g e s , d e e p u n d e r s t a n d i n g o f b o o k a n d t y p e s e t t i n g b y u s i n g v a r i o u s</p>\r\n<p xss=removed>d e s i g n i n g s o f t w a r e s .</p>\r\n<p xss=removed>2 0 0 7</p>\r\n<p xss=removed>G r a p h i c D e s i g n e r</p>\r\n<p xss=removed>D i g i t e x M e d i c a l S y s t e m s | A p r \' 0 4 â€“ N o v \' 0 5</p>\r\n<p xss=removed>H a n d l e d a l l d e s i g n i n g w o r k s a n d c r e a t e d v a r i o u s d i g i t a l a d s .</p>\r\n<p xss=removed>2 0 0 5</p>\r\n<p xss=removed>K e y T a s k s H a n d l e d A c r o s s t h e A s s i g n m e n t s ( P r o j e c t E x e c u t i o n )</p>\r\n<p xss=removed>I n t e r a c t i n g w i t h t h e c l i e n t s f o r o b t a i n i n g t h e i r s p e c i f i c r e q u i r e m e n t s , f i n a l i z a t i o n o f d e s i g n s p e c i f i c a t i o n s a n d</p>\r\n<p xss=removed>p a r a m e t e r s . T h i s i n v o l v e s :</p>\r\n<p xss=removed>C r e a t i o n o f w e b d e s i g n a n d a n i m a t e d g r a p h i c s t h a t c o m p l e m e n t s a n d e n h a n c e s t h e i m a g e o f c l i e n t s .</p>\r\n<p xss=removed>E s t a b l i s h i n g t h e q u a l i t y p a r a m e t e r s t h a t s h o u l d b e m e t d u r i n g t h e d e s i g n s t a g e .</p>\r\n<p xss=removed>U s i n g t h e l a t e s t c u t t i n g - e d g e d e s i g n t e c h n o l o g i e s a n d i n n o v a t i v e m e a s u r e s f o r t h e d e s i g n .</p>\r\n<p xss=removed>E x e c u t i n g t h e d e s i g n a c t i v i t i e s w i t h i n t h e t i m e f r a m e p r o m i s e d t o t h e c l i e n t .</p>\r\n<p xss=removed>A n a l y z i n g t h e r e s u l t s o f c a m p a i g n s i n o r d e r t o i m p r o v e u p o n t h e d e s i g n & a n i m a t i o n s t r a t e g i e s .</p>\r\n<p xss=removed>E x t e n d i n g p o s t - i m p l e m e n t a t i o n s u p p o r t t o t h e c l i e n t .</p>\r\n<p xss=removed>We b / G r a p h i c D e s i g n i n g</p>\r\n<p xss=removed>C o n c e p t u a l i z i n g w e b s i t e / g r a p h i c d e s i g n s , s o f t w a r e u s e r i n t e r f a c e s f o r e n h a n c i n g t h e i m a g e o f t h e c l i e n t .</p>\r\n<p xss=removed>T r a c k i n g t h e w e b / g r a p h i c d e s i g n p r o c e s s a t v a r i o u s s t a g e s t o e n s u r e a l l q u a l i t y s t a n d a r d s a r e b e i n g m e t a n d</p>\r\n<p xss=removed>r e c t i f y t e c h n i c a l e r r o r s i f a n y , i n t h e p r o d u c t .</p>\r\n<p xss=removed>R e g u l a r l y u p d a t i n g t h e c l i e n t a b o u t t h e p r o j e c t p r o g r e s s a n d i n c o r p o r a t i n g c h a n g e s r e q u i r e d b y t h e c l i e n t .</p>\r\n<p xss=removed>C l i e n t S e r v i c i n g</p>\r\n<p xss=removed>P r o v i d i n g v a l u e a d d e d c u s t o m e r s e r v i c e s b y a t t e n d i n g c u s t o m e r q u e r i e s & i s s u e s & r e s o l v i n g c u s t o m e r</p>\r\n<p xss=removed>c o m p l a i n t s o n p e r f o r m a n c e b o t t l e n e c k s .</p>\r\n<p xss=removed>M a p p i n g c l i e n t â€™ s r e q u i r e m e n t s a n d c o o r d i n a t i n g i m p l e m e n t i n g p r o c e s s e s i n l i n e w i t h t h e g u i d e l i n e s s p e c i f i e d</p>\r\n<p xss=removed>b y t h e c l i e n t , o n O p e r a t i o n s S h o p F l o o r .</p>\r\n<p xss=removed>A p p r a i s i n g p r o p o s a l s , c o n d u c t i n g r i s k a n a l y s i s & s c r u t i n i z i n g r e l e v a n t document s b e f o r e f o r c r e d i t c a r d s ,</p>\r\n<p xss=removed>e n s u r i n g c o m p l i a n c e w i t h o r g a n i z a t i o n a l c r e d i t p o l i c i e s .</p>\r\n<p xss=removed>I d e n t i f y i n g i m p r o v e m e n t a r e a s & i m p l e m e n t i n g m e a s u r e s t o m a x i m i z e c u s t o m e r s a t i s f a c t i o n l e v e l s .</p>\r\n<p xss=removed>E n s u r i n g c o n t i n u o u s i n t e r a c t i o n w i t h t h e c u s t o m e r t o m a k e s u r e t h a t a r e a o f c o n c e r n c a n b e w o r k e d u p o n f o r</p>\r\n<p xss=removed>i m p r o v e d s e r v i c e l e v e l s .</p>\r\n<p xss=removed>T e a m M a n a g e m e n t</p>\r\n<p xss=removed>L e a d i n g , m e n t o r i n g & m o n i t o r i n g t h e p e r f o r m a n c e o f t e a m m e m b e r s t o e n s u r e e f f i c i e n c y i n p r o c e s s o p e r a t i o n s</p>\r\n<p xss=removed>a n d m e e t i n g o f i n d i v i d u a l & g r o u p t a r g e t s .</p>\r\n<p xss=removed>C o n d u c t i n g t r a i n i n g s e s s i o n s , u n i t m e e t s & i n h o u s e f o r u m s t o b o o s t t e c h n i c a l & s o f t s k i l l s o f a s s o c i a t e s .</p>\r\n<p xss=removed>C r e a t i n g a n d s u s t a i n i n g a d y n a m i c e n v i r o n m e n t t h a t f o s t e r s d e v e l o p m e n t o p p o r t u n i t i e s a n d m o t i v a t e s h i g h</p>\r\n<p xss=removed>p e r f o r m a n c e a m o n g s t T e a m m e m b e r s .</p>\r\n<p xss=removed>P R O F E S S I O N A L C E R T I F I C A T I O N S</p>\r\n<p xss=removed>â€¢ Certificate in Computing (C.I.C) from IGNOU.</p>\r\n<p xss=removed>â€¢ Certificate in TALLY 5.0 from ET&T, New Delhi.</p>\r\n<p xss=removed>â€¢ Certificate in Multimedia and Web Designing.</p>\r\n<p xss=removed>P R O F E S S I O N A L T A S K S S U M M A R Y</p>\r\n<p xss=removed>P E R S O N A L D E T A I L S</p>\r\n<p xss=removed>â€¢ Date of Birth : 11 July 1978</p>\r\n<p xss=removed>â€¢ Marital Status : Married</p>\r\n<p xss=removed>â€¢ Visa Status : Visit Visa</p>\r\n<p xss=removed>â€¢ Languages Known : English, Hindi, Punjabi</p>', 2, 1597152160, 1597152805, '[]', '[]', NULL, NULL, '[{\"production_client_id\":null}]', '[{\"public_live_key\":null,\"secret_live_key\":null}]', '', '7d9668167ea54ec9c81479fa0b49a30f', 1),
(55, 'Sheno', 'Sonic', 'shenosonic@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', NULL, 2, 1597964299, NULL, '[]', '[]', NULL, 'mypaypalemail123@gmail.com', '[{\"production_client_id\":\"123456789\",\"paypal_email\":\"shenosonic@gmail.com\",\"bank_details\":\"1015 4879 4653 1245\\r\\nSwiss bank\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', '', '326113aa7077b132f14940b650805868', 1),
(56, 'Sid', 'Gopal', 'sidgopal@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', '{\"facebook\":\"\",\"twitter\":\"\",\"linkedin\":\"\"}', NULL, 2, 1597988439, NULL, '[]', '[]', NULL, NULL, '[{\"production_client_id\":\"\"}]', '[{\"public_live_key\":\"\",\"secret_live_key\":\"\"}]', '', '265ec45ad8f3ee4ea796876814a61728', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vimeo`
--

CREATE TABLE `vimeo` (
  `id` int(11) NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vimeo`
--

INSERT INTO `vimeo` (`id`, `key`, `value`) VALUES
(1, 'vimeo', '[{\"active\":1,\"vimeo_client_id\":\"1ed8e5a601593f5116c97efabf41e4bbbbf010dd\",\"vimeo_client_secret\":\"O+4HqwjwAwgLTLU5OJNOl78SF0S39cDa4IovJPCdbFKrFVgI8so51sEBQJEm9wSFY4Wb7xjHnijiRUlghgqaFPA4yoVJK8nHQ\\/2GqIFZbel144erE9BtsZVxYcw4Ho+6\",\"vimeo_access_token\":\"ed08454f46bee28e1c08b7c516200edf\"}]');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bkup_akeeba_common`
--
ALTER TABLE `bkup_akeeba_common`
  ADD PRIMARY KEY (`key`);

--
-- Indexes for table `bkup_ak_params`
--
ALTER TABLE `bkup_ak_params`
  ADD PRIMARY KEY (`tag`);

--
-- Indexes for table `bkup_ak_profiles`
--
ALTER TABLE `bkup_ak_profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bkup_ak_stats`
--
ALTER TABLE `bkup_ak_stats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_fullstatus` (`filesexist`,`status`),
  ADD KEY `idx_stale` (`status`,`origin`);

--
-- Indexes for table `bkup_ak_storage`
--
ALTER TABLE `bkup_ak_storage`
  ADD PRIMARY KEY (`tag`);

--
-- Indexes for table `bkup_ak_users`
--
ALTER TABLE `bkup_ak_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ccavenue`
--
ALTER TABLE `ccavenue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enrol`
--
ALTER TABLE `enrol`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frontend_settings`
--
ALTER TABLE `frontend_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hits`
--
ALTER TABLE `hits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `course_id` (`course_id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`phrase_id`);

--
-- Indexes for table `lesson`
--
ALTER TABLE `lesson`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `message_thread`
--
ALTER TABLE `message_thread`
  ADD PRIMARY KEY (`message_thread_id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vimeo`
--
ALTER TABLE `vimeo`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bkup_ak_profiles`
--
ALTER TABLE `bkup_ak_profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `bkup_ak_stats`
--
ALTER TABLE `bkup_ak_stats`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `bkup_ak_users`
--
ALTER TABLE `bkup_ak_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `ccavenue`
--
ALTER TABLE `ccavenue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT for table `enrol`
--
ALTER TABLE `enrol`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `frontend_settings`
--
ALTER TABLE `frontend_settings`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `hits`
--
ALTER TABLE `hits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `phrase_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=594;

--
-- AUTO_INCREMENT for table `lesson`
--
ALTER TABLE `lesson`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `message_thread`
--
ALTER TABLE `message_thread`
  MODIFY `message_thread_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rating`
--
ALTER TABLE `rating`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `section`
--
ALTER TABLE `section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `tag`
--
ALTER TABLE `tag`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `vimeo`
--
ALTER TABLE `vimeo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `hits`
--
ALTER TABLE `hits`
  ADD CONSTRAINT `hits_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
